function delData(type,id) {
    // console.log(type,id)
    $.ajax({ url : "admin/"+type+"/hapus",
        type: "POST",
        dataType: "JSON",
        data: {id:id},
        async: false,
        success: function(data){
            // swal('Data '+type+' berhasil dihapus','success');
            document.location = data.url;
        },
        error: function (jqXHR, textStatus, errorThrown){
            swal('Gagal memproses data dari ajax','error');
        }
  });
}