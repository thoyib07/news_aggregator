$(function () {
    // $("#cover-spin").show();
    spin();
    $(document).ajaxSend(function() {
        // setTimeout(() => {
        //     console.log("spin show");
            // $("#cover-spin").show();
        // }, 0.5 * 1000);
        spin();　
    });

    $(document).ajaxStop(function() {
        // setTimeout(() => {
            // console.log("spin hide");
            // $("#cover-spin").hide();
        // }, 2 * 1000);
        spin();　
    });
    setInterval(() => {
    // setTimeout(() => {
        $("#cover-spin").hide();
        // spin();
    }, 3 * 1000);
});
const host = window.location.host;
const isdebug = false;
// stackoverflow.com

const pathArray = window.location.pathname.split( '/' );
// ["", "questions", "21246818", "how-to-get-the-base-url-in-javascript"]
let data_berita;
let data_berita_tmp;
let data_berita_del;
let limitTopWord = 10;
function delDataBeritaArr (key){
    // alert("delete berita : "+key);
    data_berita.splice(key,1);
    // console.log(data_berita);
    drawTable("#divHasilPencarian",data_berita);
}

function spin() {
    let x = document.getElementById("cover-spin");
    if (x.style.display === "none") {
        console.log("masuk spin");
        setTimeout(() => {
            x.style.display = "block";
        }, 0.5 * 1000);
    } else {
        console.log("masuk spin hide");
        setTimeout(() => {
            x.style.display = "none";
        }, 1 * 1000);
    }
}

function graphNews(limit){
    // console.log(data_berita);
    // alert("Tampilkan Graph");
    let id_berita = new Array();
    data_berita.forEach(function(elm) {
        id_berita.push(elm.id_berita);
    });
    // // spin();
    
    $.ajax({ 
        url : base_url+'api/graphNews',
        // url : window.location.pathname+'/api/searchBerita',            
        type: "POST",
        data : {
            id_berita:JSON.stringify(id_berita),
            limit:limit
        },
        dataType: "JSON",
        async: false,        
        beforeSend: function(){
            // $("#cover-spin").show();
        },
        success: function(data){
            // // spin();
            // console.log(data.topWord);
            // let tableData = data.topWord;
            if (typeof data.topWord  !== 'undefined') {
                let xAxisTopWords = new Array();
                let dataTopWords = new Array();
                for (let prop in data.topWord) {
                    xAxisTopWords.push(prop);
                    dataTopWords.push(data.topWord[prop].berita);
                }
                let divTopWords = '<div id="divTopWords"></div>';
                $('#divGraphPencarian').append(divTopWords);
                drawGraph("divTopWords",{
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Kata Populer'
                    },
                    xAxis: {
                        categories: xAxisTopWords,
                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Berita'
                        }
                    },
                    series: [
                        {
                            name: 'Jumlah Berita',
                            data: dataTopWords                
                        },
                    ]
                });
            }
            
            if (typeof data.lokasi  !== 'undefined') {
                let xGraphLokasi = new Array();
                let dataGraphLokasi = new Array();
                for (let prop in data.lokasi) {
                    xGraphLokasi.push(data.lokasi[prop].nama_lokasi);
                    dataGraphLokasi.push(data.lokasi[prop].berita);
                }
                let divGraphLokasi = '<div id="divGraphLokasi"></div>';
                $('#divGraphPencarian').append(divGraphLokasi);
                drawGraph("divGraphLokasi",{
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Lokasi Berita'
                    },
                    xAxis: {
                        categories: xGraphLokasi,
                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Berita'
                        }
                    },
                    series: [
                        {
                            name: 'Jumlah Berita',
                            data: dataGraphLokasi                
                        },
                    ]
                });
            }
            
            if (typeof data.tanggal  !== 'undefined') {
                let xGraphTanggal = new Array();
                let dataGraphTanggal = new Array();
                for (let prop in data.tanggal) {
                    xGraphTanggal.push(prop);
                    dataGraphTanggal.push(parseInt(data.tanggal[prop]));
                }
                // console.log(dataGraphTanggal);
                let divGraphTanggal = '<div id="divGraphTanggal"></div>';
                $('#divGraphPencarian').append(divGraphTanggal);
                drawGraph("divGraphTanggal",{
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Jumlah Berita per Tanggal Publish'
                    },
                    xAxis: {
                        categories: xGraphTanggal,
                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Berita'
                        }
                    },
                    series: [
                        {
                            name: 'Jumlah Berita',
                            data: dataGraphTanggal                
                        },
                    ]
                });
            }
            // throw new Error("my error message");
        },
        error: function (jqXHR, textStatus, errorThrown){
            // // spin();
            swal("Error!", "Gagal memperoleh data dari ajax!", "error");
            // $("#btnSearch").html("Cari");
        }        
    }).done(function() {
        setTimeout(function(){
            // $("#cover-spin").hide();
            // spin();
        },500);
    });
}

function drawGraph(target,option) {
    Highcharts.chart(target, option);
}

function delNews() {
    // let arrHasil = new Array();
    let arrHasil = "";
    let checkboxes = document.getElementsByName('delIdBerita[]');
    data_berita_del = new Array();
    for (let i=0, n=checkboxes.length;i<n;i++)  {
        if (checkboxes[i].checked)  {
            data_berita_del.push(checkboxes[i].value)
        }
    }
    for (let i = data_berita.length - 1; i >= 0; --i) {
        if(data_berita_del.indexOf(data_berita[i].id_berita) != -1) { 
            data_berita.splice(i,1);
        }
    }
    
    data_berita.forEach(function(elm){
        // arrHasil.push(elm.id_berita);
        arrHasil += ","+elm.id_berita;
    });
    
    $('#id_berita').val(arrHasil.substr(1));    
    // $('#hasil_berita').val(JSON.stringify(data_berita));
    // alert("Hapus Berita"); 
    drawTable("#divHasilPencarian",data_berita);   
}
  
function drawTable(target,data){
    // console.log(data);
    let no = 0;
    let hasilSentimen = "";
    // let btnSave = '<span id="submit_btn" class="btn btn-success fa fa-save" onClick="save();"> Simpan</span>';
    $(target).append('<table id="tableHasilPencarian" class="table table-striped"><thead><tr><th>No</th><th>Berita</th><th>Situs</th><th>Tgl Publish</th><th>Prosentase</th><th>Analisa Sentimen</th><th></th></tr></thead><tbody id="listHasilPencarian"></tbody></table>');
    $('#listHasilPencarian').html('');
    data.forEach(function(el,key) {
        no++;
        // console.log(el);
        // console.log(el.link_berita);
        if (el.nilai_positif !== 'undefined') {
            if (el.nilai_positif == el.nilai_negatif) {
                hasilSentimen = '<span class="btn btn-primary">Netral</span>';
            } else if (el.nilai_positif > el.nilai_negatif) {
                hasilSentimen = '<span class="btn btn-success">Positif</span>';
            } else {
                hasilSentimen = '<span class="btn btn-danger">Negatif</span>';          
            }
        }

        listHasil = '<tr><td><span onclick="openBerita(`'+el.id_berita+'`)" style="cursor: pointer;">'+no+'</span></td><td><a href="'+el.link_berita+'" target="_blank">'+el.judul_berita+'</a></td><td>'+el.nama_situs+'</td><td>'+el.publish_date+'</td><td>'+el.hasil+' %</td><td>'+hasilSentimen+'</td><td><div class="checkbox-group"><div class="checkbox"><input type="checkbox" value="'+el.id_berita+'" id="'+el.id_berita+'" class="delIdBerita" name="delIdBerita[]"><p style="display:none;">'+el.isi_berita+'</p></div></div></td></tr>';
        $('#listHasilPencarian').append(listHasil);
    });
    $('#tableHasilPencarian').DataTable({
        // "order": [
        //     [ 4, "desc" ],
        //     [ 3, "desc" ],
        // ],
        "columnDefs": [
            { 
                "targets": [ 5 ], //first column / numbering column
                "orderable": false, //set not orderable
            },
        ],
    });
}
  
function drawTableDataSet(target,data){
    // console.log(data);
    let no = 0;
    let hasilSentimen = "";
    // let btnSave = '<span id="submit_btn" class="btn btn-success fa fa-save" onClick="save();"> Simpan</span>';
    $(target).append('<table id="tableHasilDataSet" class="table table-striped"><thead><tr><th>No</th><th>Berita</th><th>Situs</th><th>Tgl Publish</th></tr></thead><tbody id="listHasilPencarian"></tbody></table>');
    $('#listHasilPencarian').html('');
    data.forEach(function(el,key) {
        no++;
        // console.log(el);
        // console.log(el.link_berita);

        listHasil = '<tr><td><span onclick="openBerita(`'+el.id_berita+'`)" style="cursor: pointer;">'+no+'</span></td><td><a href="'+el.link_berita+'" target="_blank">'+el.judul_berita+'</a></td><td>'+el.nama_situs+'</td><td>'+el.publish_date+'</td></tr>';
        $('#listHasilPencarian').append(listHasil);
    });
    $('#tableHasilDataSet').DataTable({
        // "order": [
        //     [ 4, "desc" ],
        //     [ 3, "desc" ],
        // ],
        // "columnDefs": [
        //     { 
        //         "targets": [ 5 ], //first column / numbering column
        //         "orderable": false, //set not orderable
        //     },
        // ],
    });
}

function searchNews() {
    // // spin();
    let form = document.getElementById('formSearch');
    // let dataForm = $('#formSearch').serialize();
    // console.log(JSON.stringify(dataForm));
    // console.log($(".check-lvl-situs"));
    let checkedValue = new Array();
    let inputElements = document.getElementsByClassName('check-lvl-situs');
    for(let i=0; inputElements[i]; ++i){
        if(inputElements[i].checked){
            checkedValue.push(inputElements[i].value);
        }
    }
    
    if ($("#list_kata_kunci").val() !== "") {
        if (checkedValue.length !== 0) {
            if(($("#kata_pencarian").val() !== "")){
                // $("#btnSearch").html("Sedang Mencari");
                $("#cover-spin").show();
                $('#boxHasilPencarian').show();
                $("#divHasilPencarian").html('<span>Sedang memproses...</span>');
                $('#divGraphPencarian').html('');
                setTimeout(() => {
                    $.ajax({ 
                        url : base_url+'api/searchBerita',
                        // url : window.location.pathname+'/api/searchBerita',            
                        type: "POST",
                        data : $('#formSearch').find("input[name!=id_berita]").serialize(),                
                        // data : $('#formSearch').serialize(),
                        dataType: "JSON",
                        async: false,
                        beforeSend: function(){
                            console.log("sedang memproses...");
                            // $("#cover-spin").show();
                            $('#boxHasilPencarian').show();
                            $("#divHasilPencarian").html('<span>Sedang memproses...</span>');
                            $('#divGraphPencarian').html('');
                        },
                        success: function(data){
                            if (typeof data.msg !== 'undefined') {
                                console.log("masuk sini : "+data.msg);
                                $('#boxHasilPencarian').show();
                                $('#divHasilPencarian').html("<span>"+data.msg+"</span>");
                            } else {
                                // // spin();
                                // console.log("Fungsi SearchNews");
                                let listHasil = "";
                                // let arrHasil = new Array();
                                let arrHasil = "";
                                // console.log(JSON.stringify(data));
                                // console.log((data));
                                data_berita = data.data_berita;
                                // data_berita_tmp = data;
                                data_berita.forEach(function(elm){
                                    // arrHasil.push(elm.id_berita);
                                    arrHasil += ","+elm.id_berita;
                                });
                                // console.log(arrHasil);
                                $('#id_berita').val(arrHasil.substr(1));
                                // $('#id_berita').val("");
                                // $('#hasil_berita').val(JSON.stringify(data_berita));
                                
                                $('#boxHasilPencarian').show();
                                let btnSave = '';
                                $("#divHasilPencarian").html('');
                                $('#divGraphPencarian').html('');
                                data.beritaPerKeyword.forEach(function(elm) {
                                    $("#divHasilPencarian").append(' <span class="btn btn-default btn-lg fa fa-search" > '+elm.kata.toUpperCase()+' ('+elm.count+' Berita)</span> ');                    
                                })
                                $("#divHasilPencarian").append('<div class="clearfix"><br></div>');
                                $("#divHasilPencarian").append('<span class="btn btn-primary fa fa-pie-chart" onClick="graphNews(`'+limitTopWord+'`);"> Graph News</span> <span class="btn btn-danger fa fa-trash" onClick="delNews();"> Delete News</span> '+btnSave+'<div class="clearfix"><br></div>');
                                drawTable("#divHasilPencarian",data_berita);
                                // $("#btnSearch").html("Cari");
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown){
                            // // spin();
                            // swal("Error!", "Gagal memperoleh data dari ajax!", "error");
                            $('#boxHasilPencarian').show();
                            $('#listHasilPencarian').html('Data Kosong');
                            // $("#btnSearch").html("Cari");
                        }   
                    }).done(function() {
                        // setTimeout(function(){
                        //     $("#cover-spin").hide();
                        // },500);
                        swal("Selesai!", "Pencarian sudah selesai", "success");
                    });
                }, 2 * 1000);
            } else {
                // alert("Pencarian Tidak Boleh Kosong");
                swal("Perhatian!", "Kolom Pencarian Tidak Boleh Kosong!", "warning");
            }
        } else {
            swal("Perhatian!", "Tingkat Kepercayaan Situs Berita Tidak Boleh Kosong!", "warning");
        }        
    } else {
        swal("Perhatian!", "Project Belum Dipilih!", "warning");
    } 
}

function searchNewsProject() {
    // let form = document.getElementById('formSearch');
    // let dataForm = $('#formSearch').serialize();
    console.log("masuk searchNewsProject");
    // // spin();
    if($("#kata_pencarian").val() !== ""){
        // $("#btnSearch").html("Sedang Mencari");
        $("#cover-spin").show();
        $('#boxHasilPencarian').show();
        $("#divHasilPencarian").html('<span>Sedang memproses...</span>');
        $('#divGraphPencarian').html('');
        setTimeout(() => {
            $.ajax({ 
                url : base_url+'api/searchBerita',
                // url : window.location.pathname+'/api/searchBerita',            
                type: "POST",
                data : {
                    kata_pencarian:$('#kata_pencarian').val(),
                    threshold:$('#threshold').val(),
                    lvl_situs:$('#lvl_situs:checked').val(),
                    startDate:$('#startDate').val(),
                    endDate:$('#endDate').val(),
                    kata_positif:$('#kata_positif').val(),
                    kata_negatif:$('#kata_negatif').val(),
                    id_berita:$('#id_berita').val()
                },
                dataType: "JSON",
                async: false,        
                beforeSend: function(){
                    // $("#cover-spin").show();
                },
                success: function(data){
                    // // spin();
                    // console.log("fungsi searchNewsProject");
                    let listHasil = "";
                    // let arrHasil = new Array();
                    let arrHasil = "";
                    // console.log(JSON.stringify(data));
                    // console.log((data.data_berita));
                    data_berita = data.data_berita;
                    // data_berita_tmp = data;
                    data.data_berita.forEach(function(elm){
                        // arrHasil.push(elm.id_berita);
                        arrHasil += ","+elm.id_berita;
                    });
                    // console.log(arrHasil.substr(1));
                    $('#id_berita').val(arrHasil.substr(1));
                    // $('#hasil_berita').val(JSON.stringify(data_berita));
                    
                    $('#boxHasilPencarian').show();
                    let btnSave = '';
                    $("#divHasilPencarian").html('');
                    $('#divGraphPencarian').html('');
                    data.beritaPerKeyword.forEach(function(elm) {
                        $("#divHasilPencarian").append(' <span class="btn btn-default btn-lg fa fa-search" > '+elm.kata.toUpperCase()+' ('+elm.count+' Berita)</span> ');                    
                    })
                    $("#divHasilPencarian").append('<div class="clearfix"><br></div>');
                    $("#divHasilPencarian").append('<span class="btn btn-primary fa fa-pie-chart" onClick="graphNews('+limitTopWord+');"> Graph News</span> <span class="btn btn-danger fa fa-trash" onClick="delNews();"> Delete News</span> '+btnSave+'<div class="clearfix"><br></div>');
                    drawTable("#divHasilPencarian",data_berita);
                    // $("#btnSearch").html("Cari");
                },
                error: function (jqXHR, textStatus, errorThrown){
                    // // spin();
                    // swal("Error!", "Gagal memperoleh data dari ajax!", "error");
                    $('#boxHasilPencarian').show();
                    $('#listHasilPencarian').html('Data Kosong');
                    // $("#btnSearch").html("Cari");
                }        
            }).done(function() {
                setTimeout(function(){
                    // $("#cover-spin").hide();
                    // spin();
                },500);
            });
        }, 2 * 1000);
    } else {
        // alert("Pencarian Tidak Boleh Kosong");
        swal("Perhatian!", "Kolom Pencarian Tidak Boleh Kosong!", "warning");
    }
}

function showResProject(tag) {
    // // spin();
    // console.log(tag);
    // console.log(tag.dataset.threshold);
    // alert("masuk file app.js")
    // let form = document.getElementById('formSearch');
    // let dataForm = $('#formSearch').serialize();
    // console.log(JSON.stringify(dataForm));
    let dataset = new Array;
    dataset['kata_pencarian'] = tag.dataset.kata_pencarian;
    dataset['threshold'] = tag.dataset.threshold;
    dataset['lvl_situs'] = tag.dataset.lvl_situs;
    dataset['startDate'] = tag.dataset.startdate;
    dataset['endDate'] = tag.dataset.enddate;
    dataset['kata_positif'] = tag.dataset.kata_positif;
    dataset['kata_negatif'] = tag.dataset.kata_negatif;
    dataset['id_berita'] = tag.dataset.id_berita;
    // console.log(dataset);
    if(tag.dataset.kata_pencarian !== ""){
        // $("#btnSearch").html("Sedang Mencari");
        $("#cover-spin").show();
        $('#boxHasilPencarian').show();
        $("#divHasilPencarian").html('<span>Sedang memproses...</span>');
        $('#divGraphPencarian').html('');
        setTimeout(() => {
            $.ajax({ 
                url : base_url+'api/searchBerita',
                // url : window.location.pathname+'/api/searchBerita',            
                type: "POST",
                data : {
                    kata_pencarian:dataset['kata_pencarian'],
                    threshold:dataset['threshold'],
                    lvl_situs:dataset['lvl_situs'],
                    startDate:dataset['startDate'],
                    endDate:dataset['endDate'],
                    kata_positif:dataset['kata_positif'],
                    kata_negatif:dataset['kata_negatif'],
                    id_berita:dataset['id_berita']
                },
                dataType: "JSON",
                async: false,        
                beforeSend: function(){
                    // $("#cover-spin").show();
                },
                success: function(data){
                    // // spin();
                    // console.log("fungsi searchNewsProject");
                    let listHasil = "";
                    // let arrHasil = new Array();
                    let arrHasil = "";
                    // console.log(JSON.stringify(data));
                    // console.log((data.data_berita));
                    data_berita = data.data_berita;
                    // data_berita_tmp = data;
                    data.data_berita.forEach(function(elm){
                        // arrHasil.push(elm.id_berita);
                        arrHasil += ","+elm.id_berita;
                    });
                    // console.log(arrHasil.substr(1));
                    // $('#id_berita').val(arrHasil.substr(1));
                    // $('#hasil_berita').val(JSON.stringify(data_berita));
                    
                    $('#boxHasilPencarian').show();
                    let btnSave = '';
                    $("#divHasilPencarian").html('');
                    $('#divGraphPencarian').html('');                
                    $("#divHasilPencarian").append('<div class="callout callout-success"> <h3>Hasil Pencarian!</h3> <p>Kata Pencarian : '+dataset['kata_pencarian'].toUpperCase()+' </p> <p>Tanggal : '+dataset['startDate']+' - '+dataset['endDate']+' </p> </div>');
                    data.beritaPerKeyword.forEach(function(elm) {
                        $("#divHasilPencarian").append(' <span class="btn btn-default btn-lg fa fa-search" > '+elm.kata.toUpperCase()+' ('+elm.count+' Berita)</span> ');                    
                    })
                    $("#divHasilPencarian").append('<div class="clearfix"><br></div>');
                    $("#divHasilPencarian").append('<span class="btn btn-primary fa fa-pie-chart" onClick="graphNews('+limitTopWord+');"> Graph News</span> '+btnSave+'<div class="clearfix"><br></div>');
                    drawTable("#divHasilPencarian",data_berita);
                    // $("#btnSearch").html("Cari");
                },
                error: function (jqXHR, textStatus, errorThrown){
                    // // spin();
                    // swal("Error!", "Gagal memperoleh data dari ajax!", "error");
                    $('#boxHasilPencarian').show();
                    $('#listHasilPencarian').html('Data Kosong');
                    // $("#btnSearch").html("Cari");
                }        
            }).done(function() {
                setTimeout(function(){
                    // $("#cover-spin").hide();
                    // spin();
                },500);
            });
        },2 * 1000);
    } else {
        // alert("Pencarian Tidak Boleh Kosong");
        swal("Perhatian!", "Kolom Pencarian Tidak Boleh Kosong!", "warning");
    }
}

function setDataBerita(tag) {
    // let dataset = new Array;
    // dataset['list_kata_kunci'] = tag.dataset.list_kata_kunci;
    // console.log(dataset['list_kata_kunci']);
    $('#divDataSet').html("");
    $('#id_setting_project').val(tag.dataset.id_setting_project);
    $('#list_kata_kunci').val(tag.dataset.list_kata_kunci);
    let divSettingProjoject = document.getElementsByClassName("setting-project");
    for (let index = 0; index < divSettingProjoject.length; index++) {
        console.log(divSettingProjoject[index].classList);
        divSettingProjoject[index].style.backgroundColor = "#c3c3c3 ";
        divSettingProjoject[index].style.color = "#000 ";
        // divSettingProjoject[index].classList.add('bg-green');
    }
    tag.style.backgroundColor = "#dd4b39";
    tag.style.color = "#fff";
    setTimeout(function(){
        $("#cover-spin").show();
        $.ajax({ 
            url : base_url+'api/getDataSetBerita',
            // url : window.location.pathname+'/api/searchBerita',            
            type: "POST",
            data : {
                id_setting_project:tag.dataset.id_setting_project,
                list_kata_kunci:tag.dataset.list_kata_kunci
            },
            dataType: "JSON",
            async: false,
            beforeSend: function(){
                // $("#cover-spin").show();
            },
            success: function(data){
                console.log(data);
                let data_set_berita = data.data_berita;
                drawTableDataSet("#divDataSet",data_set_berita);
                // if (data.status) {
                //     swal("Berhasil", "Data pencarian berhasil disimpan.", "success");
                // } else {
                //     swal("Perhatian!", "Gagal memperoleh data.", "error");
                // }
            },
            error: function (jqXHR, textStatus, errorThrown){
                // // spin();
                swal("Perhatian!", "Gagal memperoses data.", "error");
                $('#boxHasilPencarian').show();
                $('#listHasilPencarian').html('Data Kosong');
                // $("#btnSearch").html("Cari");
            }        
        }).done(function() {
            setTimeout(function(){
                // $("#cover-spin").hide();
                // spin();
            },500);
        });
        // spin();
    },2 * 1000); 
    // document.getElementById("MyElement").classList.remove('MyClass');
}

function saveHistoryPencarian() {
    if ($('#id_setting_project').val() !== "") {
        if($("#kata_pencarian").val() !== ""){
            if($("#id_berita").val() !== ""){
                $.ajax({ 
                    url : base_url+'api/saveHistoryPencarian',
                    // url : window.location.pathname+'/api/searchBerita',            
                    type: "POST",
                    data : $('#formSearch').serialize(),
                    dataType: "JSON",
                    async: false,
                    beforeSend: function(){
                        // $("#cover-spin").show();
                    },
                    success: function(data){
                        // console.log(data);
                        if (data.status) {
                            swal("Berhasil", "Data pencarian berhasil disimpan.", "success");
                        } else {
                            swal("Perhatian!", "Gagal memperoleh data.", "error");
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        // // spin();
                        swal("Perhatian!", "Gagal memperoses data.", "error");
                        $('#boxHasilPencarian').show();
                        $('#listHasilPencarian').html('Data Kosong');
                        // $("#btnSearch").html("Cari");
                    }        
                }).done(function() {
                    setTimeout(function(){
                        // $("#cover-spin").hide();
                        // spin();
                    },500);
                });                
            } else {
                swal("Perhatian!", "Data Berita Belum Dicari!", "warning");
            }
        } else {
            swal("Perhatian!", "Kolom Pencarian Tidak Boleh Kosong!", "warning");
        }
    } else {
        swal("Perhatian!", "Project Belum Dipilih!", "warning"); 
    }
}

function setFormPencarian(div) {
    let spanStartDate, spanEndDate = "";
    // console.log(div);
    let e = document.getElementById("id_pencarian_setting_project");
    let opt = e.options[e.selectedIndex];
    console.log(opt.dataset);
    $("#kata_pencarian").val(opt.dataset.kata_pencarian);
    $("#id_setting_project").val(opt.dataset.id_setting_project);
    $("#list_kata_kunci").val(opt.dataset.list_kata_kunci);
    $("#threshold").val(opt.dataset.threshold);
    $("#demo").html(opt.dataset.threshold);
    document.getElementById("threshold").value = opt.dataset.threshold;

    // $("#lvl_situs").val(opt.dataset.lvl_situs);
    document.formSearch.lvl_situs.value=opt.dataset.lvl_situs;

    $("#startDate").val(opt.dataset.startdate);
    spanStartDate = opt.dataset.startdate;
    $("#endDate").val(opt.dataset.enddate);
    spanEndDate = opt.dataset.enddate;
    $('#daterange-btn span').html(spanStartDate + ' - ' + spanEndDate);
    
    // $("#kata_positif").val(opt.dataset.kata_positif);
    $('#kata_positif').tagsinput('add', opt.dataset.kata_positif);
    // $("#kata_negatif").val(opt.dataset.kata_negatif);
    $('#kata_negatif').tagsinput('add', opt.dataset.kata_negatif);
    $("#id_berita").val(opt.dataset.id_berita);
}

function delData(type,id) {
    // console.log(type,id)
    $.ajax({ url : "admin/"+type+"/hapus",
        type: "POST",
        dataType: "JSON",
        data: {id:id},
        async: false,        
        beforeSend: function(){
            // $("#cover-spin").show();
        },
        success: function(data){
            // swal('Data '+type+' berhasil dihapus','success');
            document.location = data.url;
        },
        error: function (jqXHR, textStatus, errorThrown){
            swal('Gagal memproses data dari ajax','error');
        }
    }).done(function() {
        setTimeout(function(){
            // $("#cover-spin").hide();
            // spin();
        },500);
    });
}

function openBerita(id) {
    // console.log(type,id)
    window.open(base_url+"api/get_data/berita/"+id, '_blank');
    // $.ajax({ 
    //     url : base_url+"api/get_data/berita",
    //     type: "POST",
    //     dataType: "JSON",
    //     data: {id:id},
    //     async: false,
    //     success: function(data){
    //         // console.log(data);
    //         http://localhost:8081/kerjaan/news_aggre_jn1/api/get_data/berita/9425
    //         // swal('Data '+type+' berhasil dihapus','success');
    //         // document.location = data.url;
    //     },
    //     error: function (jqXHR, textStatus, errorThrown){
    //         swal('Gagal memproses data dari ajax','error');
    //     }
    // });
}

function genPass(source,target) {
    let length = 10;
    let result           = '';
    let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let charactersLength = characters.length;
    for ( let i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    // return result;
    $("#password").val(result);
    $("#repassword").val(result);
    alert("Copied this password in a safe place! : "+result);
}