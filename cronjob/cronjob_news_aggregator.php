<?php
include 'setting_cronjob.php';

$mysqli = mysqli_connect($databaseHost, $databaseUsername, $databasePassword, $databaseName);
date_default_timezone_set("Asia/Jakarta");

function link_berita_prev() {
	$sql = "SELECT id_situs_berita FROM m_situs_berita WHERE status = 1 ORDER BY id_situs_berita DESC LIMIT 1";
	$query = array();
	$result = mysqli_query($GLOBALS['mysqli'], $sql);
	while($row = mysqli_fetch_array($result)) {
		$query[] = $row;
	}
	$max_id = $query[0]['id_situs_berita'];
	// var_dump("max_id : ",$max_id,"<hr>");

	$sql = "SELECT id_situs_berita FROM t_link_berita_prev LIMIT 1";
	$query = array();
	$result = mysqli_query($GLOBALS['mysqli'], $sql);
	while($row = mysqli_fetch_array($result)) {
		$query[] = $row;
	}
	$last_id = $query[0]['id_situs_berita'];
	// var_dump("last_id : ",$last_id,"<hr>");
	
	$sql = "SELECT id_situs_berita FROM m_situs_berita WHERE id_situs_berita = (select min(id_situs_berita) from m_situs_berita where id_situs_berita > ".$last_id." AND status = 1)";
	// $sql = "SELECT id_situs_berita FROM m_situs_berita WHERE id_situs_berita = (select min(id_situs_berita) from m_situs_berita where id_situs_berita > 4 AND status = 1)";
	// $sql = "SELECT id_situs_berita FROM m_situs_berita WHERE status = 1 ORDER BY RAND() LIMIT 3";
	$query = array();
	$result = mysqli_query($GLOBALS['mysqli'], $sql);
	while($row = mysqli_fetch_array($result)) {
		$query[] = $row;
	}
	$curent_id = $query[0]['id_situs_berita'];
	// var_dump("curent_id : ",$curent_id,"<hr>");
	
	if(isset($_POST['submit'])){
		$statusCrawl = false; // false jika test crawl		
	} else {
		$statusCrawl = true;
		if ($max_id == $curent_id) {
			$sql = "UPDATE t_link_berita_prev SET id_situs_berita=0 WHERE id_link_berita_prev=1";
			if (mysqli_query($GLOBALS['mysqli'], $sql)) {
				// echo "New record created successfully";
			} else {
				echo "Error: " . $sql . "<br>" . mysqli_error($GLOBALS['mysqli']);
			}
		} else {
			$sql = "UPDATE t_link_berita_prev SET id_situs_berita=".$curent_id." WHERE id_link_berita_prev=1";
			if (mysqli_query($GLOBALS['mysqli'], $sql)) {
				// echo "New record created successfully";
			} else {
				echo "Error: " . $sql . "<br>" . mysqli_error($GLOBALS['mysqli']);
			}
		}
	}
	
	return $curent_id;
}

function get_situs_berita() {
	$sql = "SELECT id_situs_berita FROM m_situs_berita WHERE status = 1 ORDER BY RAND() LIMIT 3";
	$query = array();
	$result = mysqli_query($GLOBALS['mysqli'], $sql);
	while($row = mysqli_fetch_array($result)) {
		$query[] = $row;
	}
	return $query;
}

function get_situs_berita_by_id($id) {
	$sql = "SELECT `sb`.*
		FROM `m_situs_berita` as `sb`
		WHERE `sb`.`id_situs_berita` = '".$id."' ";
	$query = array();
	$result = mysqli_query($GLOBALS['mysqli'], $sql);
	while($row = mysqli_fetch_array($result)) {
		$query[] = $row;
	}
	return $query;
}

function get_kata_kunci() {
	$sql = "SELECT kata_kunci FROM m_kata_kunci WHERE status = 1";
	$query = array();
	$result = mysqli_query($GLOBALS['mysqli'], $sql);
	while($row = mysqli_fetch_array($result)) {
		$query[] = $row;
	}
	return $query;
}

function cek_berita($link_berita,$judul_berita) {
	// var_dump($link_berita,$judul_berita);
	// die();

  	$sql = "SELECT link_berita FROM m_berita WHERE link_berita = '".$link_berita."' OR judul_berita = '".$judul_berita."' ";
	$query = array();
	$result = mysqli_query($GLOBALS['mysqli'], $sql);
	// while($row = mysqli_fetch_array($result)) {
	// 	$query[] = $row; // Inside while loop
	// }
	$num_rows=mysqli_num_rows($result);
	// $result = mysql_free_result($query);
	// $row = $result->fetch_array();
	// $conn->close();
	// mysqli_close($GLOBALS['link']);
	return $num_rows;
}

function insert_data_berita($row){
	// var_dump($row);
	// die();
	$sql = "INSERT INTO `m_berita` (`id_situs_berita`, `link_berita`, `judul_berita`, `deskripsi`, `isi_berita`, `publish_date`, `kata_kunci`, `cdd`, `status`) VALUES ('".$row['id_situs_berita']."', '".$row['link_berita']."', '".$row['judul_berita']."', '".$row['deskripsi']."', '".str_replace("'", '"', $row['isi_berita'])."', '".$row['publish_date']."', '".$row['kata_kunci']."', '".$row['cdd']."', '".$row['status']."')";
	// mysqli_query($GLOBALS['mysqli'], "INSERT INTO `m_berita` (`id_situs_berita`, `link_berita`, `judul_berita`, `deskripsi`, `isi_berita`, `publish_date`, `cdd`, `status`) VALUES ('".$row['id_situs_berita']."', ".$row['link_berita'].", '".$row['judul_berita']."', '".$row['deskripsi']."', '".$row['isi_berita']."', ".$row['publish_date']."', '".$row['cdd']."', ".$row['status'].")");
	// mysqli_close($GLOBALS['link']);
	if (mysqli_query($GLOBALS['mysqli'], $sql)) {
		// echo "New record created successfully";
	} else {
	    echo "Error: " . $sql . "<br>" . mysqli_error($GLOBALS['mysqli']);
	}
}

function insert_data_berita_batch($data){
	// var_dump($row);
	// die();
	// $sql = "INSERT INTO `m_berita` (`id_situs_berita`, `link_berita`, `judul_berita`, `deskripsi`, `isi_berita`, `publish_date`, `cdd`, `status`) VALUES ('".$row['id_situs_berita']."', '".$row['link_berita']."', '".$row['judul_berita']."', '".$row['deskripsi']."', '".str_replace("'", '"', $row['isi_berita'])."', '".$row['publish_date']."', '".$row['cdd']."', '".$row['status']."')";
	
	$sql_build = "INSERT INTO `m_berita` (`id_situs_berita`, `link_berita`, `judul_berita`, `deskripsi`, `isi_berita`, `publish_date`, `kata_kunci`, `cdd`, `status`) VALUES ".implode(',', $data);
	// var_dump($sql_build);
	// die;
	// mysqli_close($GLOBALS['link']);
	if (mysqli_query($GLOBALS['mysqli'],$sql_build)) {
		// echo "New record created successfully";
	} else {
	    echo "Error: " . $sql_build . "<br>" . mysqli_error($GLOBALS['mysqli']);
	}
}
// Get Next Id Link Berita

// $situs_berita = get_situs_berita();
// Get Situs Berita
if (isset($_GET['id_berita'])) {	
	$statusCrawl = true;
	$id_situs_berita = $_GET['id_berita'];
} else {
	$id_situs_berita = link_berita_prev();
}
$situs_berita = get_situs_berita_by_id($id_situs_berita);

// Set Curent Id Link Berita To t_link_berita_prev
// Cek jika Id Link Berita == Id link berita terakhir THEN set curent Id Link Berita = 1

$kata_kunci = get_kata_kunci();
$count_kata_kunci = 0;
// $link_situs = get_situs_berita_by_id(6);

// var_dump($situs_berita,"<hr>");
// var_dump($kata_kunci,"<hr>");
// var_dump($link_situs,"<hr>");
// die();
$i = 0;
$count_data_crawl = 0;
foreach ($situs_berita as $key => $val_link) {
	try {
		$time_start = microtime(true); 
		$i++;
		$respon = FALSE;
		if(isset($_POST['submit'])){
			// var_dump($_POST,"<hr>");
			// die;
			$respon = TRUE;
			$id_situs_berita = $_POST['id_situs_berita'];
			$link_situs = $_POST['link_situs'];
			$uri_segment = $_POST['uri_segment'];

			$tag_judul = $_POST['start_tag_judul'];
			if ($_POST['id_cari_tag_judul'] == "class") {
				$class_judul_val = $_POST['cari_val_judul'];
				$id_judul_val = NULL;
			} else {
				$class_judul_val = NULL;
				$id_judul_val = $_POST['cari_val_judul'];
			}

			$tag_isi = $_POST['start_tag_isi'];
			if ($_POST['id_cari_tag_isi'] == "class") {	
				$class_isi_val = $_POST['cari_val_isi'];
				$id_isi_val = NULL;
			} else {	
				$class_isi_val = NULL;
				$id_isi_val = $_POST['cari_val_isi'];
			}
		} else {
			$arr_link_situs = get_situs_berita_by_id($val_link['id_situs_berita']);
			echo "<br>Situs : ".$arr_link_situs[0]['link_situs']."<hr>";
			// if ($i >= 2) {
			// 	var_dump($val_link,"<hr>");
			// 	var_dump($arr_link_situs,"<hr>");
			// 	// die();
			// }

			$id_situs_berita = $arr_link_situs[0]['id_situs_berita'];
			$link_situs = $arr_link_situs[0]['link_situs'];
			$tag_judul = $arr_link_situs[0]['start_tag_judul'];
			$class_judul_val = $arr_link_situs[0]['class_tag_judul'];
			$id_judul_val = $arr_link_situs[0]['id_tag_judul'];
			$tag_isi = $arr_link_situs[0]['start_tag_isi'];
			$class_isi_val = $arr_link_situs[0]['class_tag_isi'];
			$id_isi_val = $arr_link_situs[0]['id_tag_isi'];
			$uri_segment = $arr_link_situs[0]['uri_segment'];
		}
		// $nodes = array();
		$post = array();
		$posts = array();
		$data_link = array();
		$post['news_valid'] = FALSE;
		$post['check_berita'] = 0;
		// var_dump($tag_judul,"<hr>",$class_judul_val,"<hr>",$id_judul_val);

		// var_dump($link_situs[0],"<hr>");
		// News Crawler
		$dom = new DOMDocument;
		@$dom->loadHTMLFile($link_situs);
		//get all anchor(a) tag on page
		$links = $dom->getElementsByTagName('a'); 

		// var_dump($dom,"<hr>");
		// var_dump($links->length,"<hr>");
		// die();
		if ($links->length == 0) {
			throw new Exception("Link ".$link_situs." tidak ditemukan");
		}
		// Iterate over the extracted links and display their URLs
		foreach ($links as $link){
			// Extract and show the "href" attribute.
			$link_val = $link->getAttribute('href');
			$real_link = $link_val;
			$link_val = preg_replace('#^https?://#', '', $link_val);
			if (substr($link_val, -1) == "/") {
				$link_val = substr($link_val, 0, -1);
			}

			$temp_link_val = explode("/", $link_val);
			// var_dump($link_val,"<hr>");
			if (count($temp_link_val) == $uri_segment) {
				array_push($data_link, $real_link);
			}
		}
		// var_dump($data_link,"<hr>");
		// die();
		// Cek Duplikasi link
		$data_link = array_unique($data_link);
		// var_dump("awal <hr>",$data_link,"<hr>");
		$sql_berita = array(); 
		// $arrExeptionLink = array("logout","login","play.google","www.republika","www.facebook","www.instagram","bantenhits.com/category/pilkada");
		$filename2 = __DIR__ . '/ignore_link.txt';
		// $fp2 = @fopen($filename2, 'r');
		$fp2 = @fopen($filename2, 'c+'); 

		// Add each line to an array
		if ($fp2) {
			$arrExeptionLink = explode("\n", fread($fp2, filesize($filename2)));
		}
		// var_dump($arrExeptionLink);
		// die;
		$newDataLink = $data_link;
		foreach ($newDataLink as $key => $val) {
			foreach ($arrExeptionLink as $key2 => $val2) {
				if (is_numeric(strpos($val,$val2))) {
					// var_dump($data_link[$key],"<hr>");
					// array_splice($data_link, $key, 1);
					unset($data_link[$key]);
				}
			}
		}
		// var_dump("akhir <hr>",$data_link,"<hr>");
		// die;
		
		// Get data berita dari halaman berita
		foreach ($data_link as $key => $value) {
			$post['news_valid'] = FALSE;
			$post['check_berita'] = 0;
			$post = array();

			// var_dump($value,"<hr>");
			// die;
			@$dom->loadHTMLFile($value);
			// $dom->saveHTMLFile("crawl.html") ;
			// $filename = __DIR__ . '/crawl.html';
			// $file = fopen($filename,"c+");
			// // fwrite($file,'<!DOCTYPE html>');
			// file_put_contents($filename, '<!DOCTYPE html>'.PHP_EOL , FILE_APPEND );
			// fclose($file);
			// $dom->loadHTMLFile($filename);
			// die;
			
			// <!DOCTYPE html>
			// $dom->validate();
			$xpath = new DOMXpath($dom);

			$nodes_isi = array();
			$nodes_judul = array();
			$nodes_isi = $dom->getElementsByTagName($tag_isi);
			$nodes_judul = $dom->getElementsByTagName($tag_judul);			
			// var_dump(json_encode($nodes_judul),"<hr>");
			// die;

			// Get Judul Berita
			$post['judul_berita'] = "";
			if (!is_null($id_judul_val)) {
				// Search By Id
				if ($id_judul_val == "") {
					foreach ($nodes_judul as $judulContent) {
						$post['judul_berita']= $judulContent->nodeValue;
					}
				} else {					
					$element_judul = $dom->getElementById($id_judul_val);
					// var_dump($id_judul_val,"<hr>",$element_judul->textContent,"<hr>");
					// var_dump($id_judul_val,"<hr>",$dom->getElementById($id_judul_val),"<hr>");
					// var_dump($id_judul_val,"<hr>",$xpath->query("*/h1"),"<hr>");
					// die;
					if (!is_null($element_judul)) {
						$post['judul_berita']	= $element_judul->textContent;
					} 
					else {
						// $dom->saveHTMLFile("crawl.html") ;
						throw new Exception("Settingan ".$link_situs." tidak ditemukan tag '".$tag_judul."' dengan ID '".$id_judul_val."' untuk judul");
					}
				}
				// var_dump($element_judul,"<hr>");
				// die();
			} else {
				// Search By Class
				if ($class_judul_val == "") {
					foreach ($nodes_judul as $judulContent) {
						$post['judul_berita']= $judulContent->nodeValue;
					}
				} else {
					foreach ($nodes_judul as $element_judul) {
						if (!is_null($element_judul)) {
							if ($element_judul->textContent != "") {
								$class_judul = $element_judul->getAttribute("class");
								// var_dump(strpos($class_judul, $class_judul_val),"<hr>");
								// var_dump($class_judul,"<hr>");
								// die;
								// if (strpos($class_judul, $class_judul_val) !== false) {
								if (is_numeric(strpos($class_judul, $class_judul_val))) {
									$post['judul_berita']	= $element_judul->textContent;
								} 
								// else {
								// 	// var_dump($link);
								// 	// die;
								// 	throw new Exception("Settingan ".$link_situs." tidak ditemukan class '".$class_judul_val."' untuk judul");
								// }
							}
						} 
						else {
							throw new Exception("Settingan ".$link_situs." tidak ditemukan tag '".$tag_judul."' untuk judul");
						}
						// var_dump($element_judul,"<hr>");
						// die();
					}
					if($post['judul_berita'] == ""){
						throw new Exception("Settingan ".$link_situs." tidak ditemukan tag '".$tag_judul."' dengan class '".$class_judul_val."' untuk judul");
					}	
				}
			}
			// var_dump($post['judul_berita'],"<hr>");

			// Get Isi Berita			
			$getDataIsi = 0;
			$findKataKunci = 0;
			if (!is_null($id_isi_val)) {
				// Search By Id
				// echo "masuk id <hr>";
				$element_id = $dom->getElementById($id_isi_val);
				// var_dump($element_id->textContent,"<hr>");
				if (!is_null($element_id)) {
					$kataKunciFind = array();
					foreach ($kata_kunci as $val_kata_kunci) {
						if (strpos(strtolower($element_id->textContent), strtolower($val_kata_kunci["kata_kunci"]))) {
							array_push($kataKunciFind,strtolower($val_kata_kunci["kata_kunci"]));
							$findKataKunci++;
						}
					}
					array_unique($kataKunciFind);					
					if (isset($_POST['submit'])) {
						$kataKunciFind[] = "test";
					}
					if(count($kataKunciFind) > 0){
						// var_dump(json_encode($kataKunciFind),"<hr>");
						$count_kata_kunci++;
						$getDataIsi++;
						$post['link_berita']	= $value;
						$post['crawl_date']		= date('Y-m-d H:i:s');
						$post['isi_berita']		= strtolower($element_id->textContent);
						$post['news_valid']		= TRUE;
						$post['kata_kunci']		= implode(";",$kataKunciFind);

						$post['check_berita']	= cek_berita($post['link_berita'],$post['judul_berita']);
						if (isset($_POST['submit'])) {
							echo json_encode(array("success"=>true, "msg"=>"Crawler data link '".$link_situs."' berhasil!"));	
							exit;
						}
						// var_dump($post['check_berita']);
						// die();
						if (($post['check_berita'] == 0) && ($post['news_valid'] == TRUE)) {
							// Insert 
							$new_isi_berita = str_replace('"',"'",$post['isi_berita']);
							// Clean quotes
							$new_isi_berita = htmlentities($new_isi_berita, ENT_QUOTES);
							$berita['data'] 	= array("id_situs_berita" => $id_situs_berita, "link_berita" => $post['link_berita'], "judul_berita" => $post['judul_berita'], "deskripsi" => "", "isi_berita" => $post['isi_berita'], "publish_date" => date('Y-m-d H:i:s'), "kata_kunci" => $post['kata_kunci'], "cdd" => $post['crawl_date'], "status" => 1);
							insert_data_berita($berita['data']);

							// $sql_berita[] = '('.$id_situs_berita.', "'.$post['link_berita'].'", "'.$post['judul_berita'].'", "", "'.$new_isi_berita.'", "'.date('Y-m-d H:i:s').'", "'.$post['kata_kunci'].'", "'.$post['crawl_date'].'", 1)';
							// insert_data_berita_batch($sql_berita);
							array_push($posts, $post);
							$count_data_crawl++;
						}
						break;
					}
				}
				else {
					// var_dump($post,"<hr>");
					// die;
					throw new Exception("Settingan ".$link_situs." tidak ditemukan tag '".$tag_isi."' dengan ID '".$id_isi_val."' untuk isi");
				}
			} else {
				// Search By Class
				// echo "masuk class <hr>";
				foreach ($nodes_isi as $element_isi) {
					// var_dump($element_isi,"<hr>");
					if(!empty($element_isi->textContent)){
						$kataKunciFind = array();
						foreach($kata_kunci as $val_kata_kunci){
							if (strpos(strtolower($element_isi->textContent), strtolower($val_kata_kunci["kata_kunci"]))) {
							// if (strpos(strtolower($element_id->textContent), strtolower($val_kata_kunci["kata_kunci"]))) {
								array_push($kataKunciFind,strtolower($val_kata_kunci["kata_kunci"]));
								$findKataKunci++;
							}
						}
						array_unique($kataKunciFind);
						if (isset($_POST['submit'])) {
							$kataKunciFind[] = "test";
						}
						if(count($kataKunciFind) > 0){
							// var_dump(json_encode($kataKunciFind),"<hr>");
							$count_kata_kunci++;
							$classy = $element_isi->getAttribute("class");
							// if (strpos($classy, $class_isi_val) !== false) {
							if (is_numeric(strpos($classy, $class_isi_val))) {
								// echo "masuk cuy";die();
								$getDataIsi++;
								$post['link_berita']	= $value;
								$post['crawl_date']		= date('Y-m-d H:i:s');
								$post['isi_berita']		= strtolower($element_isi->textContent);
								$post['news_valid']		= TRUE;
								$post['kata_kunci']		= implode(";",$kataKunciFind);

								$post['check_berita']	= cek_berita($post['link_berita'],$post['judul_berita']);									
								if (isset($_POST['submit'])) {
									// var_dump("data isi : ",$getDataIsi,"<hr>");
									echo json_encode(array("success"=>true, "msg"=>"Test crawler data link '".$link_situs."' berhasil! "));	
									exit;
								}
								// var_dump($post['check_berita']);
								// die();
								// var_dump($post,"<hr>");
								// if($post['check_berita'] && $post['news_valid']){
								if (($post['check_berita'] == 0) && ($post['news_valid'] == TRUE)) {
									// Insert data
									$new_isi_berita = str_replace('"',"'",$post['isi_berita']);
									// Clean quotes
									$new_isi_berita = htmlentities($new_isi_berita, ENT_QUOTES);
									$berita['data'] 	= array("id_situs_berita" => $id_situs_berita, "link_berita" => $post['link_berita'], "judul_berita" => $post['judul_berita'], "deskripsi" => "", "isi_berita" => $post['isi_berita'], "publish_date" => date('Y-m-d H:i:s'), "kata_kunci" => $post['kata_kunci'], "cdd" => $post['crawl_date'], "status" => 1);
									insert_data_berita($berita['data']);

									// $berita['data'] 	= array("id_situs_berita" => $id_situs_berita, "link_berita" => $post['link_berita'], "judul_berita" => $post['judul_berita'], "deskripsi" => "", "isi_berita" => $post['isi_berita'], "publish_date" => date('Y-m-d H:i:s'), "cdd" => $post['crawl_date'], "status" => 1);
									// $sql_berita[] = '('.$id_situs_berita.', "'.$post['link_berita'].'", "'.$post['judul_berita'].'", "", "'.$new_isi_berita.'", "'.date('Y-m-d H:i:s').'", "'.$post['kata_kunci'].'", "'.$post['crawl_date'].'", 1)';
									// insert_data_berita_batch($sql_berita);
									array_push($posts, $post);
									$count_data_crawl++;
								}
								// }
								break;
							} 
							// else {
							// 	var_dump($classy,"<hr>");
							// 	var_dump($post,"<hr>");
							// 	// die;
							// 	throw new Exception("Settingan ".$link_situs." tidak ditemukan class '".$class_isi_val."' untuk isi");
							// }
						}
					}
				}
				// if(!$getDataIsi){
				// 	throw new Exception("Settingan ".$link_situs." tidak ditemukan class '".$class_isi_val."' untuk isi");
				// }
			}
			// var_dump("data isi : ",$getDataIsi,"<hr>");
			// var_dump("find kata kunci : ",$findKataKunci,"<hr>");
			// die;

			if ($getDataIsi == 0) {
				if (isset($_POST['submit'])) {
					echo json_encode(array("success"=>false, "msg"=>"Settingan ".$link_situs." tidak ditemukan tag '".$tag_isi."' dengan CLASS '".$class_isi_val."' untuk isi"));	
					exit;
				}  else {
					// if (($statusCrawl) && ($findKataKunci > 0)) {
					// 	var_dump($getDataIsi,"<hr>",$findKataKunci,"<hr>",$kataKunciFind,"<hr>");
					// 	throw new Exception("Settingan ".$link_situs." tidak ditemukan tag '".$tag_isi."' dengan CLASS '".$class_isi_val."' untuk isi");
					// }
				}
			}
		}

		if (isset($_POST['submit'])) {
			echo json_encode(array("success"=>false, "msg"=>"Test crawler data link '".$link_situs."' gagal!"));
			exit;	
		} else {
			if (isset($getDataIsi)) {
				echo " getDataIsi : ".$getDataIsi."<hr>";			
				echo "<hr>".json_encode(array("success"=>true, "data"=>$posts));
			} else {
				echo json_encode(array("success"=>false, "msg"=>"Test crawler data link '".$link_situs."' gagal, tidak ditemukan link berita!"));
				throw new Exception("Link ".$link_situs." gagal, tidak ditemukan link berita!");
				exit;
			}
		}

		// Insert Betch
		/*
		if($count_kata_kunci){
			echo "count sql berita : ".count($sql_berita)."<hr>";
			if(count($sql_berita)) {
				insert_data_berita_batch($sql_berita);
				
				echo "<hr>".json_encode(array("success"=>true, "data"=>$posts));
				
				$time_end = microtime(true);	
				//dividing with 60 will give the execution time in minutes otherwise seconds
				$execution_time = ($time_end - $time_start);
				$txt = "Date : ".date('Y-m-d H:i:s')."; Situs : ".$link_situs[0]['link_situs']."; Execution Time :  ".$execution_time."; Get Data : ".$count_data_crawl." ;";
				$myfile = file_put_contents('logs_Crawl.txt', $txt.PHP_EOL , FILE_APPEND );
			}
		} else {
			echo $link_situs[0]['link_situs']." : tidak ditemukan berita yang dicari";
		}
		*/
	} catch (Exception $e) {	
		if (isset($_POST['submit'])) {
			echo json_encode(array("success"=>false, "msg"=>$e->getMessage()));
			exit;
		} else {
			echo 'Caught exception: ',  $e->getMessage(), "\n";

			require("PHPMailer/PHPMailer.php");
			require("PHPMailer/SMTP.php");

			$mail = new PHPMailer\PHPMailer\PHPMailer();
			$mail->CharSet = 'UTF-8';

			$body = $e->getMessage();
			// Create file log
			$filename = __DIR__ . '/'.$date.'_crawlLog.txt';
			$file = fopen($filename,"c+");
			// fwrite($file,$body);
			file_put_contents($filename, $dateTime.' - '.$body.PHP_EOL , FILE_APPEND );
			fclose($file);

			// Kirim email
			$mail->IsSMTP();
			$mail->Host       = $sender_host;

			$mail->SMTPSecure = $sender_smtpsecure;
			$mail->Port       = $sender_port;
			$mail->SMTPDebug  = 1;
			$mail->SMTPAuth   = true;

			$mail->Username   = $sender_mail;
			$mail->Password   = $sender_pass;
			$mail->Subject    = 'Sistem report crawler : '.$date;

			foreach ($recipient_mail as $key => $valMail) {
				$mail->SetFrom($sender_mail);
				$mail->MsgHTML($body);
				
				$mail->AddAddress($valMail);
				$mail->send();
			}
		}
			
		// die;
		// Insert t_history_crawl
	}
}
?>
