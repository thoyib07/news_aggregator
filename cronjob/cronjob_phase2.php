<?php
// include_once 'IDNstemmer.php';
// include_once 'cronjob/IDNstemmer.php';
include 'setting_cronjob.php';

$arrLokasiProp = array();
$arrLokasiKab = array();
$arrLokasiKec = array();
$arrLokasiKel = array();

// $id_situs_berita = $_GET['id_situs_berita'];

// $databaseHost = 'localhost';
// $databaseName = 'u5594592_db_apl_news_aggre';
// $databaseUsername = 'u5594592_user_news_aggre';
// $databasePassword = '$_rGr?QW&mr^';

// $databaseHost = '172.16.10.164';
// $databaseName = 'db_testing_newsagregator';
// $databaseUsername = 'userdb_testing';
// $databasePassword = '2qEwbwNgLErZ5yJ752JB';


$mysqli = mysqli_connect($databaseHost, $databaseUsername, $databasePassword, $databaseName);
date_default_timezone_set("Asia/Jakarta");

// function new_koneksi() {
// 	$conn = new mysqli("localhost", "root", "", "db_apl_news_aggre");
// 	// Check connection
// 	if ($conn->connect_error) {
// 	    die("Connection failed: " . $conn->connect_error);
// 	}
// }
function link_berita_prev() {
	$sql = "SELECT id_situs_berita FROM m_situs_berita WHERE status = 1 ORDER BY id_situs_berita DESC LIMIT 1";
	$query = array();
	$result = mysqli_query($GLOBALS['mysqli'], $sql);
	while($row = mysqli_fetch_array($result)) {
		$query[] = $row; // Inside while loop
	}
	$max_id = $query[0]['id_situs_berita'];

	$sql = "SELECT id_situs_berita FROM t_link_berita_prev LIMIT 1";
	$query = array();
	$result = mysqli_query($GLOBALS['mysqli'], $sql);
	while($row = mysqli_fetch_array($result)) {
		$query[] = $row; // Inside while loop
	}
	$last_id = $query[0]['id_situs_berita'];
	// var_dump("last_id : ",$last_id,"<hr>");
	
	$sql = "SELECT id_situs_berita FROM m_situs_berita WHERE id_situs_berita = (select min(id_situs_berita) from m_situs_berita where id_situs_berita > ".$last_id." AND status = 1)";
	$query = array();
	$result = mysqli_query($GLOBALS['mysqli'], $sql);
	while($row = mysqli_fetch_array($result)) {
		$query[] = $row; // Inside while loop
	}
	$curent_id = $query[0]['id_situs_berita'];
	
	if ($max_id == $curent_id) {
		$sql = "UPDATE t_link_berita_prev SET id_situs_berita=0 WHERE id_link_berita_prev=1";
		if (mysqli_query($GLOBALS['mysqli'], $sql)) {
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($GLOBALS['mysqli']);
		}
	} else {
		$sql = "UPDATE t_link_berita_prev SET id_situs_berita=".$curent_id." WHERE id_link_berita_prev=1";
		if (mysqli_query($GLOBALS['mysqli'], $sql)) {
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($GLOBALS['mysqli']);
		}
	}
	
	return $curent_id;
}

function get_berita($limit) {
	$sql = "SELECT * FROM m_berita WHERE has_normalize = 0 AND status = 1 ORDER BY id_berita DESC LIMIT ".$limit;
 	// $sql = "SELECT * FROM m_berita WHERE has_normalize = 0 AND status = 1 ORDER BY RAND() DESC LIMIT ".$limit;
	// $sql = "SELECT * FROM m_berita WHERE has_normalize = 0 AND status = 1 AND id_berita = 957 ORDER BY RAND() DESC LIMIT ".$limit;
	$query = array();
	$result = mysqli_query($GLOBALS['mysqli'], $sql);
	while($row = mysqli_fetch_array($result)) {
		$query[] = $row; // Inside while loop
	}
	return $query;
}

function get_berita_id($id_berita) {
	$sql = "SELECT * FROM m_berita WHERE id_berita = ".$id_berita." ORDER BY id_berita DESC ";
 	// $sql = "SELECT * FROM m_berita WHERE has_normalize = 0 AND status = 1 ORDER BY RAND() DESC LIMIT ".$limit;
	// $sql = "SELECT * FROM m_berita WHERE has_normalize = 0 AND status = 1 AND id_berita = 957 ORDER BY RAND() DESC LIMIT ".$limit;
	$query = array();
	$result = mysqli_query($GLOBALS['mysqli'], $sql);
	while($row = mysqli_fetch_array($result)) {
		$query[] = $row; // Inside while loop
	}
	return $query;
}

function get_lokasi() {
	global $arrLokasiProp, $arrLokasiKab, $arrLokasiKec, $arrLokasiKel;
	$query = array();
	
	$sql = "SELECT *, CONCAT(NAMA_PROP,',', OTHER_NAMA_PROP) AS FULL_NAMA FROM m_provinsi ORDER BY KODE ASC";
	$result_prov = mysqli_query($GLOBALS['mysqli'], $sql);
	
	$sql = "SELECT *, CONCAT(NAMA_KAB,',', OTHER_NAMA_KAB) AS FULL_NAMA FROM m_kabupaten_kota ORDER BY KODE ASC";
	$result_kota = mysqli_query($GLOBALS['mysqli'], $sql);
	
	$sql = "SELECT *, CONCAT(NAMA_KEC,',', OTHER_NAMA_KEC) AS FULL_NAMA FROM m_kecamatan ORDER BY KODE ASC";
	$result_kec = mysqli_query($GLOBALS['mysqli'], $sql);
	
	$sql = "SELECT *, CONCAT(NAMA_KEL,',', OTHER_NAMA_KEL) AS FULL_NAMA FROM m_kelurahan ORDER BY KODE ASC";
	$result_kel = mysqli_query($GLOBALS['mysqli'], $sql);

	while($row = mysqli_fetch_array($result_kel)){
	    $objRow = (object)[];
	    $noProp = $row['NO_PROP'];
	    $noKab = $row['NO_KAB'];
	    $noKec = $row['NO_KEC'];
		$noKel = $row['NO_KEL'];
		$objRow->kode = $row['KODE'];		
		$objRow->no_kel = (strlen($row['NO_KEL']) == 1)? "0".$row['NO_KEL']: $row['NO_KEL'];;		
		$objRow->nama_kel = (substr($row['FULL_NAMA'], -1) == ",")? substr($row['FULL_NAMA'], 0, -1) : $row['FULL_NAMA'];
		
		$arrLokasiKel[$noProp][$noKab][$noKec][$noKel] = $objRow;
	}
	
	while($row = mysqli_fetch_array($result_kec)){
	    $objRow = (object)[];
	    $noProp = $row['NO_PROP'];		
	    $noKab = $row['NO_KAB'];
		$noKec = $row['NO_KEC'];
		$objRow->kode = $row['KODE'];		
		$objRow->no_kec = (strlen($row['NO_KEC']) == 1)? "0".$row['NO_KEC']: $row['NO_KEC'];;		
		$objRow->nama_kec = (substr($row['FULL_NAMA'], -1) == ",")? substr($row['FULL_NAMA'], 0, -1) : $row['FULL_NAMA'];
		// $objRow->data_kel = $arrLokasiKel[$noProp][$noKab][$noKec];
		
		$arrLokasiKec[$noProp][$noKab][$noKec] = $objRow;
	}

	while($row = mysqli_fetch_array($result_kota)){
		$objRow = (object)[];
	    $noProp = $row['NO_PROP'];
		$noKab = $row['NO_KAB'];
		$objRow->kode = $row['KODE'];		
		$objRow->no_kab = (strlen($row['NO_KAB']) == 1)? "0".$row['NO_KAB']: $row['NO_KAB'];;		
		$objRow->nama_kab = (substr($row['FULL_NAMA'], -1) == ",")? substr($row['FULL_NAMA'], 0, -1) : $row['FULL_NAMA'];
		// $objRow->data_kec = $arrLokasiKec[$noProp][$noKab];
		// $noKab = (strlen($row['NO_KAB']) == 1)? "0".$row['NO_KAB']: $row['NO_KAB'];
		
		$arrLokasiKab[$noProp][$noKab] = $objRow;
	}

	while($row = mysqli_fetch_array($result_prov)) {
		$objRow = (object)[];
		$noProp = $row['NO_PROP'];
		$objRow->no_prop = $row['NO_PROP'];
		$objRow->kode = $row['KODE'];
		// $objRow->nama_prop = (substr($row['FULL_NAMA'], -1) == ",")? substr($row['FULL_NAMA'], 0, -1) : $row['FULL_NAMA'];
		$objRow->nama_prop = (substr($row['FULL_NAMA'], -1) == ",")? $row['FULL_NAMA'] : $row['FULL_NAMA'].',';
		// $objRow->data_kab = $arrLokasiKab[$noProp];
		
		$arrLokasiProp[$noProp] = $objRow;
	}
}

function migrasi_lokasi_2018(){
    // Prop 
    $sql = "SELECT * FROM wilayah_2018";
	$query = array();
    $result = mysqli_query($GLOBALS['mysqli'], $sql);
    
	$ins_sql_prop = array(); 
	$ins_sql_kota = array(); 
	$ins_sql_kec = array(); 
	$ins_sql_kel = array(); 
	while($row = mysqli_fetch_array($result)) {
	    $kode = explode(".", $row['kode']);
	    if(strlen($row['kode']) == 2){
	        $ins_sql_prop[] = '('.$kode[0].', "'.strtoupper($row['nama']).'","'.$row['kode'].'","","")';
	    }
	    
	    if(strlen($row['kode']) == 5){
	        $ins_sql_kota[] = '('.$kode[1].', "'.strtoupper(str_replace("KAB. ","",$row['nama'])).'","'.$row['kode'].'",'.$kode[0].',"","")';
		}
		
	    if(strlen($row['kode']) == 8){
	        $ins_sql_kec[] = '('.$kode[2].', "'.strtoupper($row['nama']).'","'.$row['kode'].'",'.$kode[1].','.$kode[0].',"","")';
		}
		
	    if(strlen($row['kode']) > 8){
	        $ins_sql_kel[] = '('.$kode[3].', "'.strtoupper($row['nama']).'","'.$row['kode'].'",'.$kode[2].','.$kode[1].','.$kode[0].',"","")';
	    }
	}
	// $sql_build[0] = "INSERT INTO `m_provinsi` (`NO_PROP`, `NAMA_PROP`, `KODE`, `LAT`, `LONG`) VALUES ".implode(',', $ins_sql_prop);
	$sql_build[1] = "INSERT INTO `m_kabupaten_kota` (`NO_KAB`, `NAMA_KAB`, `KODE`, `NO_PROP`, `LAT`, `LONG`) VALUES ".implode(',', $ins_sql_kota);
	// $sql_build[2] = "INSERT INTO `m_kecamatan` (`NO_KEC`, `NAMA_KEC`, `KODE`, `NO_KAB`, `NO_PROP`, `LAT`, `LONG`) VALUES ".implode(',', $ins_sql_kec);
	// $sql_build[3] = "INSERT INTO `m_kelurahan` (`NO_KEL`, `NAMA_KEL`, `KODE`, `NO_KEC`, `NO_KAB`,`NO_PROP`, `LAT`, `LONG`) VALUES ".implode(',', $ins_sql_kel);
	
	for($j=3;$j<4;$j++){
		if (isset($sql_build[$j])) {
			if (mysqli_query($GLOBALS['mysqli'],$sql_build[$j])) {
				echo "Query sql_build ".$j." done <hr>";
			} else {
				echo "Error: sql_build " . $j . "<br>" . mysqli_error($GLOBALS['mysqli']);
			}
		}
	}
	
}

function migrasi_stopword(){
    $filename = 'stopwords_id.txt';
    $fp = @fopen($filename, 'r'); 
    
    // Add each line to an array
    if ($fp) {
       $stopWord = explode("\n", fread($fp, filesize($filename)));
    }
	$ins_sql_stopword = array();
    for($i=0;$i < count($stopWord); $i++){
        $ins_sql_stopword[] = '("'.strtolower($stopWord[$i]).'","1")';
    }
	var_dump($ins_sql_stopword);
	die;
	$sql_build = "INSERT INTO `m_stopword` (`KATA`, `STATUS`) VALUES ".implode(',', $ins_sql_stopword);
	
	if (mysqli_query($GLOBALS['mysqli'],$sql_build)) {
		// echo "New record created successfully";
	} else {
	    echo "Error: " . $sql_build . "<br>" . mysqli_error($GLOBALS['mysqli']);
	}
	
}

function create_kata_dasar_file() {
	$myfile = fopen("kata_dasar_id.txt", "w") or die("Unable to open file!");

	$sql = "SELECT katadasar FROM tb_katadasar";
	$query = array();
	$result = mysqli_query($GLOBALS['mysqli'], $sql);
	while($row = mysqli_fetch_array($result)) {
		$txt = $row['katadasar']."\n";
		fwrite($myfile, $txt);
	}
	fclose($myfile);
}

function update_data_berita($data){
	$sql = "UPDATE m_berita SET isi_berita='".$data['isi_berita']."' ,lokasi_prop='".$data['lokasi_prop']."' ,lokasi_kab='".$data['lokasi_kab']."' , meta_data_kata='".$data['meta_data_kata']."', has_normalize='1' WHERE id_berita=".$data['id_berita'];
	// $sql = "UPDATE m_berita SET lokasi_prop='".$data['lokasi_prop']."' ,lokasi_kab='".$data['lokasi_kab']."' , meta_data_kata='".$data['meta_data_kata']."', has_normalize='1' WHERE id_berita=".$data['id_berita'];
	if (mysqli_query($GLOBALS['mysqli'], $sql)) {
		// echo "New record created successfully";
	} else {
	    echo "Error: " . $sql . "<br>" . mysqli_error($GLOBALS['mysqli']);
	}
}

function split_kalimat($data){
    $kalimat = explode(".",$data);
	$tmp_new_kalimat = array();
	for ($j=0; $j < count($kalimat); $j++) { 
		$kata = explode(" ",$kalimat[$j]);
		// Penyatuan 2 kalimat jika
		switch (true) {
			// Inisialisasi awal (special case)
			case ($j == 0):
				array_push($tmp_new_kalimat,$kalimat[$j]);
				break;
			// - Awalan kata berupa angka & akhir kata sebelumnya berupa angka
			case ( is_numeric(substr($kalimat[$j], 0, 1)) && is_numeric(substr($kalimat[$j-1], -1)) ):
				$lastKey = key(array_slice($tmp_new_kalimat, -1, 1, true));
				$tmp_new_kalimat[$lastKey] .= '.'.$kalimat[$j];
				break;
			// - Jumlah kata kurang dari 5 || jumlah kata sebelumnya kurang dari 5
			case ( count($kata) <= 5 || count(explode(" ",$kalimat[$j-1])) <= 5 ):
				$lastKey = key(array_slice($tmp_new_kalimat, -1, 1, true));
				$tmp_new_kalimat[$lastKey] .= '.'.$kalimat[$j];
				break;
			
			default:
				array_push($tmp_new_kalimat,$kalimat[$j]);
				break;
		}
	}
	return $tmp_new_kalimat;
}

function pencarian_lokasi($kalimat,$lokasi,$type,$parent_kode = null){
	$result = array();
	$kalimat = str_replace(" ","~",$kalimat);
	$tmp_kata = explode("~",preg_replace('/[^A-Za-z0-9\~]/', '', $kalimat));
	switch (strtolower($type)) {
		case 'prop':
			$tmp_nama_lokasi = array();
			foreach ($lokasi as $key => $val) {
				$objLokasi = (object)[];
				$nama_lokasi = explode(",",strtolower($val->nama_prop));
				// loop nama propinsi
				for ($i=0; $i < count($nama_lokasi); $i++) {
				    $start = 0;
				    if(strlen($nama_lokasi[$i]) != 0){
    					foreach ($tmp_kata as $keyKata => $valKata) {
							$tmpNama = $valKata;
							for ($a=1; $a <= count(explode(" ",$nama_lokasi[$i]))-1; $a++) { 
								if (isset($tmp_kata[$keyKata+$a])) { $tmpNama .= ' '.$tmp_kata[$keyKata+$a]; }
							}
							if($nama_lokasi[$i] == $tmpNama){
								$start = 1;
								$tmp_nama_lokasi[$keyKata] = $tmpNama;
							}
    					}
				    }
					if($start == 1){
					    $objLokasi->count = substr_count($kalimat,$nama_lokasi[$i]);
						$objLokasi->kode = $val->kode;
						$objLokasi->nama = strtolower(implode(",",$nama_lokasi));
						$objLokasi->order = $start;
						$result[$val->kode] = $objLokasi;
					}
				}
				// sorting berdasarkan key/urutan kemunculan pada berita
				ksort($tmp_nama_lokasi);
			}
			break;

		case 'kab':
			if ($parent_kode != null) {
				$parent_kode = json_decode($parent_kode);
				$kode_daerah = array();
				foreach ($parent_kode as $key => $value) {
					array_push($kode_daerah,$key);
				}
				$tmp_nama_lokasi = array();
				foreach ($lokasi as $key1 => $val1) {
					foreach ($val1 as $key2 => $val2) {
						$objLokasi = (object)[];
						$nama_lokasi = explode(",",strtolower($val2->nama_kab));
						// loop nama kabupaten/kota
						for ($i=0; $i < count($nama_lokasi); $i++) {
							$start = 0;
							if(strlen($nama_lokasi[$i]) != 0){
								foreach ($tmp_kata as $keyKata => $valKata) {
									$tmpNama = $valKata;
									for ($a=1; $a <= count(explode(" ",$nama_lokasi[$i]))-1; $a++) { 
										if (isset($tmp_kata[$keyKata+$a])) { $tmpNama .= ' '.$tmp_kata[$keyKata+$a]; }
									}
									if($nama_lokasi[$i] == $tmpNama){
										$start = ($start > 0)?$start:$keyKata;
										$tmp_nama_lokasi[$keyKata] = $tmpNama;
									}
								}
							}
							if($start > 0){
								$count = substr_count($kalimat,$nama_lokasi[$i]);
								$objLokasi->count = ($count == 0)? 1: $count;
								$objLokasi->kode = $val2->kode;
								$objLokasi->nama = strtolower(implode(",",$nama_lokasi));
								$objLokasi->order = $start;
								$result[$val2->kode] = $objLokasi;
							}
						}
						// sorting berdasarkan key/urutan kemunculan pada berita
						ksort($tmp_nama_lokasi);
					}
				}
			}
			break;
		
		case 'kec':
	
			break;

		case 'kel':
	
			break;
		
		default:
			die('Type kosong!');
			break;
	}
	return json_encode($result);
}

function del_prefix($masukan) {
	// awalan : ber, bel, be, ke, se, te, ter, me, mem, men, meng, menge, pe, pem, pen, peng, penge, pe, pel, per, memper; Sumber : http://tatabahasabm.tripod.com/tata/awalan.htm
	$masukan=strtolower($masukan);
	$AW1=substr($masukan,0,6);
	if($AW1=="memper") {
		$keluaran = substr($masukan,6,strlen($masukan));
	} else {
		$AW1=substr($masukan,0,3);
		if($AW1=="ber") {
			$keluaran = substr($masukan,3,strlen($masukan));
		} else {
			$AW1=substr($masukan,0,4);
			if($AW1=="bela") {
				$keluaran = substr($masukan,3,strlen($masukan));
			} else {
				$AW1=substr($masukan,0,2);
				if($AW1=="di" ) {
					$keluaran = substr($masukan,2,strlen($masukan));
				} else {
					$AW1=substr($masukan,0,2);
					if($AW1=="ke") {
						$keluaran = substr($masukan,2,strlen($masukan));
					} else {
						$AW1=substr($masukan,0,2);
						if($AW1=="ku") {
							$keluaran = substr($masukan,2,strlen($masukan));
						} else {
							$AW1=substr($masukan,0,3);
							if($AW1=="kau") {
								$keluaran = substr($masukan,3,strlen($masukan));
							} else {
								$AW1=substr($masukan,0,2);
								if($AW1=="me") {
									$AW1=substr($masukan,0,4);
									if($AW1=="memb") {
										$keluaran = substr($masukan,3,strlen($masukan));
									} else {
										$AW1=substr($masukan,0,4);
										if($AW1=="mend" || $AW1=="menf" || $AW1=="menj") {
											$keluaran = substr($masukan,3,strlen($masukan));
										} else {
											$AW1=substr($masukan,0,4);
											if($AW1=="meny") {
												$keluaran = "s".substr($masukan,4,strlen($masukan));
											} else {
												$AW1=substr($masukan,0,4);
												if($AW1=="meng") {
													if(substr($masukan,4,1)=="a" || substr($masukan,4,1)=="e" || substr($masukan,4,1)=="g" || substr($masukan,4,1)=="h" || substr($masukan,4,1)=="i" || substr($masukan,4,1)=="o" || substr($masukan,4,1)=="u") {
														$keluaran = substr($masukan,4,strlen($masukan));
													} else {
														$keluaran = $masukan;
													}
												} else {
													$AW1=substr($masukan,0,3);
													if($AW1=="men") {
														$keluaran = "t".substr($masukan,3,strlen($masukan));
													} else {
														if(substr($masukan,2,1)=="l" || substr($masukan,2,1)=="m" || substr($masukan,2,1)=="n" || substr($masukan,2,1)=="r") {
															$keluaran = substr($masukan,2,strlen($masukan));
														} else {
															$keluaran = $masukan;
														}
													}
												}
											}
										}
									}
								} else {
									$AW1=substr($masukan,0,2);
									if($AW1=="pe") {
										// tambahan thoyib
										if($AW1=="per"){
											$keluaran = substr($masukan,3,strlen($masukan));
										} else {
											$keluaran = substr($masukan,2,strlen($masukan));
										}
									} else {
										$AW1=substr($masukan,0,2);
										if($AW1=="se") {
											$keluaran = substr($masukan,2,strlen($masukan));
										} else {
											$AW1=substr($masukan,0,3);
											if($AW1=="ter") {
												$keluaran = substr($masukan,3,strlen($masukan));
											} else {
											/*
												$AW1=substr($masukan,0,3);
												if($AW1=="eka") {
													$keluaran = substr($masukan,3,strlen($masukan));
												} else {
													$AW1=substr($masukan,0,6);
													if($AW1=="ekstra") {
														$keluaran = substr($masukan,6,strlen($masukan));
													} else {
														$AW1=substr($masukan,0,3);
														if($AW1=="eks") {
															$keluaran = substr($masukan,3,strlen($masukan));
														} else {
															$AW1=substr($masukan,0,5);
															if($AW1=="intra") {
																$keluaran = substr($masukan,5,strlen($masukan));
															}
														}
													}
												}*/
												$keluaran = $masukan;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	return $keluaran;
}

function del_suffix($masukan) {
	$masukan=strtolower($masukan);
	$AK = substr($masukan,(strlen($masukan)-3),3);
	if($AK == "nda") {
		$keluaran = substr($masukan,0,(strlen($masukan)-3));
	} else {
		$AK = substr($masukan,(strlen($masukan)-3),3);
		if($AK == "nya") {
			$keluaran = substr($masukan,0,(strlen($masukan)-3));
		} else {
			$AK = substr($masukan,(strlen($masukan)-3),3);
			if($AK == "kan" && strlen($masukan)>5) {
				$keluaran = substr($masukan,0,(strlen($masukan)-3));
			} else {
				$AK = substr($masukan,(strlen($masukan)-2),2);
				if($AK == "an" && strlen($masukan)>5) {
					$keluaran = substr($masukan,0,(strlen($masukan)-2));
				} else {
					$AK = substr($masukan,(strlen($masukan)-2),2);
					if($AK == "ku" && strlen($masukan)>6) {
						$keluaran = substr($masukan,0,(strlen($masukan)-2));
					} else {
						$AK = substr($masukan,(strlen($masukan)-2),2);
						if($AK == "mu" && strlen($masukan)>6) {
							$keluaran = substr($masukan,0,(strlen($masukan)-2));
						} else {
							$AK = substr($masukan,(strlen($masukan)-1),1);
							if($AK == "i" && strlen($masukan)>6) {
								$keluaran = substr($masukan,0,(strlen($masukan)-1));
							} else {
								$keluaran = $masukan;
							}
						}
					}
				}
			}
		}
	}
	return $keluaran;
}

function doStemming($word) {
    global $katadasar;
	//  pengecekan apakah kata sudah termasuk kata dasar, jika belum lanjutkan proses steamming
	if (in_array($word,$katadasar)) {
		$s2 = $word;
	} else {
		$p1 = del_prefix($word);
		if (in_array($p1,$katadasar)) {
			$s2 = $p1;
		} else {
			$p2 = del_prefix($p1);
			if (in_array($p2,$katadasar)) {
				$s2 = $p2;
			} else {
				$p3 = del_prefix($p2);
				if (in_array($p3,$katadasar)) {
					$s2 = $p3;
				} else {
					$s1 = del_suffix($p3);
					if (in_array($s1,$katadasar)) {
						$s2 = $s1;
					} else {
						$s2 = del_suffix($s1);
					}
				}
			}
		}
	}
	return $s2;
}


// Get data berita
$data_berita = get_berita($limit_phase2);
if (isset($_GET['id_berita'])) {
	// var_dump("masuk sini");
	$data_berita = get_berita_id($_GET['id_berita']);
	$limit_phase2 = 1;
} else {
	$data_berita = get_berita($limit_phase2);
}

if(count($data_berita)){
	try {
		// Get data lokasi
		get_lokasi();

		$arrayDump = [""," "];

		$filename = __DIR__ . '/stopwords_id.txt';
		// $fp = @fopen($filename, 'r'); 
		$fp = @fopen($filename, 'c+'); 

		// Add each line to an array
		if ($fp) {
		$stopWord = explode("\n", fread($fp, filesize($filename)));
		}

		$filename2 = __DIR__ . '/kata_dasar_id.txt';
		// $fp2 = @fopen($filename2, 'r');
		$fp2 = @fopen($filename2, 'c+'); 

		// Add each line to an array
		if ($fp2) {
		$katadasar = explode("\n", fread($fp2, filesize($filename2)));
		}

		for($i=0; $i<$limit_phase2; $i++){
			$dataUpdate = array();

			// clean data berita
			$encodeKata = json_encode($data_berita[$i]["isi_berita"]);
			// $data_berita_clean = json_decode(str_replace(['\n','\t','\r','\u00a0','||','var ','var unruly =','window.unruly','{};','unruly','.unruly','.native','.siteid','googletag.cmd.push(function()','googletag.display','$(document).ready(function(){','appendto','function(','push(','window','adsbygoogle','google_ad_client','data:text/javascript','document','document.cookie','document.write','jquery','noconflict();','jquery.noconflict();','iframe'],"",$encodeKata));
			$exeption_string = ['\n','\t','\r','\u00a0','||','var ','var unruly =','window.unruly','{};','unruly','.unruly','.native','.siteid','googletag.cmd.push(function()','googletag.display','$(document).ready(function(){','appendto','function(','push(','window','adsbygoogle','google_ad_client','data:text/javascript','document','document.cookie','document.write','jquery','noconflict();','jquery.noconflict();','iframe'];
			$data_berita_clean = json_decode(str_replace($exeption_string,"",$encodeKata));
			$data_berita_clean = explode(" ",$data_berita_clean);
			$data_berita_clean = array_diff($data_berita_clean,$arrayDump);	
			$data_berita_clean = implode(" ",$data_berita_clean);

			// pecah data jadi kalimat 
			$new_kalimat = split_kalimat($data_berita_clean);
			
			// pecah data kalimat menjadi per kata tiap kalimat
			$meta_data_berita = (object)[];
			$new_kata = array();
			$tokenize = array();
			for($k=0; $k < count($new_kalimat); $k++){
				$new_kalimat[$k] .= ".";
				str_replace(" ","",$new_kalimat[$k]);
				$tmp_new_kata = explode(" ",$new_kalimat[$k]);
				
				// stopword
				$kata_stopword=array_intersect($stopWord,$tmp_new_kata);					
				if ($isDebug) {
					var_dump("<br>kata stopword : <hr>",$kata_stopword);
					var_dump("<hr>");
					var_dump("arr diff : <hr>",array_diff($tmp_new_kata,$kata_stopword));
				}
				foreach(array_diff($tmp_new_kata,$kata_stopword) as $val){
					array_push($new_kata,$val);
					// tokenize dari data setelah stopword dengan stemming
					$tmp = preg_replace('/[^A-Za-z0-9]/', '', $val);
					$hasil_stemming = "";
					if (strlen($tmp) > 2) {
						$hasil_stemming = doStemming($tmp);
					// } else {
						// $hasil_stemming = $tmp;
					}
					if ($hasil_stemming !== "" ) {
						array_push($tokenize,$hasil_stemming);
					}
				}
			}
			$new_kalimat_after = implode(" ",$new_kata);

			// pencarian lokasi (nama lokasi yang pertama kali ditemukan lebih dominan, pencarian berdasarkan tiap kata *setelah stopword* terhadap data lokasi *prov, kota, kec, kel*)
			$lokasi_prop = pencarian_lokasi($new_kalimat_after,$arrLokasiProp,"prop");
			$lokasi_kab = pencarian_lokasi($new_kalimat_after,$arrLokasiKab,"kab",$lokasi_prop);

			// pecah data menjadi per kalimat -> kata
			// tokenize dari data setelah stopword
			// hitung kemunculan tiap kata
			$meta_data_kata = array();
			foreach(array_count_values($tokenize) as $keyKata => $valKata){
				$tmpObj = (object)[];
				$tmpObj->kata = $keyKata;
				$tmpObj->count = $valKata;
				array_push($meta_data_kata,$tmpObj);
			}
			// sort nilai count desc
			usort($meta_data_kata, function($a, $b) {
				if($a->count == $b->count) return 0;
				return $a->count < $b->count?1:-1;
			});
			if ($isDebug) {
				throw new Exception("catche");
			}
			$dataUpdate['id_berita'] = $data_berita[$i]["id_berita"];
			$dataUpdate['isi_berita'] = htmlentities($data_berita_clean, ENT_QUOTES);
			// $dataUpdate['isi_berita'] = mysql_real_escape_string($data_berita_clean);

			$dataUpdate['lokasi_prop'] = $lokasi_prop;
			$dataUpdate['lokasi_kab'] = $lokasi_kab;
			$dataUpdate['meta_data_kata'] = json_encode($meta_data_kata);
			update_data_berita($dataUpdate);
			echo "<hr>".json_encode(array("success"=>true, "data"=>$dataUpdate));
		}	
	} catch (Exception $e) {
		die;
	}
} else {
	echo "belum ada berita terbaru.";
}
?>
