<?php
// include_once 'IDNstemmer.php';
// include_once 'cronjob/IDNstemmer.php';
include 'setting_cronjob.php';

$mysqli = mysqli_connect($databaseHost, $databaseUsername, $databasePassword, $databaseName);
date_default_timezone_set("Asia/Jakarta");

function getKataKunci() {
    $sql = "SELECT kata_kunci FROM m_kata_kunci WHERE status = 1";
	$query = array();
	$result = mysqli_query($GLOBALS['mysqli'], $sql);
	while($row = mysqli_fetch_array($result)) {
		$query[] = $row;
	}
	return $query;
}

function getBerita($days) {
    $sql = "SELECT id_berita, kata_kunci FROM m_berita WHERE DATE(cdd) in ('".implode("','",$days)."') AND status = 1 ORDER BY id_berita DESC";
	$query = array();
	$result = mysqli_query($GLOBALS['mysqli'], $sql);
	while($row = mysqli_fetch_array($result)) {
		$query[] = $row;
	}
	return $query;
}

function genDate($day) {
	$d = array();
	for($i = 0; $i < $day; $i++) {
		$newDate = date("Y-m-d", strtotime('-'. $i .' days'));
		$d[] = strval($newDate); 
	}
	return $d;
}

// Inisialisasi data
$limitHari = 3;
$listDate = genDate($limitHari);
$listKataKunci = getKataKunci();
$listBerita = getBerita($listDate);

$result = array();
foreach ($listKataKunci as $key => $val) {
	$result[$val['kata_kunci']] = 0;
}
$kata_kunci = "";
foreach ($listBerita as $key => $val) {
	$kata_kunci = $kata_kunci.$val['kata_kunci'].";";
}
$kata_kunci = str_replace("; ",";",$kata_kunci);
$arrDataKataKunci = array_filter(explode(";",$kata_kunci));
$arrKataKunci = array_count_values($arrDataKataKunci);
$newResult = array_merge($result,$arrKataKunci);

$kataKunciKosong = array();
foreach ($newResult as $key => $val) {
	if($val == 0){
		$kataKunciKosong[] = $key;
	}
}
// var_dump($kataKunciKosong,"<hr>");
// die;
// Send Mail

require("PHPMailer/PHPMailer.php");
require("PHPMailer/SMTP.php");

$mail = new PHPMailer\PHPMailer\PHPMailer();
$mail->CharSet = 'UTF-8';

$body = "Kata Kunci berikut <b>'".implode("','",$kataKunciKosong)."'</b> sudah ".$limitHari." hari terakhir tidak memperoleh data berita";

// Kirim email
$mail->IsSMTP();
$mail->Host       = $sender_host;

$mail->SMTPSecure = $sender_smtpsecure;
$mail->Port       = $sender_port;
$mail->SMTPDebug  = 1;
$mail->SMTPAuth   = true;

$mail->Username   = $sender_mail;
$mail->Password   = $sender_pass;
$mail->Subject    = 'Sistem report kata kunci : '.$date;

foreach ($recipient_mail as $key => $valMail) {
	$mail->SetFrom($sender_mail);
	$mail->MsgHTML($body);
	
	$mail->AddAddress($valMail);
	$mail->send();
}
?>