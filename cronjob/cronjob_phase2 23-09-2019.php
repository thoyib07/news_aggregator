<?php
global $link,$date,$id_situs_berita,$mysqli;
$date = date('Y-m-d');

$data_lokasi_prov = "";
$data_lokasi_kota = "";
$data_lokasi_kec = "";
$data_lokasi_kel = "";
// $id_situs_berita = $_GET['id_situs_berita'];

// $databaseHost = 'localhost';
// $databaseName = 'db_apl_news_aggre';
// $databaseUsername = 'root';
// $databasePassword = '';

$databaseHost = 'localhost';
$databaseName = 'u5594592_db_apl_news_aggre';
$databaseUsername = 'u5594592_user_news_aggre';
$databasePassword = '$_rGr?QW&mr^';

// $databaseHost = '172.16.10.164';
// $databaseName = 'db_testing_newsagregator';
// $databaseUsername = 'userdb_testing';
// $databasePassword = '2qEwbwNgLErZ5yJ752JB';


$mysqli = mysqli_connect($databaseHost, $databaseUsername, $databasePassword, $databaseName);
date_default_timezone_set("Asia/Jakarta");

// function new_koneksi() {
// 	$conn = new mysqli("localhost", "root", "", "db_apl_news_aggre");
// 	// Check connection
// 	if ($conn->connect_error) {
// 	    die("Connection failed: " . $conn->connect_error);
// 	}
// }
function link_berita_prev() {
	$sql = "SELECT id_situs_berita FROM m_situs_berita WHERE status = 1 ORDER BY id_situs_berita DESC LIMIT 1";
	$query = array();
	$result = mysqli_query($GLOBALS['mysqli'], $sql);
	while($row = mysqli_fetch_array($result)) {
		$query[] = $row; // Inside while loop
	}
	$max_id = $query[0]['id_situs_berita'];
	// var_dump("max_id : ",$max_id,"<hr>");

	$sql = "SELECT id_situs_berita FROM t_link_berita_prev LIMIT 1";
	$query = array();
	$result = mysqli_query($GLOBALS['mysqli'], $sql);
	while($row = mysqli_fetch_array($result)) {
		$query[] = $row; // Inside while loop
	}
	$last_id = $query[0]['id_situs_berita'];
	// var_dump("last_id : ",$last_id,"<hr>");
	
	$sql = "SELECT id_situs_berita FROM m_situs_berita WHERE id_situs_berita = (select min(id_situs_berita) from m_situs_berita where id_situs_berita > ".$last_id." AND status = 1)";
	// $sql = "SELECT id_situs_berita FROM m_situs_berita WHERE id_situs_berita = (select min(id_situs_berita) from m_situs_berita where id_situs_berita > 4 AND status = 1)";
	// $sql = "SELECT id_situs_berita FROM m_situs_berita WHERE status = 1 ORDER BY RAND() LIMIT 3";
	$query = array();
	$result = mysqli_query($GLOBALS['mysqli'], $sql);
	while($row = mysqli_fetch_array($result)) {
		$query[] = $row; // Inside while loop
	}
	$curent_id = $query[0]['id_situs_berita'];
	// var_dump("curent_id : ",$curent_id,"<hr>");
	
	if ($max_id == $curent_id) {
		$sql = "UPDATE t_link_berita_prev SET id_situs_berita=0 WHERE id_link_berita_prev=1";
		if (mysqli_query($GLOBALS['mysqli'], $sql)) {
			// echo "New record created successfully";
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($GLOBALS['mysqli']);
		}
	} else {
		$sql = "UPDATE t_link_berita_prev SET id_situs_berita=".$curent_id." WHERE id_link_berita_prev=1";
		if (mysqli_query($GLOBALS['mysqli'], $sql)) {
			// echo "New record created successfully";
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($GLOBALS['mysqli']);
		}
	}
	
	return $curent_id;
}

function get_berita($limit) {
// 	$sql = "SELECT * FROM m_berita WHERE has_normalize = 0 AND status = 1 ORDER BY id_berita DESC LIMIT ".$limit;
	$sql = "SELECT * FROM m_berita WHERE has_normalize = 0 AND status = 1 ORDER BY RAND() DESC LIMIT ".$limit;
	$query = array();
	$result = mysqli_query($GLOBALS['mysqli'], $sql);
	while($row = mysqli_fetch_array($result)) {
		$query[] = $row; // Inside while loop
	}
	return $query;
}

function get_lokasi() {
	$query = array();
	$arrLokasiProp = array();
	$arrLokasiKab = array();
	$arrLokasiKec = array();
	$arrLokasiKel = array();
	
	$sql = "SELECT *, CONCAT(NAMA_PROP,',', OTHER_NAMA_PROP) AS FULL_NAMA FROM m_provinsi ORDER BY KODE ASC";
	$result_prov = mysqli_query($GLOBALS['mysqli'], $sql);
	
	$sql = "SELECT *, CONCAT(NAMA_KAB,',', OTHER_NAMA_KAB) AS FULL_NAMA FROM m_kabupaten_kota ORDER BY KODE ASC";
	$result_kota = mysqli_query($GLOBALS['mysqli'], $sql);
	
	$sql = "SELECT *, CONCAT(NAMA_KEC,',', OTHER_NAMA_KEC) AS FULL_NAMA FROM m_kecamatan ORDER BY KODE ASC";
	$result_kec = mysqli_query($GLOBALS['mysqli'], $sql);
	
	$sql = "SELECT *, CONCAT(NAMA_KEL,',', OTHER_NAMA_KEL) AS FULL_NAMA FROM m_kelurahan ORDER BY KODE ASC";
	$result_kel = mysqli_query($GLOBALS['mysqli'], $sql);

	while($row = mysqli_fetch_array($result_kel)){
	    $objRow = (object)[];
	    $noProp = $row['NO_PROP'];
	    $noKab = $row['NO_KAB'];
	    $noKec = $row['NO_KEC'];
		$noKel = $row['NO_KEL'];
		$objRow->kode = $row['KODE'];		
		$objRow->no_kel = (strlen($row['NO_KEL']) == 1)? "0".$row['NO_KEL']: $row['NO_KEL'];;		
		$objRow->nama_kel = (substr($row['FULL_NAMA'], -1) == ",")? substr($row['FULL_NAMA'], 0, -1) : $row['FULL_NAMA'];
		
		$arrLokasiKel[$noProp][$noKab][$noKec][$noKel] = $objRow;
	}
	
	while($row = mysqli_fetch_array($result_kec)){
	    $objRow = (object)[];
	    $noProp = $row['NO_PROP'];		
	    $noKab = $row['NO_KAB'];
		$noKec = $row['NO_KEC'];
		$objRow->kode = $row['KODE'];		
		$objRow->no_kec = (strlen($row['NO_KEC']) == 1)? "0".$row['NO_KEC']: $row['NO_KEC'];;		
		$objRow->nama_kec = (substr($row['FULL_NAMA'], -1) == ",")? substr($row['FULL_NAMA'], 0, -1) : $row['FULL_NAMA'];
		$objRow->data_kel = $arrLokasiKel[$noProp][$noKab][$noKec];
		
		$arrLokasiKec[$noProp][$noKab][$noKec] = $objRow;
	}

	while($row = mysqli_fetch_array($result_kota)){
		$objRow = (object)[];
	    $noProp = $row['NO_PROP'];
		$noKab = $row['NO_KAB'];
		$objRow->kode = $row['KODE'];		
		$objRow->no_kab = (strlen($row['NO_KAB']) == 1)? "0".$row['NO_KAB']: $row['NO_KAB'];;		
		$objRow->nama_kab = (substr($row['FULL_NAMA'], -1) == ",")? substr($row['FULL_NAMA'], 0, -1) : $row['FULL_NAMA'];
		$objRow->data_kec = $arrLokasiKec[$noProp][$noKab];
		// $noKab = (strlen($row['NO_KAB']) == 1)? "0".$row['NO_KAB']: $row['NO_KAB'];
		
		$arrLokasiKab[$noProp][$noKab] = $objRow;
	}

	while($row = mysqli_fetch_array($result_prov)) {
		// var_dump($row);
		// die;
		$objRow = (object)[];
		$noProp = $row['NO_PROP'];
		$objRow->no_prop = $row['NO_PROP'];
		$objRow->kode = $row['KODE'];
// 		$objRow->nama_prop = (substr($row['FULL_NAMA'], -1) == ",")? substr($row['FULL_NAMA'], 0, -1) : $row['FULL_NAMA'];
		$objRow->nama_prop = (substr($row['FULL_NAMA'], -1) == ",")? $row['FULL_NAMA'] : $row['FULL_NAMA'].',';
		$objRow->data_kab = $arrLokasiKab[$noProp];
		
		$arrLokasiProp[$noProp] = $objRow;
	}
	$result = $arrLokasiProp;
	// var_dump("Prop : ",json_encode($arrLokasiProp),"<hr>Kota : ",json_encode($arrLokasiKota),"<hr>Kec : ",json_encode($arrLokasiKec),"<hr>Kel : ",json_encode($arrLokasiKel));
	// die;
	return $result;
}

function migrasi_lokasi_2018(){
    // Prop 
    $sql = "SELECT * FROM wilayah_2018";
	$query = array();
    $result = mysqli_query($GLOBALS['mysqli'], $sql);
    
	$ins_sql_prop = array(); 
	$ins_sql_kota = array(); 
	$ins_sql_kec = array(); 
	$ins_sql_kel = array(); 
	while($row = mysqli_fetch_array($result)) {
	    $kode = explode(".", $row['kode']);
	    if(strlen($row['kode']) == 2){
	        $ins_sql_prop[] = '('.$kode[0].', "'.strtoupper($row['nama']).'","'.$row['kode'].'","","")';
	    }
	    
	    if(strlen($row['kode']) == 5){
	        $ins_sql_kota[] = '('.$kode[1].', "'.strtoupper(str_replace("KAB. ","",$row['nama'])).'","'.$row['kode'].'",'.$kode[0].',"","")';
		}
		
	    if(strlen($row['kode']) == 8){
	        $ins_sql_kec[] = '('.$kode[2].', "'.strtoupper($row['nama']).'","'.$row['kode'].'",'.$kode[1].','.$kode[0].',"","")';
		}
		
	    if(strlen($row['kode']) > 8){
	        $ins_sql_kel[] = '('.$kode[3].', "'.strtoupper($row['nama']).'","'.$row['kode'].'",'.$kode[2].','.$kode[1].','.$kode[0].',"","")';
	    }
	}
	// 	var_dump($ins_sql_kel);
	// 	die;
	// $sql_build[0] = "INSERT INTO `m_provinsi` (`NO_PROP`, `NAMA_PROP`, `KODE`, `LAT`, `LONG`) VALUES ".implode(',', $ins_sql_prop);
	$sql_build[1] = "INSERT INTO `m_kabupaten_kota` (`NO_KAB`, `NAMA_KAB`, `KODE`, `NO_PROP`, `LAT`, `LONG`) VALUES ".implode(',', $ins_sql_kota);
	// $sql_build[2] = "INSERT INTO `m_kecamatan` (`NO_KEC`, `NAMA_KEC`, `KODE`, `NO_KAB`, `NO_PROP`, `LAT`, `LONG`) VALUES ".implode(',', $ins_sql_kec);
	// $sql_build[3] = "INSERT INTO `m_kelurahan` (`NO_KEL`, `NAMA_KEL`, `KODE`, `NO_KEC`, `NO_KAB`,`NO_PROP`, `LAT`, `LONG`) VALUES ".implode(',', $ins_sql_kel);
	
	for($j=3;$j<4;$j++){
		if (isset($sql_build[$j])) {
			if (mysqli_query($GLOBALS['mysqli'],$sql_build[$j])) {
				echo "Query sql_build ".$j." done <hr>";
			} else {
				echo "Error: sql_build " . $j . "<br>" . mysqli_error($GLOBALS['mysqli']);
			}
		}
	}
	
}

function migrasi_stopword(){
    $filename = 'stopwords_id.txt';
    $fp = @fopen($filename, 'r'); 
    
    // Add each line to an array
    if ($fp) {
       $stopWord = explode("\n", fread($fp, filesize($filename)));
    }
	$ins_sql_stopword = array();
    for($i=0;$i < count($stopWord); $i++){
        $ins_sql_stopword[] = '("'.strtolower($stopWord[$i]).'","1")';
    }
	var_dump($ins_sql_stopword);
	die;
	$sql_build = "INSERT INTO `m_stopword` (`KATA`, `STATUS`) VALUES ".implode(',', $ins_sql_stopword);
	
	if (mysqli_query($GLOBALS['mysqli'],$sql_build)) {
		// echo "New record created successfully";
	} else {
	    echo "Error: " . $sql_build . "<br>" . mysqli_error($GLOBALS['mysqli']);
	}
	
}

function cek_berita($link_berita,$judul_berita) {
	// var_dump($link_berita,$judul_berita);
	// die();

    $sql = "SELECT link_berita FROM m_berita WHERE link_berita = '".$link_berita."' OR judul_berita = '".$judul_berita."' ";
	$query = array();
	$result = mysqli_query($GLOBALS['mysqli'], $sql);
	// while($row = mysqli_fetch_array($result)) {
	// 	$query[] = $row; // Inside while loop
	// }
	$num_rows=mysqli_num_rows($result);
	// $result = mysql_free_result($query);
	// $row = $result->fetch_array();
	// $conn->close();
	// mysqli_close($GLOBALS['link']);
	return $num_rows;
}

function update_data_berita($data){
	// var_dump($row);
	// die();
	$sql = "UPDATE m_berita SET lokasi_prop='".$data['lokasi_prop']."' , meta_data_kalimat='".$data['meta_data_kalimat']."' WHERE id_berita=".$data['id_berita'];
	if (mysqli_query($GLOBALS['mysqli'], $sql)) {
		// echo "New record created successfully";
	} else {
	    echo "Error: " . $sql . "<br>" . mysqli_error($GLOBALS['mysqli']);
	}
}

function split_kalimat($data){
    $kalimat = explode(".",$data);
	// var_dump("<hr> kalimat : <hr>",$kalimat);
	$tmp_new_kalimat = array();
	for ($j=0; $j < count($kalimat); $j++) { 
		$kata = explode(" ",$kalimat[$j]);
		// Penyatuan 2 kalimat jika
		switch (true) {
			// Inisialisasi awal (special case)
			case ($j == 0):
				array_push($tmp_new_kalimat,$kalimat[$j]);
				break;
			// - Awalan kata berupa angka & akhir kata sebelumnya berupa angka
			case ( is_numeric(substr($kalimat[$j], 0, 1)) && is_numeric(substr($kalimat[$j-1], -1)) ):
				$lastKey = key(array_slice($tmp_new_kalimat, -1, 1, true));
				$tmp_new_kalimat[$lastKey] .= '.'.$kalimat[$j];
				break;
			// - Jumlah kata kurang dari 5 || jumlah kata sebelumnya kurang dari 5
			case ( count($kata) <= 5 || count(explode(" ",$kalimat[$j-1])) <= 5 ):
				$lastKey = key(array_slice($tmp_new_kalimat, -1, 1, true));
				$tmp_new_kalimat[$lastKey] .= '.'.$kalimat[$j];
				break;
			
			default:
				array_push($tmp_new_kalimat,$kalimat[$j]);
				break;
		}
		// var_dump("<hr> kalimat ke : <hr>",$kalimat[$j]);
		// var_dump("<hr> kata : <hr>",$kata);
	}
	return $tmp_new_kalimat;
}

function pencarian_lokasi($kalimat,$lokasi,$type,$kode = null){
	$result = array();
	$kalimat = str_replace(" ","~",$kalimat);
	$tmp_kata = explode("~",preg_replace('/[^A-Za-z0-9\~]/', '', $kalimat));
	var_dump("<hr>tmp_kata : <hr>",json_encode($tmp_kata));
	// die;
	switch (strtolower($type)) {
		case 'prop':
			// var_dump("<hr>data lokasi : <hr>",json_encode($lokasi[31]));
			$tmp_nama_prop = array();
			foreach ($lokasi as $key => $val) {
				$objLokasi = (object)[];
				// var_dump("<hr>key : ",$key," val : ",$val);
				$nama_prop = explode(",",strtolower($val->nama_prop));
				// var_dump("<hr>kode : ",$val->kode,"<br>nama_prop : ",$nama_prop);
				// loop nama propinsi
				for ($i=0; $i < count($nama_prop); $i++) {
				    $start = 0;
				    if(strlen($nama_prop[$i]) != 0){
    					foreach ($tmp_kata as $keyKata => $valKata) {
    					    switch(count(explode(" ",$nama_prop[$i]))){
    					        case 1:
    					            $tmpNama = $valKata;
            					    if($nama_prop[$i] == $valKata){
    					                $start = 1;
                					    $tmp_nama_prop[$keyKata] = $tmpNama;
            					    }
        					        break;
    					        case 2:
    					            if(isset($tmp_kata[$keyKata+1])){
    					                $tmpNama = $valKata.' '.$tmp_kata[$keyKata+1];
                					    if($nama_prop[$i] == $tmpNama){
    					                    $start = 1;
                    					    $tmp_nama_prop[$keyKata] = $tmpNama;
                					    }
    					            }
        					        break;
    					        case 3:
    					            if(isset($tmp_kata[$keyKata+2])){
    					                $tmpNama = $valKata.' '.$tmp_kata[$keyKata+1].' '.$tmp_kata[$keyKata+2];
                					    if($nama_prop[$i] == $tmpNama){
    					                    $start = 1;
                    					    $tmp_nama_prop[$keyKata] = $tmpNama;
                					    }
    					            }
        					        break;
    					        default:
    					            break;
    					    }
    					}
				    }
				    // var_dump("<hr> start ",$nama_prop[$i],' : ',$start);
					if($start == 1){
					    $objLokasi->count = substr_count($kalimat,$nama_prop[$i]);
						$objLokasi->kode = $val->kode;
						$result[$val->kode] = $objLokasi;
					}
				}
				ksort($tmp_nama_prop);
				// var_dump("<hr>kode : ",$val->kode,"<hr>nama_prop : ",$val->nama_prop);
			}
			var_dump("<hr>tmp_nama_prop : ",$tmp_nama_prop);
			var_dump("<hr>array_unique(tmp_nama_prop) : ",array_unique($tmp_nama_prop),"<hr>result : ",$result);
			die;
			break;

		case 'kab':
		
			break;
		
		case 'kec':
	
			break;

		case 'kel':
	
			break;
		
		default:
			die('Type kosong!');
			break;
	}
	return json_encode($result);
}

$num = 5;
// Get data berita
$data_berita = get_berita($num);
// migrasi_lokasi_2018();
// die;
// Get data lokasi
$data_lokasi = get_lokasi();
// var_dump("<hr>data lokasi : <hr>",json_encode($data_lokasi[31]));
// die;
$arrayDump = [""," "];

$filename = 'stopwords_id.txt';
$fp = @fopen($filename, 'r'); 

// Add each line to an array
if ($fp) {
   $stopWord = explode("\n", fread($fp, filesize($filename)));
}
// var_dump("<hr> stopWord : <hr>",$stopWord);
for($i=0; $i<$num; $i++){
    var_dump("<hr>data berita : <hr>",$data_berita[$i]["isi_berita"]);
	// die;
	// clean data berita
	$encodeKata = json_encode($data_berita[$i]["isi_berita"]);
	$data_berita = json_decode(str_replace(['\n','\t','\r','\u00a0','||','var unruly =','window.unruly','{};','unruly','.unruly','.native','.siteid','googletag.cmd.push(function()','googletag.display','$(document).ready(function(){','appendto','function(','push(','window','adsbygoogle','google_ad_client'],"",$encodeKata));
	$data_berita = explode(" ",$data_berita);
	$data_berita = array_diff($data_berita,$arrayDump);	
	// var_dump("<hr> data_berita after : <hr>",json_encode($data_berita));
	$data_berita = implode(" ",$data_berita);

	// pecah data jadi kalimat 
	$new_kalimat = split_kalimat($data_berita);
    // var_dump("<hr> new kalimat : <hr>",$new_kalimat);
    
	// pecah data kalimat menjadi per kata tiap kalimat
	$meta_data_berita = (object)[];
	$new_kata = array();
    for($k=0; $k < count($new_kalimat); $k++){
        // var_dump("<hr> new_kalimat : <hr>",$new_kalimat[$k]);
        // var_dump("<hr> new kata : <hr>",explode(" ",$new_kalimat[$k]));
        $new_kalimat[$k] .= ".";
        str_replace(" ","",$new_kalimat[$k]);
        $tmp_new_kata = explode(" ",$new_kalimat[$k]);
        
        // stopword
        $kata_stopword=array_intersect($stopWord,$tmp_new_kata);
        // var_dump("<hr> similar : <hr>",$kata_stopword);
        foreach(array_diff($tmp_new_kata,$kata_stopword) as $val){
            array_push($new_kata,$val);
        }
        // die;
	}
	$new_kalimat_after = implode(" ",$new_kata);
    var_dump("<hr> kalimat after stopword : <hr>",$new_kalimat_after);
    // var_dump("<hr> new_kata : <hr>",$new_kata);
    // pencarian lokasi (nama lokasi yang pertama kali ditemukan lebih dominan, pencarian berdasarkan tiap kata *setelah stopword* terhadap data lokasi *prov, kota, kec, kel*)
    $lokasi_prop = pencarian_lokasi($new_kalimat_after,$data_lokasi,"prop");
    die;
    // pecah data menjadi per kalimat -> kata
    // tokenize dari data setelah stopword
}
?>
