<h3 class="alert alert-info"><?php echo $title.'->Edit Kode'; ?></h3>
<form action="" method="post" enctype="multipart/form-data">
    <table class="table table-striped">
      <?php foreach ($result as $key): ?>
      <tr>
          <td>Kode</td>
          <td><input type="text" name="kode" value="<?php echo $key->kode; ?>" required></td>
      </tr>
      <tr>
          <td colspan="2"><input type="submit" class="btn btn-large btn-primary" name="submit" value="Save" onclick="javascript:checkInput()"/>
              <a href="<?php echo base_url(); ?>admin/kode/"><button type="button" name="button" class="btn btn-large btn-warning">Cancle</button></a></td>
      </tr>
      <?php endforeach; ?>
  </table>
</form>
