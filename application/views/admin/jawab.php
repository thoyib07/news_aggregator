<div class="container">

  <!-- Main component for a primary marketing message or call to action -->
  <div class="jumbotron">
    <div class="page-header">
        <!--<a href="<?php echo base_url().'admin/download/peserta'; ?>"><button type="button" name="button" class="btn btn-primary">Download Daftar Peserta</button></a>-->
    </div>
    <table class="table table-striped">
      <thead>
        <tr>
          <th>No</th>
          <th>Pertanyaan</th>
          <th>Jawaban</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($result as $key): ?>
          <tr>
            <td><?php echo $id = $id+1; ?></td>
            <td><?php echo $key->pertanyaan; ?></td>
            <td><?php echo $key->jawaban; ?></td>
            <td><a href="<?php echo base_url().'admin/edit_jawaban/'.$key->id; ?>"><button type="button" name="button" class="btn btn-info">Jawab</button></a></td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
    <?php echo $this->pagination->create_links();
      echo "<br>Total Data : ".$jumlah; ?>
  </div>

</div> <!-- /container -->
