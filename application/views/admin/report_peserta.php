<?php
  header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
  header("Content-Disposition: attachment; filename=Data-Peserta-NOVAVERSARY29.xls");  //File name extension was wrong
  header("Expires: 0");
  header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
  header("Cache-Control: private", false);
//*/
?>
<html>
  <head>
    <meta charset="utf-8">
    <title>NOVAVERSARY29</title>
  </head>
  <body>
    <table>
      <thead>
        <th>No</th>
        <th>Nama</th>
        <th>Kelamin</th>
        <th>Tempat Lahir</th>
        <th>Tgl Lahir</th>
        <th>Alamat</th>
        <th>Email</th>
        <th>Telpon</th>
        <th>Username</th>
      </thead>
      <tbody>
        <?php $id=0; foreach ($result as $r) :?>
        <tr>
          <td><?php echo $id = $id+1; ?></td>
          <td><?php echo $r->nama; ?></td>
          <td><?php if ($r->jk == '1') { echo "Laki-Laki"; } else { echo "Perempuan"; } ?></td>
          <td><?php echo $r->tempat_lahir; ?></td>
          <td><?php echo $r->tgl_lahir; ?></td>
          <td><?php echo $r->alamat; ?></td>
          <td><?php echo $r->email; ?></td>
          <td><?php echo "'".$r->telp; ?></td>
          <td><?php echo $r->username; ?></td>
        </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </body>
</html>
