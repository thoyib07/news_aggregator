<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php if (isset($title)) { echo $title; } ?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url().'assets/admin/'; ?>css/bootstrap.min.css" rel="stylesheet">

    <style media="screen">
        body { min-height: auto; padding-top: 70px; }
    </style>
  </head>

  <body>

    <?php include 'menu.php'; ?>

    <?php echo $content; ?>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url().'assets/admin/'; ?>js/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?php echo base_url().'assets/admin/'; ?>js/jquery.min.js"><\/script>')</script>
    <script src="<?php echo base_url().'assets/admin/'; ?>js/bootstrap.min.js"></script>
  </body>
</html>
