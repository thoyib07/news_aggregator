<section id="section-05" class="section-05">
  <div>
    <div class="container">
      <div class="text-center animated wow fadeIn">
        <p>Masukkan 2 kode digital* yang berbeda</p>
      </div>
      <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12 text-center animated wow fadeIn">
        <form action="<?php echo base_url().'home/cek_kode' ?>" method="post">
          <input type="text" name="kode1" required placeholder="kode 1">
          <input type="text" name="kode2" required placeholder="kode 2">
          <span class="note">*kode digital terdapat di Tabloid NOVA edisi 1513 – 1520</span>
          <button type="submit"><img src="<?php echo base_url().'assets/'; ?>images/btn-submit.png" alt="SUBMIT" class="img-responsive"></button>
        </form>
      </div>
    </div>
  </div>

  <div class="modal fade" id="myCode" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <div class="detail-img">
            <p>Kode yang anda masukan salah atau sudah pernah digunakan, silahkan coba kode yang lain.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
