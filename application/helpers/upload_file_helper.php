<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

function unggah_berkas($path, $nama_file, $type) {

	$CI = get_instance();
	$config['upload_path'] = $path;
	$config['allowed_types'] = $type;
	$config['max_size']	= '262144';	
	$config['form_file_name'] = $nama_file;
	$config['overwrite'] = TRUE;

	$new_name		= time();
	$real_name 		= str_replace([" "],"_",$_FILES[$config['form_file_name']]['name']);
	$new_nama_file 	= $new_name."-".$real_name;

	$config['file_name'] = $new_nama_file;
	// var_dump("<hr> Config Upload <br>",$config);
	$CI->upload->initialize($config);
	if ( ! $CI->upload->do_upload($nama_file)){
		$error = array('error' => $CI->upload->display_errors());
	} else {
		$data = array('upload_data' => $CI->upload->data());
	}
	return $config['file_name'];
}

/* End of file upload_file_helper.php */