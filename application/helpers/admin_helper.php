<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

function random_code($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function active_time() {
	// var_dump("<hr>Masuk Helper admin fungsi active_time");
	$timer = 60 * 60; //60 menit
	$CI = get_instance();
	// if ($CI->session->userdata('login') == 1) {
	 	//update_privilage();
	// }
	// $session = array('login' => true, 'time' => time(), 'id_user' => '1');
	// $CI->session->set_userdata($session);
	// var_dump("session di active_time : ",$_SESSION,"<hr>");
	// die;
	if ($CI->session->userdata('login') != 1) {
		redirect('auth/login');
	} elseif (time() - $CI->session->userdata('time') > $timer) {
		$session = array('login' => FALSE, 'time' => '');
		$CI->session->set_userdata($session);
		// redirect('admin/index');
		redirect('auth/login');
	}
}

function info_user() {
	$CI = get_instance();

	$user['select']		= "u.*";
	$user['table']		= "m_user as u";
	$user['join'][0]	= array('m_user_type as t', 't.id_user_type = u.id_user_type');
	$user['where'] 		= "u.status = 1 and u.id_user = '".$CI->session->userdata('id_user')."'";
	// $user['limit']		= "limit 2";
	$data['user'] 		= $CI->m_admin->getAll($user);
	// var_dump($data['user'] );
    //     exit;
	return $data['user'];
}

function info_menu() {
	$CI = get_instance();

	$menu['select']		= "m.id_menu, m.id_parent, m.name, m.name as nama, m.path, m.icon, m.has_child";
	$menu['table']		= "m_user_akses as ua";
	$menu['join'][0]	= array('m_menu as m', 'm.id_menu = ua.id_menu');
	$menu['join'][1]	= array('m_user_type as ut', 'ut.id_user_type = ua.id_user_type');
	$menu['join'][2]	= array('m_user as u', 'u.id_user_type = ut.id_user_type');
	$menu['where'] 		= "(ua.status = 1 and m.status = 1 and ut.status = 1) and u.id_user = '".$CI->session->userdata('id_user')."'";
	$data['menu'] 		= $CI->m_admin->getAll($menu);
	// var_dump($CI->db->last_query());
	// exit;
	$newMenu = array();
	foreach ($data['menu'] as $key => $value) {
		if (isset($newMenu[$value->id_parent])) {
			array_push($newMenu[$value->id_parent]->child,$value);
		} else {
			$newMenu[$value->id_menu] = $value;
			$newMenu[$value->id_menu]->child = [];
		}	
	}
	return $newMenu;
}

function get_user($username) {
	$CI = get_instance();
	$user['select']		= "u.*";
	$user['table']		= "m_user as u";
	// $user['where'] 		= "u.status = 1 and u.username = '".$username."'"; 
	$user['where'] 		= "u.status = 1 and u.nama = '".$username."'";
	$user['limit']		= "limit 1";
	$data['user'] 		= $CI->m_admin->getData($user);

	return $data['user'];
}

function has_password($username) {
	$CI = get_instance();
	$user['select']		= "u.*";
	$user['table']		= "m_user as u";
	// $user['where'] 		= "u.status = 1 and u.username = '".$username."'";
	$user['where'] 		= "u.status = 1 and u.nama = '".$username."'";
	$user['limit']		= "limit 1";
	$data['user'] 		= $CI->m_admin->getData($user);
	var_dump($data['user']);
	die();
	if (($data['user'][0]['password'] == "") || ($data['user'][0]['password'] == NULL)) {
		$allowed = false;
	} else {
		$allowed = true;
	}

	return $allowed;
}

function create_link($string) {
	$string = str_replace(' ', '_', $string); // Replaces all spaces with hyphens.
 
	return "detail_news/".preg_replace('/[^A-Za-z0-9\_]/', '', $string); // Removes special chars.
}

function send_email($setting){
	$CI = get_instance();
	// var_dump($data->detail[0]);
	// die();
	if (isset($setting->alias_from)) {
		$CI->email->from($setting->email_from, $setting->alias_from);
	} else {
		$CI->email->from($setting->email_from);
	}

	if (isset($setting->reply_to)) {
		$CI->email->reply_to($setting->email_from, $setting->alias_from);
	}
	
	$CI->email->to($setting->email_to);

	$CI->email->subject($setting->subject);
	$CI->email->message($setting->message);

	if ($CI->email->send(FALSE)) {
		return TRUE;
	} else {
		echo ($CI->email->print_debugger(array('header')));
		return FALSE;
	}
}