<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_admin extends CI_Model {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query($type=null,$id=null) {
        switch ($type) {
            case 'user':
            case 'del_user':
                $column_order = array('id_user', 'nama', 'email'); //set column field database for datatable orderable
                $column_search = array('nama', 'email'); //set column field database for datatable searchable
                $order = array('id_user' => 'desc'); // default order

                $this->db->select("u.*, l.role as jabatan");
                $this->db->from("m_user as u");
                $this->db->join("m_user_type as l","l.id_user_type = u.id_user_type");
                // $this->db->where("l.id_user_type != 1");
                break;

            case 'berita':
            case 'del_berita':
                $column_order = array('b.id_berita', 'b.judul_berita', 'b.publish_date', 's.nama_situs' ); //set column field database for datatable orderable
                $column_search = array('b.judul_berita', 'b.isi_berita', 'b.publish_date', 's.nama_situs'); //set column field database for datatable searchable
                $order = array('b.publish_date' => 'desc'); // default order

                $this->db->select("b.id_berita, b.judul_berita, b.link_berita, b.lokasi_prop, b.lokasi_kab, b.kata_kunci, b.publish_date, s.nama_situs");
                $this->db->from("m_berita as b");
                $this->db->join("m_situs_berita as s","b.id_situs_berita = s.id_situs_berita");
                break;

            case 'situs':
            case 'del_situs':
                $column_order = array('sb.id_situs_berita', 'sb.nama_situs'); //set column field database for datatable orderable
                $column_search = array( 'sb.nama_situs'); //set column field database for datatable searchable
                $order = array('sb.id_situs_berita' => 'desc'); // default order

                $this->db->select("sb.*");
                $this->db->from("m_situs_berita as sb");
                // $this->db->join("m_status_situs as ss","sb.id_status_situs = ss.id_status_situs");
                break;

            case 'media':
            case 'del_media':
                $column_order = array('m.id_media', 'm.nama_media'); //set column field database for datatable orderable
                $column_search = array('m.nama_media'); //set column field database for datatable searchable
                $order = array('m.id_media' => 'desc'); // default order

                $this->db->select("m.*");
                $this->db->from("m_media as m");
                break;

            case 'kata_kunci':
            case 'del_kata_kunci':
                $column_order = array('k.id_kata_kunci', 'k.kata_kunci'); //set column field database for datatable orderable
                $column_search = array('k.kata_kunci'); //set column field database for datatable searchable
                $order = array('k.id_kata_kunci' => 'desc'); // default order

                $this->db->select("k.*");
                $this->db->from("m_kata_kunci as k");
                break;

            case 'project':
            case 'del_project':
                $column_order = array('p.id_setting_project', 'p.nama_project'); //set column field database for datatable orderable
                $column_search = array('p.nama_project'); //set column field database for datatable searchable
                $order = array('p.id_setting_project' => 'desc'); // default order

                $this->db->select("p.*, p.nama_setting_project as nama_project");
                $this->db->from("m_setting_project as p");
                break;

            default:
                # code...
                break;
        }

        $i = 0;

        foreach ($column_search as $item) // loop column
        {
            if (isset($_POST['search'])) {
                if($_POST['search']['value']) // if datatable send POST for search
                {
                    if($i===0) // first loop
                    {
                        $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                        $this->db->like($item, $_POST['search']['value']);
                    }
                    else
                    {
                        $this->db->or_like($item, $_POST['search']['value']);
                    }

                    if(count($column_search) - 1 == $i) //last loop
                        $this->db->group_end(); //close bracket
                }
                $i++;
            }
        }

        if (!isset($order_req)) {
            if(isset($_POST['order'])) // here order processing
            {
                $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            }
            else if(isset($order))
            {
                foreach ($order as $key => $value) {
                    $this->db->order_by($key, $value);
                }
            }
        }
    }

    public function get_datatables($type=null,$id=null) {
        $this->_get_datatables_query($type);

        switch ($type) {
            case 'user': $this->db->where("u.status = 1"); break;
            case 'del_user': $this->db->where("u.status = 0"); break;
            case 'berita': $this->db->where("b.status = 1"); break;
            case 'del_berita': $this->db->where("b.status = 0"); break;
            case 'situs': $this->db->where("sb.status = 1"); break;
            case 'del_situs': $this->db->where("sb.status = 0"); break;
            case 'media': $this->db->where("m.status = 1"); break;
            case 'del_media': $this->db->where("m.status = 0"); break;
            case 'kata_kunci': $this->db->where("k.status = 1"); break;
            case 'del_kata_kunci': $this->db->where("k.status = 0"); break;
            case 'rss': $this->db->where("r.status = 1"); break;
            case 'del_rss': $this->db->where("r.status = 0"); break;
            case 'project': $this->db->where("p.status = 1"); break;
            case 'del_project': $this->db->where("p.status = 0"); break;

            default:
                # code...
                break;
        }

        if($_POST['length'] != -1){
        	$this->db->limit($_POST['length'], $_POST['start']);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function count_filtered($type=null,$id=null) {
        $this->_get_datatables_query($type);

        switch ($type) {
            case 'user': $this->db->where("u.status = 1"); break;
            case 'del_user': $this->db->where("u.status = 0"); break;
            case 'berita': $this->db->where("b.status = 1"); break;
            case 'del_berita': $this->db->where("b.status = 0"); break;
            case 'situs': $this->db->where("sb.status = 1"); break;
            case 'del_situs': $this->db->where("sb.status = 0"); break;
            case 'media': $this->db->where("m.status = 1"); break;
            case 'del_media': $this->db->where("m.status = 0"); break;
            case 'kata_kunci': $this->db->where("k.status = 1"); break;
            case 'del_kata_kunci': $this->db->where("k.status = 0"); break;
            case 'rss': $this->db->where("r.status = 1"); break;
            case 'del_rss': $this->db->where("r.status = 0"); break;
            case 'project': $this->db->where("p.status = 1"); break;
            case 'del_project': $this->db->where("p.status = 0"); break;

            default:
                # code...
                break;
        }

        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all($type=null,$id=null) {
        $this->_get_datatables_query($type);

        switch ($type) {
            case 'user': $this->db->where("u.status = 1"); break;
            case 'del_user': $this->db->where("u.status = 0"); break;
            case 'berita': $this->db->where("b.status = 1"); break;
            case 'del_berita': $this->db->where("b.status = 0"); break;
            case 'situs': $this->db->where("sb.status = 1"); break;
            case 'del_situs': $this->db->where("sb.status = 0"); break;
            case 'media': $this->db->where("m.status = 1"); break;
            case 'del_media': $this->db->where("m.status = 0"); break;
            case 'kata_kunci': $this->db->where("k.status = 1"); break;
            case 'del_kata_kunci': $this->db->where("k.status = 0"); break;
            case 'rss': $this->db->where("r.status = 1"); break;
            case 'del_rss': $this->db->where("r.status = 0"); break;
            case 'project': $this->db->where("p.status = 1"); break;
            case 'del_project': $this->db->where("p.status = 0"); break;

            default:
                # code...
                break;
        }

        return $this->db->count_all_results();
    }

    public function getData($value='') {
        $this->db->select($value['select']);

        if (isset($value['table'])) {
            $this->db->from($value['table']);
        } elseif (isset($value['from'])) {
            $this->db->from($value['from']);
        }

        if (isset($value['where'])) {
            $this->db->where($value['where']);
        }

        if (isset($value['join'])) {
            foreach ($value['join'] as $join) {
                $this->db->join($join['0'],$join['1']);
            }
        }

        if (isset($value['limit'])) {
            if (is_array($value['limit'])) {
                $this->db->limit($value['limit']['0'],$value['limit']['1']);
            } else {
                $this->db->limit($value['limit']);
            }
        }
        
        if (isset($value['group_by'])) {
            $this->db->group_by($value['group_by']);
        }
        
        if (isset($value['order'])) {
            $this->db->order_by($value['order']['0'],$value['order']['1']);
        }

        $result = $this->db->get()->result_array();
        return $result;
    }

    public function count_getData($value='') {
        $this->db->select($value['select']);

        if (isset($value['table'])) {
            $this->db->from($value['table']);
        } elseif (isset($value['from'])) {
            $this->db->from($value['from']);
        }

        if (isset($value['where'])) {
            $this->db->where($value['where']);
        }

        if (isset($value['join'])) {
            foreach ($value['join'] as $join) {
                $this->db->join($join['0'],$join['1']);
            }
        }

        if (isset($value['limit'])) {
                $this->db->limit($value['limit']['0'],$value['limit']['1']);
        }
        if (isset($value['order'])) {
            $this->db->order_by($value['order']['0'],$value['order']['1']);
        }

        $result = $this->db->get()->num_rows();
        return $result;
    }

    public function getAll($value) {
        $this->db->select($value['select']);
        $this->db->from($value['table']);

        if (isset($value['where'])) {
            $this->db->where($value['where']);
        }

        if (isset($value['join'])) {
            foreach ($value['join'] as $join) {
                $this->db->join($join['0'],$join['1']);
            }
        }

        if (isset($value['limit'])) {
            $this->db->limit($value['limit']);
        }       
        
        $result = $this->db->get()->result();
        return $result;
    }

    public function count_getAll($value) {
        $this->db->select($value['select']);
        $this->db->from($value['table']);

        if (isset($value['where'])) {
            $this->db->where($value['where']);
        }

        if (isset($value['join'])) {
            foreach ($value['join'] as $join) {
                $this->db->join($join['0'],$join['1']);
            }
        }

        if (isset($value['limit'])) {
            $this->db->limit($value['limit']);
        }       
        
        $result = $this->db->count_all_results();
        return $result;
    }
    
    public function getLimit($value) {
        //var_dump($value);die();
        $this->db->select($value['select']);
        $this->db->from($value['table']);
        $this->db->where($value['where']);
        $this->db->limit($value['limit']);
        $result = $this->db->get()->result();
        return $result;
    } 

    public function addData($value=null) {
        $this->db->insert($value['table'],$value['data']);
        $id = $this->db->insert_id();
        return $id;
    }

    public function delById($value) {
        $data = array('status' => '0',);

        if (isset($value['field'])) {
            $this->db->where($value['field'], $value['id']);
        } else {
            $this->db->where($value['search'], $value['id']);
        }        
        
        $this->db->update($value['table'], $data);
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE; 
    }

    public function restorById($value) {
        $data = array('status' => '1',);        

        if (isset($value['field'])) {
            $this->db->where($value['field'], $value['id']);
        } else {
            $this->db->where($value['search'], $value['id']);
        }

        $this->db->update($value['table'], $data);
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE; 
    }
    
    public function updateData($value) {
        if (isset($value['where'])) {
            foreach ($value['where'] as $where) {
                $this->db->where($where['0'],$where['1']);
            }
        }        
        $this->db->set($value['data']);
        $this->db->update($value['table']);
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE; 
    }

    public function getData_query($value) {
        $query = $this->db->query($value);

        return $query->result_array();

    }
}
