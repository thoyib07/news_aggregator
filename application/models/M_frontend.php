<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_frontend extends CI_Model {

    public function getData($value='') {
        $this->db->select($value['select']);

        if (isset($value['table'])) {
            $this->db->from($value['table']);
        } elseif (isset($value['from'])) {
            $this->db->from($value['from']);
        }

        if (isset($value['where'])) {
            $this->db->where($value['where']);
        }

        if (isset($value['join'])) {
            foreach ($value['join'] as $join) {
                $this->db->join($join['0'],$join['1']);
            }
        }
            
        if (isset($value['limit'])) {
                $this->db->limit($value['limit']['0'],$value['limit']['1']);
        }
        if (isset($value['order'])) {
            $this->db->order_by($value['order']);
        }
        
        $result = $this->db->get()->result_array();
        return $result;
    }

    public function count_getData($value='') {
        $this->db->select($value['select']);

        if (isset($value['table'])) {
            $this->db->from($value['table']);
        } elseif (isset($value['from'])) {
            $this->db->from($value['from']);
        }

        if (isset($value['where'])) {
            $this->db->where($value['where']);
        }

        if (isset($value['join'])) {
            foreach ($value['join'] as $join) {
                $this->db->join($join['0'],$join['1']);
            }
        }
            
        if (isset($value['limit'])) {
                $this->db->limit($value['limit']['0'],$value['limit']['1']);
        }
        if (isset($value['order'])) {
            $this->db->order_by($value['order']);
        }
        
        $result = $this->db->get()->num_rows();
        return $result;
    }
}