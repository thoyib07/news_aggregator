<head>
  <meta charset="utf-8">
  <title><?php echo TITLE; ?></title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta name="description" content="<?php echo @$metadata['description']; ?>">
  <meta name="keywords" content="<?php echo @$metadata['keywords']; ?>" />
  <meta name="title" itemprop="name" content="<?php echo @$metadata['title']; ?>" />
  <meta name="standout" content="<?php echo @$metadata['standout']; ?>"/>
  <meta name="copyright" content="<?php echo COPYRIGHT; ?>" /> 
  <meta name="author" content="<?php echo AUTHOR; ?>" />

  <meta name="robots" content="all" /> 
  <meta name="spiders" content="all" /> 
  <meta name="webcrawlers" content="all" />
  <meta name="language" content="Indonesia" />
  <meta name="revisit" content="1 days" /> 
  <meta name="distribution" content="global" /> 

  <meta name="rating" content="general" />
  <meta name="googlebot-news" content="index,follow" />
  <meta name="googlebot" content="all" />
  <meta name="MSSmartTagsPreventParsing" content="TRUE" />
  <meta name="generator" content="<?php echo base_url(); ?>" />
  <meta name="geo.position" content="-6.1780556, 106.63" />
  <meta name="geo.country" content="ID" />
  <meta name="geo.placename" content="Indonesia" />
  <meta content="index,follow" name="robots" />
  <meta name="theme-color" content="#1ab394">

  <link itemprop="mainEntityOfPage" href="<?php echo base_url(); ?>"/>
  <link rel="canonical" href="<?php echo base_url(); ?>" />

  <meta property="og:title" content="" /> 
  <meta property="og:url" content="<?php echo base_url(); ?>" /> 
  <!-- <meta property="og:image" content="assets/images/common/blank.jpg" /> -->
  <meta property="og:description" content="" /> 
  <meta property="fb:app_id"      content="816976845020258" /> 
  <meta property="og:type"        content="article" />

  <!-- Facebook Opengraph integration: https://developers.facebook.com/docs/sharing/opengraph -->
  <!-- <meta property="og:title" content="">
  <meta property="og:image" content="">
  <meta property="og:url" content="">
  <meta property="og:site_name" content="">
  <meta property="og:description" content=""> -->

  <!-- Twitter Cards integration: https://dev.twitter.com/cards/  -->
  <!-- <meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="">
  <meta name="twitter:title" content="">
  <meta name="twitter:description" content="">
  <meta name="twitter:image" content=""> -->
  <!-- Fav and touch icons -->
  <link rel="shortcut icon" href="<?php echo base_url('assets/img/'); ?>icons/icon.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url('assets/img/'); ?>icons/114x114.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url('assets/img/'); ?>icons/72x72.png">
  <link rel="apple-touch-icon-precomposed" href="<?php echo base_url('assets/img/'); ?>icons/icon.png">

  <?php include 'css.php'; ?>
</head>