<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900" rel="stylesheet">

<!-- sweetalert -->
<link rel="stylesheet" href="<?php echo base_url('assets/plugins/sweetalert/'); ?>sweetalert.css">

<!-- Bootstrap CSS File -->
<link href="<?php echo base_url('assets/css/'); ?>bootstrap.min.css" rel="stylesheet">

<!-- Libraries CSS Files -->
<link href="<?php echo base_url('assets/plugins/font-awesome/css/'); ?>font-awesome.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/css/'); ?>owl.carousel.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/css/'); ?>owl.theme.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/css/'); ?>owl.transitions.min.css" rel="stylesheet">

<!-- Main Stylesheet File -->
<link href="<?php echo base_url('assets/css/'); ?>style.css" rel="stylesheet">

<!--Your custom colour override - predefined colours are: colour-blue.css, colour-green.css, colour-lavander.css, orange is default-->
<link href="#" id="colour-scheme" rel="stylesheet">