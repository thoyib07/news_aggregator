<!-- Required JavaScript Libraries -->
<script src="<?php echo base_url('assets/js/'); ?>jquery.min.js"></script>
<script src="<?php echo base_url('assets/js/'); ?>bootstrap.min.js"></script>
<script src="<?php echo base_url('assets/js/'); ?>owl.carousel.min.js"></script>
<script src="<?php echo base_url('assets/js/'); ?>stellar.min.js"></script>
<script src="<?php echo base_url('assets/js/'); ?>waypoints.min.js"></script>
<script src="<?php echo base_url('assets/js/'); ?>counterup.min.js"></script>
<script src="<?php echo base_url('assets/js/'); ?>contactform.js"></script>

<!-- Template Specisifc Custom Javascript File -->
<script src="<?php echo base_url('assets/js/'); ?>custom.js"></script>

<!--Custom scripts demo background & colour switcher - OPTIONAL -->
<script src="<?php echo base_url('assets/js/'); ?>color-switcher.js"></script>

<!--Contactform script -->
<script src="<?php echo base_url('assets/js/'); ?>contactform.js"></script>

<!-- sweetalert -->
<script type="text/javascript" language="javascript" src="<?php echo base_url('assets/plugins/sweetalert/'); ?>sweetalert.min.js"></script>