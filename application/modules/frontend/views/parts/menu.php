<!-- ======== @Region: #navigation ======== -->
<div id="navigation" class="wrapper">
  <div class="header">
    <div class="header-inner container">
      <div class="row">
        <div class="col-md-8">
          <a class="navbar-brand" href="<?php echo base_url(); ?>" title="Home">
            <h1 class="hidden">
              <img src="<?php echo base_url('assets/img/'); ?>logo-tks.png" alt="Flexor Logo" style="background-size: cover;">
            </h1>
            <img src="<?php echo base_url('assets/img/'); ?>logo-tks-fb.png" alt="TKS Logo">
          </a>
        </div>
        <!--header rightside-->
        <div class="col-md-4">
          <ul class="list-inline user-menu pull-right">
            <li class="user-register"><i class="fa fa-edit text-primary "></i> <a href="<?php echo base_url('register/'); ?>" class="text-uppercase">Register</a></li>
            <li class="user-login"><i class="fa fa-sign-in text-primary"></i> <a href="<?php echo base_url('login/'); ?>" class="text-uppercase">Login</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="container">    
    <div class="navbar navbar-default">
      <!--mobile collapse menu button-->
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <!--social media icons-->
      <div class="navbar-text social-media social-media-inline pull-right">
        <a href="https://www.facebook.com/tksnakertrans" target="_blank"><i class="fa fa-twitter"></i></a>
        <a href="http://twitter.com/TKSNakertrans" target="_blank"><i class="fa fa-facebook"></i></a>
      </div>
      <!--everything within this div is collapsed on mobile-->
      <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav" id="main-menu">
          <li class="icon-link">
            <a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i></a>
          </li>
          <li><a href="<?php echo base_url('profil_tks/'); ?>">Profil TKS</a></li>
          <li><a href="<?php echo base_url('berita/'); ?>">Berita TKS</a></li>
          <li><a href="<?php echo base_url('data_tks/'); ?>">Data TKS</a></li>
          <li><a href="<?php echo base_url('tks_purna/'); ?>">TKS Purna</a></li>
          <li><a href="<?php echo base_url('ebook/'); ?>">E-Book</a></li>
          <li><a href="<?php echo base_url('galeri/'); ?>">Gallery</a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Forum TKS<b class="caret"></b></a>
            <!-- Dropdown Menu -->
            <ul class="dropdown-menu">
              <li class="dropdown-header">Forum TKS</li>
              <li><a href="#" tabindex="-1" class="menu-item">Kemitraan</a></li>
              <li><a href="#" tabindex="-1" class="menu-item">Wirausaha Mandiri</a></li>
              <li><a href="#" tabindex="-1" class="menu-item">Pemasaran (e-merchant)</a></li>
              <li><a href="#" tabindex="-1" class="menu-item">Teknik Produksi</a></li>
              <!-- <li class="dropdown-footer">Dropdown footer</li> -->
            </ul>
          </li>
        </ul>
      </div>
      <!--/.navbar-collapse -->
    </div>
  </div>
</div>