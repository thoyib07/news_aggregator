<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta name="description" content="<?php echo @$metadata['description']; ?>">
	<meta name="keywords" content="<?php echo @$metadata['keywords']; ?>" />
	<meta name="title" itemprop="name" content="<?php echo @$metadata['title']; ?>" />
	<meta name="standout" content="<?php echo @$metadata['standout']; ?>"/>
	<meta name="copyright" content="<?php echo COPYRIGHT; ?>" /> 
  <meta name="author" content="<?php echo AUTHOR; ?>" />

  <meta name="robots" content="all" /> 
  <meta name="spiders" content="all" /> 
  <meta name="webcrawlers" content="all" />
  <meta name="language" content="Indonesia" />
  <meta name="revisit" content="1 days" /> 
  <meta name="distribution" content="global" /> 
	
	<meta name="rating" content="general" />
	<meta name="googlebot-news" content="index,follow" />
	<meta name="googlebot" content="all" />
	<meta name="MSSmartTagsPreventParsing" content="TRUE" />
	<meta name="generator" content="<?php echo base_url(); ?>" />
	<meta name="geo.position" content="-6.1780556, 106.63" />
	<meta name="geo.country" content="ID" />
	<meta name="geo.placename" content="Indonesia" />
	<meta content="index,follow" name="robots" />
	<meta name="theme-color" content="#1ab394">

	<link itemprop="mainEntityOfPage" href="<?php echo base_url(); ?>"/>
	<link rel="canonical" href="<?php echo base_url(); ?>" />

  <meta property="og:title" content="" /> 
  <meta property="og:url" content="<?php echo base_url(); ?>" /> 
  <!-- <meta property="og:image" content="assets/images/common/blank.jpg" /> -->
  <meta property="og:description" content="" /> 
  <meta property="fb:app_id"      content="816976845020258" /> 
  <meta property="og:type"        content="article" />
  
 
  <!-- Add Your favicon here -->
  <!--<link rel="icon" href="<?php echo base_url();?>assets/img/favicon.ico">-->

  <title><?php echo (!@$metadata['title']) ? 'WEBSITE TKS' : @$metadata['title']; ?></title>
  <!-- <link rel="shortcut icon" href="<?php echo base_url('assets/img/'); ?>favicon.ico" type="image/x-icon" /> -->
  <!-- Fav and touch icons -->
  <link rel="shortcut icon" href="<?php echo base_url();?>assets/img/icons/icon.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url('assets/img/'); ?>icons/114x114.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url('assets/img/'); ?>icons/72x72.png">
  <link rel="apple-touch-icon-precomposed" href="<?php echo base_url('assets/img/'); ?>icons/icon.png">

  <?php include 'parts/css.php'; ?>
  
  <!-- <script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-20936964-7']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
  </script> -->
    
</head>
<body class="page-index has-hero">
  <!--Change the background class to alter background image, options are: benches, boots, buildings, city, metro -->
  <div id="background-wrapper" class="bromo" data-stellar-background-ratio="0.1">

    <!-- ======== @Region: #navigation ======== -->
    <div id="navigation" class="wrapper">
      <div class="header">
        <div class="header-inner container">
          <div class="row">
            <div class="col-md-8">
              <!--navbar-branding/logo - hidden image tag & site name so things like Facebook to pick up, actual logo set via CSS for flexibility -->
              <a class="navbar-brand" href="index.html" title="Home">
                <h1 class="hidden">
                  <img src="<?php echo base_url();?>assets/img/logo-tks.png" alt="Flexor Logo" style="background-size: cover;">
                </h1>
                <img src="<?php echo base_url();?>assets/img/logo-tks-fb.png" alt="TKS Logo">
              </a>
            </div>
            <!--header rightside-->
            <div class="col-md-4">
              <ul class="list-inline user-menu pull-right">
                <li class="user-register"><i class="fa fa-edit text-primary "></i> <a href="register.html" class="text-uppercase">Register</a></li>
                <li class="user-login"><i class="fa fa-sign-in text-primary"></i> <a href="login.html" class="text-uppercase">Login</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="navbar navbar-default">
          <!--mobile collapse menu button-->
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          <!--social media icons-->
          <div class="navbar-text social-media social-media-inline pull-right">
            <a href="https://www.facebook.com/tksnakertrans" target="_blank"><i class="fa fa-twitter"></i></a>
            <a href="http://twitter.com/TKSNakertrans" target="_blank"><i class="fa fa-facebook"></i></a>
          </div>
          <!--everything within this div is collapsed on mobile-->
          <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav" id="main-menu">
              <li class="icon-link">
                <a href="index.html"><i class="fa fa-home"></i></a>
              </li>
              <li><a href="#">Profil TKS</a></li>
              <li><a href="#">Berita TKS</a></li>
              <li><a href="#">Data TKS</a></li>
              <li><a href="#">TKS Purna</a></li>
              <li><a href="#">E-Book</a></li>
              <li><a href="#">Gallery</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Forum TKS<b class="caret"></b></a>
                <!-- Dropdown Menu -->
                <ul class="dropdown-menu">
                  <li class="dropdown-header">Forum TKS</li>
                  <li><a href="#" tabindex="-1" class="menu-item">Kemitraan</a></li>
                  <li><a href="#" tabindex="-1" class="menu-item">Wirausaha Mandiri</a></li>
                  <li><a href="#" tabindex="-1" class="menu-item">Pemasaran (e-merchant)</a></li>
                  <li><a href="#" tabindex="-1" class="menu-item">Teknik Produksi</a></li>
                  <!-- <li class="dropdown-footer">Dropdown footer</li> -->
                </ul>
              </li>
            </ul>
          </div>
          <!--/.navbar-collapse -->
        </div>
      </div>
    </div>
    <div class="hero" id="highlighted">
      <div class="inner">
        <!--Slideshow-->
        <div id="highlighted-slider" class="container">
          <div class="item-slider" data-toggle="owlcarousel" data-owlcarousel-settings='{"singleItem":true, "navigation":true, "transitionStyle":"fadeUp"}'>
            <!--Slideshow content-->
            <!--Slide 1-->
            <div class="item">
              <div class="row">
                <div class="col-md-6 col-md-push-6 item-caption">
                  <h2 class="h1 text-weight-light">
                    Selamat datang di <span class="text-primary">TKSkemnakerRI</span>
                  </h2>
                  <h4>
                    TKSkemnakerRI - Website informasi dan berita program Pendayagunaan Tenaga Kerja Sarjana dari Kementerian Ketenagakerjaan Republik Indonesia.
                  </h4>
                </div>
                <div class="col-md-6 col-md-pull-6 hidden-xs">
                  <img src="<?php echo base_url();?>assets/img/logo-tks.png" alt="Slide 1" class="center-block img-responsive">
                </div>
              </div>
            </div>
            <!--Slide 2-->
            <div class="item">
              <div class="row">
                <div class="col-md-6 text-right-md item-caption">
                  <h2 class="h1 text-weight-light">
                      <span class="text-primary">TKSkemnakerRI</span> Kementerian Ketenagakerjaan Republik Indonesia
                    </h2>
                  <h4>
                      TKSkemnakerRI - Website informasi dan berita program Pendayagunaan Tenaga Kerja Sarjana dari Kementerian Ketenagakerjaan Republik Indonesia.
                    </h4>
                </div>
                <div class="col-md-6 hidden-xs">
                  <img src="<?php echo base_url();?>assets/img/logo-tks-fb.png" alt="Slide 2" class="center-block img-responsive">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- ======== @Region: #content ======== -->
  <div id="content">
    <!-- Mission Statement -->
    <div class="mission text-center block block-pd-sm block-bg-noise">
      <div class="container">
        <h2 class="text-shadow-white">
          Program Pendayagunaan Tenaga Kerja Sarjana (TKS) adalah salah satu program prioritas Kemenakertrans RI untuk mendayagunakan para sarjana dalam pemberdayaan masyarakat, khususnya pendampingan kepada kelompok-kelompok masyarakat di pedesaan<br>
          <a href="about.html" class="btn btn-more"><i class="fa fa-plus"></i>Baca Selengkapnya</a>
        </h2>
      </div>
    </div>
    <!--Showcase-->
    <div class="showcase block block-border-bottom-grey">
      <div class="container">
        <h2 class="block-title">
            Berita TKS
          </h2>
        <p>Berita TKS terbaru.</p>
        <div class="item-carousel" data-toggle="owlcarousel" data-owlcarousel-settings='{"items":4, "pagination":false, "navigation":true, "itemsScaleUp":true}'>
          <div class="item">
            <a href="#" class="overlay-wrapper">
                <img src="<?php echo base_url();?>assets/img/showcase/project1.png" alt="Berita 1 image" class="img-responsive underlay">
                <span class="overlay">
                  <span class="overlay-content"> <span class="h4">Berita 1</span> </span>
                </span>
              </a>
            <div class="item-details bg-noise">
              <h4 class="item-title">
                  <a href="#">Berita 1</a>
                </h4>
              <a href="#" class="btn btn-more"><i class="fa fa-plus"></i>Baca selengkapnya</a>
            </div>
          </div>
          <div class="item">
            <a href="#" class="overlay-wrapper">
                <img src="<?php echo base_url();?>assets/img/showcase/project2.png" alt="Berita 2 image" class="img-responsive underlay">
                <span class="overlay">
                  <span class="overlay-content"> <span class="h4">Berita 2</span> </span>
                </span>
              </a>
            <div class="item-details bg-noise">
              <h4 class="item-title">
                  <a href="#">Berita 2</a>
                </h4>
              <a href="#" class="btn btn-more"><i class="fa fa-plus"></i>Baca selengkapnya</a>
            </div>
          </div>
          <div class="item">
            <a href="#" class="overlay-wrapper">
                <img src="<?php echo base_url();?>assets/img/showcase/project3.png" alt="Berita 3 image" class="img-responsive underlay">
                <span class="overlay">
                  <span class="overlay-content"> <span class="h4">Berita 3</span> </span>
                </span>
              </a>
            <div class="item-details bg-noise">
              <h4 class="item-title">
                  <a href="#">Berita 3</a>
                </h4>
              <a href="#" class="btn btn-more"><i class="fa fa-plus"></i>Baca selengkapnya</a>
            </div>
          </div>
          <div class="item">
            <a href="#" class="overlay-wrapper">
                <img src="<?php echo base_url();?>assets/img/showcase/project4.png" alt="Berita 4 image" class="img-responsive underlay">
                <span class="overlay">
                  <span class="overlay-content"> <span class="h4">Berita 4</span> </span>
                </span>
              </a>
            <div class="item-details bg-noise">
              <h4 class="item-title">
                  <a href="#">Berita 4</a>
                </h4>
              <a href="#" class="btn btn-more"><i class="fa fa-plus"></i>Baca selengkapnya</a>
            </div>
          </div>
          <div class="item">
            <a href="#" class="overlay-wrapper">
                <img src="<?php echo base_url();?>assets/img/showcase/project5.png" alt="Berita 5 image" class="img-responsive underlay">
                <span class="overlay">
                  <span class="overlay-content"> <span class="h4">Berita 5</span> </span>
                </span>
              </a>
            <div class="item-details bg-noise">
              <h4 class="item-title">
                  <a href="#">Berita 5</a>
                </h4>
              <a href="#" class="btn btn-more"><i class="fa fa-plus"></i>Baca selengkapnya</a>
            </div>
          </div>
          <div class="item">
            <a href="#" class="overlay-wrapper">
                <img src="<?php echo base_url();?>assets/img/showcase/project6.png" alt="Berita 6 image" class="img-responsive underlay">
                <span class="overlay">
                  <span class="overlay-content"> <span class="h4">Berita 6</span> </span>
                </span>
              </a>
            <div class="item-details bg-noise">
              <h4 class="item-title">
                  <a href="#">Berita 6</a>
                </h4>
              <a href="#" class="btn btn-more"><i class="fa fa-plus"></i>Baca selengkapnya</a>
            </div>
          </div>
          <div class="item">
            <a href="#" class="overlay-wrapper">
                <img src="<?php echo base_url();?>assets/img/showcase/project7.png" alt="Berita 7 image" class="img-responsive underlay">
                <span class="overlay">
                  <span class="overlay-content"> <span class="h4">Berita 7</span> </span>
                </span>
              </a>
            <div class="item-details bg-noise">
              <h4 class="item-title">
                  <a href="#">Berita 7</a>
                </h4>
              <a href="#" class="btn btn-more"><i class="fa fa-plus"></i>Baca selengkapnya</a>
            </div>
          </div>
          <div class="item">
            <a href="#" class="overlay-wrapper">
                <img src="<?php echo base_url();?>assets/img/showcase/project8.png" alt="Berita 8 image" class="img-responsive underlay">
                <span class="overlay">
                  <span class="overlay-content"> <span class="h4">Berita 8</span> </span>
                </span>
              </a>
            <div class="item-details bg-noise">
              <h4 class="item-title">
                  <a href="#">Berita 8</a>
                </h4>
              <a href="#" class="btn btn-more"><i class="fa fa-plus"></i>Baca selengkapnya</a>
            </div>
          </div>
          <div class="item">
            <a href="#" class="overlay-wrapper">
                <img src="<?php echo base_url();?>assets/img/showcase/project9.png" alt="Berita 9 image" class="img-responsive underlay">
                <span class="overlay">
                  <span class="overlay-content"> <span class="h4">Berita 9</span> </span>
                </span>
              </a>
            <div class="item-details bg-noise">
              <h4 class="item-title">
                  <a href="#">Berita 9</a>
                </h4>
              <a href="#" class="btn btn-more"><i class="fa fa-plus"></i>Baca selengkapnya</a>
            </div>
          </div>
          <div class="item">
            <a href="#" class="overlay-wrapper">
                <img src="<?php echo base_url();?>assets/img/showcase/project10.png" alt="Berita 10 image" class="img-responsive underlay">
                <span class="overlay">
                  <span class="overlay-content"> <span class="h4">Berita 10</span> </span>
                </span>
              </a>
            <div class="item-details bg-noise">
              <h4 class="item-title">
                  <a href="#">Berita 10</a>
                </h4>
              <a href="#" class="btn btn-more"><i class="fa fa-plus"></i>Baca selengkapnya</a>
            </div>
          </div>
          <div class="item">
            <a href="#" class="overlay-wrapper">
                <img src="<?php echo base_url();?>assets/img/showcase/project11.png" alt="Berita 11 image" class="img-responsive underlay">
                <span class="overlay">
                  <span class="overlay-content"> <span class="h4">Berita 11</span> </span>
                </span>
              </a>
            <div class="item-details bg-noise">
              <h4 class="item-title">
                  <a href="#">Berita 11</a>
                </h4>
              <a href="#" class="btn btn-more"><i class="fa fa-plus"></i>Baca selengkapnya</a>
            </div>
          </div>
          <div class="item">
            <a href="#" class="overlay-wrapper">
                <img src="<?php echo base_url();?>assets/img/showcase/project12.png" alt="Berita 12 image" class="img-responsive underlay">
                <span class="overlay">
                  <span class="overlay-content"> <span class="h4">Berita 12</span> </span>
                </span>
              </a>
            <div class="item-details bg-noise">
              <h4 class="item-title">
                  <a href="#">Berita 12</a>
                </h4>
              <a href="#" class="btn btn-more"><i class="fa fa-plus"></i>Baca selengkapnya</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- ======== @Region: #footer ======== -->
  <footer id="footer" class="block block-bg-grey-dark" data-block-bg-img="<?php echo base_url();?>assets/img/bg_footer-map.png" data-stellar-background-ratio="0.4">
    <div class="container">

      <div class="row" id="contact">

        <div class="col-md-9">
          <address>
              <strong>DIREKTORAT PENGEMBANGAN DAN PERLUASAN KESEMPATAN KERJA DITJEN BINAPENTA DAN PKK<br>Kementerian Ketenagakerjaan Republik Indonesia</strong>
              <br>
              <i class="fa fa-map-pin fa-fw text-primary"></i> Jl. Jend. Gatot Subroto Kav. 51 Lt. IV A Jakarta Pusat, DKI Jakarta Indonesia
              <br>
              <i class="fa fa-phone fa-fw text-primary"></i> 021-5274930 ext. 541
              <br>
              <i class="fa fa-envelope-o fa-fw text-primary"></i> tks.nakertrans@gmail.com
              <br>
            </address>
        </div>

        <div class="col-md-3">
          <h4 class="text-uppercase">
              Follow Us On:
            </h4>
          <!--social media icons-->
          <div class="social-media social-media-stacked">
            <!--@todo: replace with company social media details-->
            <a href="https://www.facebook.com/tksnakertrans" target="_blank"><i class="fa fa-twitter fa-fw"></i> Twitter</a>
            <a href="http://twitter.com/TKSNakertrans" target="_blank"><i class="fa fa-facebook fa-fw"></i> Facebook</a>
          </div>
        </div>

      </div>

      <div class="row subfooter">
        <!--@todo: replace with company copyright details-->
        <div class="col-md-7">
          <p>Copyright © Flexor Theme Used By Thoyib Hidayat</p>
          <div class="credits">
          </div>
        </div>
        <div class="col-md-5">
          <ul class="list-inline pull-right">
            <li><a href="#">Terms</a></li>
            <li><a href="#">Privacy</a></li>
            <li><a href="#">Contact Us</a></li>
          </ul>
        </div>
      </div>

      <a href="#top" class="scrolltop">Top</a>

    </div>
  </footer>
  <?php include 'parts/js.php'; ?>

</body>
</html>