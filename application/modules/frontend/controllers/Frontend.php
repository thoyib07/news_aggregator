<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index() {
		$data['metadata'] = array('description' => '',
							'keywords' => '',
							'title' => 'News Aggregator Kota Tangerang - Website informasi dan kumpulan berita Kota Tangerang',
							'standout' => base_url(),
							 );
		// $slider['select'] 	= "*";
		// $slider['from'] 	= "t_berita";
		// $slider['where'] 	= "publish = 1 and status = 1";
		// $slider['limit'] 	= array(6,0);
		// $data['slider'] = $this->m_frontend->getData($slider);

		// $data['page'] = "home";
		// $data['content'] = $this->load->view('home',$data,TRUE);
		$this->load->view('index',$data);
	}

	public function get_data($type) {
		$return['html'] = "";
		switch ($type) {
			case 'berita':
				$daftar_berita['select'] 	= "*";
				$daftar_berita['from'] 	= "t_berita";
				$daftar_berita['where'] 	= "publish = 1 and status = 1";
				$daftar_berita['limit'] 	= array($_GET['get'],$_GET['start']);
				$data['daftar_berita'] = $this->m_frontend->getData($daftar_berita);
				$i = 1;
				foreach ($data['daftar_berita'] as $key => $val) {
					$return['html'] .= '
						<div class="col-lg-4 col-md-4" style="margin-top: 30px;">
				            <div class="item">
				              	<a href="'.base_url('detail_berita/').$val['id_berita'].'" class="overlay-wrapper">
				                	<div style="width: 100%; max-height: 250px; float: left; overflow: hidden;">
				                  		<img src="'.base_url($val['gambar']).'" alt="'.$val['judul'].'" class="img-responsive underlay" >
				                	</div>
				                	<div class="item-details bg-noise" style="padding: 1em; background: #f3f3f3; border-bottom: 1px solid #bfbfbf;">
				                  		<h4 class="item-title">
				                      		<a href="'.base_url('detail_berita/').$val['id_berita'].'"><p style="margin-left: 10px;10px;color: #343331;"><b>'.$val['judul'].'</b></p></a>
				                  		</h4>

					                  	<p style="margin-left: 10px;10px;color: #343331;">';
					                    	$leng = 50; 
					                    	if (strlen($val['isi']) < $leng) {
					                      		$return['html'] .= $val['isi'];
					                    	} else {
					                      		$return['html'] .= substr($val['isi'], 0, ($leng-3)).'...</p>';
					                    	}
					                  	$return['html'] .= '</p>
					                  	<a href="'.base_url('detail_berita/').$val['id_berita'].'" class="btn btn-more" style="margin-left:  10px;margin-bottom: 20px;"><i class="fa fa-plus"></i>Baca selengkapnya</a>
				                	</div>
				              	</a>
				            </div>
				        </div>
					';
					if ($i > 3) {
						$i = 0;
						$return['html'] .= '<div class="clearfix"></div>';
					}
					$i++;
				}
				break;
			
			case 'galeri':
				$daftar_galeri['select'] 	= "d.*, a.judul_album";
				$daftar_galeri['from'] 	= "t_gambar_detail as d";
				$daftar_galeri['join'][0]	= array('t_gambar_album as a','a.id_gambar_album = d.id_gambar_album');
				$daftar_galeri['where'] 	= "d.status = 1 and a.status = 1";
				$daftar_galeri['limit'] 	= array($_GET['get'],$_GET['start']);
				$data['daftar_galeri'] = $this->m_frontend->getData($daftar_galeri);
				$i = 1;
				foreach ($data['daftar_galeri'] as $key => $val) {
					$return['html'] .= '
					<div class="col-lg-4 col-md-4" style="margin-top: 30px;">
			            <div style="width: 100%; max-height: 250px; float: left; overflow: hidden;">
			            	<img src="'.base_url($val['gambar']).'" alt="'.$val['judul_album'].'" class="img-responsive underlay" onclick="imgPop_up('."`".base_url($val['gambar'])."`,`".$val['judul_album']."`".');">
			            </div>
			            <div class="item-details bg-noise" style="padding: 1em; background: #f3f3f3; border-bottom: 1px solid #bfbfbf;">
			              <h4 class="item-title">
			                <p style="margin-left: 10px;10px;color: #343331;"><b>'.$val['judul_album'].'</b></p>
			              </h4>
			            </div>
			        </div>
					';
					if ($i > 3) {
						$i = 0;
						$return['html'] .= '<div class="clearfix"></div>';
					}
					$i++;
				}
				break;
			
			case 'ebook':
				$daftar_ebook['select'] 	= "*";
				$daftar_ebook['from'] 	= "t_ebook";
				$daftar_ebook['where'] 	= "status = 1";
				$daftar_ebook['limit'] 	= array($_GET['get'],$_GET['start']);
				$data['daftar_ebook'] = $this->m_frontend->getData($daftar_ebook);
				$i = 1;
				foreach ($data['daftar_ebook'] as $key => $val) {
					$return['html'] .= '
					<div class="col-lg-4 col-md-4" style="margin-top: 30px;">
			            <div class="item">
			              	<a href="'.base_url($val['ebook']).'" class="overlay-wrapper" target="_blank">
			                	<div style="width: 100%; max-height: 250px; float: left; overflow: hidden;">
			                  		<img src="'.base_url('assets/img/ebook.png').'" alt="'.$val['judul_ebook'].'" class="img-responsive underlay" width="150" >
			                	</div>
			              	</a>
			              	<div class="item-details bg-noise" style="padding: 1em; background: #f3f3f3; border-bottom: 1px solid #bfbfbf;">
			                	<h4 class="item-title">
			                  		<a href="'.base_url($val['ebook']).'" target="_blank">'.$val['judul_ebook'].'</a>
			                	</h4>
			                	<a href="'.base_url($val['ebook']).'" class="btn btn-more" target="_blank"><i class="fa fa-download"></i>Unduh Ebook</a>
			              	</div>
			            </div>
			        </div>
					';
					if ($i > 3) {
						$i = 0;
						$return['html'] .= '<div class="clearfix"></div>';
					}
					$i++;
				}
				break;
			
			default:
				# code...
				break;
		}
		echo json_encode($return);
	}

	public function error() {
		die('masuk error');
		/*
		$data['title'] = "SISTAD";
		$data['zone'] = "tad";

		$data['ktda'] = menu_ktda();

		$data['kecamatan'] = menu_kcda();

		$data['content'] = $this->load->view('admin/error404',$data,TRUE);
		$this->load->view('admin/layout',$data);
		*/	
	}
}
