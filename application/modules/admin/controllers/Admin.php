<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	var $katadasar;
	var $stopWord;
	public function __construct() {
		parent::__construct();		
		active_time();
		// var_dump("this user",info_user());
		$this->create_date = date('Y-m-d H:i:s');
		$this->update_date = date('Y-m-d H:i:s');

		$filename = __DIR__ . '/stopwords_id.txt';
		// $fp = @fopen($filename, 'r'); 
		$fp = @fopen($filename, 'c+'); 

		// Add each line to an array
		if ($fp) {
			$stopWord = explode("\n", fread($fp, filesize($filename)));
		}
		
        $filename2 = __DIR__ . '/kata_dasar_id.txt';
        // $filename2 = base_url() . 'cronjob/kata_dasar_id.txt';
        // $fp2 = @fopen($filename2, 'r');
        $fp2 = @fopen($filename2, 'c+'); 
        if ($fp2) {
			// die('masuk sini bos');
           	$katadasar = explode("\n", fread($fp2, filesize($filename2)));
		}
		$session = array('katadasar' => $katadasar, 'stopword' => $stopWord);
		$this->session->set_userdata($session);
		// defined('KATA_DASAR') OR define('KATA_DASAR', $katadasar);
		// var_dump(json_encode($katadasar));
    }

	public function index() {
		if(!isset($_SESSION)) { session_start(); }
		// active_time();
		if ($this->session->userdata('login') == 1) {
			//var_dump('masuk');
			redirect(base_url().'admin/home');
		} else {
			redirect(base_url().'auth/login');
		}
	}

	public function home() {
		active_time();
		$data['page'] 	= array('p' => 'home', 'c' => '' );
		$data['title'] 	= ADMIN_TITLE;
		$data['menu'] = info_menu();
		$data['user'] = info_user();

		// $data = array_merge($data,side_menu());

		// var_dump("<hr>",$data);
		// die();
		if($this->session->userdata('akses') == 1){			
			$list_kata_kunci['select']	= "id_kata_kunci";
			$list_kata_kunci['table']	= "m_kata_kunci";

			$data['list_kata_kunci'] 	= $this->m_admin->getData($list_kata_kunci);
			$data['content'] 	= $this->load->view('home',$data,TRUE);
		} else {
			$detail_project['select']	= "*";
			$detail_project['table']	= "m_setting_project";
			$detail_project['where']	= "user_access like '%|".$_SESSION['id_user']."|%' and status = 1";
			
			$data['detail_project'] 	= $this->m_admin->getData($detail_project);

			$detail_pencarian_setting_project['select']		= "p.*, s.list_kata_kunci , u.nama";
			$detail_pencarian_setting_project['table']		= "t_pencarian_setting_project as p";
			$detail_pencarian_setting_project['join'][0]	= array("m_setting_project as s","s.id_setting_project = p.id_setting_project");
			$detail_pencarian_setting_project['join'][1]	= array("m_user as u","u.id_user = p.cdb");
			$detail_pencarian_setting_project['where']		= "p.cdb = ".$_SESSION['id_user']." and p.status = 1";

			$data['detail_pencarian_setting_project'] 	= $this->m_admin->getData($detail_pencarian_setting_project);

			$data['content'] 	= $this->load->view('home_user',$data,TRUE);
		}
		$this->load->view('layout',$data);
	}

	public function berita($action=null,$id=null) {
		active_time();
		$data['page'] 	= array('p' => 'berita', 'c' => '' );
		$data['title'] 		= ADMIN_TITLE.' - Berita';		
		$data['menu'] = info_menu();
		$data['user'] = info_user();
		//menu
		// $data = array_merge($data,side_menu());

		$data['back']	= base_url('admin/berita');
		$data['proses'] = ucwords($action);
		// $data['sub_ktda'] = all_sub_ktda();
		// var_dump("<hr>",$data['m_kecamatan']);
		switch ($action) {
			case 'ubah':
				$data['action'] = base_url('admin/berita/ubah');
				$detail_berita['select']	= "b.*";
				$detail_berita['table']		= "m_berita as b";

				if (isset($id)) {
					$detail_berita['where']	= "b.id_berita = '".$id."' and b.status = 1";
				} else {
					$detail_berita['where']	= "b.id_berita = '".$_POST['id_berita']."' and b.status = 1";
				}

				$data['detail_berita'] 	= $this->m_admin->getData($detail_berita);
				// var_dump($_POST);
				// die();
				if (isset($_POST['submit'])) {
					// var_dump($_POST,"<hr>");
					// var_dump($data['detail_berita'],"<hr>");
					// die();
					$data['data']['judul_berita'] = $_POST['judul_berita'];
					$data['data']['isi_berita'] = $_POST['isi_berita'];

					$data['table']		= "m_berita";
					$data['where'][0] 	= array('id_berita', $_POST['id_berita']);
  					$this->m_admin->updateData($data);
  					// redirect('admin/berita');
  					$json['status'] = TRUE;
					echo json_encode($json);
				} else {
					//var_dump($data['detail_berita']);
					$data['page'] 	= array('p' => 'berita', 'c' => 'ubah berita' );
					$data['content'] 	= $this->load->view('berita/form_berita',$data,TRUE);
					echo json_encode($data);
				}
			break;

			case 'hapus':
				$data['search']	= "id_berita";
				$data['id']		= $_POST['id'];
				$data['table']	= "m_berita";

				$this->m_admin->delById($data);
				echo json_encode(array('url' => base_url('admin/berita')));
				// redirect('admin/berita');
			break;

			case 'restore':
				$data['search']	= "id_berita";
				$data['id']		= $id;
				$data['table']	= "m_berita";

				$this->m_admin->restorById($data);
				redirect('admin/berita');
			break;

			default:
				$data['page'] 		= array('p' => 'berita', 'c' => '' );

				$data['content'] 	= $this->load->view('berita',$data,TRUE);
				// echo $data['content'];
				$this->load->view('layout',$data);
			break;
		}
	}

	public function situs($action=null,$id=null) {
		active_time();
		$data['page'] 	= array('p' => 'situs', 'c' => '' );
		$data['title'] 		= ADMIN_TITLE.' - Situs';		
		$data['menu'] = info_menu();
		$data['user'] = info_user();
		//menu
		// $data = array_merge($data,side_menu());

		$data['back']	= base_url('admin/situs');
		$data['proses'] = ucwords($action);
		// $data['sub_ktda'] = all_sub_ktda();
		// var_dump("<hr>",$data);
		// die();
		switch ($action) {
			case 'tambah':
				$data['action'] = base_url('admin/situs/tambah');
				if (isset($_POST['submit'])) {
					// var_dump($_POST);
					// die();

					// Judul Berita
					if ($_POST['id_cari_tag_judul'] == "id") {
						$data['data']['id_tag_judul'] = $_POST['cari_val_judul'];
					} else if ($_POST['id_cari_tag_judul'] == "class") {
						$data['data']['class_tag_judul'] = $_POST['cari_val_judul'];
					}

					// Isi Berita
					if ($_POST['id_cari_tag_isi'] == "id") {
						$data['data']['id_tag_isi'] = $_POST['cari_val_isi'];
					} else if ($_POST['id_cari_tag_isi'] == "class") {
						$data['data']['class_tag_isi'] = $_POST['cari_val_isi'];
					}

					// $data['data']['id_status_situs'] 	= $_POST['id_status_situs'];
					// if ($_POST['id_media'] !== "") {
					// 	$data['data']['id_media'] 			= $_POST['id_media'];
					// }
					$data['data']['nama_situs'] 		= $_POST['nama_situs'];
					$data['data']['link_situs'] 		= $_POST['link_situs'];
					$data['data']['uri_segment'] 		= $_POST['uri_segment'];
					$data['data']['lvl_situs'] 			= $_POST['lvl_situs'];
					$data['data']['start_tag_judul'] 	= $_POST['start_tag_judul'];
					$data['data']['start_tag_isi'] 		= $_POST['start_tag_isi'];
					$data['data']['cdd'] = date('Y-m-d H:i:s');
					$data['data']['status'] = 1;

  					$data['table']		= "m_situs_berita";
  					$this->m_admin->addData($data);
					// redirect('admin/situs');
					$json['status'] = TRUE;
					echo json_encode($json);
				} else {
					//var_dump($_SESSION);
					$data['page'] 	= array('p' => 'situs', 'c' => 'tambah situs' );
					$data['content'] 	= $this->load->view('situs/form_situs',$data,TRUE);
					echo json_encode($data);
				}
			break;

			case 'ubah':
				$situs_berita_detail['select']	= 'sb.*';
				$situs_berita_detail['table']	= "m_situs_berita as sb";
				
				if (isset($id)) {
					$situs_berita_detail['where']	= "sb.id_situs_berita = '".$id."' and sb.status = 1";
				} else {
					$situs_berita_detail['where']	= "sb.id_situs_berita = '".$_POST['id_situs_berita']."' and sb.status = 1";
				}

				$data['situs_berita_detail'] 	= $this->m_admin->getData($situs_berita_detail);

				// Judul Berita Detail
				if (!is_null($data['situs_berita_detail'][0]['id_tag_judul'])) {
					$data['situs_berita_detail'][0]['id_cari_tag_judul'] = "id";
					$data['situs_berita_detail'][0]['cari_val_judul'] = $data['situs_berita_detail'][0]['id_tag_judul'];
				} else {
					$data['situs_berita_detail'][0]['id_cari_tag_judul'] = "class";
					$data['situs_berita_detail'][0]['cari_val_judul'] = $data['situs_berita_detail'][0]['class_tag_judul'];
				}

				// Isi Berita Detail
				if (!is_null($data['situs_berita_detail'][0]['id_tag_isi'])) {
					$data['situs_berita_detail'][0]['id_cari_tag_isi'] = "id";
					$data['situs_berita_detail'][0]['cari_val_isi'] = $data['situs_berita_detail'][0]['id_tag_isi'];
				} else {
					$data['situs_berita_detail'][0]['id_cari_tag_isi'] = "class";
					$data['situs_berita_detail'][0]['cari_val_isi'] = $data['situs_berita_detail'][0]['class_tag_isi'];
				}

				$data['action'] = base_url('admin/situs/ubah');

				if (isset($_POST['submit'])) {
					// var_dump($_POST);
					// die();
					// $data['data'] 		= array('username' => $_POST['username'], 'nama' => $_POST['nama'], 'password' => sha1($_POST['password']), 'id_user_type' => $_POST['hak_akses']);
					// Judul Berita
					if ($_POST['id_cari_tag_judul'] == "id") {
						$data['data']['id_tag_judul'] = $_POST['cari_val_judul'];
						$data['data']['class_tag_judul'] = null;
					} else if ($_POST['id_cari_tag_judul'] == "class") {
						$data['data']['id_tag_judul'] = null;
						$data['data']['class_tag_judul'] = $_POST['cari_val_judul'];
					}

					// Isi Berita
					if ($_POST['id_cari_tag_isi'] == "id") {
						$data['data']['id_tag_isi'] = $_POST['cari_val_isi'];
						$data['data']['class_tag_isi'] = null;
					} else if ($_POST['id_cari_tag_isi'] == "class") {
						$data['data']['id_tag_isi'] = null;
						$data['data']['class_tag_isi'] = $_POST['cari_val_isi'];
					}

					// $data['data']['id_status_situs'] 	= $_POST['id_status_situs'];
					// if ($_POST['id_media'] !== "") {
					// 	$data['data']['id_media'] 			= $_POST['id_media'];
					// }
					$data['data']['nama_situs'] 		= $_POST['nama_situs'];
					$data['data']['link_situs'] 		= $_POST['link_situs'];
					$data['data']['uri_segment'] 		= $_POST['uri_segment'];
					$data['data']['lvl_situs'] 			= $_POST['lvl_situs'];
					$data['data']['start_tag_judul'] 	= $_POST['start_tag_judul'];
					$data['data']['start_tag_isi'] 		= $_POST['start_tag_isi'];

  					$data['table']		= "m_situs_berita";
					$data['where'][0] 	= array('id_situs_berita', $_POST['id_situs_berita']);
  					$this->m_admin->updateData($data);
					// die();
  					// redirect('admin/situs');
  					$json['status'] = TRUE;
					echo json_encode($json);
				} else {
					//var_dump($data['situs']);
					$data['page'] 	= array('p' => 'situs', 'c' => 'ubah situs' );
					$data['content'] 	= $this->load->view('situs/form_situs',$data,TRUE);
					echo json_encode($data);
				}
			break;

			case 'hapus':
				$data['id']		= $_POST['id'];
				$data['search']	= "id_situs_berita";
				$data['table']	= "m_situs_berita";

				$this->m_admin->delById($data);
				echo json_encode(array('url' => base_url('admin/situs')));
			break;

			case 'restore':
				$data['id']		= $id;
				$data['search']	= "id_situs_berita";
				$data['table']	= "m_situs_berita";

				$this->m_admin->restorById($data);
				redirect('admin/situs');
			break;

			default:
				$data['page'] 		= array('p' => 'situs', 'c' => '' );

				$data['content'] 	= $this->load->view('situs',$data,TRUE);
				// echo $data['content'];
				$this->load->view('layout',$data);
			break;
		}
	}

	public function kata_kunci($action=null,$id=null) {
		active_time();
		$data['page'] 	= array('p' => 'kata_kunci', 'c' => '' );
		$data['title'] 		= ADMIN_TITLE.' - Kata Kunci';		
		$data['menu'] = info_menu();
		$data['user'] = info_user();
		//menu
		// $data = array_merge($data,side_menu());

		$data['back']	= base_url('admin/kata_kunci');
		$data['proses'] = ucwords($action);
		// $data['sub_ktda'] = all_sub_ktda();
		// var_dump("<hr>",$data['m_kecamatan']);
		switch ($action) {
			case 'tambah':
				$data['action'] = base_url('admin/kata_kunci/tambah');
				if (isset($_POST['submit'])) {
					// var_dump($_POST,"<hr>");
					// die();

					$data['data']['kata_kunci'] = strtolower($_POST['kata_kunci']);
					$data['data']['cdd'] = date('Y-m-d H:i:s');
					$data['data']['status'] = 1;
					// var_dump($data['data']);
					// die();
					$data['table']		= "m_kata_kunci";
  					$json['id_kata_kunci'] 	= $this->m_admin->addData($data);
					// redirect('admin/kata_kunci');

					$json['status'] = TRUE;
  					echo json_encode($json);
				} else {
					//var_dump($_SESSION);
					$data['page'] 	= array('p' => 'kata_kunci', 'c' => 'tambah kata kunci' );
					$data['content'] 	= $this->load->view('kata_kunci/form_kata_kunci',$data,TRUE);
					echo json_encode($data);
				}
			break;

			case 'ubah':
				$data['action'] = base_url('admin/kata_kunci/ubah');
				$kata_kunci_detail['select']	= "k.*";
				$kata_kunci_detail['table']		= "m_kata_kunci as k";

				if (isset($id)) {
					$kata_kunci_detail['where']	= "k.id_kata_kunci = '".$id."' and k.status = 1";
				} else {
					$kata_kunci_detail['where']	= "k.id_kata_kunci = '".$_POST['id_kata_kunci']."' and k.status = 1";
				}

				$data['kata_kunci_detail'] 	= $this->m_admin->getData($kata_kunci_detail);

				if (isset($_POST['submit'])) {
					// var_dump($data['kata_kunci_detail'],"<hr>");
					// var_dump($_POST,"<hr>");
					// die();

					$data['data']['kata_kunci'] = strtolower($_POST['kata_kunci']);
					$data['table']		= "m_kata_kunci";
					$data['where'][0] 	= array('id_kata_kunci', $_POST['id_kata_kunci']);
  					$this->m_admin->updateData($data);
  					// redirect('admin/kata_kunci');

					$json['status'] = TRUE;
  					echo json_encode($json);
				} else {
					//var_dump($data['berita_detail']);
					$data['page'] 	= array('p' => 'kata_kunci', 'c' => 'ubah kata kunci' );
					$data['content'] 	= $this->load->view('kata_kunci/form_kata_kunci',$data,TRUE);
					echo json_encode($data);
				}
			break;

			case 'hapus':
				$data['id']		= $_POST['id'];
				$data['search']	= "id_kata_kunci";
				$data['table']	= "m_kata_kunci";

				$this->m_admin->delById($data);
				echo json_encode(array('url' => base_url('admin/kata_kunci')));
			break;

			case 'restore':
				$data['search']	= "id_kata_kunci";
				$data['id']		= $id;
				$data['table']	= "m_kata_kunci";

				$this->m_admin->restorById($data);
				redirect('admin/kata_kunci');
			break;

			default:
				$data['page'] 		= array('p' => 'kata_kunci', 'c' => '' );

				$data['content'] 	= $this->load->view('kata_kunci',$data,TRUE);
				// echo $data['content'];
				$this->load->view('layout',$data);
			break;
		}
	}

	public function lokasi($action=null,$id=null) {
		active_time();
		$data['page'] 	= array('p' => 'lokasi', 'c' => '' );
		$data['title'] 		= ADMIN_TITLE.' - Lokasi';		
		$data['menu'] = info_menu();
		$data['user'] = info_user();
		//menu
		// $data = array_merge($data,side_menu());

		$data['back']	= base_url('admin/lokasi');
		$data['proses'] = ucwords($action);
		// $data['sub_ktda'] = all_sub_ktda();
		// var_dump("<hr>",$data['m_kecamatan']);
		switch ($action) {
			case 'ubah':
				// var_dump($_POST);
				if (isset($_POST['submit'])) {
					// var_dump($data['kata_kunci_detail'],"<hr>");
					// var_dump($_POST,"<hr>");
					// die();

					$data['data']['lat'] = $_POST['lat'];
					$data['data']['long'] = $_POST['long'];
					if ($_POST['no_prop']) {
						$data['table']		= "m_provinsi";
						$data['where'][0] 	= array('NO_PROP', $_POST['no_prop']);
						if ($_POST['no_kab']) {
							$data['table']		= "m_kabupaten_kota";
							$data['where'][0] 	= array('NO_PROP', $_POST['no_prop']);
							$data['where'][1] 	= array('NO_KAB', $_POST['no_kab']);
							if ($_POST['no_kec']) {
								$data['table']		= "m_kecamatan";
								$data['where'][0] 	= array('NO_PROP', $_POST['no_prop']);
								$data['where'][1] 	= array('NO_KAB', $_POST['no_kab']);
								$data['where'][2] 	= array('NO_KEC', $_POST['no_kec']);
								if ($_POST['no_kel']) {
									$data['table']		= "m_kelurahan";
									$data['where'][0] 	= array('NO_PROP', $_POST['no_prop']);
									$data['where'][1] 	= array('NO_KAB', $_POST['no_kab']);
									$data['where'][2] 	= array('NO_KEC', $_POST['no_kec']);
									$data['where'][3] 	= array('NO_KEL', $_POST['no_kel']);
								}
							}
						}
					}
  					$this->m_admin->updateData($data);
  					// redirect('admin/kata_kunci');

					$json['status'] = TRUE;
  					echo json_encode($json);
				} else {
					//var_dump($data['berita_detail']);
					$data['page'] 	= array('p' => 'kata_kunci', 'c' => 'ubah kata kunci' );
					$data['content'] 	= $this->load->view('kata_kunci/form_kata_kunci',$data,TRUE);
					echo json_encode($data);
				}
			break;

			default:
				$data['page'] 		= array('p' => 'lokasi', 'c' => '' );

				$data['content'] 	= $this->load->view('lokasi',$data,TRUE);
				// echo $data['content'];
				$this->load->view('layout',$data);
			break;
		}
	}

	public function user($action=null,$id=null) {
		active_time();
		$data['page'] 	= array('p' => 'user', 'c' => '' );
		$data['title'] 		= ADMIN_TITLE.' - User';		
		$data['menu'] = info_menu();
		$data['user'] = info_user();
		//menu
		// $data = array_merge($data,side_menu());

		$data['back']	= base_url('admin/user');
		$data['proses'] = ucwords($action);
		// $data['sub_ktda'] = all_sub_ktda();
		// var_dump("<hr>",$data);
		// die();
		switch ($action) {			
			case 'tambah':
				$data['action'] = base_url('admin/user/tambah');
				if (isset($_POST['submit'])) {
					// var_dump($_POST,"<hr>");
					// die();
					// qecLAJgqcT
					$data['data']['id_user_type'] = $_POST['id_user_type'];
					$data['data']['nama'] = $_POST['nama'];
					$data['data']['email'] = $_POST['email'];
					$data['data']['password'] = sha1($_POST['password']);
					$data['data']['cdd'] = date('Y-m-d H:i:s');
					$data['data']['status'] = 1;
					// var_dump($data['data']);
					// die();
					$data['table']		= "m_user";
  					// $json['id_user'] 	= $this->m_admin->addData($data);
					// redirect('admin/user');
					if ($this->m_admin->addData($data)) {
						$email_setting = (object) array();
						$email_setting->email_from = $this->config->item('smtp_user');
						$email_setting->email_to = $_POST['email'];
						$email_setting->subject = "Access to news aggregator JN1";
						$email_setting->message = "Berikut adalah password untuk login ke situs ".base_url()."<hr>";
						$email_setting->message .= "Password : <b>".$_POST['password']."</b><hr>";
						$email_setting->message .= "<b>*NB</b> : Diharap untuk menjaga kerahasiaan akses ini.";

						// send_email($email_setting);
						$json['status'] = send_email($email_setting);
						$json['status_add'] = "berhasil tambah";
						echo json_encode($json);
					} else {						
						$json['status'] = FALSE;
						echo json_encode($json);
					}
				} else {
					//var_dump($_SESSION);
					$data['page'] 	= array('p' => 'user', 'c' => 'tambah user' );
					$data['content'] 	= $this->load->view('user/form_user',$data,TRUE);
					echo json_encode($data);
				}
			break;

			case 'ubah':
				$data['action'] = base_url('admin/user/ubah');
				$user_detail['select']	= "k.*";
				$user_detail['table']		= "m_user as k";

				if (isset($id)) {
					$user_detail['where']	= "k.id_user = '".$id."' and k.status = 1";
				} else {
					$user_detail['where']	= "k.id_user = '".$_POST['id_user']."' and k.status = 1";
				}

				$data['user_detail'] 	= $this->m_admin->getData($user_detail);

				if (isset($_POST['submit'])) {
					// var_dump($data['user_detail'],"<hr>");
					// var_dump($_POST,"<hr>");
					// die();
					
					$data['data']['id_user_type'] = $_POST['id_user_type'];
					$data['data']['nama'] = $_POST['nama'];
					$data['data']['email'] = $_POST['email'];
					$data['data']['password'] = sha1($_POST['password']);
					$data['table']		= "m_user";
					$data['where'][0] 	= array('id_user', $_POST['id_user']);
  					// $this->m_admin->updateData($data);
  					// redirect('admin/user');
					if ($this->m_admin->updateData($data)) {						
						$email_setting = (object) array();
						$email_setting->email_from = $this->config->item('smtp_user');
						$email_setting->email_to = $_POST['email'];
						$email_setting->subject = "Update access to news aggregator JN1";
						$email_setting->message = "Berikut adalah password untuk login ke situs ".base_url()."<hr>";
						$email_setting->message .= "Password : <b>".$_POST['password']."</b><hr>";
						$email_setting->message .= "<b>*NB</b> : Diharap untuk menjaga kerahasiaan akses ini.";
						// send_email($email_setting);
						$json['status'] = send_email($email_setting);
						$json['status_add'] = "berhasil ubah";
						echo json_encode($json);
					} else {
						$json['status'] = FALSE;
						echo json_encode($json);
					}
				} else {
					//var_dump($data['berita_detail']);
					$data['page'] 	= array('p' => 'user', 'c' => 'ubah kata kunci' );
					$data['content'] 	= $this->load->view('user/form_user',$data,TRUE);
					echo json_encode($data);
				}
			break;

			case 'hapus':
				$data['id']		= $_POST['id'];
				$data['search']	= "id_user";
				$data['table']	= "m_user";

				$this->m_admin->delById($data);
				echo json_encode(array('url' => base_url('admin/user')));
			break;

			case 'restore':
				$data['id']		= $id;
				$data['search']	= "id_user";
				$data['table']	= "m_user";

				$this->m_admin->restorById($data);
				redirect('admin/user');
			break;

			default:
				$data['page'] 		= array('p' => 'user', 'c' => '' );

				$data['content'] 	= $this->load->view('user',$data,TRUE);
				// echo $data['content'];
				$this->load->view('layout',$data);
			break;
		}

	}

	public function project($action=null,$id=null) {
		active_time();
		$data['page'] 	= array('p' => 'project', 'c' => '' );
		$data['title'] 		= ADMIN_TITLE.' - Setting Project';		
		$data['menu'] = info_menu();
		$data['user'] = info_user();
		//menu
		// $data = array_merge($data,side_menu());

		$data['back']	= base_url('admin/project');
		$data['proses'] = ucwords($action);
		// $data['sub_ktda'] = all_sub_ktda();
		// var_dump("<hr>",$data['m_kecamatan']);
		$kata_kunci_detail['select']	= "id_kata_kunci, kata_kunci";
		$kata_kunci_detail['table']	= "m_kata_kunci";
		$kata_kunci_detail['where']	= "status = 1";
		$data['kata_kunci_detail'] 	= $this->m_admin->getData($kata_kunci_detail);

		$user_detail['select']	= "id_user, nama";
		$user_detail['table']	= "m_user";
		$user_detail['where']	= "status = 1";
		$data['user_detail'] 	= $this->m_admin->getData($user_detail);

		switch ($action) {
			case 'tambah':
				$data['action'] = base_url('admin/project/tambah');
				if (isset($_POST['submit'])) {
					// var_dump($_POST,"<hr>");
					// die();

					$data['data']['nama_setting_project'] = $_POST['nama_setting_project'];
					$data['data']['desc_setting_project'] = $_POST['desc_setting_project'];
					// $data['data']['user_access'] = implode(",",$_POST['user_access']);
					$data['data']['user_access'] = "";
					foreach ($_POST['user_access'] as $key => $val) {
						$data['data']['user_access'] .= "|".$val."|,";
					}

					$data['data']['list_kata_kunci'] = "";
					foreach ($_POST['list_kata_kunci'] as $key => $val) {
						$data['data']['list_kata_kunci'] .= "|".$val."|,";
					}
					$data['data']['cdd'] = date('Y-m-d H:i:s');
					$data['data']['cdb'] = $_SESSION['id_user'];
					$data['data']['status'] = 1;
					// var_dump($data['data']);
					// die();
					$data['table']		= "m_setting_project";
  					// $json['id_kata_kunci'] 	= $this->m_admin->addData($data);
					// redirect('admin/kata_kunci');
					if ($this->m_admin->addData($data)) {
						$json['status'] = TRUE;
						echo json_encode($json);
					} else {
						$json['status'] = FALSE;
						echo json_encode($json);
					}
				} else {
					//var_dump($_SESSION);
					$data['page'] 	= array('p' => 'project', 'c' => 'tambah project' );
					$data['content'] 	= $this->load->view('project/form_project',$data,TRUE);
					echo json_encode($data);
				}
			break;

			case 'ubah':
				$data['action'] = base_url('admin/project/ubah');
				$setting_project_detail['select']	= 'p.*';
				$setting_project_detail['table']	= "m_setting_project as p";

				if (isset($id)) {
					$setting_project_detail['where']	= "p.id_setting_project = '".$id."' and p.status = 1";
				} else {
					$setting_project_detail['where']	= "p.id_setting_project = '".$_POST['id_setting_project']."' and p.status = 1";
				}

				$data['setting_project_detail'] 	= $this->m_admin->getData($setting_project_detail);

				if (isset($_POST['submit'])) {
					// var_dump($data['setting_project_detail'],"<hr>");
					// var_dump($_POST,"<hr>");
					// die();				

					$data['data']['nama_setting_project'] = $_POST['nama_setting_project'];
					$data['data']['desc_setting_project'] = $_POST['desc_setting_project'];
					
					$data['data']['user_access'] = "";
					foreach ($_POST['user_access'] as $key => $val) {
						$data['data']['user_access'] .= "|".$val."|,";
					}

					$data['data']['list_kata_kunci'] = "";
					foreach ($_POST['list_kata_kunci'] as $key => $val) {
						$data['data']['list_kata_kunci'] .= "|".$val."|,";
					}
					$data['data']['udd'] = date('Y-m-d H:i:s');
					$data['data']['udb'] = $_SESSION['id_user'];
					// var_dump($data['data'],"<hr>");
					// die();

					$data['table']		= "m_setting_project";
					$data['where'][0] 	= array('id_setting_project', $_POST['id_setting_project']);
  					$this->m_admin->updateData($data);
  					// redirect('admin/project');

					$json['status'] = TRUE;
  					echo json_encode($json);
				} else {
					//var_dump($data['berita_detail']);
					$data['page'] 	= array('p' => 'project', 'c' => 'ubah setting project' );
					$data['content'] 	= $this->load->view('project/form_project',$data,TRUE);
					// $this->load->view('layout',$data);
					echo json_encode($data);
				}
			break;

			case 'hapus':
				$data['search']	= "id_setting_project";
				$data['id']		= $_POST['id'];
				$data['table']	= "m_setting_project";

				$this->m_admin->delById($data);
				echo json_encode(array('url' => base_url('admin/project')));
			break;

			case 'restore':
				$data['search']	= "id_setting_project";
				$data['id']		= $id;
				$data['table']	= "m_setting_project";

				$this->m_admin->restorById($data);
				redirect('admin/project');
			break;

			default:
				$data['page'] 		= array('p' => 'project', 'c' => '' );

				$data['content'] 	= $this->load->view('project',$data,TRUE);
				// echo $data['content'];
				$this->load->view('layout',$data);
			break;
		}
	}

	public function ajax_list() {
		$data 	= array();
        $no 	= $_POST['start'];
        $type 	= $_GET['type'];
        if (isset($_GET['id'])) {
        	$id 	= $_GET['id'];
        }
        switch ($type) {
        	case 'user':
        	case 'del_user':
        		$list = $this->m_admin->get_datatables($type);
        		// var_dump($list);
        		// die();
	    			foreach ($list as $l) {
			            $no++;
			            $row = array();
			            $row[] = $no;
			            $row[] = $l['nama'];
			            $row[] = $l['email'];
						
						if ($type == 'user') {
			            	$row[] = '<button class="btn btn-warning fa fa-pencil" type="button" title="Ubah Data User" onclick="edit('.$l['id_user'].');"></button>'.''.'<a href="javascript:void(0)" onclick="if(confirm(`Apa Anda Yakin Untuk Menghapus Data Ini???`)) delData(`user`,'.$l['id_user'].')" ><button class="btn btn-danger fa fa-power-off" type="button" title="Nonaktifkan Data User"></button></a>';
			            } else {
			            	 $row[] = '<a href="'.base_url('admin/user/restore').'/'.$l['id_user'].'" onclick="return confirm(`Apa Anda Yakin Untuk Mengaktifkan Kembali Data Ini???`)" ><button class="btn btn-success fa fa-recycle" type="button" title="Aktifkan Data User"></button></a>';
			            }

			            $data[] = $row;
			        }

			        $output = array(
	                        "draw" => $_POST['draw'],
	                        "recordsTotal" => $this->m_admin->count_all($type),
	                        "recordsFiltered" => $this->m_admin->count_filtered($type),
	                        "data" => $data,
	                );
			break;
			
        	case 'berita':
        	case 'del_berita':
        		$list = $this->m_admin->get_datatables($type);
        		// var_dump($list);
        		// die();
	    			foreach ($list as $l) {
			            $no++;
			            $row = array();
			            $row[] = $no;
			            $row[] = '<a href="'.$l['link_berita'].'" target="blank">'.$l['judul_berita'].'</a>';
			            $row[] = $l['publish_date'];
						// $row[] = $l['lokasi_prop'];
						if ((!is_null($l['lokasi_prop'])) || (!is_null($l['lokasi_kab']))) {						
							$tmp_lokasi_prop = array();
							if (json_decode($l['lokasi_prop'])) {
								foreach (json_decode($l['lokasi_prop']) as $key => $val) {
									$nama_lokasi = explode(",",$val->nama);
									// $tmp_lokasi = $tmp_lokasi." ".$nama_lokasi[0].", ";
									array_push($tmp_lokasi_prop,strtoupper($nama_lokasi[0]));
								}
							}

							$tmp_lokasi_kab = array();
							// var_dump(json_decode($l['lokasi_kab']));
							// die;
							if (json_decode($l['lokasi_kab'])) {
								foreach (json_decode($l['lokasi_kab']) as $key => $val) {
									$nama_lokasi = explode(",",$val->nama);
									// $tmp_lokasi = $tmp_lokasi." ".$nama_lokasi[0].", ";
									array_push($tmp_lokasi_kab,strtoupper($nama_lokasi[0]));
								}
							}
							$row[] = implode(", ",$tmp_lokasi_prop)." (".implode(", ",$tmp_lokasi_kab).")";
						} else {
							$row[] = "-";
						}
						
						$tmpKataKunci = explode(";",$l['kata_kunci']);
						array_unique($tmpKataKunci);
						// $row[] = str_replace(";",",",$l['kata_kunci']);
						$row[] = implode(",",$tmpKataKunci);
			            $row[] = $l['nama_situs'];

			            if ($type == 'berita') {
			            	$row[] = '<button class="btn btn-warning fa fa-pencil" type="button" title="Ubah Data Berita" onclick="edit('.$l['id_berita'].');"></button>'.''.'<a href="javascript:void(0)" onclick="if(confirm(`Apa Anda Yakin Untuk Menghapus Data Ini???`)) delData(`berita`,'.$l['id_berita'].')" ><button class="btn btn-danger fa fa-power-off" type="button" title="Nonaktifkan Data Berita"></button></a>';
			            } else {
			            	 $row[] = '<a href="'.base_url('admin/berita/restore').'/'.$l['id_berita'].'" onclick="return confirm(`Apa Anda Yakin Untuk Mengaktifkan Kembali Data Ini???`)" ><button class="btn btn-success fa fa-recycle" type="button" title="Aktifkan Data Berita"></button></a>';
			            }

			            $data[] = $row;
			        }

			        $output = array(
	                        "draw" => $_POST['draw'],
	                        "recordsTotal" => $this->m_admin->count_all($type),
	                        "recordsFiltered" => $this->m_admin->count_filtered($type),
	                        "data" => $data,
	                );
			break;
			
        	case 'situs':
        	case 'del_situs':
        		$list = $this->m_admin->get_datatables($type);
        		// var_dump($list);
        		// die();
	    			foreach ($list as $l) {
			            $no++;
			            $row = array();
			            $row[] = $no;
			            $row[] = '<a href="'.$l['link_situs'].'" target="blank">'.$l['nama_situs']."</a>";

			            if ($type == 'situs') {
			            	$row[] = '<button class="btn btn-warning fa fa-pencil" type="button" title="Ubah Data Situs" onclick="edit('.$l['id_situs_berita'].');"></button>'.''.'<a href="javascript:void(0)" onclick="if(confirm(`Apa Anda Yakin Untuk Menghapus Data Ini???`)) delData(`situs`,'.$l['id_situs_berita'].')" ><button class="btn btn-danger fa fa-power-off" type="button" title="Nonaktifkan Data Berita"></button></a>';
			            } else {
			            	 $row[] = '<a href="'.base_url('admin/situs/restore').'/'.$l['id_situs_berita'].'" onclick="return confirm(`Apa Anda Yakin Untuk Mengaktifkan Kembali Data Ini???`)" ><button class="btn btn-success fa fa-recycle" type="button" title="Aktifkan Data Berita"></button></a>';
			            }

			            $data[] = $row;
			        }

			        $output = array(
	                        "draw" => $_POST['draw'],
	                        "recordsTotal" => $this->m_admin->count_all($type),
	                        "recordsFiltered" => $this->m_admin->count_filtered($type),
	                        "data" => $data,
	                );
			break;	

			case 'kata_kunci':
        	case 'del_kata_kunci':
        		$list = $this->m_admin->get_datatables($type);
        		// var_dump($list);
        		// die();
	    			foreach ($list as $l) {
			            $no++;
			            $row = array();
			            $row[] = $no;
			            $row[] = $l['kata_kunci'];

			            if ($type == 'kata_kunci') {
			            	$row[] = '<button class="btn btn-warning fa fa-pencil" type="button" title="Ubah Data Kata Kunci" onclick="edit('.$l['id_kata_kunci'].');"></button>'.''.'<a href="javascript:void(0)" onclick="if(confirm(`Apa Anda Yakin Untuk Menghapus Data Ini???`)) delData(`kata_kunci`,'.$l['id_kata_kunci'].')" ><button class="btn btn-danger fa fa-power-off" type="button" title="Nonaktifkan Data Berita"></button></a>';
			            } else {
			            	 $row[] = '<a href="'.base_url('admin/kata_kunci/restore').'/'.$l['id_kata_kunci'].'" onclick="return confirm(`Apa Anda Yakin Untuk Mengaktifkan Kembali Data Ini???`)" ><button class="btn btn-success fa fa-recycle" type="button" title="Aktifkan Data Berita"></button></a>';
			            }

			            $data[] = $row;
			        }

			        $output = array(
	                        "draw" => $_POST['draw'],
	                        "recordsTotal" => $this->m_admin->count_all($type),
	                        "recordsFiltered" => $this->m_admin->count_filtered($type),
	                        "data" => $data,
	                );
			break;
			
			case 'project':
			case 'del_project':
				$list = $this->m_admin->get_datatables($type);
				// var_dump($list);
				// die();
				foreach ($list as $l) {
							$no++;
							$row = array();
							$row[] = $no;
							$row[] = $l['nama_project'];
	
							if ($type == 'project') {
								$row[] = '<button class="btn btn-warning fa fa-pencil" type="button" title="Ubah Data Project" onclick="edit('.$l['id_setting_project'].');"></button>'.''.'<a href="javascript:void(0)" onclick="if(confirm(`Apa Anda Yakin Untuk Menghapus Data Ini???`)) delData(`project`,'.$l['id_setting_project'].')" ><button class="btn btn-danger fa fa-power-off" type="button" title="Nonaktifkan Data Project"></button></a>';
							} else {
								 $row[] = '<a href="'.base_url('admin/project/restore').'/'.$l['id_setting_project'].'" onclick="return confirm(`Apa Anda Yakin Untuk Mengaktifkan Kembali Data Ini???`)" ><button class="btn btn-success fa fa-recycle" type="button" title="Aktifkan Data Project"></button></a>';
							}

							$data[] = $row;
					}

					$output = array(
											"draw" => $_POST['draw'],
											"recordsTotal" => $this->m_admin->count_all($type),
											"recordsFiltered" => $this->m_admin->count_filtered($type),
											"data" => $data,
							);
			break;

        	default:
        		# code...
        	break;
        }
        //output to json format
        echo json_encode($output);
	}

	public function ajax($type) {
		switch ($type) {
			case 'ajax_status_situs':
				$data = (object) array();
				$status_situs['select'] 	= "*";
				$status_situs['table']		= "m_status_situs";
				$status_situs['where']		= "status = 1";
				$status_situs['order']		= array("id_status_situs","asc");
		        $data->status_situs = $this->m_admin->getData($status_situs);
				break;

			case 'ajax_situs':
				$data = (object) array();
				$situs_berita['select'] 	= "*";
				$situs_berita['table']		= "m_situs_berita";
				$situs_berita['where']		= "status = 1";
				$situs_berita['order']		= array("nama_situs","asc");
		    $data->situs_berita = $this->m_admin->getData($situs_berita);
				break;

			case 'ajax_media':
				$data = (object) array();
				$media['select'] 	= "*";
				$media['table']		= "m_media";
				$media['where']		= "status = 1";
				$media['order']		= array("nama_media","asc");
		        $data->media = $this->m_admin->getData($media);
				break;

			case 'ajax_chart_all':
				$data = array();
				$data = $this->ajax('ajax_chart_crawl');
				break;

			case 'ajax_chart_crawl':
				$data = array();
				$d = array();
				for($i = 0; $i < 30; $i++) {
					$newDate = date("Y-m-d", strtotime('-'. $i .' days'));
					$d[] = strval($newDate); 
				}
				// $d = array_map('strval', $d);
				// $query = "SELECT DATE(cdd) AS tgl, COUNT(id_berita) AS jumlah FROM `m_berita` WHERE `status` = 1 GROUP BY DATE(cdd) ORDER BY DATE(cdd) DESC LIMIT 30";
				$query = "SELECT DATE(cdd) AS tgl, COUNT(id_berita) AS jumlah FROM `m_berita` WHERE DATE(cdd) in ('".implode("','",$d)."') AND `status` = 1 GROUP BY DATE(cdd) ORDER BY DATE(cdd) DESC LIMIT 30";
				$data = $this->m_admin->getData_query($query);
				// var_dump($d,"<hr>",$data,"<hr>");
				// die;
				$tmpResult = array();
				$result = array();
				foreach ($d as $key => $val) {
					$result[] = array("tgl" => $val, "jumlah" => "0");
				}
				$tmpResult = $result;
				foreach ($result as $key => $val) {
					foreach ($data as $key2 => $val2) {
						// var_dump($val['tgl'],"<hr>",$val2['tgl'],"<hr>");
						// die;
						if ($val["tgl"] == $val2['tgl']) {
							$tmpResult[$key]["jumlah"] = $val2['jumlah'];
						}
					}
				}
				// var_dump($tmpResult,"<hr>");
				// die;
				$data = $tmpResult;
				break;

			case 'ajax_kata_kunci_chart':
				$data = array();				
				$d = array();
				$day = (isset($_GET['day']))? $_GET['day']:3;
				
				for($i = 0; $i < $day; $i++) {
					$newDate = date("Y-m-d", strtotime('-'. $i .' days'));
					$d[] = strval($newDate); 
				}

				$query = "SELECT kata_kunci FROM `m_kata_kunci` WHERE `status` = 1";
				$data_kata_kunci = $this->m_admin->getData_query($query);
				$result = array();
				foreach ($data_kata_kunci as $key => $val) {
					$result[$val['kata_kunci']] = 0;
				}

				$query = "SELECT kata_kunci, DATE(cdd) AS tgl FROM `m_berita` WHERE DATE(cdd) in ('".implode("','",$d)."') AND `status` = 1";
				$data = $this->m_admin->getData_query($query);
				$jumlah_berita = count($data);
				$kata_kunci = "";
				foreach ($data as $key => $val) {
					$kata_kunci = $kata_kunci.$val['kata_kunci'].";";
				}

				$kata_kunci = str_replace("; ",";",$kata_kunci);
				$arrDataKataKunci = array_filter(explode(";",$kata_kunci));
				$arrKataKunci = array_count_values($arrDataKataKunci);
				$newResult = array_merge($result,$arrKataKunci);
				// var_dump($result,"<hr>",$arrKataKunci);
				// die;
				$data = array('kata_kunci' => $newResult, 'jumlah_berita' => $jumlah_berita);
				break;

			default:
				# code...
				break;
		}
		echo json_encode($data);
	}

	public function search() {
		var_dump(base_url());die();
	}

	public function api($type) {
		switch ($type) {
			case 'get_news_rss':
				$link_rss['select'] 	= "l.*, sb.* , ss.jenis_situs";
				$link_rss['from'] 		= "m_link_rss as l";
				$link_rss['join'][0]	= array('m_situs_berita as sb','sb.id_situs_berita = l.id_situs_berita');
				$link_rss['join'][1]	= array('m_status_situs as ss','sb.id_status_situs = ss.id_status_situs');
				$link_rss['where'] 		= "l.id_link_rss = ".$_GET['id_link_rss']." and l.status = 1";
				$data['link_rss'] 		= $this->m_frontend->getData($link_rss);

				$kata_kunci['select']		= "kata_kunci";
				$kata_kunci['from']		= "m_kata_kunci";
				$kata_kunci['where']		= "status = 1";
				$data['kata_kunci']		= $this->m_frontend->getData($kata_kunci);
				// var_dump($data['link_rss'][0]['start_tag']);
				// die();
				$id_situs_berita = $data['link_rss'][0]['id_situs_berita'];
				$link = $data['link_rss'][0]['link_rss'];
				$tag = $data['link_rss'][0]['start_tag'];
				$class_val = $data['link_rss'][0]['class_tag'];
				$id_val = $data['link_rss'][0]['id_tag'];
				// var_dump($link,"<hr>");
				// die();
				$file_or_url = $this->resolveFile($link);
				// var_dump($file_or_url,"<hr>");
				// die();
				$xml=simplexml_load_file($file_or_url) or die("Error: Cannot create object");
				$i = 0;
				// var_dump("<hr>",$xml->channel->item);
				// var_dump("<hr>",$xml->channel);
				// die();
		        $post = array();
		        $posts = array();
		        $i = 0;
				foreach ($xml->channel->item as $items) {
					$post['news_valid'] = FALSE;
		        	$post['check_berita'] = 0;

		    		$post['link']  = (string) $items->link;
		            $post['date']  = (string) $items->pubDate;
		            $post['ts']    = strtotime($items->pubDate);
		            $post['title'] = (string) $items->title;

		            // Create summary as a shortened body and remove images,
		            // extraneous line breaks, etc.
		            $post['summary'] = $this->summarizeText((string) $items->description);

		            // Get Content from web page
					$dom = new DOMDocument;
					@$dom->loadHTMLFile($post['link']);
					$nodes = array();
					$nodes = $dom->getElementsByTagName($tag);
					// cek tag berdasarkan
					if (!is_null($id_val)) {
						// ID
						$element_id = $dom->getElementById($id_val);
						foreach ($data['kata_kunci'] as $val_kata_kunci) {
							if (strpos(strtolower($element_id->textContent), strtolower($val_kata_kunci["kata_kunci"]))) {
								$post['text'] = strtolower($element_id->textContent);
								$post['news_valid'] = TRUE;

					    		$check_berita['select']	= "link_berita";
					            $check_berita['from']	= "m_berita";
					            $check_berita['where']	= "link_berita = '".$post['link']."'";
					            $post['check_berita']	= $this->m_frontend->count_getData($check_berita);
					        }
					    }
					} else {
						// Class
						foreach ($nodes as $element) {
							foreach ($data['kata_kunci'] as $val_kata_kunci) {
								if (strpos(strtolower($element->textContent), strtolower($val_kata_kunci["kata_kunci"]))) {
									$classy = $element->getAttribute("class");
									if (strpos($classy, $class_val) !== false) {
										$post['text'] = strtolower($element->textContent);
										$post['news_valid'] = TRUE;

							    		$check_berita['select']	= "link_berita";
							            $check_berita['from']	= "m_berita";
							            $check_berita['where']	= "link_berita = '".$post['link']."'";
							            $post['check_berita']	= $this->m_frontend->count_getData($check_berita);
									}
								}
							}
				        }
					}

					// var_dump(json_encode($post),"<hr>");

		            if (($post['check_berita'] == 0) && ($post['news_valid'] == TRUE)) {
		            	// Insert data
		            	$berita['data'] 	= array("id_situs_berita" => $id_situs_berita, "link_berita" => $post['link'], "judul_berita" => $post['title'], "deskripsi" => $post['summary'], "isi_berita" => $post['text'], "publish_date" => date('Y-m-d H:i:s',$post['ts']), "cdd" => date('Y-m-d H:i:s'), "status" => 1);
		            	$berita['table']	= "m_berita";
		            	$this->m_admin->addData($berita);
		            }

		        }
		        // var_dump(json_encode($posts));
				break;

			case 'get_news_rss_lib':
				$id_situs_berita = $_GET['id_link_rss'];
				$link_rss['select'] 	= "l.*, sb.* , ss.jenis_situs";
				$link_rss['from'] 		= "m_link_rss as l";
				$link_rss['join'][0]	= array('m_situs_berita as sb','sb.id_situs_berita = l.id_situs_berita');
				$link_rss['join'][1]	= array('m_status_situs as ss','sb.id_status_situs = ss.id_status_situs');
				$link_rss['where'] 		= "sb.id_situs_berita = ".$id_situs_berita." and sb.status = 1";
				$data['link_rss'] 		= $this->m_frontend->getData($link_rss);

				$kata_kunci['select']		= "kata_kunci";
				$kata_kunci['from']		= "m_kata_kunci";
				$kata_kunci['where']		= "status = 1";
				$data['kata_kunci']		= $this->m_frontend->getData($kata_kunci);
				// var_dump($data['link_rss'][0]['start_tag']);
				// die();
				$id_situs_berita = $data['link_rss'][0]['id_situs_berita'];
				$link = $data['link_rss'][0]['link_rss'];
				$tag = $data['link_rss'][0]['start_tag'];
				$class_val = $data['link_rss'][0]['class_tag'];
				$id_val = $data['link_rss'][0]['id_tag'];

				$this->load->library('rssparser');							// load library
				$this->rssparser->set_feed_url($link); 	// get feed
				$this->rssparser->set_cache_life(30); 						// Set cache life time in minutes
				$rss = $this->rssparser->getFeed(10);
				// var_dump($data['link_rss'][0]);die();
				$dom = new DOMDocument;
				@$dom->loadHTMLFile($rss[0]['link']);
				$nodes = array();
				$post = array();
				$nodes = $dom->getElementsByTagName($tag);
				// cek tag berdasarkan
				if (!is_null($id_val)) {
					// ID
					$element_id = $dom->getElementById($id_val);
					foreach ($data['kata_kunci'] as $val_kata_kunci) {
						if (strpos(strtolower($element_id->textContent), strtolower($val_kata_kunci["kata_kunci"]))) {
							$post['text'] = strtolower($element_id->textContent);
							$post['news_valid'] = TRUE;

				    		$check_berita['select']	= "link_berita";
				            $check_berita['from']	= "m_berita";
				            $check_berita['where']	= "link_berita = '".$post['link']."'";
				            $post['check_berita']	= $this->m_frontend->count_getData($check_berita);
				        }
				    }
				} else {
					// Class
					foreach ($nodes as $element) {
						foreach ($data['kata_kunci'] as $val_kata_kunci) {
							if (strpos(strtolower($element->textContent), strtolower($val_kata_kunci["kata_kunci"]))) {
								$classy = $element->getAttribute("class");
								if (strpos($classy, $class_val) !== false) {
									// echo "masuk cuy";die();
									$post['text'] = strtolower($element->textContent);
									$post['news_valid'] = TRUE;

						    		$check_berita['select']	= "link_berita";
						            $check_berita['from']	= "m_berita";
						            $check_berita['where']	= "link_berita = '".$post['link']."'";
						            $post['check_berita']	= $this->m_frontend->count_getData($check_berita);
								}
							}
						}
			        }
				}
				// var_dump(json_encode($rss),"<hr>");
				// var_dump(json_encode($post),"<hr>");
				break;

			case 'get_news_crawl':
				$id_situs_berita 		= $_GET['id_situs_berita'];
				$link_rss['select'] 	= "sb.* , ss.jenis_situs";
				$link_rss['from'] 		= "m_situs_berita as sb";
				$link_rss['join'][0]	= array('m_status_situs as ss','sb.id_status_situs = ss.id_status_situs');
				$link_rss['where'] 		= "sb.id_situs_berita = ".$id_situs_berita." and sb.status = 1";
				$data['link_rss'] 		= $this->m_frontend->getData($link_rss);

				$kata_kunci['select']		= "kata_kunci";
				$kata_kunci['from']		= "m_kata_kunci";
				$kata_kunci['where']		= "status = 1";
				$data['kata_kunci']		= $this->m_frontend->getData($kata_kunci);
				// var_dump($data['link_rss'][0]['start_tag']);
				// die();
				$id_situs_berita = $data['link_rss'][0]['id_situs_berita'];
				$link = $data['link_rss'][0]['link_situs'];
				$tag_judul = $data['link_rss'][0]['start_tag_judul'];
				$class_judul_val = $data['link_rss'][0]['class_tag_judul'];
				$id_judul_val = $data['link_rss'][0]['id_tag_judul'];
				$tag_isi = $data['link_rss'][0]['start_tag_isi'];
				$class_isi_val = $data['link_rss'][0]['class_tag_isi'];
				$id_isi_val = $data['link_rss'][0]['id_tag_isi'];
				$uri_segment = $data['link_rss'][0]['uri_segment'];
				$post['news_valid'] = FALSE;
		        $post['check_berita'] = 0;

				// $nodes = array();
				$post = array();
				$posts = array();
				$data_link = array();

				// var_dump($data['link_rss'][0],"<hr>");
				// News Crawler
				$dom = new DOMDocument;
				@$dom->loadHTMLFile($link);
				$links = $dom->getElementsByTagName('a');

				// var_dump($dom,"<hr>");
				// var_dump($links,"<hr>");
				// die();
				//Iterate over the extracted links and display their URLs
				foreach ($links as $link){
				    // Extract and show the "href" attribute.
					$link_val = $link->getAttribute('href');
					$real_link = $link_val;
				    $link_val = preg_replace('#^https?://#', '', $link_val);
				    if (substr($link_val, -1) == "/") {
				    	$link_val = substr($link_val, 0, -1);
				    }

				    $temp_link_val = explode("/", $link_val);
				    if (count($temp_link_val) == $uri_segment) {
				    	array_push($data_link, $real_link);
				    }
				}
				// var_dump($data_link,"<hr>");
				// die();
				// Cek Duplikasi
				$data_link = array_unique($data_link);
				// var_dump($data_link,"<hr>");
				// die();

 				foreach ($data_link as $key => $value) {
					$post['news_valid'] = FALSE;
		        	$post['check_berita'] = 0;
					$post = array();

					@$dom->loadHTMLFile($value);
					$nodes_isi = array();
					$nodes_judul = array();
					$nodes_isi = $dom->getElementsByTagName($tag_isi);
					$nodes_judul = $dom->getElementsByTagName($tag_judul);
					// var_dump($value,"<hr>");
					// Get Judul
					$post['judul_berita'] = "";
					if (!is_null($id_judul_val)) {
						// Id
						$element_judul = $dom->getElementById($id_judul_val);
						if (!is_null($element_judul)) {
							$post['judul_berita']	= $element_judul->textContent;
						}
						// var_dump($element_judul,"<hr>");
						// die();
					} else {
						// Class
						foreach ($nodes_judul as $element_judul) {
							if (!is_null($element_judul)) {
								if ($element_judul->textContent != "") {
									$classy_judul = $element_judul->getAttribute("class");
									if (strpos($classy_judul, $class_judul_val) !== false) {
										$post['judul_berita']	= $element_judul->textContent;
									}
								}
							}
							// var_dump($element_judul,"<hr>");
							// die();
						}
					}

					// foreach ($nodes_judul as $element_judul) {
					// 	$post['judul_berita'] = "";
					// 	if ($element_judul->textContent != "") {
					// 		$classy_judul = $element_judul->getAttribute("class");
					// 		if (strpos($classy_judul, $class_judul_val) !== false) {
					// 			$post['judul_berita']	= $element_judul->textContent;
					// 		}
					// 	}
					// }


					// Get Isi
					// cek tag berdasarkan
					// var_dump($value,"<hr>");
					if (!is_null($id_isi_val)) {
						// ID
						// echo "masuk id <hr>";
						$element_id = $dom->getElementById($id_isi_val);
						// var_dump($element_id->textContent,"<hr>");
						if (!is_null($element_id)) {
							foreach ($data['kata_kunci'] as $val_kata_kunci) {
								if (strpos(strtolower($element_id->textContent), strtolower($val_kata_kunci["kata_kunci"]))) {
									$post['link_berita']	= $value;
									$post['crawl_date']		= date('Y-m-d H:i:s');
									$post['isi_berita']		= strtolower($element_id->textContent);
									$post['news_valid']		= TRUE;

						    		$check_berita['select']	= "link_berita";
						            $check_berita['from']	= "m_berita";
						            $check_berita['where']	= "link_berita = '".$post['link_berita']."' OR judul_berita = '".$post['judul_berita']."'";
						            $post['check_berita']	= $this->m_frontend->count_getData($check_berita);
						            if (($post['check_berita'] == 0) && ($post['news_valid'] == TRUE)) {
										// Insert data
										$berita['data'] 	= array("id_situs_berita" => $id_situs_berita, "link_berita" => $post['link_berita'], "judul_berita" => $post['judul_berita'], "deskripsi" => "", "isi_berita" => $post['isi_berita'], "publish_date" => date('Y-m-d H:i:s'), "cdd" => $post['crawl_date'], "status" => 1);
										$berita['table']	= "m_berita";
										$this->m_admin->addData($berita);
										array_push($posts, $post);
									}
						            break;
						        }
						    }
						}
					} else {
						// Class
						// echo "masuk class <hr>";
						foreach ($nodes_isi as $element_isi) {
							// var_dump($element,"<hr>");
							if(!empty($element_isi->textContent)){
								foreach($data['kata_kunci'] as $val_kata_kunci){
									if (strpos(strtolower($element_isi->textContent), strtolower($val_kata_kunci["kata_kunci"]))) {
										$classy = $element_isi->getAttribute("class");
										if (strpos($classy, $class_isi_val) !== false) {
											// echo "masuk cuy";die();
											$post['link_berita']	= $value;
											$post['crawl_date']		= date('Y-m-d H:i:s');
											$post['isi_berita']		= strtolower($element_isi->textContent);
											$post['news_valid']		= TRUE;

											$check_berita['select']	= "link_berita";
											$check_berita['from']	= "m_berita";
											$check_berita['where']	= "link_berita = '".$post['link_berita']."' OR judul_berita = '".$post['judul_berita']."'";
											$post['check_berita']	= $this->m_frontend->count_getData($check_berita);
											// var_dump($post,"<hr>");
											// if($post['check_berita'] && $post['news_valid']){
											if (($post['check_berita'] == 0) && ($post['news_valid'] == TRUE)) {
												// Insert data
												$berita['data'] 	= array("id_situs_berita" => $id_situs_berita, "link_berita" => $post['link_berita'], "judul_berita" => $post['judul_berita'], "deskripsi" => "", "isi_berita" => $post['isi_berita'], "publish_date" => date('Y-m-d H:i:s'), "cdd" => $post['crawl_date'], "status" => 1);
												$berita['table']	= "m_berita";
												$this->m_admin->addData($berita);
												array_push($posts, $post);
											}
											// }
											break;
										}
									}
								}
							}
				        }
					}
				}
				echo json_encode(array("success"=>true, "data"=>$posts));
				break;
			default:
				# code...
				break;
		}
	}

	public function resolveFile($file_or_url) {
		if (!preg_match('|^https?:|', $file_or_url))
			$feed_uri = $_SERVER['DOCUMENT_ROOT'] .'/shared/xml/'. $file_or_url;
		else
			$feed_uri = $file_or_url;

		return $feed_uri;
	}

	public function summarizeText($summary) {
		$summary = strip_tags($summary);

		// Truncate summary line to 100 characters
		$max_len = 100;
		if (strlen($summary) > $max_len)
		$summary = substr($summary, 0, $max_len) . '...';

		return $summary;
	}

	public function get_string_between($string, $start, $end){
		$string = ' ' . $string;
		// var_dump($string,"<hr>");
		// var_dump($start,"<hr>");
		// var_dump($end,"<hr>");
		// die();
		$ini = strpos($string, $start);
		if ($ini == 0) return '';
		$ini += strlen($start);
		$len = strpos($string, $end, $ini) - $ini;
		return substr($string, $ini, $len);
	}
}
?>
