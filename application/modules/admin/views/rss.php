<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo @$title; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('admin/rss'); ?>" ><i class="fa fa-dashboard"></i> <?php echo ucwords($page['p']); ?></a></li>
        <li class="active"><?php echo ucwords($page['c']); ?></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">

      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true" title="Link RSS Aktif">Link RSS</a></li>
          <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false" title="Link RSS Tidak Aktif">Link RSS Tidak Aktif</a></li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="tab_1">
              <button class="btn btn-success fa fa-plus" onclick="add();"> Tambah Link RSS </button>
              <table id="rss" class="table table-bordered text-center" style="width: 100% !important;">
                <thead>
                  <tr>
                    <td>No</td>
                    <td>Nama Situs</td>
                    <td>Link RSS</td>
                    <td>Perintah</td>
                  </tr>
                </thead>
              </table>
          </div>

          <!-- /.tab-pane -->
          <div class="tab-pane" id="tab_3">
            <table id="trash_rss" class="table table-bordered text-center" style="width: 100% !important;">
              <thead>
                  <tr>
                    <td>No</td>
                    <td>Nama Situs</td>
                    <td>Link RSS</td>
                    <td>Perintah</td>
                  </tr>
                </thead>
            </table>
          </div>

        </div>
      <!-- /.tab-content -->
      </div>
    </section>
    <!-- /.content -->

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-admin">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="modal_header"></h4>
      </div>
      <div class="modal-body" id="modal_body">
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">

var table;
var table_trash;

$(document).ready(function() {

    //datatables
    //function kategori() {
      table = $('#rss').DataTable({

        "processing"  : true, //Feature control the processing indicator.
        "serverSide"  : true, //Feature control DataTables' server-side processing mode.
        "searchDelay" : 0.5 * 1000,
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('admin/admin/ajax_list?type=rss')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        {
            "targets": [ 3 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
      });
    //}

    //function trash_kategori() {
      table_trash = $('#trash_rss').DataTable({

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "searchDelay" : 0.5 * 1000,
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('admin/admin/ajax_list?type=del_rss')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        {
            "targets": [ 3 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
      });
    //}
    //}
    // Setiap 1 menit perbarui data
    setInterval( function () {
      table.ajax.reload(null,false);
      table_trash.ajax.reload(null,false);
    }, 60 * 1000 );

    jQuery.validator.setDefaults({
      debug: true,
      success: "valid"
    });
    var action
    var action_label

});

function add() {
  action = "<?php echo base_url('admin/rss/tambah')?>";
  action_label = "Tambah";
  $.ajax({ url : "<?php echo base_url('admin/rss/tambah')?>",
      type: "GET",
      dataType: "JSON",
      async: false,
      success: function(data){
        $('#myModal #modal_header').html("");
        $('#myModal #modal_body').html("");

        $('#myModal #modal_header').append(data.proses);
        $('#myModal #modal_body').append(data.content);
        $('#myModal').modal({backdrop: 'static', keyboard: false});
      },
      error: function (jqXHR, textStatus, errorThrown){
          swal("Error!", "Gagal memperoleh data dari ajax!", "error");
      }
  });

}

function edit(id) {
  action = "<?php echo base_url('admin/rss/ubah')?>";
  action_label = "Ubah";
  $.ajax({ url : "<?php echo base_url('admin/rss/ubah/')?>"+id,
      type: "GET",
      dataType: "JSON",
      async: false,
      success: function(data){
        $('#myModal #modal_header').html("");
        $('#myModal #modal_body').html("");

        $('#myModal #modal_header').append(data.proses);
        $('#myModal #modal_body').append(data.content);
        $('#myModal').modal({backdrop: 'static', keyboard: false});
      },
      error: function (jqXHR, textStatus, errorThrown){
          swal("Error!", "Gagal memperoleh data dari ajax!", "error");
      }
  });

}

function save() {
    var form = $( "#myform" );
    form.validate();
    if (form.valid()) {
      $("#submit_btn").attr('disabled','disabled');
      $("#submit_btn").val('Sedang menyimpan data!');
      $("#cancel_btn").attr('style','display:none;');
      $.ajax({ url : action,
          type: "POST",
          data : form.serialize(),
          dataType: "JSON",
          async: false,
          success: function(data){
            table.ajax.reload(null,false);
            $('#myModal').modal("hide");
          },
          error: function (jqXHR, textStatus, errorThrown){
            // $("#submit_btn").removeAttr("disabled");
            // $("#submit_btn").val(action_label);
            // $("#cancel_btn").attr('style','display:block;');
            swal("Error!", "Gagal memperoleh data dari ajax!", "error");
          }
      });
    } else {

    }
}
</script>
