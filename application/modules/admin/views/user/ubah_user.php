<div id="area_input_new" style="display:none"></div>
<div id="content">
  <section class="content-header">
    <h1>Ubah User -  <?php echo $title?> <small> <b>Dashboard</b> Control panel</small> </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('admin/home') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url('admin/user') ?>"> User</a></li>
        <li class="active">Ubah User</a></li>
      </ol>
  </section>
  <section class='content'>
    <div class='row'>    
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-body">
            <?php if ($this->session->flashdata('error')) { ?>
              <div class="alert alert-danger">
                <strong><?= $this->session->flashdata('error') ?></strong>
              </div>
            <?php } ?>
              <div class="form-group">
                <label for="nama">Nama <font color="red">*</font></label>
                <input class="form-control" id="nama" placeholder="Nama User" type="text" name="nama" value="<?php echo @$nama ?>" required>
              </div>

              <div class="form-group">
                <label for="username">Username <font color="red">*</font></label>
                <input class="form-control" id="username" placeholder="Username" type="text" name="username" value="<?php echo @$username ?>" required>
              </div>

              <div class="form-group">
                <label for="password">Passwors <font color="red">*</font></label>
                <input class="form-control" id="password" placeholder="Password" type="password" name="password" value="" required>
              </div>

              <div class="form-group">
                <label for="hak_akses">Hak Akses <font color="red">*</font></label>
                <select class="form-control" id="hak_akses" name="hak_akses" required>
                  <option value="1" <?php if ($u->user_type_id == "1") { echo "selected"; } ?> >Super Admin</option>
                </select>
              </div>

              <div class="form-group">
                <input type="submit" name="submit" value="Ubah" class="btn btn-success">
                <a href="<?= base_url('admin/user'); ?>" class="btn btn-warning">Batal</a>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </section>
</div>

<script type="text/javascript">
$(document).ready(function () {
    akses();
    getopendata()
});

function akses() {
  var src = $('#hak_akses').val();
  if (src == "1") {
    $('div#div_sub_ktda').hide();
  } else {
    $('div#div_sub_ktda').show();
  }
}

function getopendata(){
    //alert("came");
var nip = $("#nip").val();// value in field email
var nama = $("#nama").val();// value in field email
var jabatan = $("#jabatan").val();// value in field email
var nama_unor = $("#nama_unor").val();// value in field email
var nama_opd = $("#nama_opd").val();// value in field email
$('#div_loading').append('<p id="p_loading">Loading...</p>');
$.ajax({
    type:'post',
        url:'<?php echo base_url('admin/api/pegawaibynip/')?>',// put your real file name 
        data:{nip: nip},
        success:function(res){
          data = JSON.parse(res);
          document.getElementById("nama").value = data['nama'];
          document.getElementById("jabatan").value = data['jabatan'];
          document.getElementById("nama_unor").value = data['nama_unor'];
          document.getElementById("nama_opd").value = data['nama_opd'];
          $('#div_loading').html(''); 
        }
 });
} 

$( "form" ).submit(function(e) {
  if ($('#hak_akses').val() == 1) {
    return;
  } else {
    if( ($(".sub_ktda").is(':checked')) || ($(".kecamatan").is(':checked')) ) {
      return;
    } else {
      alert('Hak akses harus dipilih!!!');
      e.preventDefault(e);
    }
  }  
});

</script>