
<form id="myform" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-body">
      <?php if ($this->session->flashdata('error')) { ?>
        <div class="alert alert-danger">
          <strong><?= $this->session->flashdata('error') ?></strong>
        </div>
      <?php } ?>
        <input type="hidden" name="id_user" id="id_user" value="<?php echo @$user_detail[0]['id_user'] ?>">
        <input type="hidden" name="id_user_type" id="id_user_type" value="2">
        <input type="hidden" name="submit" id="submit" value="submit">

        <div class="form-group">
          <label for="nama">Nama <font color="red">*</font></label>
          <input class="form-control" id="nama" placeholder="Nama User" type="text" name="nama" value="<?php echo @$user_detail[0]['nama'] ?>" required>
        </div>

        <div class="form-group">
          <label for="email">Email <font color="red">*</font></label>
          <input class="form-control" id="email" placeholder="Email" type="email" name="email" value="<?php echo @$user_detail[0]['email'] ?>" required>
        </div>

        <?php if (strtolower($proses) !== "ubah") { ?>
        <?php } ?>
        <div class="form-group">
          <label for="password">Passwors <font color="red">*</font></label>
          <input class="form-control" id="password" placeholder="Password" type="password" name="password" value="" required>
          <div class="clearfix"><br></div>
          <span class="btn btn-primary" onclick="genPass();">Generate Password</span>
        </div>

        <div class="form-group">
          <label for="repassword">Re-Passwors <font color="red">*</font></label>
          <input class="form-control" id="repassword" placeholder="Re-Password" type="password" name="repassword" value="" required>
        </div>

        <div class="form-group">
          <span id="text_submit"></span>
          <!-- <input type="submit" id="submit_btn" name="submit" value="<?php echo $proses; ?>" class="btn btn-success"> -->
          <input type="submit" id="submit_btn" name="submit" value="<?php echo $proses; ?>" class="btn btn-success" onclick="save();">          
          <button type="button" id="cancel_btn" class="btn btn-warning" data-dismiss="modal">Batal</button>
        </div>
      </div>
    </div>
  </div>
</form>

<script type="text/javascript">
  $(document).ready(function () {
      // akses();
      // getopendata();

    jQuery.validator.setDefaults({
      debug: true,
      success: "valid"
    });
    $( "#myform" ).validate({
      rules: {
        nama: "required",
        email: {
          required: true,
          email: true
        },
        password: {
          required: true,
          minlength: 10
        },
        repassword: {
          equalTo: "#password"
        }
      }
    });
  });

  $('#submit_btn').on('click',function () {
    // $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
    // // $('#submit_btn').attr('disabled',true); //set button disable
    // $('#submit_btn').attr('style','display:none;'); // hide button
    // $('#cancle_btn').attr('style','display:none;'); // hide button
  });

  $( "form" ).submit(function(e) {
    // $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
    // $('#submit_btn').attr('disabled',true); //set button disable
    // $('#submit_btn').attr('style','display:none;'); // hide button
    // $('#cancle_btn').attr('style','display:none;'); // hide button
    // if ($('#hak_akses').val() == 1) {
    //   return;
    // } else {
    //   if( ($(".sub_ktda").is(':checked')) || ($(".kecamatan").is(':checked')) ) {
    //     return;
    //   } else {
    //     alert('Hak akses harus dipilih!!!');
    //     e.preventDefault(e);
    //   }
    // }
  });
</script>
