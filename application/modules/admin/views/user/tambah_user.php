
<div id="area_input_new" style="display:none"></div>
<div id="content">
  <section class="content-header">
    <h1>Tambah User - <?= $title; ?> <small> <b>Dashboard</b> Control panel</small> </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('admin/home') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url('admin/user') ?>"> User</a></li>
        <li class="active">Tambah User</a></li>
      </ol>
  </section>
  <section class='content'>
    <div class='row'>    
      <form action="<?= base_url('admin/user/tambah'); ?>" method="post" id="theForm" name="theForm" enctype="multipart/form-data">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-body">
            <?php if ($this->session->flashdata('error')) { ?>
              <div class="alert alert-danger">
                <strong><?= $this->session->flashdata('error') ?></strong>
              </div>
            <?php } ?>              

              <div id="div_nip" class="form-group">
                <label for="nip">NIP <font color="red">*</font></label>
                <input class="form-control" id="nip" placeholder="Masukan NIP" type="text" name="nip" maxlength="18" pattern="[0-9]{18}" required onchange="getopendata()">
              </div>

              <div id="div_loading">
              </div>

              <div class="form-group">
                <label for="nama">Nama Pegawai <font color="red">*</font></label>
                <input class="form-control" id="nama" placeholder="Nama Pegawai" type="text" name="nama" value="" readonly required>
              </div>

              <div class="form-group">
                <label for="jabatan">Jabatan Pegawai <font color="red">*</font></label>
                <input class="form-control" id="jabatan" placeholder="Jabatan Pegawai" type="text" name="jabatan" value="" readonly required>
              </div>

              <div class="form-group">
                <label for="nama_unor">Nama Unor <font color="red">*</font></label>
                <input class="form-control" id="nama_unor" placeholder="Nama Unor" type="text" name="nama_unor" value="" readonly required>
              </div>

              <div class="form-group">
                <label for="nama_opd">Nama OPD <font color="red">*</font></label>
                <input class="form-control" id="nama_opd" placeholder="Nama OPD" type="text" name="nama_opd" value="" readonly required>
              </div>

              <div class="form-group">
                <label for="hak_akses">Hak Akses <font color="red">*</font></label>
                <select class="form-control" id="hak_akses" name="hak_akses" onchange="akses()" required>
                  <option value="1">Super Admin</option>
                  <option value="2">Operator</option>
                </select>
              </div>

              <div id="div_sub_ktda" class="form-group">
                <label>Hak Akses Kategori</label><br>

                <h3>KTDA</h3><br> 
                <div class="row">              
                  <?php $i=0; foreach ($ktda as $k) { $i++ ?>
                    <div class="col-md-4">
                      <fieldset>
                        <legend><?= $k->judul; ?></legend>
                        <?php 
                          foreach ($sub_ktda as $sk) { 
                            if ($k->id == $sk->parent_id) {
                              echo '<label><input type="checkbox" class="sub_ktda" name="sub_ktda[]" value="'.$sk->id.'">'.$sk->judul.'</label><br>';
                            }
                        ?>
                        <?php } ?>
                      </fieldset>
                    </div>
                  <?php if ($i==3) { $i=0; echo "<div class='clearfix'></div>"; } } ?>                
                </div>

                <h3>KCDA</h3><br>
                <div class="row">
                  <div class="col-md-12">
                  <?php foreach ($m_kecamatan as $kec): ?>
                    <label><input type="checkbox" class="kecamatan" name="kecamatan[]" value="<?= $kec->id; ?>"><?= $kec->nama; ?></label><br>
                  <?php endforeach ?>
                  </div>
                </div>

              </div>          

              <div class="form-group">
                <input id="submit" type="submit" name="submit" value="Kirim" class="btn btn-success">
                <a href="<?= base_url('admin/user'); ?>" class="btn btn-warning">Batal</a>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </section>
</div>

<script type="text/javascript">
$(document).ready(function () {
    akses();
});

function akses() {
  var src = $('#hak_akses').val();
  if (src == "1") {
    $('div#div_sub_ktda').hide();
  } else {
    $('div#div_sub_ktda').show();
  }
}

function getopendata(){
    //alert("came");
var nip = $("#nip").val();// value in field email
var nama = $("#nama").val();// value in field email
var jabatan = $("#jabatan").val();// value in field email
var nama_unor = $("#nama_unor").val();// value in field email
var nama_opd = $("#nama_opd").val();// value in field email
$('#div_loading').append('<p id="p_loading">Loading...</p>');
$.ajax({
    type:'post',
        url:'<?php echo base_url('admin/api/pegawaibynip/')?>',// put your real file name 
        data:{nip: nip},
        success:function(res){
          data = JSON.parse(res);
          document.getElementById("nama").value = data['nama'];
          document.getElementById("jabatan").value = data['jabatan'];
          document.getElementById("nama_unor").value = data['nama_unor'];
          document.getElementById("nama_opd").value = data['nama_opd'];
          $('#div_loading').html(''); 
        }
 });
} 

$( "form" ).submit(function(e) {
  if ($('#hak_akses').val() == 1) {
    return;
  } else {
    if( ($(".sub_ktda").is(':checked')) || ($(".kecamatan").is(':checked')) ) {
      return;
    } else {
      alert('Hak akses harus dipilih!!!');
      e.preventDefault(e);
    }
  }  
});

</script>
