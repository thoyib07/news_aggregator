
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/'); ?>bower_components/multiselect/css/multi-select.css" />
<script type="text/javascript" src="<?php echo base_url('assets/'); ?>bower_components/multiselect/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets/'); ?>bower_components/jquery-quicksearch/jquery.quicksearch.js"></script>

<form method="post" id="myform" name="myform" enctype="multipart/form-data">
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-body">
      <?php if ($this->session->flashdata('error')) { ?>
        <div class="alert alert-danger">
          <strong><?= $this->session->flashdata('error') ?></strong>
        </div>
      <?php } ?>

          <input type="hidden" name="id_setting_project" id="id_setting_project" value="<?php echo @$setting_project_detail[0]['id_setting_project'] ?>">
          <input type="hidden" name="submit" id="submit" value="submit">

          <div class="form-group">
            <label for="nama_setting_project">Nama Project <font color="red">*</font></label><br>
            <input class="form-control" id="nama_setting_project" placeholder="Nama Project" type="text" name="nama_setting_project" value="<?php echo @$setting_project_detail[0]['nama_setting_project'] ?>" required>
          </div>

          <div class="form-group">
            <label for="desc_setting_project">Deskripsi Project <font color="red">*</font></label><br>
            <textarea name="desc_setting_project" id="desc_setting_project" class="form-control" cols="2" rows="10" style="resize:none; height:100px;" required><?php echo @$setting_project_detail[0]['desc_setting_project'] ?></textarea>
          </div>          
          
          <div class="form-group col-sm-12">
            <label>Kata Kunci:</label>
            <?php 
              if (isset($setting_project_detail[0]['list_kata_kunci'])) {
                $setting_project_detail[0]['list_kata_kunci'] = str_replace('|','',$setting_project_detail[0]['list_kata_kunci']);
                $arr_kata_kunci = explode(",",$setting_project_detail[0]['list_kata_kunci']); 
              }
            ?>
            <select id='list_kata_kunci' name='list_kata_kunci[]' multiple='multiple' required>
            <?php foreach ($kata_kunci_detail as $key => $val) { 
              $selected = "";
              if (isset($arr_kata_kunci)) {
                if (in_array($val['id_kata_kunci'],$arr_kata_kunci)) {
                  $selected = "selected='selected'";
                }
              }
              echo "<option value='".$val['id_kata_kunci']."' ".$selected.">".$val['kata_kunci']."</option>";
            } ?>
            </select>
          </div> 
                  
          <div class="form-group col-sm-12">
            <label>User Akses:</label>
            <?php 
              if (isset($setting_project_detail[0]['user_access'])) {
                $setting_project_detail[0]['user_access'] = str_replace('|','',$setting_project_detail[0]['user_access']);
                $arr_user_access = explode(",",$setting_project_detail[0]['user_access']); 
              }
            ?>
            <select id='user_access' name='user_access[]' multiple='multiple'>
            <?php foreach ($user_detail as $key => $val) {               
              $selected = "";
              if (isset($arr_user_access)) {
                if (in_array($val['id_user'],$arr_user_access)) {
                  $selected = "selected='selected'";
                }
              }
              echo "<option value='".$val['id_user']."' ".$selected.">".$val['nama']."</option>";
            } ?>
            </select>
          </div>

          <div class="clearfix"></div>
          <div class="form-group">
            <span id="text_submit"></span>
            <input type="submit" id="submit_btn" name="submit" value="<?php echo $proses; ?>" class="btn btn-success" onclick="save();">
            <!-- <a href="<?php echo $back; ?>" id="cancle_btn" class="btn btn-warning">Batal</a> -->
            <button type="button" id="cancel_btn" class="btn btn-warning" data-dismiss="modal">Batal</button>
          </div>
      </div>
    </div>
  </div>
</form>
<script type="text/javascript">
  $(function () {
    $("#list_kata_kunci").multiSelect({
      selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='Cari Kata Kunci'>",
      selectionHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='Cari Kata Kunci Terpilih'>",
      afterInit: function(ms){
        var that = this,
            $selectableSearch = that.$selectableUl.prev(),
            $selectionSearch = that.$selectionUl.prev(),
            selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
            selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

        that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
        .on('keydown', function(e){
          if (e.which === 40){
            that.$selectableUl.focus();
            return false;
          }
        });

        that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
        .on('keydown', function(e){
          if (e.which == 40){
            that.$selectionUl.focus();
            return false;
          }
        });
      },
      afterSelect: function(){
        this.qs1.cache();
        this.qs2.cache();
      },
      afterDeselect: function(){
        this.qs1.cache();
        this.qs2.cache();
      }
    });

    $("#user_access").multiSelect({
      selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='Cari User'>",
      selectionHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='Cari User Terpilih'>",
      afterInit: function(ms){
        var that = this,
            $selectableSearch = that.$selectableUl.prev(),
            $selectionSearch = that.$selectionUl.prev(),
            selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
            selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

        that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
        .on('keydown', function(e){
          if (e.which === 40){
            that.$selectableUl.focus();
            return false;
          }
        });

        that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
        .on('keydown', function(e){
          if (e.which == 40){
            that.$selectionUl.focus();
            return false;
          }
        });
      },
      afterSelect: function(){
        this.qs1.cache();
        this.qs2.cache();
      },
      afterDeselect: function(){
        this.qs1.cache();
        this.qs2.cache();
      }
    });

    setInterval( function () {
    }, 60 * 1000 );
  });
</script>
