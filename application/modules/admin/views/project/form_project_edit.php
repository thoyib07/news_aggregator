<style>
  .slidecontainer {
    width: 100%;
  }

  .slider {
    -webkit-appearance: none;
    width: 100%;
    height: 25px;
    background: #d3d3d3;
    outline: none;
    opacity: 0.7;
    -webkit-transition: .2s;
    transition: opacity .2s;
  }

  .slider:hover {
    opacity: 1;
  }

  .slider::-webkit-slider-thumb {
    -webkit-appearance: none;
    appearance: none;
    width: 25px;
    height: 25px;
    background: #4CAF50;
    cursor: pointer;
  }

  .slider::-moz-range-thumb {
    width: 25px;
    height: 25px;
    background: #4CAF50;
    cursor: pointer;
  }
</style>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/'); ?>bower_components/multiselect/css/multi-select.css" />
<link rel="stylesheet" href="<?php echo base_url('assets/'); ?>bower_components/bootstrap-daterangepicker/daterangepicker.css">
<link rel="stylesheet" href="<?php echo base_url('assets/'); ?>bower_components/bootstrap-tagsinput/css/bootstrap-tagsinput.css">

<script src="<?php echo base_url('assets/'); ?>bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets/'); ?>bower_components/multiselect/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets/'); ?>bower_components/jquery-quicksearch/jquery.quicksearch.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets/'); ?>bower_components/bootstrap-tagsinput/js/bootstrap-tagsinput.js"></script>

<script type="text/javascript" src="<?php echo base_url('assets/'); ?>bower_components/highcharts/highcharts.js"></script>
<!-- <script type="text/javascript" src="<?php echo base_url('assets/'); ?>bower_components/highcharts/modules/exporting.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets/'); ?>bower_components/highcharts/modules/export-data.js"></script> -->
<script src="<?php echo base_url('assets/'); ?>bower_components/highcharts/modules/accessibility.js"></script>

  <div class="col-md-12">
    <div class="box box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Edit Setting Project</h3>
      </div>
      <div class="box-body">
        <div class="box-group" id="accordion">
          <form method="post" id="formSearch" name="formSearch" enctype="multipart/form-data">
            <div class="panel box box-primary">
              <div class="box-header with-border">
                <h4 class="box-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#boxSettingProject" aria-expanded="true" class="collapsed">
                  Setting Project
                  </a>
                </h4>
              </div>
              <div id="boxSettingProject" class="panel-collapse collapse in" aria-expanded="true" style="">
                <div class="box-body">
                  <?php if ($this->session->flashdata('error')) { ?>
                  <div class="alert alert-danger">
                    <strong><?= $this->session->flashdata('error') ?></strong>
                  </div>
                  <?php } ?>
                  <input type="hidden" name="id_setting_project" id="id_setting_project" value="<?php echo @$setting_project_detail[0]['id_setting_project'] ?>">
                  <input type="hidden" name="submit" id="submit" value="submit">

                  <div class="form-group col-sm-12">
                    <label for="nama_setting_project">Nama Project <font color="red">*</font></label><br>
                    <input class="form-control" id="nama_setting_project" placeholder="Nama Project" type="text" name="nama_setting_project" value="<?php echo @$setting_project_detail[0]['nama_setting_project'] ?>" required>
                  </div>

                  <div class="form-group col-sm-12">
                    <label for="desc_setting_project">Deskripsi Project <font color="red">*</font></label><br>
                    <textarea name="desc_setting_project" id="desc_setting_project" class="form-control" cols="2" rows="10" style="resize:none; height:100px;" required><?php echo @$setting_project_detail[0]['desc_setting_project'] ?></textarea>
                  </div>
                  
                  <div class="form-group col-sm-12">
                    <label>User Akses:</label>
                    <?php $setting_project_detail[0]['user_access'] = str_replace('|','',$setting_project_detail[0]['user_access']); ?>
                    <?php $arr_user_access = explode(",",$setting_project_detail[0]['user_access']); ?>
                    <select id='user_access' name='user_access[]' multiple='multiple'>
                    <?php foreach ($user_detail as $key => $val) { 
                      $selected = (in_array($val['id_user'],$arr_user_access)) ? "selected='selected'" : "" ;
                      echo "<option value='".$val['id_user']."' ".$selected.">".$val['nama']."</option>";
                    } ?>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="panel box box-warning">
              <div class="box-header with-border">
                <h4 class="box-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#boxSettingPencarian" class="collapsed" aria-expanded="false">
                    Setting Pencarian
                  </a>
                </h4>
              </div>
              <div id="boxSettingPencarian" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                <div class="box-body">
                  <div class="clearfix"></div>
                  <div class="form-group col-sm-12">
                    <input id="kata_pencarian" name="kata_pencarian" class="form-control input-lg" type="text" placeholder="Cari Berita" value="<?php echo @$setting_project_detail[0]['kata_pencarian'] ?>">
                  </div>
                  <div class="form-group slidecontainer col-sm-12">
                    <input type="range" min="0" max="100" value="<?php echo ($setting_project_detail[0]['threshold'] !== "") ? $setting_project_detail[0]['threshold'] : '90'?>" class="slider" id="threshold" name="threshold">
                    <p>Tingkat Kemiripan : <span id="demo"></span> %</p>
                  </div>                  
                  <div class="form-group col-sm-12">
                    <label for="lvl_situs">Tingkat Kepercayaan Situs Berita</label>
                    <br>
                    <div class="checkbox-inline"><label><input type="checkbox" name="lvl_situs" value="1" checked="checked"> Kurang</label></div>
                    <div class="checkbox-inline"><label><input type="checkbox" name="lvl_situs" value="2" checked="checked"> Normal</label></div>
                    <div class="checkbox-inline"><label><input type="checkbox" name="lvl_situs" value="3" checked="checked"> Baik</label></div>
                  </div>
                  <div class="form-group col-sm-12">
                    <label>Waktu Publish:</label>
                    <div class="input-group">
                      <button type="button" class="btn btn-default pull-right" id="daterange-btn">
                        <span>
                          <i class="fa fa-calendar"></i> Pilih waktu publish
                        </span>
                        <i class="fa fa-caret-down"></i>
                      </button>
                    </div>
                  </div>
                  <div class="form-group col-sm-6">
                    <label>Kata Positif:</label>
                    <div class="input-group" style="width: 100%;">
                      <input id="kata_positif" name="kata_positif" class="form-control input-lg" style="width: 100%;" value="<?php echo @$setting_project_detail[0]['kata_positif'] ?>" data-role="tagsinput">
                      <p><span style="color:red;">*</span> Pisah kata dengan tanda koma (",")</p>
                    </div>
                  </div>
                  <div class="form-group col-sm-6">
                    <label>Kata Negatif:</label>
                    <div class="input-group" style="width: 100%;">
                      <input id="kata_negatif" name="kata_negatif" class="form-control input-lg" style="width: 100%;" value="<?php echo @$setting_project_detail[0]['kata_negatif'] ?>" data-role="tagsinput">
                      <p><span style="color:red;">*</span> Pisah kata dengan tanda koma (",")</p>
                    </div>
                  </div>
                  <div class="form-group col-sm-12">
                      <input type="hidden" id="startDate" name="startDate" value="<?php echo @$setting_project_detail[0]['startDate'] ?>">
                      <input type="hidden" id="endDate" name="endDate" value="<?php echo @$setting_project_detail[0]['endDate'] ?>">
                      <input type="hidden" id="id_berita" name="id_berita" value="<?php echo @$setting_project_detail[0]['id_berita'] ?>">
                  </div>
                  <div class="form-group col-sm-12">
                    <span id="text_submit"></span>
                    <!-- <input type="submit" id="submit_btn" name="submit" value="<?php echo $proses; ?>" class="btn btn-success" onclick="save();"> -->            
                    <span id="submit_btn" class="btn btn-success" onClick="save();" style="display:none;"> Simpan</span>
                    <span id="btnSearch" class="btn btn-info" onClick="searchNews(); document.getElementById('submit_btn').style.display = '';"> Cari</span>
                    <a href="<?php echo $back; ?>" id="cancle_btn" class="btn btn-warning">Batal</a>
                    <!-- <button type="button" id="cancel_btn" class="btn btn-warning" data-dismiss="modal">Batal</button> -->
                  </div>
                </div>
              </div>
            </div>
            <div class="panel box box-success">
              <div class="box-header with-border">
                <h4 class="box-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#boxHasilPencarian" class="collapsed" aria-expanded="false" onclick="togleResult();">
                    Hasil Pencarian
                  </a>
                </h4>
              </div>
              <div id="boxHasilPencarian" style="display:none;" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                <div class="box-body">
                  <div id="divGraphPencarian" style="/* display:none; */">
                  </div>
                  <div id="divHasilPencarian" style="/* display:none; */">
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
<script type="text/javascript">
  var action = "<?php echo base_url('admin/project/ubah')?>";
  var slider = document.getElementById("threshold");
  var output = document.getElementById("demo");
  var dataUserSelected;
  var togleRes = 0;
  limitTopWord = 20;
  $(function () {
    var spanStartDate, spanEndDate = "";
    <?php if (is_null($setting_project_detail[0]['startDate'])) { ?>
      $('#startDate').val(moment().subtract(6, 'days').format('YYYY-MM-DD')); 
      spanStartDate = moment().subtract(6, 'days').format('MMMM D, YYYY');
    <?php } else { ?>
      // $('#startDate').val("<?php echo @$setting_project_detail[0]['startDate'] ?>");  
      spanStartDate = "<?php echo @$setting_project_detail[0]['startDateView'] ?>";
    <?php } ?>

    <?php if (is_null($setting_project_detail[0]['endDate'])) { ?>
      $('#endDate').val(moment().format('YYYY-MM-DD'));
      spanEndDate = moment().format('MMMM D, YYYY');
    <?php } else { ?>
      // $('#endDate').val("<?php echo @$setting_project_detail[0]['endDate'] ?>");
      spanEndDate = "<?php echo @$setting_project_detail[0]['endDateView'] ?>";
    <?php } ?>

    $('#daterange-btn span').html(spanStartDate + ' - ' + spanEndDate);
    output.innerHTML = slider.value;

    slider.oninput = function() {
      output.innerHTML = this.value;
    }

    $('#daterange-btn').daterangepicker(
        {
            ranges   : {
              'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
              'Last 30 Days': [moment().subtract(29, 'days'), moment()],
              'Last 90 Days (3 Months)': [moment().subtract(89, 'days'), moment()],
              'Last 180 Days (6 Months)': [moment().subtract(179, 'days'), moment()]
            },
            startDate: moment().subtract(6, 'days'),
            endDate  : moment()
        },
        function (start, end) {
            $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            $('#startDate').val(start.format('YYYY-MM-DD'));
            $('#endDate').val(end.format('YYYY-MM-DD'));
        }
    );
    $('.daterangepicker .ranges').attr('style','overflow: auto; height: 200px;');
    dataUserSelected = "<?php echo $setting_project_detail[0]['user_access']; ?>";

    $("#user_access").multiSelect({
      selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='Cari User'>",
      selectionHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='Cari User Terpilih'>",
      afterInit: function(ms){
        var that = this,
            $selectableSearch = that.$selectableUl.prev(),
            $selectionSearch = that.$selectionUl.prev(),
            selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
            selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

        that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
        .on('keydown', function(e){
          if (e.which === 40){
            that.$selectableUl.focus();
            return false;
          }
        });

        that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
        .on('keydown', function(e){
          if (e.which == 40){
            that.$selectionUl.focus();
            return false;
          }
        });
      },
      afterSelect: function(){
        this.qs1.cache();
        this.qs2.cache();
      },
      afterDeselect: function(){
        this.qs1.cache();
        this.qs2.cache();
      }
    });

    searchNewsProject();
    setInterval( function () {
    }, 60 * 1000 );
  });  

  function save() {
      var form = $( "#formSearch" );
      form.validate();
      if (form.valid()) {
        $("#submit_btn").attr('disabled','disabled');
        $("#submit_btn").val('Sedang menyimpan data!');
        $("#cancel_btn").attr('style','display:none;');
        $.ajax({ url : action,
            type: "POST",
            data : form.serialize(),
            dataType: "JSON",
            async: false,
            success: function(data){
              // table.ajax.reload(null,false);
              // $('#myModal').modal("hide"); 
              if (data.status) { 
                document.location.href = "<?php echo base_url('admin/project') ?>";
              }
            },
            error: function (jqXHR, textStatus, errorThrown){
              // $("#submit_btn").removeAttr("disabled");
              // $("#submit_btn").val(action_label);
              // $("#cancel_btn").attr('style','display:block;');
              swal("Error!", "Gagal memperoleh data dari ajax!", "error");
            }        
        });
      } else {

      }
  }

  function togleResult() {
    if (togleRes) {
      $("#boxHasilPencarian").hide();
      togleRes = 0;
    } else {
      $("#boxHasilPencarian").show();
      togleRes = 1;
    }
  }
</script>
