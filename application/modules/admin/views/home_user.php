<style>
	.slidecontainer {
		width: 100%;
	}

	.slider {
		-webkit-appearance: none;
		width: 100%;
		height: 25px;
		background: #d3d3d3;
		outline: none;
		opacity: 0.7;
		-webkit-transition: .2s;
		transition: opacity .2s;
	}

	.slider:hover {
		opacity: 1;
	}

	.slider::-webkit-slider-thumb {
		-webkit-appearance: none;
		appearance: none;
		width: 25px;
		height: 25px;
		background: #4CAF50;
		cursor: pointer;
	}

	.slider::-moz-range-thumb {
		width: 25px;
		height: 25px;
		background: #4CAF50;
		cursor: pointer;
	}
</style>
<!-- Morris charts -->
<!-- 
-->
<link rel="stylesheet" href="<?php echo base_url('assets/'); ?>bower_components/morris.js/morris.css">
<script src="<?php echo base_url('assets/'); ?>bower_components/raphael/raphael.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>bower_components/morris.js/morris.min.js"></script> 

<!-- Tagsinput -->
<link rel="stylesheet" href="<?php echo base_url('assets/'); ?>bower_components/bootstrap-tagsinput/css/bootstrap-tagsinput.css">
<script type="text/javascript" src="<?php echo base_url('assets/'); ?>bower_components/bootstrap-tagsinput/js/bootstrap-tagsinput.js"></script>

<!-- Highcharts -->
<link rel="stylesheet" href="<?php echo base_url('assets/'); ?>bower_components/highcharts/css/highcharts.css">
<script src="<?php echo base_url('assets/'); ?>bower_components/highcharts/highcharts.js"></script>
<script src="<?php echo base_url('assets/'); ?>bower_components/highcharts/modules/exporting.js"></script>
<script src="<?php echo base_url('assets/'); ?>bower_components/highcharts/modules/export-data.js"></script>
<script src="<?php echo base_url('assets/'); ?>bower_components/highcharts/modules/accessibility.js"></script>

<link rel="stylesheet" href="<?php echo base_url('assets/'); ?>bower_components/bootstrap-daterangepicker/daterangepicker.css">
<script src="<?php echo base_url('assets/'); ?>bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- Content Header (Page header) -->
<!-- Main content -->
<section class="content">

	<!-- Default box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">News Aggregator</h3>
		</div>
		<div class="box-body">
			<div class="row" style="margin-left: 10px;">
				<?php foreach ($detail_project as $key => $val) { ?>
				<div class="col-lg-3 col-xs-6">
					<div class="setting-project small-box" 
							onclick="setDataBerita(this);" 
							style="cursor: pointer; background-color: #c3c3c3;"							 
							data-id_setting_project="<?= $val['id_setting_project'] ?>"
							data-list_kata_kunci="<?= substr(str_replace("|","",$val['list_kata_kunci']),0,-1) ?>"
						>
						<div class="inner">
							<h3 style="white-space:normal;"><?= $val['nama_setting_project'] ?></h3>
							<p><?= $val['desc_setting_project'] ?></p>
						</div>
						<div class="icon">
							<i class="ion ion-stats-bars"></i>
						</div>
						<!-- <a href="#" class="small-box-footer"> -->
						<span class="small-box-footer" >
							
						</span>
						<!-- </a> -->
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
  
  <div class="box-group" id="dataSet">
    <div class="panel box box-primary">
      <div class="box-header with-border">
        <h4 class="box-title">
          <a data-toggle="collapse" data-parent="#dataSet" href="#collapseDataSet" aria-expanded="true" class="">
            Data Set Project
          </a>
        </h4>
      </div>
      <div id="collapseDataSet" class="panel-collapse collapse in" aria-expanded="true" style="">
        <div class="box-body">
          <div id="divDataSet"></div>
        </div>
      </div>
    </div>
  </div>

	<div class="box">
    <form action="" method="post" id="formSearch" name="formSearch">
      <div class="box-header with-border">
        <h3 class="box-title"><?php echo ADMIN_TITLE; ?> </h3>
      </div>
      <div class="box-body">
        <div class="form-group">
          <select name="id_pencarian_setting_project" id="id_pencarian_setting_project" class="form-control input-lg" onchange="setFormPencarian(this);">
            <option>Pilih History Pencarian</option>
            <?php foreach ($detail_pencarian_setting_project as $key => $val) { ?>
            <option value="<?= $val['id_pencarian_setting_project'] ?>"
              data-kata_pencarian="<?= $val['kata_pencarian']; ?>"
              data-id_setting_project="<?= $val['id_setting_project']; ?>"
              data-list_kata_kunci="<?= substr(str_replace("|","",$val['list_kata_kunci']),0,-1); ?>"
              data-threshold="<?= $val['threshold']; ?>"
              data-lvl_situs="<?= $val['lvl_situs']; ?>"
              data-startDate="<?= $val['startDate']; ?>"
              data-endDate="<?= $val['endDate']; ?>"
              data-kata_positif="<?= $val['kata_positif']; ?>"
              data-kata_negatif="<?= $val['kata_negatif']; ?>"
              data-id_berita="<?= $val['id_berita']; ?>"
            >
              <?= $val['kata_pencarian']." ( ".$val['cdd']." )  *".$val['nama'];?>
            </option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group">
          <input id="kata_pencarian" name="kata_pencarian" class="form-control input-lg" type="text" placeholder="Cari Berita">
          <!-- <span class="input-group-btn">
            <i class="btn btn-info btn-flat fa fa-search"></i>
          </span> -->
          <input type="hidden" id="id_setting_project" name="id_setting_project">
          <input type="hidden" id="list_kata_kunci" name="list_kata_kunci"> 
          <input type="hidden" id="id_berita" name="id_berita">          
        </div>
        <div class="clearfix"><br></div>
        <div class="box-group" id="accordion">
          <div class="panel box box-warning">
            <div class="box-header with-border">
              <h4 class="box-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" class="collapsed">
                  Pengaturan Pencarian
                </a>
              </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
              <div class="box-body">
                <div class="form-group slidecontainer">
                  <input type="range" min="0" max="100" value="90" class="slider" id="threshold" name="threshold">
                  <p>Tingkat Kemiripan : <span id="demo"></span> %</p>
                </div>
                <div class="form-group">
                  <label for="lvl_situs">Tingkat Kepercayaan Situs Berita</label>
                  <br>
                  <div class="checkbox-inline"><label><input type="checkbox" name="lvl_situs" value="1" checked="checked"> Kurang</label></div>
                  <div class="checkbox-inline"><label><input type="checkbox" name="lvl_situs" value="2" checked="checked"> Normal</label></div>
                  <div class="checkbox-inline"><label><input type="checkbox" name="lvl_situs" value="3" checked="checked"> Baik</label></div>
                </div>
                <div class="form-group">
                  <label>Waktu Publish:</label>
                  <div class="input-group">
                    <button type="button" class="btn btn-default pull-right" id="daterange-btn">
                      <span>
                        <i class="fa fa-calendar"></i> Pilih waktu publish
                      </span>
                      <i class="fa fa-caret-down"></i>
                    </button>
                    <input type="hidden" id="startDate" name="startDate">
                    <input type="hidden" id="endDate" name="endDate">
                  </div>
                </div>                    
                <div class="form-group col-sm-6">
                  <label>Kata Positif:</label>
                  <div class="input-group" style="width: 100%;">
                    <input id="kata_positif" name="kata_positif" class="form-control input-lg" style="width: 100%;" value="<?php echo @$setting_project_detail[0]['kata_positif'] ?>" data-role="tagsinput">
                    <p><span style="color:red;">*</span> Pisah kata dengan tanda koma (",")</p>
                  </div>
                </div>
                <div class="form-group col-sm-6">
                  <label>Kata Negatif:</label>
                  <div class="input-group" style="width: 100%;">
                    <input id="kata_negatif" name="kata_negatif" class="form-control input-lg" style="width: 100%;" value="<?php echo @$setting_project_detail[0]['kata_negatif'] ?>" data-role="tagsinput">
                    <p><span style="color:red;">*</span> Pisah kata dengan tanda koma (",")</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="box-footer">
          <span id="btnSearch" class="btn btn-info btn-flat fa fa-search pull-right" onClick="searchNews();"> Cari</span>
          <span id="btnSearch" class="btn btn-success btn-flat fa fa-search pull-right" onClick="saveHistoryPencarian();"> Simpan Pencarian</span>
      </div>
    </form>
  </div>

  <div class="box" id="boxHasilPencarian" style="display:none;">
		<div class="box-header with-border">
			<h3 class="box-title">Hasil Pencarian</h3>
		</div>
		<div id="divGraphPencarian" class="box-body">
		</div>
		<div id="divHasilPencarian" class="box-body">
		</div>
	</div>

	<script type="text/javascript">
        var area;
        var data_chart;
        var slider = document.getElementById("threshold");
        var output = document.getElementById("demo");
        $(function () {

          $('#startDate').val(moment().subtract(6, 'days').format('YYYY-MM-DD'));
          $('#endDate').val(moment().format('YYYY-MM-DD'));
          $('#daterange-btn span').html(moment().subtract(6, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
          output.innerHTML = slider.value;

          slider.oninput = function() {
            output.innerHTML = this.value;
          }
          // get_data_chart();
          // build_line_chart();
          $('#daterange-btn').daterangepicker(
              {
                  ranges   : {
                    'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'Last 90 Days (3 Months)': [moment().subtract(89, 'days'), moment()],
                    'Last 180 Days (6 Months)': [moment().subtract(179, 'days'), moment()]
                  },
                  startDate: moment().subtract(6, 'days'),
                  endDate  : moment()
              },
              function (start, end) {
                  $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                  $('#startDate').val(start.format('YYYY-MM-DD'));
                  $('#endDate').val(end.format('YYYY-MM-DD'));
              }
          );

          setInterval( function () {
            // area.redraw();
            // get_data_chart();
          }, 60 * 1000 );
        }); 
		// function showResProject(id) {
		// 	alert("show hasil")
		// } 
	</script>
</section>