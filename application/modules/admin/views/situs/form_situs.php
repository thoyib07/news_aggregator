<?php
// var_dump($action);
?>
<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/'); ?>bower_components/ckeditor/dataTables.bootstrap.css"> -->
<form method="post" id="myform" name="myform" enctype="multipart/form-data" data-parsley-validate="" novalidate="">
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-body">
      <?php if ($this->session->flashdata('error')) { ?>
        <div class="alert alert-danger">
          <strong><?= $this->session->flashdata('error') ?></strong>
        </div>
      <?php } ?>

          <input type="hidden" name="id_situs_berita" id="id_situs_berita" value="<?php echo @$situs_berita_detail[0]['id_situs_berita'] ?>">
          <input type="hidden" name="submit" id="submit" value="submit">

          <div class="form-group">
            <label for="nama_situs">Nama Situs <font color="red">*</font></label>
            <input type="text" class="form-control" name="nama_situs" id="nama_situs" value="<?php echo @$situs_berita_detail[0]['nama_situs'] ?>"  data-parsley-type="alphanum" required>
          </div>

          <div class="form-group">
            <label for="link_situs">Link Situs <font color="red">*</font></label>
            <input type="url" class="form-control" name="link_situs" id="link_situs" value="<?php echo @$situs_berita_detail[0]['link_situs'] ?>" data-parsley-type="url" required>
          </div>

          <div class="form-group">
            <label for="uri_segment">Uri Segment Berita<font color="red">*</font></label>
            <input type="text" class="form-control" name="uri_segment" id="uri_segment" min="2" max="15" value="<?php echo @$situs_berita_detail[0]['uri_segment'] ?>" data-parsley-type="number" required>
          </div>

          <div class="form-group">
            <label for="lvl_situs">Tingkat Kepercayaan Situs Berita<font color="red">*</font></label>
            <br>
            <div class="radio-inline"><label><input type="radio" name="lvl_situs" id="lvl_situs" value="1" <?php echo (@$situs_berita_detail[0]['lvl_situs'] == 1)? 'checked="checked"':''; ?>> Kurang</label></div>
            <div class="radio-inline"><label><input type="radio" name="lvl_situs" id="lvl_situs" value="2" <?php echo (@$situs_berita_detail[0]['lvl_situs'] == 2)? 'checked="checked"':''; ?>> Normal</label></div>
            <div class="radio-inline"><label><input type="radio" name="lvl_situs" id="lvl_situs" value="3" <?php echo (@$situs_berita_detail[0]['lvl_situs'] == 3)? 'checked="checked"':''; ?>> Baik</label></div>
          </div>

          <div class="box-group">

            <div class="panel box box-primary">
              <div class="box-header with-border">
                <h4 class="box-title"><a data-toggle="collapse" href="#group_judul" aria-expanded="true" class="">Pencarian Judul Berita</a></h4>
              </div>

              <div id="group_judul" class="panel-collapse collapse in">
                <div class="box-body">

                  <div class="form-group">
                    <label for="start_tag_judul">Tag Judul Berita <font color="red">*</font></label>
                    <input type="text" class="form-control" name="start_tag_judul" id="start_tag_judul" value="<?php echo @$situs_berita_detail[0]['start_tag_judul'] ?>" required>
                  </div>

                  <div class="form-group">
                    <label for="id_cari_tag_judul">Pencari Tag Judul Berita Berdasarkan <font color="red">*</font></label>
                    <select class="form-control " id="id_cari_tag_judul" name="id_cari_tag_judul" required>
                      <option>Pilih Jenis Pencarian Tag</option>
                      <option value="id" <?php if (@$situs_berita_detail[0]['id_cari_tag_judul'] == "id") { echo "selected";}?> > ID </option>
                      <option value="class" <?php if (@$situs_berita_detail[0]['id_cari_tag_judul'] == "class") { echo "selected";}?> > CLASS </option>
                    </select>
                  </div>

                  <div class="form-group">
                    <label for="cari_val_judul">Nama Class/Id Judul Berita </label>
                    <input type="text" class="form-control" name="cari_val_judul" id="cari_val_judul" value="<?php echo @$situs_berita_detail[0]['cari_val_judul'] ?>">
                  </div>


                </div>
              </div>
            </div>

            <div class="panel box box-success">
              <div class="box-header with-border">
                <h4 class="box-title"><a data-toggle="collapse" href="#group_isi">Pencarian Isi Berita</a></h4>
              </div>

              <div id="group_isi" class="panel-collapse collapse in">
                <div class="box-body">

                  <div class="form-group">
                    <label for="start_tag_isi">Tag Isi Berita <font color="red">*</font></label>
                    <input type="text" class="form-control" name="start_tag_isi" id="start_tag_isi" value="<?php echo @$situs_berita_detail[0]['start_tag_isi'] ?>" required>
                  </div>

                  <div class="form-group">
                    <label for="id_cari_tag_isi">Pencari Tag Isi Berita Berdasarkan <font color="red">*</font></label>
                    <select class="form-control " id="id_cari_tag_isi" name="id_cari_tag_isi" required>
                      <option>Pilih Jenis Pencarian Tag</option>
                      <option value="id" <?php if (@$situs_berita_detail[0]['id_cari_tag_isi'] == "id") { echo "selected";}?> > ID </option>
                      <option value="class" <?php if (@$situs_berita_detail[0]['id_cari_tag_isi'] == "class") { echo "selected";}?> > CLASS </option>
                    </select>
                  </div>

                  <div class="form-group">
                    <label for="cari_val_isi">Nama Class/Id Isi Berita </label>
                    <input type="text" class="form-control" name="cari_val_isi" id="cari_val_isi" value="<?php echo @$situs_berita_detail[0]['cari_val_isi'] ?>">
                  </div>

                </div>
              </div>
            </div>

          </div>
          <div class="form-group">
            <button type="button" id="test_btn" class="btn btn-primary" onclick="testKoneksi();">Test Koneksi</button>
          </div>
        <div class="clearfix"></div>
        <div class="form-group">
          <span id="text_submit"></span>
          <!-- <input type="submit" id="submit_btn" name="submit" value="<?php echo $proses; ?>" class="btn btn-success" onclick="save();"> -->
          <!-- <a href="<?php echo $back; ?>" id="cancle_btn" class="btn btn-warning">Batal</a> -->
          <button type="button" id="submit_btn" class="btn btn-success" onclick="save();"><?php echo $proses; ?></button>
          <button type="button" id="cancel_btn" class="btn btn-warning" data-dismiss="modal">Batal</button>
        </div>
      </div>
    </div>
  </div>
</form>

<script type="text/javascript">
$(document).ready(function () {
    // akses();
    // getopendata();
    // $('.select2').select2({ width: '100%' });

    var target_status_situs = "#id_status_situs";
    get_status_situs(target_status_situs);

    var target_media = "#id_media";
    get_media(target_media);

    <?php if (strtolower($proses) == "ubah") { ?>

      $("#id_status_situs").val("<?php echo @$situs_berita_detail[0]['id_status_situs'] ?>");
      $("#id_media").val("<?php echo @$situs_berita_detail[0]['id_media'] ?>");
    <?php } ?>
});

function get_status_situs(target) {
  $.ajax({ url : "<?php echo base_url('admin/ajax/ajax_status_situs')?>",
      type: "GET",
      dataType: "JSON",
      async: false,
      success: function(data){
        $(target).empty();
        $(target).append('<option value="">Pilih Status Situs</option>');
        for (var i = 0; i < data.status_situs.length; i++) {
          $(target).append('<option value=' + data.status_situs[i].id_status_situs + '>' + data.status_situs[i].jenis_situs.toUpperCase() + '</option>');
        }
      },
      error: function (jqXHR, textStatus, errorThrown){
          alert('Gagal memperoleh data status situs');
      }
  });
}

function get_media(target) {
  $.ajax({ url : "<?php echo base_url('admin/ajax/ajax_media')?>",
      type: "GET",
      dataType: "JSON",
      async: false,
      success: function(data){
        $(target).empty();
        $(target).append('<option value="">Pilih Media Group</option>');
        for (var i = 0; i < data.media.length; i++) {
          $(target).append('<option value=' + data.media[i].id_media + '>' + data.media[i].nama_media.toUpperCase() + '</option>');
        }
      },
      error: function (jqXHR, textStatus, errorThrown){
          alert('Gagal memperoleh data status situs');
      }
  });
}

function testKoneksi() {
  var form = $("#myform");
  $.ajax({ url : "<?= base_url('cronjob/cronjob_news_aggregator.php')?>",
      type: "POST",
      data: form.serialize(),
      dataType: "JSON",
      async: false,
      success: function(data){
        // console.log(data);
        if(data.success){
          swal("Berhasil!", data.msg, "success");
        } else {
          swal("Perhatian!", data.msg, "error");
        }
      },
      error: function (jqXHR, textStatus, errorThrown){
        swal("Perhatian!", "Terjadi kesalahan mengirim data!", "error");
      }
  });
}

</script>
