<section class="content-header">
  <h1>
    <?php echo @$title; ?>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url('admin/lokasi'); ?>" ><i class="fa fa-dashboard"></i> <?php echo ucwords($page['p']); ?></a></li>
    <li class="active"><?php echo ucwords($page['c']); ?></li>
  </ol>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">Lokasi Provinsi, Kabupaten/Kota, Kecamatan & Kelurahan</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <form method="post" id="myform" name="myform" enctype="multipart/form-data">
        <input type="hidden" name="submit" id="submit" value="submit">
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="no_prop">Provinsi <font color="red">*</font></label><br>
                <select class="form-control" name="no_prop" id="no_prop" onchange="getKab('#no_kab');" required>
                </select>
              </div>
              <div class="form-group">
                <label for="no_kab">Kabupaten / Kota </label><br>
                <select class="form-control" name="no_kab" id="no_kab" onchange="getKec('#no_kec');">
                </select>
              </div>
              <div class="form-group">
                <label for="no_kec">Kecamatan </label><br>
                <select class="form-control" name="no_kec" id="no_kec" onchange="getKel('#no_kel');">
                </select>
              </div>
              <div class="form-group">
                <label for="no_kel">Kelurahan </label><br>
                <select class="form-control" name="no_kel" id="no_kel">
                </select>
              </div>
              <div class="form-group">
                <div class="col-md-6">
                  <label for="lat">Latitude <font color="red">*</font></label><br>
                  <input class="form-control" type="text" name="lat" id="lat" required>
                </div>
                <div class="col-md-6">
                  <label for="long">Longitude <font color="red">*</font></label><br>
                  <input class="form-control" type="text" name="long" id="long" required>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="box-footer">          
          <div class="form-group pull-right">
            <span class="btn btn-danger">Reset</span>
            <span class="btn btn-success" onclick="update()">Submit</span>
          </div>
        </div>
        </form>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">
  var action
  var action_label
  let no_prop, no_kab, no_kec;
  $(document).ready(function() {
    getProv('#no_prop');  
    $('#no_prop').change(function(){
      // alert($('#no_prop option:selected').data('lat'));
      // console.log($('#no_prop option:selected').data('lat'),$('#no_prop option:selected').data('long'));
      $('#lat').val($('#no_prop option:selected').data('lat'));
      $('#long').val($('#no_prop option:selected').data('long'));
    }); 
    $('#no_kab').change(function(){
      // alert($('#no_prop option:selected').data('lat'));
      // console.log($('#no_prop option:selected').data('lat'),$('#no_prop option:selected').data('long'));
      $('#lat').val($('#no_kab option:selected').data('lat'));
      $('#long').val($('#no_kab option:selected').data('long'));
    });
  });

  function update() {
    var form = $( "#myform" );
    var dataForm = form.serialize();
    console.log(form.validate());
    // if(($( "#no_prop" ).val() !== "")&&($( "#lat" ).val() !== "")&&($( "#long" ).val() !== "")){
    form.validate()
    if(form.valid()){
      $.ajax({ url : "<?php echo base_url('admin/lokasi/ubah/')?>",
          type: "POST",
          data: dataForm,
          dataType: "JSON",
          async: false,
          success: function(data){
            swal("Berhasil!", "Data lokasi berhasil diperbarui", "success");
            // location.reload();
          },
          error: function (jqXHR, textStatus, errorThrown){
              swal("Error!", "Gagal memperoleh data dari ajax!", "error");
          }        
      });
    } else {
      swal("Peringatan!", "Silahkan pilih lokasi terlebih dahulu!!", "warning");
    }    
  }  

  function getProv(target) {
    $(target).html("");
    var url = "<?php echo base_url('api/get_data/propinsi'); ?>";
    $.ajax({ url : url,
      type : 'GET',
      dataType : "JSON",
      success: function(data) {
        $(target).append("<option value=''> Pilih Provinsi </option>");
        Object.keys(data.lokasi).forEach(function(el){
          $(target).append("<option value='"+data.lokasi[el].NO_PROP+"' data-lat='"+data.lokasi[el].LAT+"' data-long='"+data.lokasi[el].LONG+"'>"+data.lokasi[el].NAMA_PROP+"</option>");
        });
        // $(target).html(JSON.parse(data));
        // $(target).html((data));
      },
      error: function(jqXHR, textStatus, errorThrown){      
        swal("Terjadi Kesalahan!", "Terjadi Kesalahan Dalam Mengambil Data!", "error");
      }
    });
  }

  function getKab(target) {
    no_prop = $('#no_prop').val();
    $(target).html("");
    $.ajax({ url : '<?php echo base_url('api/get_data/kabupaten'); ?>',
      type : 'GET',
      data:{no_prop: no_prop},
      dataType: "JSON",
      success: function(data){
        // $(target).html(data);
        $(target).append("<option value=''> Pilih Kabupaten / Kota </option>");
        Object.keys(data.lokasi).forEach(function(el){
          $(target).append("<option value='"+data.lokasi[el].NO_KAB+"' data-lat='"+data.lokasi[el].LAT+"' data-long='"+data.lokasi[el].LONG+"'>"+data.lokasi[el].NAMA_KAB+"</option>");
        });
      },
      error: function (jqXHR, textStatus, errorThrown){
          swal("Terjadi Kesalahan!", "Terjadi Kesalahan Dalam Mengambil Data!", "error");
      }
    });
  }

  function getKec(target) {
    no_prop = $('#no_prop').val();
    no_kab = $('#no_kab').val();
    $(target).html("");
    $.ajax({ url : '<?php echo base_url('api/get_data/kecamatan?data_type=html'); ?>',
      type : 'get',
      data:{no_prop: no_prop, no_kab: no_kab},
      dataType: "JSON",
      success: function(data){
        $(target).html(data);
      },
      error: function (jqXHR, textStatus, errorThrown){
          swal("Terjadi Kesalahan!", "Terjadi Kesalahan Dalam Mengambil Data!", "error");
      }
    });
  }

  function getKel(target) {
    no_prop = $('#no_prop').val();
    no_kab = $('#no_kab').val();
    no_kec = $('#no_kec').val();
    $(target).html("");
    $.ajax({ url : '<?php echo base_url('api/get_data/kelurahan?data_type=html'); ?>',
      type : 'get',
      data:{no_prop: no_prop, no_kab: no_kab, no_kec: no_kec},
      dataType: "JSON",
      success: function(data){
        $(target).html(data);
      },
      error: function (jqXHR, textStatus, errorThrown){
          swal("Terjadi Kesalahan!", "Terjadi Kesalahan Dalam Mengambil Data!", "error");
      }
    });
  }
</script>