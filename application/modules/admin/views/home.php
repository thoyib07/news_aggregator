<style>
    .slidecontainer {
        width: 100%;
    }

    .slider {
        -webkit-appearance: none;
        width: 100%;
        height: 25px;
        background: #d3d3d3;
        outline: none;
        opacity: 0.7;
        -webkit-transition: .2s;
        transition: opacity .2s;
    }

    .slider:hover {
        opacity: 1;
    }

    .slider::-webkit-slider-thumb {
        -webkit-appearance: none;
        appearance: none;
        width: 25px;
        height: 25px;
        background: #4CAF50;
        cursor: pointer;
    }

    .slider::-moz-range-thumb {
        width: 25px;
        height: 25px;
        background: #4CAF50;
        cursor: pointer;
    }
</style>
<!-- Morris charts -->
<!-- 
-->
<link rel="stylesheet" href="<?php echo base_url('assets/'); ?>bower_components/morris.js/morris.css">
<script src="<?php echo base_url('assets/'); ?>bower_components/raphael/raphael.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>bower_components/morris.js/morris.min.js"></script> 

<!-- Tagsinput -->
<link rel="stylesheet" href="<?php echo base_url('assets/'); ?>bower_components/bootstrap-tagsinput/css/bootstrap-tagsinput.css">
<script type="text/javascript" src="<?php echo base_url('assets/'); ?>bower_components/bootstrap-tagsinput/js/bootstrap-tagsinput.js"></script>

<!-- Highcharts -->
<link rel="stylesheet" href="<?php echo base_url('assets/'); ?>bower_components/highcharts/css/highcharts.css">
<script src="<?php echo base_url('assets/'); ?>bower_components/highcharts/highcharts.js"></script>
<script src="<?php echo base_url('assets/'); ?>bower_components/highcharts/modules/exporting.js"></script>
<script src="<?php echo base_url('assets/'); ?>bower_components/highcharts/modules/export-data.js"></script>
<script src="<?php echo base_url('assets/'); ?>bower_components/highcharts/modules/accessibility.js"></script>

<link rel="stylesheet" href="<?php echo base_url('assets/'); ?>bower_components/bootstrap-daterangepicker/daterangepicker.css">
<script src="<?php echo base_url('assets/'); ?>bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<section class="content">
    <?php 
        $newList = "";
        foreach ($list_kata_kunci as $key => $val) {
            $newList .= $val['id_kata_kunci'].",";
        }
        $newList = substr($newList,0,-1);
    ?>
    <!-- Default box -->
    <div class="box">
        <form action="" method="post" id="formSearch" name="formSearch">
            <div class="box-header with-border">
                <h3 class="box-title text-bold"><?php echo ADMIN_TITLE; ?> </h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <input id="kata_pencarian" name="kata_pencarian" class="form-control input-lg" type="text" placeholder="Cari Berita">
                    <!-- <span class="input-group-btn">
                    <i class="btn btn-info btn-flat fa fa-search"></i>
                    </span> -->
                    <input type="hidden" id="list_kata_kunci" name="list_kata_kunci" value="<?= $newList; ?>">
                </div>
                <div class="clearfix"><br></div>
                <div class="box-group" id="accordion">
                    <div class="panel box box-warning">
                        <div class="box-header with-border">
                            <h4 class="box-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" class="collapsed">
                                    Pengaturan Pencarian
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                            <div class="box-body">
                                <div class="form-group slidecontainer">
                                    <input type="range" min="0" max="100" value="65" class="slider" id="threshold" name="threshold">
                                    <p>Tingkat Kemiripan : <span id="demo"></span> %</p>
                                </div>
                                <div class="form-group">
                                    <label for="lvl_situs">Tingkat Kepercayaan Situs Berita</label>
                                    <br>
                                    <div class="checkbox-inline"><label><input type="checkbox" name="lvl_situs[]" id="lvl_situs1" class="check-lvl-situs" value="1" checked="checked"> Kurang</label></div>
                                    <div class="checkbox-inline"><label><input type="checkbox" name="lvl_situs[]" id="lvl_situs2" class="check-lvl-situs" value="2" checked="checked"> Normal</label></div>
                                    <div class="checkbox-inline"><label><input type="checkbox" name="lvl_situs[]" id="lvl_situs3" class="check-lvl-situs" value="3" checked="checked"> Baik</label></div>
                                </div>
                                <div class="form-group">
                                    <label>Waktu Publish:</label>
                                    <div class="input-group">
                                        <button type="button" class="btn btn-default pull-right" id="daterange-btn">
                                            <span>
                                            <i class="fa fa-calendar"></i> Pilih waktu publish
                                            </span>
                                            <i class="fa fa-caret-down"></i>
                                        </button>
                                        <input type="hidden" id="startDate" name="startDate">
                                        <input type="hidden" id="endDate" name="endDate">
                                    </div>
                                </div>   
                                <div class="row">        
                                    <div class="form-group col-sm-6">
                                        <label>Kata Positif:</label>
                                        <div class="input-group" style="width: 100%;">
                                            <input id="kata_positif" name="kata_positif" class="form-control input-lg" style="width: 100%;" value="<?php echo @$setting_project_detail[0]['kata_positif'] ?>" data-role="tagsinput">
                                            <p><span style="color:red;">*</span> Pisah kata dengan tanda koma (",")</p>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label>Kata Negatif:</label>
                                        <div class="input-group" style="width: 100%;">
                                            <input id="kata_negatif" name="kata_negatif" class="form-control input-lg" style="width: 100%;" value="<?php echo @$setting_project_detail[0]['kata_negatif'] ?>" data-role="tagsinput">
                                            <p><span style="color:red;">*</span> Pisah kata dengan tanda koma (",")</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <span id="btnSearch" class="btn btn-kopasus btn-flat fa fa-search pull-right text-white" onClick="searchNews();"> Cari</span>
            </div>
        </form>
    </div>

    <div class="box" id="boxHasilPencarian" style="display:none;">
        <div class="box-header with-border">
            <h3 class="box-title">Hasil Pencarian</h3>
        </div>
        <div id="divGraphPencarian" class="box-body">
        </div>
        <div id="divHasilPencarian" class="box-body">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title text-red">Rekap Jumlah Data Crawl 30 Hari Terakhir</h3>
                </div>

                <div class="box-body chart-responsive">
                    <div class="chart" id="crawl-chart" style="height: 300px;"></div>
                    <!-- <div class="chart" id="container-chart" style="height: 300px;"></div> -->
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title text-red">Rekap Data Crawl Berdasarkan Kata Kunci</h3>
                </div>

                <div class="box-body chart-responsive">
                    <select name="kataKunciDay" id="kataKunciDay" class="form-control" onchange="get_kata_kunci_chart();">
                    <option value="3" selected>3 Hari Terakhir</option>
                    <option value="7">7 Hari Terakhir</option>
                    <option value="14">14 Hari Terakhir</option>
                    </select>
                    <div class="clearfix"><hr></div>
                    <div class="chart row mx-0" id="kataKunci-chart" style="/*height: 300px;*/"></div>
                    <!-- <div class="chart" id="container-chart" style="height: 300px;"></div> -->
                </div>
            </div>
        </div>
    </div>

    <div id="chart"></div>

    <script type="text/javascript">
        var area;
        var data_chart;
        var data_chart_kata_kunci;
        var slider = document.getElementById("threshold");
        var output = document.getElementById("demo");
        $(function () {

            $('#startDate').val(moment().subtract(6, 'days').format('YYYY-MM-DD'));
            $('#endDate').val(moment().format('YYYY-MM-DD'));
            $('#daterange-btn span').html(moment().subtract(6, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
            output.innerHTML = slider.value;

            slider.oninput = function() {
                output.innerHTML = this.value;
            }
            get_data_chart();
            get_kata_kunci_chart();
            // build_line_chart();
            $('#daterange-btn').daterangepicker(
                {
                    ranges   : {
                    'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'Last 90 Days (3 Months)': [moment().subtract(89, 'days'), moment()],
                    'Last 180 Days (6 Months)': [moment().subtract(179, 'days'), moment()]
                    },
                    startDate: moment().subtract(6, 'days'),
                    endDate  : moment()
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                    $('#startDate').val(start.format('YYYY-MM-DD'));
                    $('#endDate').val(end.format('YYYY-MM-DD'));
                }
            );

            setInterval( function () {
            // area.redraw();
            get_data_chart();
            // get_kata_kunci_chart();
            }, 5 * (60 * 1000 ));
        });

        function get_data_chart() {
            $.ajax({ url : "<?php echo base_url('admin/ajax/ajax_chart_crawl')?>",
                type: "GET",
                dataType: "JSON",
                async: false,
                success: function(data){
                    // console.log(data);
                    data_chart = data;
                    Morris.Area({
                        element   : 'crawl-chart',
                        resize    : true,
                        data      : data,
                        xkey      : 'tgl',
                        ykeys     : ['jumlah'],
                        labels    : ['Data yang diperoleh'],
                        lineColors: ['#3c8dbc'],
                        hideHover : 'auto'
                    });
                },
                error: function (jqXHR, textStatus, errorThrown){
                    swal('Gagal memperoleh data dari ajax','error');
                    window.location.reload(true);
                }
            });
        }

        function get_kata_kunci_chart() {
            var valDay = $("#kataKunciDay").val();
            $.ajax({ url : "<?php echo base_url('admin/ajax/ajax_kata_kunci_chart')?>",
                type: "GET",
                data: {day: valDay },
                dataType: "JSON",
                async: false,
                success: function(data){
                    $("#kataKunci-chart").html("");
                    // console.log(data.kata_kunci);
                    data_chart_kata_kunci = data.kata_kunci;
                    data_jumlah_berita = data.jumlah_berita;
                    $("#kataKunci-chart").append('<div class="col-lg-12 col-xs-12"> <div class="small-box bg-kopasus2 px-3 row d-flex" style="border-radius: 25px"> <div class="icon col-2 pr-3" style="right: 0px; position: relative;"> <i class="ion ion-stats-bars"></i> </div> <div class="inner col-8 d-flex align-content-center"> <h3>Total berita selama '+valDay+' hari</h3> </div> <div class="col-2 d-flex align-content-center align-items-end"><h3>'+data_jumlah_berita+'</h3></div> </div> </div><br>');
                    for (var prop in data_chart_kata_kunci) {
                        // console.log("data_chart_kata_kunci." + prop + " = " + data_chart_kata_kunci[prop]);
                        if (data_chart_kata_kunci[prop] > 0) {
                            var className = "kopasus2";                      
                        } else {
                            var className = "kopasus2";
                        }
                        $("#kataKunci-chart").append('<div class="col-lg-3 col-xs-6"><div class="small-box bg-'+className+' px-3" style="border-radius: 25px"><div class="inner"><p class="m-0">'+prop+'</p><h3>'+data_chart_kata_kunci[prop]+'</h3><div class="icon pr-3"><i class="ion ion-search"></i></div></div></div></div>');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    swal('Gagal memperoleh data dari ajax','error');
                    window.location.reload(true);
                }
            });
        }      
    </script>
</section>