<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo @$title; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('admin/kata_kunci'); ?>" ><i class="fa fa-dashboard"></i> <?php echo ucwords($page['p']); ?></a></li>
        <li class="active"><?php echo ucwords($page['c']); ?></li>
      </ol>
    </section>
    <section class="content">
      <div class="card card-primary card-outline card-outline-tabs">
        <div class="card-header p-0 border-bottom-0">
          <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="content_tab_1" data-toggle="pill" href="#tab_1" role="tab" aria-controls="tab_1" aria-selected="true" title="Kata Kunci Aktif">Kata Kunci</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="content_tab_3" data-toggle="pill" href="#tab_3" role="tab" aria-controls="tab_3" aria-selected="false" title="Kata Kunci Tidak Aktif">Kata Kunci Tidak Aktif</a>
            </li>
          </ul>
        </div>
        <div class="card-body">
          <div class="tab-content" id="custom-tabs-four-tabContent">
            <div class="tab-pane fade active show" id="tab_1" role="tabpanel" aria-labelledby="content_tab_1">
                <button class="btn btn-success fa fa-plus" onclick="add();"> Tambah Kata Kunci </button>
                <table id="kata_kunci" class="table table-bordered text-center" style="width: 100% !important;">
                  <thead style="background-color: #efe09d;">
                    <tr>
                      <td>No</td>
                      <td>Kata Kunci</td>
                      <td>Perintah</td>
                    </tr>
                  </thead>
                </table>
            </div>
            <div class="tab-pane fade" id="tab_3" role="tabpanel" aria-labelledby="content_tab_3">
                <table id="trash_kata_kunci" class="table table-bordered text-center" style="width: 100% !important;">
                  <thead style="background-color: #efe09d;">
                      <tr>
                        <td>No</td>
                        <td>Kata Kunci</td>
                        <td>Perintah</td>
                      </tr>
                    </thead>
                </table>
            </div>
          </div>
        </div>
        <!-- /.card -->
      </div>
    </section>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-admin">

    <!-- Modal content-->
    <div class="modal-content" style="border-radius: 25px;">
      <div class="modal-header">
        <h4 class="modal-title" id="modal_header"></h4>
        <button type="button" class="close btn btn-danger" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" id="modal_body">
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">

var table;
var table_trash;

$(document).ready(function() {
    
    //datatables
    //function kategori() {
      table = $('#kata_kunci').DataTable({ 
        "scrollX": true,
        "processing"  : true, //Feature control the processing indicator.
        "serverSide"  : true, //Feature control DataTables' server-side processing mode.
        "searchDelay" : 0.5 * 1000,
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('admin/admin/ajax_list?type=kata_kunci')?>",
            "type": "POST"
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 2 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
      });
    //}
    
    //function trash_kategori() {
      table_trash = $('#trash_kata_kunci').DataTable({ 
        "scrollX": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "searchDelay" : 0.5 * 1000,
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('admin/admin/ajax_list?type=del_kata_kunci')?>",
            "type": "POST"
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 2 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
      });
    //}
    //}
    // Setiap 1 menit perbarui data
    setInterval( function () {
      table.ajax.reload(null,false);
      table_trash.ajax.reload(null,false);
    }, 60 * 1000 );

    jQuery.validator.setDefaults({
      debug: true,
      success: "valid"
    });
    var action
    var action_label
 
});

function add() {
  action = "<?php echo base_url('admin/kata_kunci/tambah')?>";
  action_label = "Tambah";
  $.ajax({ url : "<?php echo base_url('admin/kata_kunci/tambah')?>",
      type: "GET",
      dataType: "JSON",
      async: false,
      success: function(data){
        $('#myModal #modal_header').html("");
        $('#myModal #modal_body').html("");

        $('#myModal #modal_header').append(data.proses);
        $('#myModal #modal_body').append(data.content);
        $('#myModal').modal({backdrop: 'static', keyboard: false}); 
      },
      error: function (jqXHR, textStatus, errorThrown){
          swal("Error!", "Gagal memperoleh data dari ajax!", "error");
      }        
  });
  
}

function edit(id) {
  action = "<?php echo base_url('admin/kata_kunci/ubah')?>";
  action_label = "Ubah";
  $.ajax({ url : "<?php echo base_url('admin/kata_kunci/ubah/')?>"+id,
      type: "GET",
      dataType: "JSON",
      async: false,
      success: function(data){
        $('#myModal #modal_header').html("");
        $('#myModal #modal_body').html("");

        $('#myModal #modal_header').append(data.proses);
        $('#myModal #modal_body').append(data.content);
        $('#myModal').modal({backdrop: 'static', keyboard: false}); 
      },
      error: function (jqXHR, textStatus, errorThrown){
          swal("Error!", "Gagal memperoleh data dari ajax!", "error");
      }        
  });
  
}

function save() {
    var form = $( "#myform" );
    form.validate();
    if (form.valid()) {
      $("#submit_btn").attr('disabled','disabled');
      $("#submit_btn").val('Sedang menyimpan data!');
      $("#cancel_btn").attr('style','display:none;');
      $.ajax({ url : action,
          type: "POST",
          data : form.serialize(),
          dataType: "JSON",
          async: false,
          success: function(data){
            table.ajax.reload(null,false);
            $('#myModal').modal("hide"); 
          },
          error: function (jqXHR, textStatus, errorThrown){
            // $("#submit_btn").removeAttr("disabled");
            // $("#submit_btn").val(action_label);
            // $("#cancel_btn").attr('style','display:block;');
            swal("Error!", "Gagal memperoleh data dari ajax!", "error");
          }        
      });
    } else {

    }
}
</script>