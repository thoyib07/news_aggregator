<?php 
// var_dump($action); 
?>
<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/'); ?>bower_components/ckeditor/dataTables.bootstrap.css"> -->
<form method="post" id="myform" name="myform" enctype="multipart/form-data">
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-body">
      <?php if ($this->session->flashdata('error')) { ?>
        <div class="alert alert-danger">
          <strong><?= $this->session->flashdata('error') ?></strong>
        </div>
      <?php } ?>

          <input type="hidden" name="id_berita" id="id_berita" value="<?php echo @$detail_berita[0]['id_berita'] ?>">
          <input type="hidden" name="submit" id="submit" value="submit">
          <div class="form-group">
            <label for="judul_berita">Judul Berita <font color="red">*</font></label>
            <input class="form-control" id="judul_berita" placeholder="Judul Berita" type="text" name="judul_berita" value="<?php echo @$detail_berita[0]['judul_berita'] ?>" required>
          </div>

          <div class="form-group">
            <label for="isi_berita">Isi Berita <font color="red">*</font></label>
            <textarea class="form-control" name="isi_berita" id="isi_berita" rows="10" required ><?php echo @$detail_berita[0]['isi_berita'] ?></textarea>
          </div>

          <div class="form-group">
            <label for="isi_berita">Lokasi Prop <font color="red">*</font></label>
            <textarea class="form-control" name="lokasi_prop" id="lokasi_prop" rows="10" required ><?php echo @$detail_berita[0]['lokasi_prop'] ?></textarea>
          </div>

          <div class="form-group">
            <label for="isi_berita">Lokasi Kab <font color="red">*</font></label>
            <textarea class="form-control" name="lokasi_kab" id="lokasi_kab" rows="10" required ><?php echo @$detail_berita[0]['lokasi_kab'] ?></textarea>
          </div>

          <div class="form-group">
            <label for="isi_berita">Meta Data Kata <font color="red">*</font></label>
            <textarea class="form-control" name="meta_data_kata" id="meta_data_kata" rows="10" required ><?php echo @$detail_berita[0]['meta_data_kata'] ?></textarea>
          </div>

        <div class="clearfix"></div>
        <div class="form-group">
          <span id="text_submit"></span>
          <!-- <input type="submit" id="submit_btn" name="submit" value="<?php echo $proses; ?>" class="btn btn-success" onclick="save();"> -->
          <!-- <a href="<?php echo $back; ?>" id="cancle_btn" class="btn btn-warning">Batal</a> -->
          <button type="button" id="submit_btn" class="btn btn-success" onclick="save();"><?php echo $proses; ?></button>
          <button type="button" id="cancel_btn" class="btn btn-warning" data-dismiss="modal">Batal</button>
        </div>
      </div>
    </div>
  </div>
</form>

<script type="text/javascript">
$(document).ready(function () {
    // akses();
    // getopendata();
    // CKEDITOR.replace( 'isi_berita_' );
});

</script>