
<form method="post" id="myform" name="myform" enctype="multipart/form-data">
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-body">
      <?php if ($this->session->flashdata('error')) { ?>
        <div class="alert alert-danger">
          <strong><?= $this->session->flashdata('error') ?></strong>
        </div>
      <?php } ?>

          <input type="hidden" name="id_media" id="id_media" value="<?php echo @$media_detail[0]['id_media'] ?>">
          <input type="hidden" name="submit" id="submit" value="submit">

          <div class="form-group">
            <label for="nama_media">Nama Media <font color="red">*</font></label><br>
            <input class="form-control" id="nama_media" placeholder="Nama Media" type="text" name="nama_media" value="<?php echo @$media_detail[0]['nama_media'] ?>" required>

        <div class="clearfix"></div>
        <div class="form-group">
          <span id="text_submit"></span>
          <input type="submit" id="submit_btn" name="submit" value="<?php echo $proses; ?>" class="btn btn-success" onclick="save();">
          <!-- <a href="<?php echo $back; ?>" id="cancle_btn" class="btn btn-warning">Batal</a> -->
          <button type="button" id="cancel_btn" class="btn btn-warning" data-dismiss="modal">Batal</button>
        </div>
      </div>
    </div>
  </div>
</form>
<script type="text/javascript">
  $(document).ready(function () {
      // akses();
      // getopendata();
      //Initialize Select2 Elements
      // $('.select2').select2({ width: '100%' });
      // $("#myselect").select2({ width: 'resolve' }); 
  });

</script>