<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo (!$title) ? '' : $title; ?> </title>
  <link rel="shortcut icon" href="<?php echo base_url('assets/img/'); ?>favicon.ico" type="image/x-icon" />
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/'); ?>bower_components/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/'); ?>bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/'); ?>bower_components/Ionicons/css/ionicons.min.css">
  <!-- jvectormap -->
  <!-- <link rel="stylesheet" href="<?php echo base_url('assets/admin/'); ?>bower_components/jvectormap/jquery-jvectormap.css"> -->
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/'); ?>dist/css/AdminLTE.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/'); ?>dist/css/skins/_all-skins.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?= base_url('assets/admin/'); ?>bower_components/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?= base_url('assets/admin/'); ?>bower_components/datatables-responsive/css/responsive.bootstrap4.min.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <!-- Fav and touch icons -->
  <!--   
  <link rel="shortcut icon" href="<?php echo base_url('assets/img/'); ?>icons/icon.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url('assets/img/'); ?>icons/114x114.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url('assets/img/'); ?>icons/72x72.png">
  <link rel="apple-touch-icon-precomposed" href="<?php echo base_url('assets/img/'); ?>icons/icon.png">
  -->
  <!-- jQuery 3 -->
  <script src="<?php echo base_url('assets/admin/'); ?>bower_components/jquery/dist/jquery.min.js"></script>
  <!-- popper -->
  <script src="<?php echo base_url('assets/'); ?>bower_components/popper/js/popper.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="<?php echo base_url('assets/admin/'); ?>bower_components/bootstrap/js/bootstrap.min.js"></script>
  <!-- jQuery 2.2.3 -->
  <!-- <script src="<?php echo base_url().'assets/admin/'; ?>js/jquery-2.1.1.js"></script> -->
  <!-- <script type="text/javascript" language="javascript" src="<?php echo base_url('assets/admin/'); ?>bower_components/datatables/jquery.dataTables.js"></script>
  <script type="text/javascript" language="javascript" src="<?php echo base_url('assets/admin/'); ?>bower_components/datatables/dataTables.bootstrap.min.js"></script> -->
  <script type="text/javascript" language="javascript" src="<?= base_url('assets/admin/'); ?>bower_components/datatables/jquery.dataTables.js"></script>
  <script type="text/javascript" language="javascript" src="<?= base_url('assets/admin/'); ?>bower_components/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
  <!-- Sweetalert -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/'); ?>bower_components/sweetalert/sweetalert.css">
  <script src="<?php echo base_url('assets/admin/'); ?>bower_components/sweetalert/sweetalert.min.js"></script>
  <!-- Form Validation -->
  <script src="<?php echo base_url('assets/admin/'); ?>bower_components/jquery-validation/jquery.validate.js"></script>
  <script src="<?php echo base_url('assets/admin/'); ?>bower_components/jquery-validation/additional-methods.js"></script>

  <!-- Moment JS -->
  <script src="<?php echo base_url('assets/'); ?>bower_components/moment/moment.js"></script>
  <!-- Custom App JS -->
  <script src="<?php echo base_url('assets/'); ?>js/app.js"></script>
  <style type="text/css">
    body .modal-admin {
        /* new custom width */
        max-width: 80%;
    }
    .error {
        color: red;
        /*background-color: #acf;*/
    }
    #cover-spin {
        position:fixed;
        width:100%;
        left:0;right:0;top:0;bottom:0;
        background-color: rgba(255,255,255,0.7);
        z-index:9999;
        /* display:none; */
    }

    @-webkit-keyframes spin {
        from {-webkit-transform:rotate(0deg);}
        to {-webkit-transform:rotate(360deg);}
    }

    @keyframes spin {
        from {transform:rotate(0deg);}
        to {transform:rotate(360deg);}
    }

    #cover-spin::after {
        content:'';
        /* display:block; */
        position:absolute;
        left:48%;top:40%;
        width:40px;height:40px;
        border-style:solid;
        border-color:black;
        border-top-color:transparent;
        border-width: 4px;
        border-radius:50%;
        -webkit-animation: spin .8s linear infinite;
        animation: spin .8s linear infinite;
    }
  </style>
  <script>
    // var base_url = window.location.origin;
    var base_url = "<?= base_url(); ?>";
    // "http://stackoverflow.com"
  </script>
</head>