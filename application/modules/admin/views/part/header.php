<header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url('admin/'); ?>" class="logo"  style="height: 10vh;">
        <div class="d-flex justify-content-center">
            <img class="pull-left" src="<?= base_url('assets/admin/').'images/logo-01.png' ?>" alt="logo kopasus" srcset="" style="height: 120px;top: -25px;position: absolute;">
        </div>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top py-0" style="">
        <!-- Sidebar toggle button-->
        <!-- <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a> -->
        <a href="#" class="sidebar-toggle d-flex" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
            <!-- <h3 class="text-kopasus2 px-2">News Aggregator</h3> -->
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
            <!-- User Account: style can be found in dropdown.less -->
            <!-- <li class="messages-menu">
                <a href="<?php echo base_url(); ?>" target="_blank">
                <i class="fa fa-home"></i><span> Halaman depan</span>
                </a>
            </li> -->
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <img src="<?php echo base_url('assets/'); ?>images/profile.png" class="user-image" alt="User Image">
                    <span class="hidden-xs"><?php echo $this->session->userdata('nama_lengkap'); ?></span>
                </a>
                <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header">
                        <img src="<?php echo base_url('assets/'); ?>images/profile.png" class="img-circle" alt="User Image" style="border-radius: 50%;">

                        <p class="text-black"><?php echo $this->session->userdata('nama_lengkap'); ?></p>
                    </li>

                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <div class="pull-right">
                            <a href="<?php echo base_url('auth/logout/'); ?>" class="btn btn-danger btn-flat">Keluar</a>
                        </div>
                    </li>
                </ul>
            </li>
            <!-- Control Sidebar Toggle Button -->
            <!--
            <li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
            </li>
            -->
            </ul>
        </div>
    </nav>
  </header>
