<?php
// if (isset($user_akses)) {
//   $hak_akses = array();
//   foreach ($user_akses as $ua) {
//     array_push($hak_akses, $ua->ktda_id);
//   }
// }
// var_dump($hak_akses);
// var_dump($_SESSION);
?>
<!-- =============================================== -->

  <aside class="main-sidebar">
    <section class="sidebar" style="padding-top: 5vh;">
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>

        <?php 
          foreach ($menu as $key => $val) {
            if (count($val->child) >= 1) {
              echo '<li class="treeview menu-open" style="height: auto;">
                      <a href="javascript:void(0)">
                        <i class="'.$val->icon.'"></i>
                        <span>'.$val->nama.'</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul class="treeview-menu" style="display: block;">';
                        foreach ($val->child as $key2 => $val2) {
                          $icon = ($val2->icon == "") ? "" : '<i class="'.$val2->icon.'"></i>';
                          echo '<li>
                            <a href="'.base_url($val2->path).'">
                              '.$icon.'
                              <span>'.$val2->nama.'</span>
                            </a>
                          </li>';
                        }
                      echo '</ul>
                  </li>';
            } else {
              echo '<li class="menu">
                      <a href="'.base_url($val->path).'">
                          <i class="'.$val->icon.'"></i>
                          <span>'.$val->nama.'</span>
                      </a>
                  </li>';
            }
              
          } 
        ?>

        <!--
        <li>
          <a href="<?php // echo base_url('admin/statistik/kcda'); ?>" title="Tambah Menu Statistik Kecamatan">
            <i class="fa fa-plus"></i> <span>Menu Statistik Kecamatan</span>
          </a>
        </li>
        -->
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
