
<form method="post" id="myform" name="myform" enctype="multipart/form-data">
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-body">
      <?php if ($this->session->flashdata('error')) { ?>
        <div class="alert alert-danger">
          <strong><?= $this->session->flashdata('error') ?></strong>
        </div>
      <?php } ?>

          <input type="hidden" name="id_link_rss" id="id_link_rss" value="<?php echo @$link_rss_detail[0]['id_link_rss'] ?>">
          <input type="hidden" name="submit" id="submit" value="submit">

          <div class="form-group">
            <label for="id_situs_berita">Situs <font color="red">*</font></label><br>
            <select class="form-control" name="id_situs_berita" id="id_situs_berita" onchange="setNamaSitus(this);" required>
            </select>
            <input class="form-control" id="nama_situs" placeholder="Nama Situs" type="hidden" name="nama_situs" value="<?php echo @$link_rss_detail[0]['nama_situs'] ?>" readonly required>
          </div>

          <div class="form-group">
            <label for="link_rss">Link RSS <font color="red">*</font></label><br>
            <input class="form-control" id="link_rss" placeholder="Link RSS" type="text" name="link_rss" value="<?php echo @$link_rss_detail[0]['link_rss'] ?>" required>
          </div>

          <div class="clearfix"></div>
          <div class="form-group">
            <span id="text_submit"></span>
            <input type="submit" id="submit_btn" name="submit" value="<?php echo $proses; ?>" class="btn btn-success" onclick="save();">
            <!-- <a href="<?php echo $back; ?>" id="cancle_btn" class="btn btn-warning">Batal</a> -->
            <button type="button" id="cancel_btn" class="btn btn-warning" data-dismiss="modal">Batal</button>
          </div>
      </div>
    </div>
  </div>
</form>
<script type="text/javascript">
  $(document).ready(function () {
      // akses();
      // getopendata();
      //Initialize Select2 Elements
      // $('.select2').select2({ width: '100%' });
      // $("#myselect").select2({ width: 'resolve' });
      var target = "#id_situs_berita";
      var target2 = "#nama_media";
      getSitus(target);
      <?php if ($proses = "Ubah") { ?>
        $(target).val(<?php echo @$link_rss_detail[0]['id_situs_berita'] ?>);
      <?php } ?>
  });

  function getSitus(target) {
    $.ajax({ url : "<?php echo base_url('admin/ajax/ajax_situs')?>",
        type: "GET",
        dataType: "JSON",
        async: false,
        success: function(data){
          $(target).empty();
          $(target).append('<option value="">Pilih Situs</option>');
          for (var i = 0; i < data.situs_berita.length; i++) {
            $(target).append('<option value=' + data.situs_berita[i].id_situs_berita + '>' + data.situs_berita[i].nama_situs + '</option>');
          }
        },
        error: function (jqXHR, textStatus, errorThrown){
            alert('Gagal memperoleh data status situs');
        }
    });
  }

  function setNamaSitus(sel) {
    $("#nama_situs").val(sel.options[sel.selectedIndex].text);
  }

</script>
