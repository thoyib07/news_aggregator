<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo ADMIN_TITLE; ?> | Log in</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="<?php echo base_url('assets/admin/'); ?>bower_components/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url('assets/admin/'); ?>bower_components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?php echo base_url('assets/admin/'); ?>bower_components/Ionicons/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?= base_url('assets/admin/'); ?>css/AdminLTE.min.css">
        <!-- iCheck -->
        <!-- <link rel="stylesheet" href="<?= base_url('assets/'); ?>plugins/iCheck/square/blue.css"> -->

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

        <!-- jQuery 3 -->
        <script src="<?= base_url('assets/admin/'); ?>js/jquery.min.js"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="<?= base_url('assets/admin/'); ?>bower_components/bootstrap/js/bootstrap.min.js"></script>
        <!-- iCheck -->
        <!-- <script src="<?= base_url('assets/'); ?>plugins/iCheck/icheck.min.js"></script> -->
        <!-- Sweetalert -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/'); ?>bower_components/sweetalert/sweetalert.css">
        <script src="<?php echo base_url('assets/admin/'); ?>bower_components/sweetalert/sweetalert.min.js"></script>
        <!-- Form Validation -->
        <script src="<?php echo base_url('assets/admin/'); ?>bower_components/jquery-validation/jquery.validate.js"></script>
        <script src="<?php echo base_url('assets/admin/'); ?>bower_components/jquery-validation/additional-methods.js"></script>
        <style>
            .input-borderless {
                border: 0;
                outline: 0;
                border-bottom: 2px solid #ffffff;
                font-size: 1.4rem;
                color: #e3b902;
                background: transparent;
                width: 100%;
            }
            .text-kopasus {
                color: #e3b902 !important;
            }
            .text-kopasus2 {
                color: #efe09d;
            }
        </style>
    </head>
    <body class="hold-transition login-page" style="background: url(../assets/admin/images/background.png) center center no-repeat fixed;background-size: cover;">
        <div class="row" style="top: 10vh; padding: 5vh 7vw;">
            <!-- <div class="container">   -->
                <div class="col-lg-7 col-12 d-flex justify-content-center align-items-center text-white">
                    <h1 class="d-flex align-content-center text-center text-kopasus" style="font-size: 70px; font-weight: 700;">NEWS<br>AGGREGATOR</h1>
                </div>  
                <div class="col-lg-5 col-12 login-box text-white" style="padding-right: 5vw;">
                    <div class="login-box-body px-lg-5" style="background: #34362ecc;border-radius: 15px;padding: 7vh 5vw !important;">
                        <div class="login-logo">
                            <div class="d-flex justify-content-center">
                                <img class="pull-left" src="<?= base_url('assets/admin/').'images/icon.png' ?>" alt="logo kopasus" srcset="" style="height: 10vh;">
                            </div>
                            <!-- <br> -->
                            <!-- <div class="clearfix"><br></div> -->
                            <h1 class="pull-left py-lg-3 text-kopasus2">Log In</h1>
                        </div>
                        <!-- <p class="login-box-msg card-title">Sign in to start your session</p> -->
                        <form id="formLogin" name="formLogin" action="" method="post">
                        <div class="form-group has-feedback">
                            <label for="email">Username</label>
                            <input type="text" name="email" id="email" class="form-control input-borderless" placeholder="Username" required>
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="password" name="pass" id="pass" class="form-control input-borderless" placeholder="Password" required>
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>
                        <div class="row pt-5">
                            <div class="container">
                                <div class="col-12 pull-right d-flex justify-content-center">
                                    <span class="btn btn-primary btn-block btn-flat bg-red w-50" onclick="checkAuth();" style="border-radius: 5px;">Masuk</span>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            <!-- </div> -->
        </div>
        <script>
            $(function () {
                // $('input').iCheck({
                //   checkboxClass: 'icheckbox_square-blue',
                //   radioClass: 'iradio_square-blue',
                //   increaseArea: '20%' // optional
                // });
            });
            function checkAuth(params) {
                var form = $( "#formLogin" );
                form.validate();
                if (form.valid()) {
                $.ajax({ url : "<?= base_url('auth/checkAPI'); ?>",
                    type: "POST",
                    data : form.serialize(),
                    dataType: "JSON",
                    async: false,
                    success: function(data){
                        console.log(data);
                        if (data.status) { 
                        document.location.href = "<?= base_url('admin/') ?>";
                        } else {
                        swal("Error!", data.msg, "error");
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        swal("Error!", "Gagal memperoleh data dari ajax!", "error");
                    }        
                });
                } else {

                }
            }
        </script>
    </body>
</html>
