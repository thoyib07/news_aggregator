<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function index() {
		// echo "auth/index";
		// $this->login();
		redirect(base_url().'admin/');
	}

	public function login() {
		//var_dump('masuk');
		$data['title'] = ADMIN_TITLE;
		$this->load->view('login',$data);
	}

	public function logout() {
		$this->session->unset_userdata('time');
 		$this->session->unset_userdata('login');
 		$this->session->sess_destroy();
		redirect(base_url().'admin/');
	}

	public function check() {
		// $NIP 	= $_POST['username'];
		$email 	= $_POST['email'];
	  	$pass		= $_POST['pass'];
	  	$pass_sha	= sha1($_POST['pass']);
		/*
	  	$this->curl->create('http://opendatav2.tangerangkota.go.id/services/auth/login/uid/'.$NIP.'/pid/'.$pass.'/format/json');

		$this->curl->http_login(OPENDATAV2_U, OPENDATAV2_P); // login open data
		$result = json_decode($this->curl->execute(), true);
		// var_dump($_POST,"<hr>");
		// var_dump($result,"<hr>");
		// var_dump($result['data']['id_pegawai'],"<hr>");
		// die();
		if ($result !== NULL) {
			// var_dump("<hr>",$akses_ktda_array_parent);
			// die("masuk opendatav2");
			if ((substr($result['data']['kode_unor'],0,8) == "04.09.04") || (substr($result['data']['kode_unor'],0,8) == "04.09.05") || ($result['data']['user_id'] == 2242)) {
				$gelar_depan = (!empty($result['data']['gelar_depan'])) ? $result['data']['gelar_depan'].' ' : '';
					$gelar_nonakademis = (!empty($result['data']['gelar_nonakademis'])) ? $result['data']['gelar_nonakademis'].' ' : '';
					$gelar_belakang = (!empty($result['data']['gelar_belakang'])) ? ', '.$result['data']['gelar_belakang'] : '';
					$nama_pegawai = $result['data']['nama_pegawai'];
					$nama_pegawai = $gelar_depan.' '.$gelar_nonakademis.' '.$nama_pegawai.$gelar_belakang;
					$nama_pegawai = str_replace("-","",$nama_pegawai);
				$nama_lengkap = $nama_pegawai;

					$session = array('login_admin' => true, 'time_admin' => time(), 'id_user_admin' => $result['data']['id_pegawai'], 'nip' => $_POST['username'], 'nama_lengkap' => $nama_lengkap, 'akses_admin' => '1');
					$this->session->set_userdata($session);
					redirect(base_url().'admin/index');
			} else {
				$this->session->set_userdata('error_login','Anda tidak memiliki akses di aplikasi ini!!');
				redirect('auth');
			}
		} else {
			die("gagal opendatav2");
			$this->session->set_userdata('error_login','NIP dan Password anda salah!!');
			redirect('auth');
		}
		*/
		$data['select']	= '*';
		$data['table']	= 'm_user';
		$data['where']	= "email = '".$email."' and password = '".$pass_sha."' and status = 1";
		$data['limit']	= "1";
		$user_login = $this->m_admin->getData($data);
		// var_dump("data : ",$data,"<hr>");
		// var_dump("user_login : ",$user_login,"<hr>");
		// die;
		if ($user_login) {
			$session = array('login' => true, 'time' => time(), 'id_user' => $user_login[0]['id_user'], 'email' => $_POST['email'], 'nama_lengkap' => $user_login[0]['nama'], 'akses' => $user_login[0]['id_user_type']);
			$this->session->set_userdata($session);
			redirect(base_url().'admin/index');
		} else {
			$this->session->set_userdata('error_login','Anda tidak memiliki akses di aplikasi ini!!');
			redirect('auth');
		}
	}

	public function checkAPI() {		
		$email 	= $_POST['email'];
	  	$pass		= $_POST['pass'];
		$pass_sha	= sha1($_POST['pass']);
		$res = array();
		
		$data_user['select']	= '*';
		$data_user['table']		= 'm_user';
		$data_user['where']		= "email = '".$email."' and status = 1";
		$data_user['limit']		= "1";
		$user_data = $this->m_admin->getData($data_user);
		
		if ($user_data) {
			$data['select']	= '*';
			$data['table']	= 'm_user';
			$data['where']	= "email = '".$email."' and password = '".$pass_sha."' and status = 1";
			$data['limit']	= "1";
			$user_login = $this->m_admin->getData($data);
			// var_dump("data : ",$data,"<hr>");
			// var_dump("user_login : ",$user_login,"<hr>");
			// die;
			if ($user_login) {
				$session = array('login' => true, 'time' => time(), 'id_user' => $user_login[0]['id_user'], 'email' => $_POST['email'], 'nama_lengkap' => $user_login[0]['nama'], 'akses' => $user_login[0]['id_user_type']);
				$this->session->set_userdata($session);
				// redirect(base_url().'admin/index');
				$res['status'] = true;
				$res['msg'] = "";
			} else {
				$this->session->set_userdata('error_login','Anda tidak memiliki akses di aplikasi ini!!');
				// redirect('auth');
				
				$res['status'] = false;
				$res['msg'] = "Password yang anda masukan salah!!";
			}
		} else {
			$res['status'] = false;
			$res['msg'] = "Email yang anda masukan tidak terdaftar!!";
		}
		echo json_encode($res);
	}

	public function forget($token) {
		if (isset($token)) {
			//cek user yang punya token
			//set password baru
			//set ulang random di table user
		} else {
			if (isset($_POST)) {
			$data['select']	= 'email';
			$data['table']	= 'm_user';
			$data['where']	= "username = '".$_POST['user']."' and email LIKE '%".$_POST['email']."%'";
			$data['limit']	= "limit 1";
			$email = $this->m_admin->getData($data);
			if ($_POST['email'] == $email[0]['email']) {
				$value = array('email' => $_POST['email'], 'user' => $_POST['user'], 'token' => random(), 'date' => date('Y-m-d H:i:s'));
				email($value);
			}
			var_dump($_POST);die();
			} else {
				redirect(base_url().'auth/login');
			}
		}
	}

	public function relogin($id) {
		// var_dump($_SESSION,"<hr>");
		// var_dump($id,"<hr>");
		if ($this->session->userdata('id_user') == $id) {
			$session = array('login' => true, 'time' => time(), 'id_user' => $id);
			// var_dump($session,"<hr>");
			// die();
			$this->session->set_userdata($session);
			//redirect(base_url().'admin/index');
		}
		// die();
	}

	// public function register($rand) {
	// 	$data['select']	= 'rand';
	// 	$data['table']	= 'random';
	// 	$data['where']	= "type = 1";
	// 	$data['rand'] = $this->m_admin->getData($data);
	// 	if ($rand == $data['rand'][0]->rand) {
	// 		if (isset($_POST['submit'])) {
	// 			$data['name'] 			= $_POST['nama'];
	// 			$data['sex'] 			= $_POST['sex'];
	// 			$data['birthday'] 		= $_POST['tanggal'];
	// 			$data['phone'] 			= $_POST['telp'];
	// 			$data['mobile'] 		= $_POST['hp'];
	// 			$data['address'] 		= $_POST['alamat'];
	// 			$data['zip'] 			= $_POST['zip'];
	// 			$data['email'] 			= $_POST['email'];
	// 			$data['username'] 		= $_POST['user'];
	// 			$data['password'] 		= md5(crc32($_POST['pass'].'J8ih6y'));
	// 			$data['activation_key'] = random();

	// 			$this->m_admin->addUser($data);
	// 			redirect(base_url().'admin/');
	// 		} else {
	// 			$data['title'] = "SISTAD";
	// 			$data['zone'] = "tad";
	// 			$data['rand'] = $rand;
	// 			$this->load->view('register',$data);
	// 		}
	// 	} else {
	// 		redirect(base_url());
	// 	}
	// }
}
