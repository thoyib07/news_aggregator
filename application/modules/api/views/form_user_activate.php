<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin <?= APP_NAME ?> | Log in</title>

  <link rel="icon" href="<?= base_url(); ?>assets\images\favicon.png" type="image/x-icon">
  <!-- Google font-->
  <!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet"> -->
  <!-- Required Fremwork -->
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/bower_components\bootstrap\css\bootstrap.min.css">
  <!-- themify-icons line icon -->
  <!-- <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets\icon\themify-icons\themify-icons.css"> -->
  <!-- ico font -->
  <!-- <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets\icon\icofont\css\icofont.css"> -->
  <!-- Style.css -->
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets\css\style-admin.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic"> -->
</head>

<body class="fix-menu">
  <!-- Pre-loader start -->
  <div class="theme-loader">
      <div class="ball-scale">
          <div class='contain'>
              <div class="ring"><div class="frame"></div></div>
              <div class="ring"><div class="frame"></div></div>
              <div class="ring"><div class="frame"></div></div>
              <div class="ring"><div class="frame"></div></div>
              <div class="ring"><div class="frame"></div></div>
              <div class="ring"><div class="frame"></div></div>
              <div class="ring"><div class="frame"></div></div>
              <div class="ring"><div class="frame"></div></div>
              <div class="ring"><div class="frame"></div></div>
              <div class="ring"><div class="frame"></div></div>
          </div>
      </div>
  </div>
  <!-- Pre-loader end -->

  <section class="login-block">
      <!-- Container-fluid starts -->
      <div class="container">
          <div class="row">
              <div class="col-sm-12">
                  <!-- Authentication card start -->
                  
                      <form class="md-float-material form-material" action="<?= base_url('api/activate_user'); ?>" method="post">
                          <div class="text-center">
                              <!-- <img src="<?= base_url(); ?>assets\images\logo.png" alt="logo.png"> -->
                          </div>
                          <div class="auth-box card">
                              <div class="card-block">
                                  <div class="row m-b-20">
                                      <div class="col-md-12">
                                          <h3 class="text-center">Activate User</h3>
                                      </div>
                                  </div>
                                    <input type="hidden" id="id_pegawai" name="id_pegawai" value="<?php echo $pegawai[0]->id_pegawai ?>">
                                    <div class="form-group form-primary">
                                        <input type="text" name="username" class="form-control" required="" placeholder="Username  Anda">
                                        <span class="form-bar"></span>
                                    </div>
                                    <div class="form-group form-primary">
                                        <input type="password" id="password" name="password" onChange="same_pass();" class="form-control" required="" placeholder="Password Anda">
                                        <span class="form-bar"></span>
                                    </div>
                                    <div class="form-group form-primary">
                                        <input type="password" id="re_password" name="re_password" onChange="same_pass();" class="form-control" required="" placeholder="Ulangi Password Anda">
                                        <span class="form-bar"></span>
                                    </div>
                                    <div id="div_submit" class="row m-t-30">
                                        <div class="col-md-12">
                                            <button id="submit" disabled="true" type="submit" class="btn btn-primary btn-md btn-block waves-effect waves-light text-center m-b-20">Submit</button>
                                        </div>
                                    </div>
                              </div>
                          </div>
                      </form>
                      <!-- end of form -->
              </div>
              <!-- end of col-sm-12 -->
          </div>
          <!-- end of row -->
      </div>
      <!-- end of container-fluid -->
  </section>
  <!-- Warning Section Starts -->
  <!-- Older IE warning message -->
  
  <!-- Warning Section Ends -->
  <!-- Required Jquery -->
  <script type="text/javascript" src="<?= base_url(); ?>assets/bower_components\jquery\js\jquery.min.js"></script>
  <script type="text/javascript" src="<?= base_url(); ?>assets/bower_components\jquery-ui\js\jquery-ui.min.js"></script>
  <script type="text/javascript" src="<?= base_url(); ?>assets/bower_components\popper.js\js\popper.min.js"></script>
  <script type="text/javascript" src="<?= base_url(); ?>assets/bower_components\bootstrap\js\bootstrap.min.js"></script>
  <!-- jquery slimscroll js -->
  <script type="text/javascript" src="<?= base_url(); ?>assets/bower_components\jquery-slimscroll\js\jquery.slimscroll.js"></script>
  <!-- modernizr js -->
  <script type="text/javascript" src="<?= base_url(); ?>assets/bower_components\modernizr\js\modernizr.js"></script>
  <script type="text/javascript" src="<?= base_url(); ?>assets/bower_components\modernizr\js\css-scrollbars.js"></script>
  <!-- i18next.min.js -->
  <!-- <script type="text/javascript" src="<?= base_url(); ?>assets/bower_components\i18next\js\i18next.min.js"></script> -->
  <!-- <script type="text/javascript" src="<?= base_url(); ?>assets/bower_components\i18next-xhr-backend\js\i18nextXHRBackend.min.js"></script> -->
  <!-- <script type="text/javascript" src="<?= base_url(); ?>assets/bower_components\i18next-browser-languagedetector\js\i18nextBrowserLanguageDetector.min.js"></script> -->
  <!-- <script type="text/javascript" src="<?= base_url(); ?>assets/bower_components\jquery-i18next\js\jquery-i18next.min.js"></script> -->
  <script type="text/javascript" src="<?= base_url(); ?>assets\js\common-pages.js"></script>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <!-- <script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script> -->
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-23581568-13');
    (function() {

    })();
    function same_pass() {       
        if (document.getElementById("password").value === document.getElementById("re_password").value) {
            console.log("sama");
            document.getElementById("submit").disabled = false;
        } else {
            console.log("beda");
            document.getElementById("submit").disabled = true;
        }
    }
  </script>
</body>
</html>
