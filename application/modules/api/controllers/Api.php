<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends MY_Controller {
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function index() {
		echo "It's Runing";
	}

	public function buildPass($pass) {
		echo sha1($pass);
	}

	public function get_data($type=NULL,$id=NULL) {
		$type = (isset($type))? $type: $_GET['type'] ;
		/*
		$start = (isset($start))? 1: $_GET['start'] ;
		$limit = (isset($limit))? 1: $_GET['limit'] ;
		*/
		switch ($type) {
			case 'send_email':
				$data = (object) array();
				$id_output_mutasi 	= $_GET['id_output_mutasi'];
				$output['select'] 	= "o.*, w.email, w.nama";
				$output['table']	= "t_output_mutasi as o";
				$output['join'][0]	= array('t_form_mutasi_siswa as f','f.id_form_mutasi_siswa = o.id_form_mutasi_siswa');
				$output['join'][1]	= array('m_warga as w','f.id_warga = w.id_warga');
				$output['where']	= "o.id_output_mutasi = '".$id_output_mutasi."'";
				$data->detail 		= $this->m_admin->getData($output);
				// var_dump($data->detail[0]);
				// die();
				$this->email->from('noreply@tangerangkota.go.id', 'Dinas Komunikasi dan Informatika');
				$this->email->to($data->detail[0]->email);

				$this->email->subject('Download Surat Persetujuan Pindah Sekolah Dinas Pendidikan Kota Tangerang');
				$this->email->message('Berikut adalah dokumen surat persetujuan pindah sekolah yang anda ajukan, jika terjadi kesalahan hubungi Dinas Pendidikan Kota Tangerang Jl. Satria sudirman No.1, Gd. Pusat Pemerintahan Lt.III.');
				$this->email->attach(base_url('assets/downloads/surat_ca/').$data->detail[0]->file_output);

				if ($this->email->send()) {
  					$output_edit['data'] 		= array('status_kirim' => "1",);
  					$output_edit['table']		= "t_output_mutasi";
					$output_edit['where'][0] 	= array('id_output_mutasi', $id_output_mutasi);
  					$this->m_admin->updateData($output_edit);

					echo json_encode(array('status' => TRUE));
				} else {
					echo json_encode(array('status' => FALSE));
				}
			break;
			
			case 'propinsi':
				if (!isset($_GET['data_type'])) {
					$_GET['data_type'] = "json";
				}
				$data = (object) array();
				$output['select'] 	= "*";
				$output['table']	= "m_provinsi";
				$output['order']	= array('NAMA_PROP','ASC');
				$data->detail 		= $this->m_admin->getData($output);
				switch ($_GET['data_type']) {
					case 'json':						
						$result = (object) array();
						foreach ($data->detail as $key => $value) {
							$result->lokasi[$value['NO_PROP']] = $value;
						}
						break;
					case 'html':
						$result = "<option value=''> Pilih Provinsi </option>";
						foreach ($data->detail as $d) {
							$result .= "<option value='".$d['NO_PROP']."'>".$d['NAMA_PROP']."</option>";
						}
						break;
					
					default:												
						$result = (object) array();
						foreach ($data->detail as $key => $value) {
							$result->lokasi[$value['NO_PROP']] = $value;
						}
						break;
				}
				echo json_encode($result);
			break;
			
			case 'kabupaten':
				if (!isset($_GET['data_type'])) {
					$_GET['data_type'] = "json";
				}
				$data = (object) array();
				$output['select'] 	= "*";
				$output['table']	= "m_kabupaten_kota";
				$output['where']	= "NO_PROP = '".$_GET['no_prop']."'";
				$output['order']	= array('NO_KAB','ASC');
				$data->detail 		= $this->m_admin->getData($output);
				switch ($_GET['data_type']) {
					case 'json':						
						$result = (object) array();
						foreach ($data->detail as $key => $value) {
							$result->lokasi[$value['NO_KAB']] = $value;
						}
						break;
					case 'html':
						$result = "<option value=''> Pilih Kabupaten / Kota </option>";
						foreach ($data->detail as $d) {
							$result .= "<option value='".$d['NO_KAB']."'>".$d['NAMA_KAB']."</option>";
						}
						break;
					
					default:												
						$result = (object) array();
						foreach ($data->detail as $key => $value) {
							$result->lokasi[$value['NO_KAB']] = $value;
						}
						break;
				}
				echo json_encode($result);
			break;
			
			case 'kecamatan':
				if (!isset($_GET['data_type'])) {
					$_GET['data_type'] = "json";
				}
				$data = (object) array();
				$output['select'] 	= "*";
				$output['table']	= "m_kecamatan";
				$output['where']	= "NO_PROP = '".$_GET['no_prop']."' AND NO_KAB = '".$_GET['no_kab']."'";
				$output['order']	= array('NO_KEC','ASC');
				$data->detail 		= $this->m_admin->getData($output);
				switch ($_GET['data_type']) {
					case 'json':						
						$result = (object) array();
						foreach ($data->detail as $key => $value) {
							$result->lokasi[$value['NO_KEC']] = $value;
						}
						break;
					case 'html':
						$result = "<option value=''> Pilih Kecamatan </option>";
						foreach ($data->detail as $d) {
							$result .= "<option value='".$d['NO_KEC']."'>".$d['NAMA_KEC']."</option>";
						}
						break;
					
					default:												
						$result = (object) array();
						foreach ($data->detail as $key => $value) {
							$result->lokasi[$value['NO_KEC']] = $value;
						}
						break;
				}
				echo json_encode($result);
			break;
			
			case 'kelurahan':
				if (!isset($_GET['data_type'])) {
					$_GET['data_type'] = "json";
				}
				$data = (object) array();
				$output['select'] 	= "*";
				$output['table']	= "m_kelurahan";
				$output['where']	= "NO_PROP = '".$_GET['no_prop']."' AND NO_KAB = '".$_GET['no_kab']."' AND NO_KEC = '".$_GET['no_kec']."'";
				$output['order']	= array('NO_KEL','ASC');
				$data->detail 		= $this->m_admin->getData($output);
				switch ($_GET['data_type']) {
					case 'json':						
						$result = (object) array();
						foreach ($data->detail as $key => $value) {
							$result->lokasi[$value['NO_KEL']] = $value;
						}
						break;
					case 'html':
						$result = "<option value=''> Pilih Kelurahan </option>";
						foreach ($data->detail as $d) {
							$result .= "<option value='".$d['NO_KEL']."'>".$d['NAMA_KEL']."</option>";
						}
						break;
					
					default:												
						$result = (object) array();
						foreach ($data->detail as $key => $value) {
							$result->lokasi[$value['NO_KEL']] = $value;
						}
						break;
				}
				echo json_encode($result);
			break;

			case 'searchBerita':
				ini_set('memory_limit', '-1');
				$data = (object) array();
				$new_kata = array();
				$tokenize = array();
				// var_dump("post : <br>",$_POST,"<hr>");
				$threshold = $_POST['threshold'] / 100;
				// var_dump("stopword : <br>",json_encode($_SESSION['stopword']),"<hr>");
				$tmp_new_kata = explode(" ",$_POST["kata_pencarian"]);
				// var_dump("tmp_new_kata : <br>",$tmp_new_kata,"<hr>");
				
				// stopword
				$kata_stopword=array_intersect($_SESSION['stopword'],$tmp_new_kata);
				// var_dump(json_encode($kata_stopword),"<hr>");
				
				foreach(array_diff($tmp_new_kata,$kata_stopword) as $val){
					array_push($new_kata,$val);
					// tokenize dari data setelah stopword dengan stemming
					$tmp = preg_replace('/[^A-Za-z0-9]/', '', $val);
					$hasil_stemming = "";
					if (strlen($tmp) > 2) {
						// doStemming($tmp);
						$hasil_stemming = $this->doStemming($tmp);
						// var_dump("<hr> hasil_stemming : <hr>",$hasil_stemming);
						// die;
						// } else {
							// $hasil_stemming = $tmp;
						}
						if ($hasil_stemming !== "" ) {
							array_push($tokenize,$hasil_stemming);
						}
				}
				$meta_data_kata = array();
				foreach(array_count_values($tokenize) as $keyKata => $valKata){
					$tmpObj = new StdClass();
					$tmpObj->kata = $keyKata;
					$tmpObj->count = $valKata;
					// $tmpObj->countS = $valKata;
					// $tmpObj->countB = 0;
					array_push($meta_data_kata,$tmpObj);
				}
				
				$meta_data_kata_pembanding = array();
				foreach(array_count_values($tokenize) as $keyKata => $valKata){
					$tmpObj = new StdClass();
					$tmpObj->kata = $keyKata;
					$tmpObj->count = 0;
					// $tmpObj->countS = $valKata;
					// $tmpObj->countB = 0;
					array_push($meta_data_kata_pembanding,$tmpObj);
				}
				// var_dump("metadata search : <hr>",json_encode($meta_data_kata));
				// die;
				foreach($meta_data_kata as $word){
					$where[] = 'meta_data_kata LIKE "%'.$word->kata.'%"';
				}
				// Cari data berdasarkan parameter waktu dan tokenize				
				$berita['select'] 	= "id_berita, meta_data_kata";
				$berita['table']	= "m_berita";
				$berita['where']	= implode(" OR ", $where);
				$berita['where']	.= " AND publish_date BETWEEN '".$_POST['startDate']."' AND '".$_POST['startDate']."' AND has_normalize = '1' AND status = '1'";
				$berita['order']	= array('publish_date','DESC');
				$data->berita 		= $this->m_admin->getData($berita);
				$arrTmp = array();
				foreach($data->berita as $dataBerita){
					$tmp_meta_data_kata = json_decode($dataBerita['meta_data_kata']);
					// var_dump(json_encode($tmp_meta_data_kata));
					// die;
					$replace_meta_data_kata = array();
					if(!is_null($tmp_meta_data_kata)){
						foreach($tmp_meta_data_kata as $valMetadata){
							if(in_array($valMetadata->kata,$tokenize)){
								array_push($replace_meta_data_kata,$valMetadata);
							}
						}
						// var_dump("<hr>isi replace_meta_data_kata : ",json_encode($replace_meta_data_kata),"<hr>");
						foreach($meta_data_kata_pembanding as $val){
							$punya = 0;
							foreach($replace_meta_data_kata as $val2){
								if($val->kata == $val2->kata){ $punya = 1; }
							}
							if($punya == 0){ array_push($replace_meta_data_kata,$val); }
						}						
						// var_dump("<hr>isi replace_meta_data_kata : ",json_encode($replace_meta_data_kata),"<hr>");
						// die;
						$dataBerita['meta_data_kata'] = json_encode($replace_meta_data_kata);
						if ($dataBerita['meta_data_kata'] !== "[]"){
							$arrTmp[] = $dataBerita;
						}
					}
				}
				// var_dump("<hr> new data berita : <hr>",json_encode($arrTmp));
				// die;
				/*
				*/
				$filename = __DIR__ . '/hasil.txt';
				$file = fopen($filename,"c+");
				// echo fwrite($file,json_encode($arrTmp));
				fclose($file);

				// Perhitungan Algoritma Cosine Similarity
				// - buat union kata search dengan meta_data_kata tiap berita
				$newDataBerita = array();
				foreach($arrTmp as $val){ // loop data berita
					$tmpKata = json_decode($val['meta_data_kata']);
					$new_meta_data_kata = array();
					foreach($tmpKata as $val2){	 // loop meta_data_kata di data berita
						$tmpVal = $val2;
						foreach($meta_data_kata as $val3){
							if ($val3->kata == $val2->kata) {
								$tmpVal->countS = $val3->count;
							}
						}
						array_push($new_meta_data_kata,$tmpVal);
					}
					$val['meta_data_kata'] = json_encode($new_meta_data_kata);
					$newDataBerita[] = $val;
				}
				// var_dump("<hr> newDataBerita : <hr>",json_encode($newDataBerita));
				// die;
				$listIdBerita = array();
				$listHasil = array();
				foreach($newDataBerita as $val){
					// var_dump("<hr> val newDataBerita : <hr>",($val));
					$test1 = json_decode($val['meta_data_kata']);
					// var_dump("<hr> test1 : <hr>",($test1));
					$sumAB = 0;
					$sqrtA = 0;
					$sqrtB = 0;
					foreach($test1 as $val2){
						$sumAB = $sumAB + ($val2->count * $val2->countS);
						$sqrtA = $sqrtA + ($val2->count * $val2->count);
						$sqrtB = $sqrtB + ($val2->countS * $val2->countS);
					}
					$divider = (sqrt($sqrtA) * sqrt($sqrtB));
					if ($divider > 0) {
						$hasil = $sumAB / $divider;
						if ($hasil > $threshold) {
							// var_dump("<hr>".$val['id_berita']." lebih dari threshold dengan score : ".$hasil);
							array_push($listIdBerita,$val['id_berita']);
							$listHasil[$val['id_berita']] = $hasil;
						} else {
							// var_dump("<hr>".$val['id_berita']." tidak lebih dari threshold dengan score : ".$hasil);
						}
					}
				}
				
				// Filter dan sorting hasil perhitungan dengan threshold
				// var_dump("<hr> listIdBerita : <hr>",($listIdBerita));
				$resBerita['select'] 	= "id_berita, judul_berita, link_berita, isi_berita, lokasi_prop, lokasi_kab, meta_data_kata, publish_date";
				$resBerita['table']		= "m_berita";
				$resBerita['where']		= "id_berita IN ( ".implode(" , ", $listIdBerita).")";
				$resBerita['order']		= array('publish_date','DESC');
				$data->resBerita 		= $this->m_admin->getData($resBerita);
				// var_dump("<hr> data->resBerita : <hr>",json_encode($data->resBerita));
				$result = array();
				foreach($data->resBerita as $res){
					foreach($listHasil as $kli => $li){
						if($kli == $res['id_berita']){
							$res['hasil'] = $li;
						}
					}
					array_push($result,$res);
				}
				echo json_encode($result);
			break;

			case 'berita':	
				$id = (isset($_POST['id']))? $_POST['id']:$id;			
				$data = (object) array();
				$output['select'] 	= "*";
				$output['table']	= "m_berita";
				$output['where']	= "id_berita = '".$id."' AND status = 1";
				$data->berita 		= $this->m_admin->getData($output);
				echo json_encode($data->berita);
			break;
			/*
			*/
			default:
			break;
		}
	}

	public function searchBerita() {
		// var_dump($_POST);
		// die;
		ini_set('memory_limit', '-1');
		$data = (object) array();
		$new_kata = array();
		$tokenize = array();		
		$beritaPerKeyword = array();
		// var_dump("post : <br>",$_POST,"<hr>");
		$threshold = $_POST['threshold'] / 100;
		// var_dump("stopword : <br>",json_encode($_SESSION['stopword']),"<hr>");
		$tmp_new_kata = explode(" ",$_POST["kata_pencarian"]);
		// var_dump("tmp_new_kata : <br>",$tmp_new_kata,"<hr>");
		
		// stopword
		$kata_stopword=array_intersect($_SESSION['stopword'],$tmp_new_kata);
		// var_dump(json_encode($kata_stopword),"<hr>");
		
		foreach(array_diff($tmp_new_kata,$kata_stopword) as $val){
			array_push($new_kata,$val);
			// tokenize dari data setelah stopword dengan stemming
			$tmp = preg_replace('/[^A-Za-z0-9]/', '', $val);
			$hasil_stemming = "";
			//if (strlen($tmp) > 2) {
				// doStemming($tmp);
				$hasil_stemming = $this->doStemming($tmp);
				// var_dump("<hr> hasil_stemming : <hr>",$hasil_stemming);
				// die;
			// } else {
				// $hasil_stemming = $tmp;
			//}
			if ($hasil_stemming !== "" ) {
				array_push($tokenize,$hasil_stemming);
			}
		}
		
		$meta_data_kata = array();
		foreach(array_count_values($tokenize) as $keyKata => $valKata){
			$tmpObj = new StdClass();
			$tmpObj->kata = $keyKata;
			$tmpObj->count = $valKata;
			// $tmpObj->countS = $valKata;
			// $tmpObj->countB = 0;
			array_push($meta_data_kata,$tmpObj);
		}
		
		$meta_data_kata_pembanding = array();
		foreach(array_count_values($tokenize) as $keyKata => $valKata){
			$tmpObj = new StdClass();
			$tmpObj->kata = $keyKata;
			$tmpObj->count = 0;
			// $tmpObj->countS = $valKata;
			// $tmpObj->countB = 0;
			array_push($meta_data_kata_pembanding,$tmpObj);
			array_push($beritaPerKeyword,$tmpObj);			
		}
		// var_dump("metadata search : <hr>",json_encode($meta_data_kata));
		// die;
		if (isset($_POST['lvl_situs'])) {
			$lvl_situs = $_POST['lvl_situs'];	
		}  else {
			$lvl_situs = ['2'];
		}
		
		$situs['select'] 	= 'id_situs_berita';
		$situs['table']		= "m_situs_berita";
		// $situs['where']		= "lvl_situs >= ".$lvl_situs;
		$situs['where']		= "lvl_situs IN ( ".implode(",",$lvl_situs).")";
		$data->situs 		= $this->m_admin->getData($situs);
		$arrIdSitusBerita = array();
		foreach ($data->situs as $valSitus) {
			array_push($arrIdSitusBerita,$valSitus['id_situs_berita']);
		}
		$strIdSitusBerita = implode(",",$arrIdSitusBerita);
		// $where_kata_kunci = array();
		$where_kata_kunci = "";
		if (isset($_POST['list_kata_kunci'])) {
			$kata_kunci['select'] = "kata_kunci";
			$kata_kunci['table'] = "m_kata_kunci";
			$kata_kunci['where'] = "id_kata_kunci IN (".$_POST['list_kata_kunci'].") AND status = 1 ";
			$data->kata_kunci 	= $this->m_admin->getData($kata_kunci);
			$where_kata_kunci = "( ";
			foreach ($data->kata_kunci as $key => $val) {
				$where_kata_kunci .= 'isi_berita LIKE ';
				$where_kata_kunci .= "'%".$val['kata_kunci']."%' OR ";
			}
			$where_kata_kunci = substr($where_kata_kunci,0,-3);
			$where_kata_kunci .= " ) AND";
			// $where_kata_kunci[] = $kondisi;
			// var_dump($where_kata_kunci);
			// die;
		}

		foreach($meta_data_kata as $word){
		    $kondisi = 'meta_data_kata LIKE ';
		    $kondisi .= "'%kata";
		    $kondisi .= '":"';
		    $kondisi .= $word->kata;
		    $kondisi .= "%'";
			// $where[] = 'meta_data_kata LIKE `%kata":"'.$word->kata.'%`';
			$where[] = $kondisi;
		}
		// Cari data berdasarkan parameter waktu dan tokenize				
		$berita['select'] 	= 'id_berita, isi_berita, meta_data_kata';
		$berita['table']	= "m_berita";
		$berita['where']	= $where_kata_kunci;
		$berita['where']	.= "( ".implode(" OR ", $where)." )";
		$berita['where']	.= " AND id_situs_berita IN (".$strIdSitusBerita.") AND publish_date BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' AND has_normalize = '1' AND status = '1'";
		$berita['order']	= array('publish_date','DESC');
		$data->berita 		= $this->m_admin->getData($berita);
		// var_dump("<hr> data->berita : <hr>",count($data->berita));
		// die;
		$arrTmp = array();
		foreach($data->berita as $dataBerita){
			$tmp_meta_data_kata = json_decode($dataBerita['meta_data_kata']);
			// var_dump(json_encode($tmp_meta_data_kata));
			// die;
			$replace_meta_data_kata = array();
			if(!is_null($tmp_meta_data_kata)){
				foreach($tmp_meta_data_kata as $valMetadata){
					if(in_array($valMetadata->kata,$tokenize)){
						array_push($replace_meta_data_kata,$valMetadata);
					}
				}
				// var_dump("<hr>isi replace_meta_data_kata : ",json_encode($replace_meta_data_kata),"<hr>");
				foreach($meta_data_kata_pembanding as $val){
					$punya = 0;
					foreach($replace_meta_data_kata as $val2){
						if($val->kata == $val2->kata){ $punya = 1; }
					}
					if($punya == 0){ array_push($replace_meta_data_kata,$val); }
				}						
				// var_dump("<hr>isi replace_meta_data_kata : ",json_encode($replace_meta_data_kata),"<hr>");
				// die;
				$dataBerita['meta_data_kata'] = json_encode($replace_meta_data_kata);
				if ($dataBerita['meta_data_kata'] !== "[]"){
					$arrTmp[] = $dataBerita;
				}
			}
		}
		// var_dump("<hr> new data berita : <hr>",json_encode($arrTmp));
		// die;
		/*
		*/
		$filename = __DIR__ . '/'.$_SESSION['id_user'].'_hasil.txt';
		$file = fopen($filename,"c+");
		// echo fwrite($file,json_encode($arrTmp));
		fclose($file);

		// Perhitungan Algoritma Cosine Similarity
		// - buat union kata search dengan meta_data_kata tiap berita
		$newDataBerita = array();
		// loop data berita
		foreach($arrTmp as $val){ 
			$tmpKata = json_decode($val['meta_data_kata']);
			$new_meta_data_kata = array();
			// loop meta_data_kata di data berita
			foreach($tmpKata as $val2){	 
				$tmpVal = $val2;
				foreach($meta_data_kata as $val3){
					if ($val3->kata == $val2->kata) {
						$tmpVal->countS = $val3->count;
					}
				}
				array_push($new_meta_data_kata,$tmpVal);
			}
			$val['meta_data_kata'] = json_encode($new_meta_data_kata);
			$newDataBerita[] = $val;
		}
		// var_dump("<hr> newDataBerita : <hr>",json_encode($newDataBerita));
		// die;
		$listIdBerita = array();
		$listHasil = array();
		foreach($newDataBerita as $val){
			// var_dump("<hr> val newDataBerita : <hr>",($val));
			$test1 = json_decode($val['meta_data_kata']);
			// var_dump("<hr> test1 : <hr>",($test1));
			$sumAB = 0;
			$sqrtA = 0;
			$sqrtB = 0;
			foreach($test1 as $val2){
				$sumAB = $sumAB + ($val2->count * $val2->countS);
				$sqrtA = $sqrtA + ($val2->count * $val2->count);
				$sqrtB = $sqrtB + ($val2->countS * $val2->countS);
			}
			$divider = (sqrt($sqrtA) * sqrt($sqrtB));
			if ($divider > 0) {
				$hasil = $sumAB / $divider;
				if ($hasil > $threshold) {
					// var_dump("<hr>".$val['id_berita']." lebih dari threshold dengan score : ".$hasil);
					array_push($listIdBerita,$val['id_berita']);
					$listHasil[$val['id_berita']] = $hasil;
				} else {
					// var_dump("<hr>".$val['id_berita']." tidak lebih dari threshold dengan score : ".$hasil);
				}
			}
		}

		// Analisa Sentimen +/-
		// var_dump($newDataBerita);
		// die;
		// looping berita
		$listHasilSentimen = array();
		foreach ($newDataBerita as $key => $val) {
			// var_dump($val,"<hr>");
			// die;
			$tmpVal = array();
			$tmpVal['nilai_pos'] = 0;
			$tmpVal['nilai_neg'] = 0;
			if($_POST['kata_positif'] !== "") {
				// var_dump("masuk positif <hr>");
				// cari kemunculan kata positif
				$str_positif = explode(",",$_POST['kata_positif']);
				foreach ($str_positif as $key_pos => $val_pos) {
					// var_dump($val_pos,"<hr>");
					if(strpos(strtolower($val['isi_berita']),strtolower($val_pos))){
						// die("masuk positif");
						// $tmpVal['nilai_pos'] = $tmpVal['nilai_pos'] + substr_count(strtolower($val['isi_berita']),strtolower($val_pos));
						$tmpVal['nilai_pos'] = $tmpVal['nilai_pos'] + 1;
					}
				}
			}
			
			if($_POST['kata_negatif'] !== ""){
				// var_dump("masuk negatif <hr>");
				// cari kemunculan kata negatif
				$str_negatif = explode(",",$_POST['kata_negatif']);
				foreach ($str_negatif as $key_neg => $val_neg) {
					// var_dump($val_neg,"<hr>");
					if(strpos(strtolower($val['isi_berita']),strtolower($val_neg))){
						// die("masuk negatif");
						// $tmpVal['nilai_neg'] = $tmpVal['nilai_neg'] + substr_count(strtolower($val['isi_berita']),strtolower($val_neg));
						$tmpVal['nilai_neg'] = $tmpVal['nilai_neg'] + 1;
					}
				}
			}
			$listHasilSentimen[$val['id_berita']] = $tmpVal;
		}
		// var_dump($listHasilSentimen,"<hr>");
		// die;

		// Filter dan sorting hasil perhitungan dengan threshold
		// var_dump("<hr> listIdBerita : <hr>",($listIdBerita));
		$resBerita['select'] 	= 'b.id_berita, b.judul_berita, b.link_berita, b.isi_berita, b.meta_data_kata, DATE_FORMAT(b.publish_date, "%d %M %Y") AS publish_date, s.nama_situs';
		$resBerita['table']		= "m_berita as b";
		$resBerita['join'][0]	= array("m_situs_berita as s","b.id_situs_berita = s.id_situs_berita");
		if (isset($_POST['id_berita'])) {
			$resBerita['where']		= "b.id_berita IN ( ".$_POST['id_berita'].")";
		} else {
			if ($listIdBerita) {			
				$resBerita['where']		= "b.id_berita IN ( ".implode(" , ", $listIdBerita).")";
			} else {
				echo json_encode(array('msg' => "Hasil Pencarian Kosong"));
				exit;
			}
		}	
		// $resBerita['where']		.= " AND id_situs_berita IN (".$strIdSitusBerita.") ";
			
		$resBerita['order']		= array('b.publish_date','DESC');
		$data->resBerita 		= $this->m_admin->getData($resBerita);
		// var_dump("<hr> data->resBerita : <hr>",json_encode($data->resBerita));
		$resultDataBerita = array();
		$resultKeyword = array();
		foreach($data->resBerita as $res){
			$tmpRes = array();
			foreach($listHasil as $kli => $li){
				if($kli == $res['id_berita']){
					$res['hasil'] = $li*100;
				}
			}

			foreach($listHasilSentimen as $kli => $li){
				if($kli == $res['id_berita']){
					$res['nilai_positif'] = $li['nilai_pos'];
					$res['nilai_negatif'] = $li['nilai_neg'];
				}
			}

			foreach ($beritaPerKeyword as $word) {
				$pos = strpos($res['isi_berita'], $word->kata);
				if ($pos) {
					// var_dump("kata ".$word->kata." muncul di berita ".$res['id_berita']." di posisi ".$pos."<hr>");
					$word->count = $word->count + 1;
				}
				// array_push($resultKeyword,$word);
			}
			// $tmpRes['id_berita'] = $res['id_berita'];
			// $tmpRes['judul_berita'] = $res['judul_berita'];
			// $tmpRes['link_berita'] = $res['link_berita'];
			// $tmpRes['publish_date'] = $res['publish_date'];
			// $tmpRes['nama_situs'] = $res['nama_situs'];
			// $tmpRes['hasil'] = $res['hasil'];
			array_push($resultDataBerita,$res);
		}
		// var_dump($beritaPerKeyword);
		// die;
		// echo json_encode($result);
		echo json_encode(array('data_berita' => $resultDataBerita, 'beritaPerKeyword' => $beritaPerKeyword));
		
	}

	public function saveHistoryPencarian() {
		// var_dump($_POST);		
		$data['data']['id_setting_project'] 		= $_POST['id_setting_project'];
		$data['data']['kata_pencarian'] 		= $_POST['kata_pencarian'];
		$data['data']['threshold'] 		= $_POST['threshold'];
		$data['data']['lvl_situs'] 			= $_POST['lvl_situs'];
		$data['data']['startDate'] 	= $_POST['startDate'];
		$data['data']['endDate'] 		= $_POST['endDate'];
		$data['data']['kata_positif'] 		= $_POST['kata_positif'];
		$data['data']['kata_negatif'] 		= $_POST['kata_negatif'];
		$data['data']['id_berita'] 		= $_POST['id_berita'];
		
		$data['data']['cdd'] = date('Y-m-d H:i:s');
		$data['data']['cdb'] = $_SESSION['id_user'];
		$data['data']['status'] = 1;

		$data['table']		= "t_pencarian_setting_project";
		// $this->m_admin->addData($data);
		// redirect('admin/situs');
		if ($this->m_admin->addData($data)) {
			$json['status'] = TRUE;
			echo json_encode($json);
		}
		// die;
	}

	public function graphNews() {
		// var_dump($_POST['id_berita']);
		// die;
		$data = (object) array();
		if (is_array($_POST['id_berita'])) {
			$dataIdBerita = $_POST['id_berita'];
		} else if (is_string($_POST['id_berita'])) {
			$dataIdBerita = json_decode($_POST['id_berita']);
		} else {
			$dataIdBerita = "";
		}
		
		if ($dataIdBerita !== "") {		
			$data->topWord = $this->dashTopWord($dataIdBerita);
			$data->lokasi = $this->dashLokasi($dataIdBerita);
			$data->tanggal = $this->dashTanggal($dataIdBerita);
			echo json_encode($data);
		} else {
			echo json_encode($data);
		}
	}

	public function dashTopWord($arrIdBerita) {
		try {
			$limit = $_POST['limit'];
			$data = (object) array();
			$berita['select'] 	= "*";
			$berita['table']	= "m_berita";
			$berita['where']	= "id_berita IN ( ".implode(" , ", $arrIdBerita).") AND status = 1";
			$data->berita 		= $this->m_admin->getData($berita);

			$tmpKata = array();
			$result = array();
			foreach ($data->berita as $key => $val) {
				// var_dump(json_decode($val['meta_data_kata']));
				// die;
				foreach (json_decode($val['meta_data_kata']) as $key2 => $val2) {
					if (isset($tmpKata[$val2->kata])) {
						$tmpKata[$val2->kata]['berita'] = $tmpKata[$val2->kata]['berita'] + 1;
						$tmpKata[$val2->kata]['count'] = $tmpKata[$val2->kata]['count'] + $val2->count;
					} else {
						$tmpKata[$val2->kata]['berita'] = 1;
						$tmpKata[$val2->kata]['count'] = $val2->count;
					}
				}
			}
			
			arsort($tmpKata);
			$i = 0;
			foreach ($tmpKata as $key => $val) {
				$i++;
				$result[$key] = $val;
				if ($limit == "*") {  } elseif ($i == $limit) { break; }
			}
			return $result;
		} catch (Exception  $th) {
			return false;
		}
	}

	public function dashLokasi($arrIdBerita) {
		try {
			$data = (object) array();
			$berita['select'] 	= "id_berita, lokasi_prop, lokasi_kab";
			$berita['table']	= "m_berita";
			$berita['where']	= "id_berita IN ( ".implode(" , ", $arrIdBerita).") AND status = 1";
			$data->berita 		= $this->m_admin->getData($berita);
			$tmpLokasi = array();
			$result = array();
			foreach ($data->berita as $key => $val) {
				if ($val['lokasi_prop'] !== "[]") {
					// var_dump($val['lokasi_prop']);
					// die;
					$objLokasiProp = json_decode($val['lokasi_prop']);
					if (json_last_error() === JSON_ERROR_NONE) {
						foreach ($objLokasiProp as $key2 => $val2) {
							array_push($tmpLokasi,$key2);
						}
					}
				} elseif ($val['lokasi_kab'] !== "[]") {
					$objLokasiKab = json_decode($val['lokasi_kab']);
					if (json_last_error() === JSON_ERROR_NONE) {
						foreach ($objLokasiKab as $key2 => $val2) {
							$lokasi = explode(".",$key2);
							array_push($tmpLokasi,$lokasi[0]);
						}
					}
				}
				// $tmpLokasiProp = (object) array_merge(  (array) $tmpLokasiProp, (array) json_decode($val['lokasi_prop']) ); 
				// array_merge($tmpLokasiProp,json_decode($val['lokasi_prop']));
				
				// $tmpLokasiKab = $val['lokasi_kab'];
				// var_dump(json_decode($tmpLokasiKab));
			}
			
			$lokasiProp['select'] 	= "NO_PROP, NAMA_PROP as NAMA";
			$lokasiProp['table']	= "m_provinsi";
			$lokasiProp['where']	= "NO_PROP IN (".implode(" , ", array_unique($tmpLokasi)).")";
			$data->lokasiProp 		= $this->m_admin->getData($lokasiProp);
			// var_dump($data->lokasiProp);
			// die;

			foreach ($tmpLokasi as $val2) {			
				// var_dump($val2);
				// die;
				foreach ($data->lokasiProp as $val3) {
					if ($val3['NO_PROP'] == $val2) {
						# code...
						if (isset($result[$val2])) {
							$result[$val2]['berita'] = $result[$val2]['berita'] + 1;
						} else {
							$result[$val2]['berita'] = 1;
							$result[$val2]['nama_lokasi'] = $val3['NAMA'];
						}
					}
				}
			}
			// $tmpLokasi = array_unique($tmpLokasi);
			// asort($tmpLokasi);
			ksort($result);
			// var_dump($result);
			// die;
			return $result;
		} catch (Exception  $th) {
		}
	}

	public function dashTanggal($arrIdBerita) {
		try {
			$data = (object) array();
			$berita['select'] 	= 'DATE_FORMAT(publish_date,"%Y-%M-%d") as publish_date, COUNT(status) as jumlah';
			$berita['table']	= "m_berita";
			$berita['where']	= "id_berita IN ( ".implode(" , ", $arrIdBerita).") AND status = 1";
			$berita['group_by'] = 'DATE_FORMAT(publish_date, "%Y-%M-%d")';
			$data->berita 		= $this->m_admin->getData($berita);
			$tmpLokasi = array();
			$result = array();
			// $result = $data->berita;
			foreach ($data->berita as $key =>$val) {	
				// var_dump($val);
				// die;
				$result[$val['publish_date']] = $val['jumlah'];
			}
			return $result;
		} catch (Exception  $th) {
		}
	}

	public function getDataSetBerita() {
		$data = (object) array();
		$where_kata_kunci = "";
		if (isset($_POST['list_kata_kunci'])) {
			$kata_kunci['select'] = "kata_kunci";
			$kata_kunci['table'] = "m_kata_kunci";
			$kata_kunci['where'] = "id_kata_kunci IN (".$_POST['list_kata_kunci'].") AND status = 1 ";
			$data->kata_kunci 	= $this->m_admin->getData($kata_kunci);
			$where_kata_kunci = "( ";
			foreach ($data->kata_kunci as $key => $val) {
				$where_kata_kunci .= 'kata_kunci LIKE ';
				$where_kata_kunci .= "'%".$val['kata_kunci']."%' OR ";
			}
			$where_kata_kunci = substr($where_kata_kunci,0,-3);
			$where_kata_kunci .= " ) AND";
			// $where_kata_kunci[] = $kondisi;
			// var_dump($where_kata_kunci);
			// die;
		}
		$berita['select'] 	= 'b.id_berita, b.isi_berita, b.judul_berita, b.link_berita, b.publish_date, s.nama_situs';
		$berita['table']	= "m_berita as b";
		$berita['where']	= $where_kata_kunci;
		$berita['where']	.= " b.has_normalize = '1' AND b.status = '1'";
		$berita['join'][0]	= array("m_situs_berita as s","b.id_situs_berita = s.id_situs_berita");
		$berita['order']	= array('b.publish_date','DESC');
		$data->berita 		= $this->m_admin->getData($berita);
		// var_dump($data->berita);
		// die;
		echo json_encode(array('data_berita' => $data->berita));
	}
	
	public function save_msg_contact() {
		// var_dump($_POST);
		// die;
		if(isset($_POST['g-recaptcha-response'])){
            $captcha=$_POST['g-recaptcha-response'];
        } else {
			$captcha = false;
		}

        if(!$captcha){
			//Do something with error
			var_dump("captcha fail");
			die;
        } else {
            $secret = '6LcITLMUAAAAAMwU6XIExkln3BhoRagazuBrlHCW';
            $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
			// use json_decode to extract json response
            if($response.success==false) {
				//Do something with error
				var_dump("respon fail");
				die;
            }
        }
		$contactUs['name'] 			= $_POST['name'];
		$contactUs['email']			= $_POST['email'];
		$contactUs['subject'] 		= $_POST['subject'];
		$contactUs['message'] 		= $_POST['message'];
		$contactUs['isreply'] 		= "0";
		$contactUs['cdd'] 			= date('Y-m-d H:i:s');
		$contactUs['status'] 		= "1";

		$data['data'] 	= $contactUs;
		$data['table']	= "t_contact_us";
		
		$id_contact_us = $this->m_admin->addData($data); 
		echo json_encode(array('success' => TRUE));
	}

	public function form_user_pass($code) {
		$data = (object) array();
		$pegawai['select']		= "p.*";
		$pegawai['table']		= "m_pegawai as p";
		$pegawai['where']		= "p.code = '".$code."' AND p.code IS NOT NULL";
		$data->pegawai 			= $this->m_admin->getData($pegawai);
		
		echo $this->load->view('form_user_activate',$data,TRUE);
	}

	public function activate_user() {
		// var_dump($_POST);
		// die;
		$user['email'] 			= $_POST['email'];
		$user['password']			= sha1($_POST['password']);

		$data['data'] 	= $user;
		$data['table']	= "m_user";
		$data['where'][0]	= array('id_pegawai', $_POST['id_pegawai'] );

		$this->m_admin->updateData($data); 

		$pegawai['code']			= "";

		$data['data'] 	= $pegawai;
		$data['table']	= "m_pegawai";
		$data['where'][0]	= array('id_pegawai', $_POST['id_pegawai'] );

		$this->m_admin->updateData($data); 
		redirect(base_url().'admin/');
	}

	public function del_prefix($masukan) {
		// awalan : ber, bel, be, ke, se, te, ter, me, mem, men, meng, menge, pe, pem, pen, peng, penge, pe, pel, per, memper; Sumber : http://tatabahasabm.tripod.com/tata/awalan.htm
		$masukan=strtolower($masukan);
		$AW1=substr($masukan,0,6);
		if($AW1=="memper") {
			$keluaran = substr($masukan,6,strlen($masukan));
		} else {
			$AW1=substr($masukan,0,3);
			if($AW1=="ber") {
				$keluaran = substr($masukan,3,strlen($masukan));
			} else {
				$AW1=substr($masukan,0,4);
				if($AW1=="bela") {
					$keluaran = substr($masukan,3,strlen($masukan));
				} else {
					$AW1=substr($masukan,0,2);
					if($AW1=="di" ) {
						$keluaran = substr($masukan,2,strlen($masukan));
					} else {
						$AW1=substr($masukan,0,2);
						if($AW1=="ke") {
							$keluaran = substr($masukan,2,strlen($masukan));
						} else {
							$AW1=substr($masukan,0,2);
							if($AW1=="ku") {
								$keluaran = substr($masukan,2,strlen($masukan));
							} else {
								$AW1=substr($masukan,0,3);
								if($AW1=="kau") {
									$keluaran = substr($masukan,3,strlen($masukan));
								} else {
									$AW1=substr($masukan,0,2);
									if($AW1=="me") {
										$AW1=substr($masukan,0,4);
										if($AW1=="memb") {
											$keluaran = substr($masukan,3,strlen($masukan));
										} else {
											$AW1=substr($masukan,0,4);
											if($AW1=="mend" || $AW1=="menf" || $AW1=="menj") {
												$keluaran = substr($masukan,3,strlen($masukan));
											} else {
												$AW1=substr($masukan,0,4);
												if($AW1=="meny") {
													$keluaran = "s".substr($masukan,4,strlen($masukan));
												} else {
													$AW1=substr($masukan,0,4);
													if($AW1=="meng") {
														if(substr($masukan,4,1)=="a" || substr($masukan,4,1)=="e" || substr($masukan,4,1)=="g" || substr($masukan,4,1)=="h" || substr($masukan,4,1)=="i" || substr($masukan,4,1)=="o" || substr($masukan,4,1)=="u") {
															$keluaran = substr($masukan,4,strlen($masukan));
														} else {
															$keluaran = $masukan;
														}
													} else {
														$AW1=substr($masukan,0,3);
														if($AW1=="men") {
															$keluaran = "t".substr($masukan,3,strlen($masukan));
														} else {
															if(substr($masukan,2,1)=="l" || substr($masukan,2,1)=="m" || substr($masukan,2,1)=="n" || substr($masukan,2,1)=="r") {
																$keluaran = substr($masukan,2,strlen($masukan));
															} else {
																$keluaran = $masukan;
															}
														}
													}
												}
											}
										}
									} else {
										$AW1=substr($masukan,0,2);
										if($AW1=="pe") {
											$keluaran = substr($masukan,2,strlen($masukan));
										} else {
											$AW1=substr($masukan,0,2);
											if($AW1=="se") {
												$keluaran = substr($masukan,2,strlen($masukan));
											} else {
												$AW1=substr($masukan,0,3);
												if($AW1=="ter") {
													$keluaran = substr($masukan,3,strlen($masukan));
												} else {
												/*
													$AW1=substr($masukan,0,3);
													if($AW1=="eka") {
														$keluaran = substr($masukan,3,strlen($masukan));
													} else {
														$AW1=substr($masukan,0,6);
														if($AW1=="ekstra") {
															$keluaran = substr($masukan,6,strlen($masukan));
														} else {
															$AW1=substr($masukan,0,3);
															if($AW1=="eks") {
																$keluaran = substr($masukan,3,strlen($masukan));
															} else {
																$AW1=substr($masukan,0,5);
																if($AW1=="intra") {
																	$keluaran = substr($masukan,5,strlen($masukan));
																}
															}
														}
													}*/
													$keluaran = $masukan;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return $keluaran;
	}
	
	public function del_suffix($masukan) {
		$masukan=strtolower($masukan);
		$AK = substr($masukan,(strlen($masukan)-3),3);
		if($AK == "nda") {
			$keluaran = substr($masukan,0,(strlen($masukan)-3));
		} else {
			$AK = substr($masukan,(strlen($masukan)-3),3);
			if($AK == "nya") {
				$keluaran = substr($masukan,0,(strlen($masukan)-3));
			} else {
				$AK = substr($masukan,(strlen($masukan)-3),3);
				if($AK == "kan" && strlen($masukan)>5) {
					$keluaran = substr($masukan,0,(strlen($masukan)-3));
				} else {
					$AK = substr($masukan,(strlen($masukan)-2),2);
					if($AK == "an" && strlen($masukan)>5) {
						$keluaran = substr($masukan,0,(strlen($masukan)-2));
					} else {
						$AK = substr($masukan,(strlen($masukan)-2),2);
						if($AK == "ku" && strlen($masukan)>6) {
							$keluaran = substr($masukan,0,(strlen($masukan)-2));
						} else {
							$AK = substr($masukan,(strlen($masukan)-2),2);
							if($AK == "mu" && strlen($masukan)>6) {
								$keluaran = substr($masukan,0,(strlen($masukan)-2));
							} else {
								$AK = substr($masukan,(strlen($masukan)-1),1);
								if($AK == "i" && strlen($masukan)>6) {
									$keluaran = substr($masukan,0,(strlen($masukan)-1));
								} else {
									$keluaran = $masukan;
								}
							}
						}
					}
				}
			}
		}
		return $keluaran;
	}
	
	public function doStemming($word) {
		//  pengecekan apakah kata sudah termasuk kata dasar, jika belum lanjutkan proses steamming
		
		// 	var_dump("in_array : ",in_array($word,$_SESSION['katadasar']));
		// 	die;
		if (in_array($word,$_SESSION['katadasar'])) {
			$s2 = $word;
		} else {
			$p1 = $this->del_prefix($word);
			if (in_array($p1,$_SESSION['katadasar'])) {
				$s2 = $p1;
			} else {
				$p2 = $this->del_prefix($p1);
				if (in_array($p2,$_SESSION['katadasar'])) {
					$s2 = $p2;
				} else {
					$p3 = $this->del_prefix($p2);
					if (in_array($p3,$_SESSION['katadasar'])) {
						$s2 = $p3;
					} else {
						$s1 = $this->del_suffix($p3);
						if (in_array($s1,$_SESSION['katadasar'])) {
							$s2 = $s1;
						} else {
							$s2 = $this->del_suffix($s1);
						}
					}
				}
			}
		}
		return $s2;
	}

}
