<section id="list_mutasi" class="sections lightbg">
    <div class="container">

      <div class="heading-content text-center">
          <h3>Daftar Pengajuan Pindah Sekolah</h3>
      </div>

      <!-- Example row of columns -->
      <div class="row">

        <!-- <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="features-content">
                <i class="fa fa-file-text" onclick="add('lkdk');"></i>
                <h5>Mutasi Luar Kota &rArr; Dalam Kota</h5>
            </div>
        </div> -->

        <div class="nav-tabs-custom">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true" title="Daftar Form Pindah Sekolah Yang Sedang Dipriksa">Daftar Pengajuan Pindah Sekolah Yang Sedang Dipriksa</a></li>
            <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="true" title="Daftar Form Pindah Sekolah Yang Sudah Dipriksa">Daftar Pengajuan Pindah Sekolah Yang Sudah Dipriksa</a></li>
          </ul>
          <div class="tab-content">
            <!-- /.tab-pane -->
            <div class="tab-pane active" id="tab_1">
              <button type="button" class="btn btn-primary fa fa-refresh" onclick="reload_table()" title="Refresh Table"></button> 
              <!-- <button type="button" class="btn btn-success fa fa-plus" data-toggle="modal" data-target="#modal_progres"></button>  -->
              <div class="table-responsive">
                <table id="not_valid" class="table table-bordered text-center" style="width: 100% !important;">
                      <thead>
                        <tr>
                          <td>No</td>
                          <td>Nama Siswa</td>
                          <td>Sekolah Asal</td>
                          <td>Sekolah Tujuan</td>
                          <td width="100px">Status Pengajuan</td>
                          <td>Edit</td>
                        </tr>
                      </thead>
                    </table>
              </div> 
             </div>

            <div class="tab-pane" id="tab_2">
              <button type="button" class="btn btn-primary fa fa-refresh" onclick="reload_table()" title="Refresh Table"></button>
              <div class="table-responsive">
                <table id="valid" class="table table-bordered text-center" style="width: 100% !important;">
                  <thead>
                    <tr>
                      <td>No</td>
                      <td>Nama Siswa</td>
                      <td>Sekolah Asal</td>
                      <td>Sekolah Tujuan</td>
                      <td width="100px">Status Pengajuan</td>
                      <td>Unduh</td>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div>
        <!-- /.tab-content -->
        </div>

        <dir class="col-md-6">
          <p> Keterangan Status Pengajuan </p>
          <table border="0" class='table table-hover'>
            <thead>
              <tr>
                <td>Warna </td>
                <td> Keterangan</td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><button class="btn btn-danger"></button></td>
                <td> Permohonan Ditolak, Perlu Perbaikan</td>
              </tr>
              <tr>
                <td><button class="btn btn-primary"></button></td>
                <td> Permohonan Sedang diproses</td>
              </tr>
              <tr>
                <td><button class="btn btn-success"></button></td>
                <td> Permohonan Sudah Selesai</td>
              </tr>
            </tbody>
          </table>
        </dir>

      </div>

    </div> <!-- /container -->   

    <div id="modal_progres" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 id="judul_progres" class="modal-title">Progres Berkas</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12">
                <div style="display:inline-block;width:100%;overflow-y:auto;">
                  <ul class="timeline timeline-horizontal">

                    <li class="timeline-item">
                      <div id="div_status_pengajuan" class="timeline-badge"><span><i class="glyphicon glyphicon-check" id="icon"></i></span></div>
                      <div id="div_pengajuan" class="timeline-panel">
                        <div class="timeline-heading"> 
                          <!-- <p id="title_pengajuan" class="timeline-title" style="font-size: : 15px; font-weight: bold;">Pengajuan Pindah Sekolah</p><hr> -->
                          <p id="tgl_pengajuan"></p>
                          <div id="div_edit_progres"></div>
                         </div>
                      </div>
                    </li>

                    <li class="timeline-item">
                      <div id="div_status_valid" class="timeline-badge"><span><i class="glyphicon glyphicon-check"></i></span> </div>
                      <div id="div_valid" class="timeline-panel">
                        <div id="div_valid_icon">
                          <img src="<?php echo base_url('assets/images/timeline/verifikator/Pengecekan-Kelengkapan-Berkas.png');?>" width="100%">
                        </div>
                        <!-- <div class="timeline-heading">
                          <p class="timeline-title" style="font-size: : 15px; font-weight: bold;">Pengecekan Kelengkapan Berkas</p><hr>
                          <p id="tgl_valid"></p>
                        </div> -->
                      </div>
                    </li>

                    <li class="timeline-item">
                      <div id="div_status_paraf" class="timeline-badge"><i class="glyphicon glyphicon-check"></i></div>
                      <div id="div_paraf" class="timeline-panel">
                      <div id="div_paraf_icon">
                          <img src="<?php echo base_url('assets/images/timeline/paraf/Persetujuan-Draft-Oleh-Kepala-Seksi.png');?>" width="100%">
                      </div>
                        <!-- <div class="timeline-heading">
                          <p class="timeline-title" style="font-size: : 15px; font-weight: bold;">Persetujuan Draft Surat Oleh Kepala Seksi</p><hr>
                          <p id="tgl_paraf"></p>
                        </div> -->
                      </div>
                    </li>

                    <li class="timeline-item">
                      <div id="div_status_ttd" class="timeline-badge"><i class="glyphicon glyphicon-check"></i></div>
                      <div id="div_ttd" class="timeline-panel">
                      <div id="div_ttd_icon">
                          <img src="<?php echo base_url('assets/images/timeline/ttd/Persetujuan-Draft-Oleh-Kepala-Bidang.png');?>" width="100%">
                      </div>
                        <!-- <div class="timeline-heading">
                          <p class="timeline-title" style="font-size: : 15px; font-weight: bold;">Persetujuan Surat Oleh Kepala Bidang</p><hr>
                          <p id="tgl_ttd"></p>
                        </div> -->
                      </div>
                    </li>

                    <li class="timeline-item">
                      <div id="div_status_selesai" class="timeline-badge"><span><i class="glyphicon glyphicon-check"></i></span> </div>
                      <div id="div_selesai" class="timeline-panel">
                      <div id="div_selesai_icon">
                          <img src="<?php echo base_url('assets/images/timeline/selesai/Selesai.png');?>" width="100%">
                      </div>
                        <!-- <div class="timeline-heading">
                          <p class="timeline-title" style="font-size: : 15px; font-weight: bold;">Pengiriman Surat. Persetujuan Selesai</p><hr>
                          <p id="tgl_selesai"></p>
                        </div> -->
                      <!-- </div> -->
                    </li>
                  </ul>
                  <div class="alert alert-success" id="info_timeline"  style="text-align: center;" >
                      <span id="info"></span>
                  </div>

                  <div id="load_view_history">
                    <table id="myTable_history" class="table table-hover">
                      <tbody>
                        <!-- <tr>
                          <td>test</td>
                        </tr> -->
                      </tbody>
                    </table>
                  </div>

                <!-- <div id="div_pesan_penolakan" style="display: none;">
                  <h5>Pesan Penolakan Permohonan :</h5>
                  <p class="alert alert-danger" id="pesan_penolakan" style="font-weight: bold;"></p>
                </div> -->
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
          </div>
        </div>

      </div>
    </div>
</section>
<script type="text/javascript">

var table1;
var table2;

$(document).ready(function() {

  //datatables
  table1 = $('#valid').DataTable({ 

    "processing"  : true, //Feature control the processing indicator.
    "serverSide"  : true, //Feature control DataTables' server-side processing mode.
    "searchDelay" : 0.5 * 1000,
    "order": [], //Initial no order.

    // Load data for the table's content from an Ajax source
    "ajax": {
        "url": "<?php echo base_url('frontend/ajax/ajax_list?type=valid')?>",
        "type": "POST"
    },

    //Set column definition initialisation properties.
    "columnDefs": [
    { 
        "targets": [ 2, 3, 4, 5 ], //first column / numbering column
        "orderable": false, //set not orderable
    },
    ],
  });

  table2 = $('#not_valid').DataTable({ 

    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "searchDelay" : 0.5 * 1000,
    "order": [], //Initial no order.

    // Load data for the table's content from an Ajax source
    "ajax": {
        "url": "<?php echo base_url('frontend/ajax/ajax_list?type=not_valid')?>",
        "type": "POST"
    },

    //Set column definition initialisation properties.
    "columnDefs": [
    { 
        "targets": [ 2, 3, 4, 5 ], //first column / numbering column
        "orderable": false, //set not orderable
    },
    ],
  });

});

function progres(id) {
 // alert('test')
  $.ajax({ url : "<?php echo base_url('frontend/ajax_get_status_mutasi_dua/'); ?>"+id,
    type : 'GET',
    dataType : 'JSON',
    success: function(data) {
    
      // for (var i = 0; i < data.status_mutasi.length; i++) 
      // console.log(data.status_mutasi);
      //  $('#Modal_lkdk #skl_tujuan').append('<option value="' + data.daftar_skl_tujuan[i].id_sekolah +'|*'+ data.daftar_skl_tujuan[i].nama_sekolah + '">' + data.daftar_skl_tujuan[i].nama_sekolah + '</option>');
  
      $('#modal_progres #judul_progres').html("Progres Pengajuan : "+data.status_mutasi[0].nama_siswa+" ("+data.status_mutasi[0].skl_asal+" Ke "+data.status_mutasi[0].skl_tujuan+")");
      // for (var i = 0; i < data.status_mutasi.length; i++) {
      //   $('#modal_progres #myTable_history').append( '<tr class="child"><td>'+data.status_mutasi[i].create_by+'</td><td>'+data.status_mutasi[i].create_date+'</td><td>'+data.status_mutasi[i].pesan_mutasi+'</td>');
      // }
      // '<tr class="child"><td>blahblah</td></tr>'
      // alert(data.status_mutasi[0].create_by+" || "+data.status_mutasi[0].create_date+" || "+data.status_mutasi[0].pesan_mutasi);
      // $('#modal_progres #create_by').val(data.status_mutasi[0].create_by);
      // Reset status
      $('#modal_progres #div_status_pengajuan').removeClass("success");
      $('#modal_progres #div_status_pengajuan').removeClass("danger");
      $('#modal_progres #div_status_valid').removeClass("success");
      $('#modal_progres #div_status_paraf').removeClass("success");
      $('#modal_progres #div_status_ttd').removeClass("success");
      $('#modal_progres #div_status_selesai').removeClass("success");

      // $('#modal_progres #div_status_valid').removeClass("danger"); 

      $('#modal_progres #div_pengajuan').attr("style","");
      $('#modal_progres #div_valid').attr("style","");
      $('#modal_progres #div_paraf').attr("style","");
      $('#modal_progres #div_ttd').attr("style","");
      $('#modal_progres #div_selesai').attr("style","");

      $('#modal_progres #div_pesan_penolakan').attr("style","display:none;"); 
      $('#modal_progres #tgl_pengajuan').html('');
      $('#modal_progres #tgl_valid').html('');
      $('#modal_progres #tgl_paraf').html('');
      $('#modal_progres #tgl_ttd').html('');
      $('#modal_progres #tgl_selesai').html('');
      $('#modal_progres #info_timeline #info').html('');
      $('#modal_progres #table-timeline').html('');
      $('#modal_progres #pesan_penolakan').html('');
      $('#modal_progres #div_edit_progres').html('');

      $('#modal_progres #div_pengajuan').html('');
      $('#modal_progres #div_paraf').html('');
      $('#modal_progres #div_ttd').html('');
      $('#modal_progres #div_selesai_icon').html('');
        

      $('#modal_progres #tgl_pengajuan').attr("style","");
      $('#modal_progres #tgl_valid').attr("style","");
      $('#modal_progres #tgl_paraf').attr("style","");
      $('#modal_progres #tgl_ttd').attr("style","");
      $('#modal_progres #tgl_selesai').attr("style","");

      var step_1 = false;
      var step_2 = false;
      var step_3 = false;
      var step_4 = false;
      var step_5 = false;
      //Step 1 & 2

      $('#modal_progres #load_view_history #myTable_history').html('');
      
      for (var i = 0; i < data.status_mutasi.length; i++) {
        var warna = data.status_mutasi[i].status_warna;
        var aktor = data.status_mutasi[i].create_by;
        if(data.status_mutasi[i].create_by==1){
          aktor = "Warga";
        }else{
          aktor = "Admin";
        }

        if(data.status_mutasi[i].status_warna==0){
          warna = 'class="success">';
          // warna = 'style="background:red"';  class="table-success">
        }else{
          warna = 'class="danger">';
          // warna = 'style="background:green"';
        }
        $('#modal_progres #load_view_history #myTable_history').append( '<tr '+warna+'><td>'+aktor+'</td><td>'+data.status_mutasi[i].create_date+'</td><td>'+data.status_mutasi[i].pesan_mutasi+'</td>');
      }

      if ((data.status_mutasi[0].valid_status == 0) && (data.status_mutasi[0].status_dibuka == 0)) {
        $('#modal_progres #info_timeline').removeClass("alert-danger"); 
        $('#modal_progres #info').append("PENGAJUAN BERKAS");// adun
        $('#modal_progres #div_status_pengajuan').addClass("success"); 
        $('#modal_progres #div_status_pengajuan #icon').removeClass('glyphicon glyphicon-remove').addClass('glyphicon glyphicon-check')
        $('#modal_progres #div_pengajuan').html(' <img src="<?php echo base_url('assets/images/timeline/pengajuan/Pengajuan-Pindah-Sekolah-Hijau.png'); ?>" width="100%">');
        $('#modal_progres #div_valid').html(' <img src="<?php echo base_url('assets/images/timeline/verifikator/Pengecekan-Kelengkapan-Berkas.png'); ?>" width="100%">');
        $('#modal_progres #div_paraf').html(' <img src="<?php echo base_url('assets/images/timeline/paraf/Persetujuan-Draft-Oleh-Kepala-Seksi.png'); ?>" width="100%">');
        $('#modal_progres #div_ttd').html(' <img src="<?php echo base_url('assets/images/timeline/ttd/Persetujuan-Draft-Oleh-Kepala-Bidang.png'); ?>" width="100%">');
        $('#modal_progres #div_selesai_icon').html(' <img src="<?php echo base_url('assets/images/timeline/selesai/Selesai.png'); ?>" width="100%">');
        
        step_1 = true;
      } else  if ((data.status_mutasi[0].valid_status == 1) && (data.status_mutasi[0].status_dibuka == 1)) { 
        //console.log("step 1");
        // console.log("step 2");
        $('#modal_progres #div_status_pengajuan #icon').removeClass('glyphicon glyphicon-remove').addClass('glyphicon glyphicon-check')
        $('#modal_progres #div_status_pengajuan').addClass("success"); 
        $('#modal_progres #div_pengajuan').html(' <img src="<?php echo base_url('assets/images/timeline/pengajuan/Pengajuan-Pindah-Sekolah-Hijau.png'); ?>" width="100%">');
        $('#modal_progres #div_valid').html(' <img src="<?php echo base_url('assets/images/timeline/verifikator/Pengecekan-Kelengkapan-Berkas-Hijau.png'); ?>" width="100%">');
        $('#modal_progres #div_paraf').html(' <img src="<?php echo base_url('assets/images/timeline/paraf/Persetujuan-Draft-Oleh-Kepala-Seksi.png'); ?>" width="100%">');
        $('#modal_progres #div_ttd').html(' <img src="<?php echo base_url('assets/images/timeline/ttd/Persetujuan-Draft-Oleh-Kepala-Bidang.png'); ?>" width="100%">');
        $('#modal_progres #div_selesai_icon').html(' <img src="<?php echo base_url('assets/images/timeline/selesai/Selesai.png'); ?>" width="100%">');
       
        $('#modal_progres #info_timeline').removeClass("alert-danger"); 
        $('#modal_progres #div_status_valid').addClass("success");
        $('#modal_progres #info').append("PENGECEKAN KELENGKAPAN BERKAS OLEH VERIFIKATOR");  
        
        step_1 = true;
        step_2 = true;
      } else  if ((data.status_mutasi[0].valid_status == 0) && (data.status_mutasi[0].status_dibuka == 1)) {
        // console.log("step 1 red");
        $('#modal_progres #div_status_pengajuan').removeClass("success"); 
        $('#modal_progres #div_status_pengajuan').addClass("danger"); 
        $('#modal_progres #div_status_pengajuan #icon').removeClass('glyphicon glyphicon-check').addClass('glyphicon glyphicon-remove');
        $('#modal_progres #div_pengajuan').html(' <img src="<?php echo base_url('assets/images/timeline/pengajuan/Pengajuan-Pindah-Sekolah-Merah.png'); ?>" width="100%">');
        $('#modal_progres #div_valid').html(' <img src="<?php echo base_url('assets/images/timeline/verifikator/Pengecekan-Kelengkapan-Berkas.png'); ?>" width="100%">');
        $('#modal_progres #div_paraf').html(' <img src="<?php echo base_url('assets/images/timeline/paraf/Persetujuan-Draft-Oleh-Kepala-Seksi.png'); ?>" width="100%">');
        $('#modal_progres #div_ttd').html(' <img src="<?php echo base_url('assets/images/timeline/ttd/Persetujuan-Draft-Oleh-Kepala-Bidang.png'); ?>" width="100%">');
        $('#modal_progres #div_selesai_icon').html(' <img src="<?php echo base_url('assets/images/timeline/selesai/Selesai.png'); ?>" width="100%">');
       
        // class="alert alert-success" id="info_timeline"
        $('#modal_progres #info_timeline').addClass("alert-danger");  
        // $('#modal_progres #info').append(" <strong> PENGAJUAN HARUS DIPERBAIKI </strong> <br>Pesan Penolakan :" +data.status_mutasi[0].pesan_penolakan);  
        
        if (data.status_mutasi[0].pesan_penolakan == null) {
          $('#modal_progres #info').append("Form Pengajuan Anda Ditolak :"  +data.status_mutasi[0].btn_edit);
        } else {
          $('#modal_progres #info').append("Form Pengajuan Anda Ditolak : "+data.status_mutasi[0].pesan_penolakan+ "<br>" +data.status_mutasi[0].btn_edit);
        }

        $('#modal_progres #div_pesan_penolakan').attr("style","display:block;"); 
        $('#modal_progres #tgl_pengajuan').attr("style","display:none;"); 
        $('#modal_progres #tgl_valid').attr("style","display:none;");  

        $('#modal_progres #div_edit_progres').html(data.status_mutasi[0].btn_edit); 
              
        // if (data.status_mutasi[0].pesan_penolakan == null) {
        //   $('#modal_progres #pesan_penolakan').append("Form Pengajuan Anda Ditolak");
        // } else {
        //   $('#modal_progres #pesan_penolakan').append("Form Pengajuan Anda Ditolak : "+data.status_mutasi[0].pesan_penolakan);
        // }
        
      }
      //alert(step_2);
      // Step 3
      if ((step_1==true) && (step_2==true)) {
       //alert('step_2');
        if ((data.status_mutasi[0].status_paraf == 1) && (data.status_mutasi[0].pesan_penolakan == null)) {
          // console.log("step 3");
          //alert(step_2);
          // $('#modal_progres #div_ttd').html('');
          // $('#modal_progres #div_selesai_icon').html('');
          $('#modal_progres #info_timeline').removeClass("alert-danger"); 
          $('#modal_progres #info_timeline #info').html('');
          $('#modal_progres #info').append("PERSETUJUAN DRAFT SURAT OLEH KEPALA SEKSI");  
          $('#modal_progres #div_status_paraf').addClass("success");
          // $('#modal_progres #div_ttd').removeClass("success");  
          // $('#modal_progres #div_selesai_icon').removeClass("success");  
          //$('#modal_progres #div_status_ttd').removeClass("success"); 

          $('#modal_progres #div_paraf').html(' <img src="<?php echo base_url('assets/images/timeline/paraf/Persetujuan-Draft-Oleh-Kepala-Seksi-Hijau.png'); ?>" width="100%">');
          
          step_3 = true;
          
        } else if ((data.status_mutasi[0].status_paraf == 0) && (data.status_mutasi[0].pesan_penolakan != null)) {
          // console.log("step 3 else");
          $('#modal_progres #div_status_pengajuan').removeClass("success"); 
          $('#modal_progres #div_status_pengajuan').addClass("danger");  

          $('#modal_progres #div_pesan_penolakan').attr("style","display:block;"); 
          $('#modal_progres #tgl_pengajuan').attr("style","display:none;"); 
          $('#modal_progres #tgl_valid').attr("style","display:none;");  

          $('#modal_progres #pesan_penolakan').append("Form Pengajuan Anda Ditolak : "+data.status_mutasi[0].pesan_penolakan);
        }
        // step 4 dan 5
      } 
      
      if ((step_1==true) && (step_2==true) && (step_3==true)) {
       // alert(step_3);
       console.log(data.status_mutasi[0]);
      //  alert(data.status_mutasi[0].pesan_penolakan);
        if ((data.status_mutasi[0].status_ttd == 1) && (data.status_mutasi[0].pesan_penolakan == null)) {
          // console.log("step 4 & 5");
          //alert('test');
          $('#modal_progres #info_timeline').removeClass("alert-danger"); 
          $('#modal_progres #info_timeline #info').html('');
          $('#modal_progres #info').append("SURAT DIKIRIM KE EMAIL");
          $('#modal_progres #div_status_ttd').addClass("success"); 
          $('#modal_progres #div_status_selesai').addClass("success"); 
          // $('#modal_progres #div_ttd').attr("style","background-color:#4ff222");
          $('#modal_progres #div_ttd').html(' <img src="<?php echo base_url('assets/images/timeline/ttd/Persetujuan-Draft-Oleh-Kepala-Bidang-Hijau.png'); ?>" width="100%">');
          $('#modal_progres #div_selesai_icon').html(' <img src="<?php echo base_url('assets/images/timeline/selesai/Selesai-Hijau.png'); ?>" width="100%">');
         
          // $('#modal_progres #div_selesai').html('<small class="text-muted"><i class="glyphicon glyphicon-time" style="color:#075000;"> '+data.status_mutasi[0].berkas_date+'</i></small>');

          // $('#modal_progres #div_status_selesai').addClass("success"); 
          //$('#modal_progres #div_selesai').attr("style","background-color:#4ff222");
          // $('#modal_progres #tgl_selesai').html('<small class="text-muted"><i class="glyphicon glyphicon-time" style="color:#075000;"> '+data.status_mutasi[0].berkas_date+'</i></small>');
        } else if ((data.status_mutasi[0].status_ttd != 1) && (data.status_mutasi[0].pesan_penolakan != null)) {
          // console.log("step 4 & 5 else");
          $('#modal_progres #div_status_pengajuan').removeClass("success"); 
          $('#modal_progres #div_status_pengajuan').addClass("danger");  
          $('#modal_progres #div_edit_progres').html(data.status_mutasi[0].btn_edit);

          // $('#modal_progres #div_status_valid').removeClass("success"); 
          // $('#modal_progres #div_status_valid').addClass("danger");  

          $('#modal_progres #div_pengajuan').attr("style","background-color:#e11e1e"); 
          // $('#modal_progres #div_valid').attr("style","background-color:#e11e1e"); 

          $('#modal_progres #div_pesan_penolakan').attr("style","display:block;"); 
          $('#modal_progres #tgl_pengajuan').attr("style","display:none;"); 
          $('#modal_progres #tgl_valid').attr("style","display:none;");  

          $('#modal_progres #pesan_penolakan').append("Form Pengajuan Anda Ditolak : "+data.status_mutasi[0].pesan_penolakan);
        }
      }
          // alert("step_1 "+step_1);
          // alert("step_2 "+step_2);
          // alert("step_3 "+step_3);
          // alert("step_4 "+step_4);
          // alert("step_5 "+step_5);
      $("#modal_progres").modal({
        backdrop: 'static'
      });
    },
    error: function (jqXHR, textStatus, errorThrown) {
      swal("Error!", "Aplikasi gagal mendapatkan data!", "error");
    }
  });
}

    // function edit_progres(type,id) {
    //   console.log(type,id);
    //   $("#modal_progres").modal("hide");
    //   $('body').addClass('modal-open');
    //   $('body').attr('style','');
    //   edit(type,id);
    // }

function edit_progres(type,id) {
    console.log(type,id);
      $("#modal_progres").modal("hide");
      $('.modal').on('hidden.bs.modal', function (e) {
      $('body').addClass('modal-open');
      });
      $('body').attr('style','');
      edit(type,id);
  }

function edit(type,id, jenjang_sekolah){
  save_method = 'edit';
  switch(type) {
    case 'dklk':
        $('.prev_f_surat_pindah').attr('src',"");

        $('#form_dklk')[0].reset(); // reset form on modals
        get_prov('#Modal_dklk #prov_tujuan');   
        get_pekerjaan('#Modal_dklk #pkj_wali');   
        $('#Modal_dklk #btnSave').text('simpan'); //change button text
        $('#Modal_dklk #btnSave').attr('disabled',false); //set button enable
        break;
    case 'lkdk':
        $('.prev_surat_dinas_asal').attr('src',"");
        $('.prev_surat_akreditasi').attr('src',"");

        $('#form_lkdk')[0].reset(); // reset form on modals
        get_prov('#Modal_lkdk #prov_asal');   
        get_pekerjaan('#Modal_lkdk #pkj_wali'); 
        $('#Modal_lkdk #btnSave').text('simpan'); //change button text
        $('#Modal_lkdk #btnSave').attr('disabled',false); //set button enable
        break;
    default:
        $('.prev_surat_pindah').attr('src',"");
        $('.prev_penerimaan_sekolah_baru').attr('src',"");        
        $('.prev_surat_akreditasi').attr('src',"");

        $('#form_dkdk')[0].reset(); // reset form on modals  
        get_pekerjaan('#Modal_dkdk #pkj_wali'); 
        $('#Modal_dkdk #btnSave').text('simpan'); //change button text
        $('#Modal_dkdk #btnSave').attr('disabled',false); //set button enable
  }
  
  $('.prev_rapot_identitas_siswa').attr('src',"");
  $('.prev_rapot_nilai_akhir').attr('src',"");
  $('.prev_rapot_riwayat_pindah').attr('src',"");

  $('.form-group').removeClass('has-error'); // clear error class
  $('.col-md-12').removeClass('has-error'); // clear error class
  $('.help-block').empty(); // clear error string
  $('.help-block').removeClass('alert alert-danger'); // clear error string
  // $("#Modal_lkdk #jenjang_sd").hide();
  // $("#Modal_lkdk #jenjang_smp").hide();
  $('.btnNext').hide();
  $('.btnPrev').show();
  $('.btnSave').show();
  $('.btnCancel').show();
  $.ajax({ url : "<?php echo base_url('frontend/ajax_get_mutasi/'); ?>"+id,
      type : 'GET',
      dataType : 'JSON',
      success: function(data) {
        // console.log(data.detail[0]); 
        $('[name="id"]').val(data.detail[0].id_form_mutasi_siswa);
        $('[name="id_siswa"]').val(data.detail[0].id_siswa);
        $('[name="id_jenjang_sekolah"]').val(data.detail[0].id_jenjang_sekolah);
        $('[name="id_status_domisili"]').val(data.detail[0].status_warga);
        $('[name="id_warga"]').val(data.detail[0].id_warga);
        $('[name="id_validasi"]').val(data.detail[0].id_validasi_mutasi_siswa);
        $('[name="nik"]').val(data.detail[0].nik);
        $('[name="nisn"]').val(data.detail[0].nisn);
        $('[name="nama_siswa"]').val(data.detail[0].nama_siswa);
        // $('[name="kelas"]').val(data.detail[0].kelas);
        $('[name="tmp_lhr"]').val(data.detail[0].tmp_lhr);
        $('[name="tgl_lhr"]').val(data.detail[0].tgl_lhr);
        //$('[name="kelas"]').val(data.detail[0].kelas);
        
        $('[name="nama_wali"]').val(data.detail[0].nama_wali);
        $('[name="pkj_wali"]').val(data.detail[0].pkj_wali);
        $('[name="alamat"]').text(data.detail[0].alamat_tinggal_sekarang); 
        

        var arr_asal = data.detail[0].wilayah_skl_asal.split('.'); 
        var arr_tujuan = data.detail[0].wilayah_skl_tujuan.split('.');        
        // console.log(arr_asal,arr_tujuan);

        switch(type) {
          case 'dklk': 
            // $("#Modal_dklk #id_jenjang_sekolah").val(jenjang_sekolah); 
            // alert(jenjang_sekolah)
            if (data.detail[0].pkj_wali == "LAINNYA") {
              $("#Modal_dklk #div_pkj_wali_lain").show();
              $('[name="pkj_wali_lain"]').val(data.detail[0].pkj_wali_lain);
            } else {
              $("#Modal_dklk #div_pkj_wali_lain").hide();
            }
            
            if ( (data.detail[0].status_warga=="1")|| (data.detail[0].status_warga=="2")) {
              $("#Modal_dklk #domisili_siswa ").val("dk");
            }else{
              $("#Modal_dklk #domisili_siswa ").val("lk");
            }

            if (data.detail[0].id_jenjang_sekolah=='2'){
              $("#Modal_dklk #jenjang_sd").show();
              $("#Modal_dklk #jenjang_smp").hide();

              if (data.detail[0].kelas == "I") {
              $("#Modal_dklk #kelas_1").attr('checked', 'checked');
              } else if (data.detail[0].kelas == "II") {
              $("#Modal_dklk #kelas_2").attr('checked', 'checked');  
              } else if (data.detail[0].kelas == "III") {
              $("#Modal_dklk #kelas_3").attr('checked', 'checked');  
              } else if (data.detail[0].kelas == "IV") {
              $("#Modal_dklk #kelas_4").attr('checked', 'checked');  
              } else if (data.detail[0].kelas == "V") {
              $("#Modal_dklk #kelas_5").attr('checked', 'checked');  
              } else {
              $("#Modal_dklk #kelas_6").attr('checked', 'checked'); 
              }

            }else if(data.detail[0].id_jenjang_sekolah=='3'){
              $("#Modal_dklk #jenjang_sd").hide();
              $("#Modal_dklk #jenjang_smp").show();

              if (data.detail[0].kelas == "VII") {
                $("#Modal_dklk #kelas_7").attr('checked', 'checked');
              } else if (data.detail[0].kelas == "VIII") {
                $("#Modal_dklk #kelas_8").attr('checked', 'checked');  
              } else {
                $("#Modal_dklk #kelas_9").attr('checked', 'checked'); 
              }
            }

            //$('#Modal_dklk #jenjang_sekolah').val(data.detail[0].id_jenjang_sekolah);  // adun
            $('#Modal_dklk #status_sekolah_asal').val(data.skl_asal[0].status_sekolah);
            get_sekolah_by_status('#Modal_dklk #id_jenjang_sekolah','#Modal_dklk #skl_asal','#Modal_dklk #div_akr_skl_asal','#Modal_dklk #status_sekolah_asal');
            for (var i = 0; i < data.daftar_skl_asal.length; i++) {
              $('#Modal_dklk #skl_asal ').append('<option value="' + data.daftar_skl_asal[i].id_sekolah +'|*'+ data.daftar_skl_asal[i].nama_sekolah + '">' + data.daftar_skl_asal[i].nama_sekolah + '</option>');
              // $("#Modal_dklk #skl_asal ").val(data.daftar_skl_asal[i].nama_sekolah);
            
            }

            $('#Modal_dklk #skl_asal').val(data.detail[0].skl_asal);
            $('#Modal_dklk #akr_skl_asal').val(data.detail[0].akreditasi_skl_asal);

            $('#Modal_dklk #div_prev_surat_pindah').attr('style','display:block;');
            $('#Modal_dklk #prev_surat_pindah').attr('src','<?php echo base_url(); ?>'+data.detail[0].f_surat_pindah);
            $('#Modal_dklk #lokasi_f_surat_pindah').val(data.detail[0].f_surat_pindah);
            if ( (data.detail[0].valid_surat_pindah == 0) && ((data.detail[0].komen_surat_pindah != null) && (data.detail[0].komen_surat_pindah != "")) ) {
              $('#Modal_dklk #error_surat_pindah').text(data.detail[0].komen_surat_pindah);
              $('#Modal_dklk #error_surat_pindah').addClass( "alert alert-danger" );  
            }

            $('#Modal_dklk #no_surat_pindah').val(data.detail[0].no_surat_pindah);
            $('#Modal_dklk #tgl_surat_pindah').datepicker('update', data.detail[0].tgl_surat_pindah);

            if (data.detail[0].wilayah_skl_tujuan != "0.0") {              
              $('#Modal_dklk #prov_tujuan').val(arr_tujuan[0]);

              for (var i = 0; i < data.daftar_kota_tujuan.length; i++) {
                $('#Modal_dklk #id_wly_skl_tujuan').append('<option value=' + data.daftar_kota_tujuan[i].NO_PROP +'.'+ data.daftar_kota_tujuan[i].NO_KAB + '>' + data.daftar_kota_tujuan[i].NAMA_KAB + '</option>');
              }
              $('#Modal_dklk #id_wly_skl_tujuan').val(data.detail[0].wilayah_skl_tujuan);
              $('#Modal_dklk #skl_tujuan').val(data.detail[0].skl_tujuan);
              $('#Modal_dklk #akr_skl_tujuan').val(data.detail[0].akreditasi_skl_tujuan);              
            }            

            $('#Modal_dklk #div_prev_rapot_identitas_siswa').attr('style','display:block;');
            $('#Modal_dklk #prev_rapot_identitas_siswa').attr('src','<?php echo base_url(); ?>'+data.detail[0].f_rapot_identitas_siswa);
            $('#Modal_dklk #lokasi_f_rapot_identitas_siswa').val(data.detail[0].f_rapot_identitas_siswa);
            if ( (data.detail[0].valid_rapot_identitas_siswa == 0) && ((data.detail[0].komen_rapot_identitas_siswa != null) && (data.detail[0].komen_rapot_identitas_siswa != "")) ) {
              $('#Modal_dklk #error_rapot_identitas_siswa').text(data.detail[0].komen_rapot_identitas_siswa);
              $('#Modal_dklk #error_rapot_identitas_siswa').addClass( "alert alert-danger" );  
            }

            $('#Modal_dklk #div_prev_rapot_nilai_akhir').attr('style','display:block;');
            $('#Modal_dklk #prev_rapot_nilai_akhir').attr('src','<?php echo base_url(); ?>'+data.detail[0].f_rapot_nilai_akhir);
            $('#Modal_dklk #lokasi_f_rapot_nilai_akhir').val(data.detail[0].f_rapot_nilai_akhir);
            if ( (data.detail[0].valid_rapot_nilai_akhir == 0) && ((data.detail[0].komen_rapot_nilai_akhir != null) && (data.detail[0].komen_rapot_nilai_akhir != "")) ) {
              $('#Modal_dklk #error_rapot_nilai_akhir').text(data.detail[0].komen_rapot_nilai_akhir);
              $('#Modal_dklk #error_rapot_nilai_akhir').addClass( "alert alert-danger" );  
            }

            $('#Modal_dklk #div_prev_rapot_riwayat_pindah').attr('style','display:block;');
            $('#Modal_dklk #prev_rapot_riwayat_pindah').attr('src','<?php echo base_url(); ?>'+data.detail[0].f_rapot_riwayat_pindah);
            $('#Modal_dklk #lokasi_f_rapot_riwayat_pindah').val(data.detail[0].f_rapot_riwayat_pindah);
            if ( (data.detail[0].valid_rapot_riwayat_pindah == 0) && ((data.detail[0].komen_rapot_riwayat_pindah != null) && (data.detail[0].komen_rapot_riwayat_pindah != "")) ) {
              $('#Modal_dklk #error_rapot_riwayat_pindah').text(data.detail[0].komen_rapot_riwayat_pindah); 
              $('#Modal_dklk #error_rapot_riwayat_pindah').addClass( "alert alert-danger" );  
            }

            $('#Modal_dklk').modal({
              backdrop: 'static'
            }); // show bootstrap modal dklk
            $('#Modal_dklk #data_mutasi').show();
            $('#Modal_dklk #data_diri').hide();
          break;

          case 'lkdk': 
            if (data.detail[0].pkj_wali == "LAINNYA") {
              $("#Modal_lkdk #div_pkj_wali_lain").show();
              $('[name="pkj_wali_lain"]').val(data.detail[0].pkj_wali_lain);
            } else {
              $("#Modal_lkdk #div_pkj_wali_lain").hide();
            }            
            
            if ( (data.detail[0].status_warga=="1")|| (data.detail[0].status_warga=="2")) {
              $("#Modal_lkdk #domisili_siswa ").val("dk");
            }else{
              $("#Modal_lkdk #domisili_siswa ").val("lk");
            }

            if (data.detail[0].id_jenjang_sekolah=='2'){
              $("#Modal_lkdk #jenjang_sd").show();
              $("#Modal_lkdk #jenjang_smp").hide();
              if (data.detail[0].kelas == "I") {
              $("#Modal_lkdk #kelas_1").attr('checked', 'checked');
              } else if (data.detail[0].kelas == "II") {
              $("#Modal_lkdk #kelas_2").attr('checked', 'checked');  
              } else if (data.detail[0].kelas == "III") {
              $("#Modal_lkdk #kelas_3").attr('checked', 'checked');  
              } else if (data.detail[0].kelas == "IV") {
              $("#Modal_lkdk #kelas_4").attr('checked', 'checked');  
              } else if (data.detail[0].kelas == "V") {
              $("#Modal_lkdk #kelas_5").attr('checked', 'checked');  
              } else {
              $("#Modal_lkdk #kelas_6").attr('checked', 'checked'); 
              }
            }else if(data.detail[0].id_jenjang_sekolah=='3'){
              $("#Modal_lkdk #jenjang_sd").hide();
              $("#Modal_lkdk #jenjang_smp").show();

              if (data.detail[0].kelas == "VII") {
                $("#Modal_lkdk #kelas_7").attr('checked', 'checked');
              } else if (data.detail[0].kelas == "VIII") {
                $("#Modal_lkdk #kelas_8").attr('checked', 'checked');  
              } else {
                $("#Modal_lkdk #kelas_9").attr('checked', 'checked'); 
              }
            }

            $('#Modal_lkdk #prov_asal').val(arr_asal[0]);

            for (var i = 0; i < data.daftar_kota_asal.length; i++) {
              $('#Modal_lkdk #id_wly_skl_asal').append('<option value=' + data.daftar_kota_asal[i].NO_PROP +'.'+ data.daftar_kota_asal[i].NO_KAB + '>' + data.daftar_kota_asal[i].NAMA_KAB + '</option>');
            }

            $('#Modal_lkdk #id_wly_skl_asal').val(data.detail[0].wilayah_skl_asal);
            $('#Modal_lkdk #skl_asal').val(data.detail[0].skl_asal);
            $('#Modal_lkdk #akr_skl_asal').val(data.detail[0].akreditasi_skl_asal);

            $('#Modal_lkdk #status_sekolah_tujuan').val(data.skl_tujuan[0].status_sekolah);

            for (var i = 0; i < data.daftar_skl_tujuan.length; i++) {
              $('#Modal_lkdk #skl_tujuan').append('<option value="' + data.daftar_skl_tujuan[i].id_sekolah +'|*'+ data.daftar_skl_tujuan[i].nama_sekolah + '">' + data.daftar_skl_tujuan[i].nama_sekolah + '</option>');
            }

            $('#Modal_lkdk #skl_tujuan').val(data.detail[0].skl_tujuan);

            // Surat Dinas
            $('#Modal_lkdk #div_prev_surat_dinas_asal').attr('style','display:block;');
            $('#Modal_lkdk #prev_surat_dinas_asal').attr('src','<?php echo base_url(); ?>'+data.detail[0].f_surat_dinas_asal);
            $('#Modal_lkdk #lokasi_f_surat_dinas_asal').val(data.detail[0].f_surat_dinas_asal);
            $('#Modal_lkdk #error_surat_dinas_asal').text(data.detail[0].komen_surat_dinas_asal);
            if ( (data.detail[0].komen_surat_dinas_asal != "") && (data.detail[0].komen_surat_dinas_asal != null)) {
              $('#Modal_lkdk #error_surat_dinas_asal').addClass( "alert alert-danger" );  
            }

            $('#Modal_lkdk #no_surat_dinas').val(data.detail[0].no_surat_dinas);
            $('#Modal_lkdk #tgl_surat_dinas').datepicker('setDate', data.detail[0].tgl_surat_dinas);
            $('#Modal_lkdk #tgl_surat_dinas').datepicker('SetValue', data.detail[0].tgl_surat_dinas);
            $('#Modal_lkdk #tgl_surat_dinas').datepicker('update', data.detail[0].tgl_surat_dinas);
            // $('#Modal_lkdk #tgl_surat_dinas').datepicker('update', new Date());

            // Surat Sekolah
            $('#Modal_lkdk #div_prev_surat_pindah').attr('style','display:block;');
            $('#Modal_lkdk #prev_surat_pindah').attr('src','<?php echo base_url(); ?>'+data.detail[0].f_surat_pindah);
            $('#Modal_lkdk #lokasi_f_surat_pindah').val(data.detail[0].f_surat_pindah);
            $('#Modal_lkdk #error_surat_pindah').text(data.detail[0].komen_surat_pindah);
            if ( (data.detail[0].komen_surat_dinas_asal != "") && (data.detail[0].komen_surat_pindah != null)) {
              $('#Modal_lkdk #error_surat_pindah').addClass( "alert alert-danger" );  
            }

            $('#Modal_lkdk #no_surat_pindah').val(data.detail[0].no_surat_pindah);
            $('#Modal_lkdk #tgl_surat_pindah').datepicker('update', data.detail[0].tgl_surat_pindah);

            // Surat Penerimaan Sekolah Baru
            $('#Modal_lkdk #div_prev_penerimaan_sekolah_baru').attr('style','display:block;');
            $('#Modal_lkdk #prev_penerimaan_sekolah_baru').attr('src','<?php echo base_url(); ?>'+data.detail[0].f_penerimaan_sekolah_baru);
            $('#Modal_lkdk #lokasi_f_penerimaan_sekolah_baru').val(data.detail[0].f_penerimaan_sekolah_baru);
            $('#Modal_lkdk #error_penerimaan_sekolah_baru').text(data.detail[0].komen_penerimaan_sekolah_baru);
            if ( (data.detail[0].komen_penerimaan_sekolah_baru != "") && (data.detail[0].komen_penerimaan_sekolah_baru != null)) {
              $('#Modal_lkdk #error_penerimaan_sekolah_baru').addClass( "alert alert-danger" );  
            }

            $('#Modal_lkdk #no_surat_penerimaan').val(data.detail[0].no_surat_penerimaan);
            $('#Modal_lkdk #tgl_surat_penerimaan').datepicker('update', data.detail[0].tgl_surat_penerimaan);

            // if (data.detail[0].wilayah_skl_tujuan != "0.0") {              
            //   $('#Modal_dklk #prov_tujuan').val(arr_tujuan[0]);

            //   for (var i = 0; i < data.daftar_kota_tujuan.length; i++) {
            //     $('#Modal_dklk #id_wly_skl_tujuan').append('<option value=' + data.daftar_kota_tujuan[i].NO_PROP +'.'+ data.daftar_kota_tujuan[i].NO_KAB + '>' + data.daftar_kota_tujuan[i].NAMA_KAB + '</option>');
            //   }
            //   $('#Modal_dklk #id_wly_skl_tujuan').val(data.detail[0].wilayah_skl_tujuan);
            //   $('#Modal_dklk #skl_tujuan').val(data.detail[0].skl_tujuan);
            //   $('#Modal_dklk #akr_skl_tujuan').val(data.detail[0].akreditasi_skl_tujuan);              
            // }  

            $('#Modal_lkdk #skl_tujuan').val(data.detail[0].skl_tujuan);
            $('#Modal_lkdk #akr_skl_tujuan').val(data.detail[0].akreditasi_skl_tujuan); 

            // Raport
            $('#Modal_lkdk #div_prev_rapot_identitas_siswa').attr('style','display:block;');
            $('#Modal_lkdk #prev_rapot_identitas_siswa').attr('src','<?php echo base_url(); ?>'+data.detail[0].f_rapot_identitas_siswa);
            $('#Modal_lkdk #lokasi_f_rapot_identitas_siswa').val(data.detail[0].f_rapot_identitas_siswa);
            $('#Modal_lkdk #error_rapot_identitas_siswa').text(data.detail[0].komen_rapot_identitas_siswa);
            if ( (data.detail[0].komen_rapot_identitas_siswa != "") && (data.detail[0].komen_rapot_identitas_siswa != null)) {
              $('#Modal_lkdk #error_rapot_identitas_siswa').addClass( "alert alert-danger" );  
            }
            
            $('#Modal_lkdk #div_prev_rapot_nilai_akhir').attr('style','display:block;');
            $('#Modal_lkdk #prev_rapot_nilai_akhir').attr('src','<?php echo base_url(); ?>'+data.detail[0].f_rapot_nilai_akhir);
            $('#Modal_lkdk #lokasi_f_rapot_nilai_akhir').val(data.detail[0].f_rapot_nilai_akhir);
            $('#Modal_lkdk #error_rapot_nilai_akhir').text(data.detail[0].komen_rapot_nilai_akhir);
            if ( (data.detail[0].komen_rapot_nilai_akhir != "") && (data.detail[0].komen_rapot_nilai_akhir != null)) {
              $('#Modal_lkdk #error_rapot_nilai_akhir').addClass( "alert alert-danger" );  
            }
            
            $('#Modal_lkdk #div_prev_rapot_riwayat_pindah').attr('style','display:block;');
            $('#Modal_lkdk #prev_rapot_riwayat_pindah').attr('src','<?php echo base_url(); ?>'+data.detail[0].f_rapot_riwayat_pindah);
            $('#Modal_lkdk #lokasi_f_rapot_riwayat_pindah').val(data.detail[0].f_rapot_riwayat_pindah);
            $('#Modal_lkdk #error_rapot_riwayat_pindah').text(data.detail[0].komen_rapot_riwayat_pindah); 
            if ( (data.detail[0].komen_rapot_riwayat_pindah != "") && (data.detail[0].komen_rapot_riwayat_pindah != null)) {
              $('#Modal_lkdk #error_rapot_riwayat_pindah').addClass( "alert alert-danger" );  
            }            
            
            $('#Modal_lkdk #div_prev_surat_akreditasi').attr('style','display:block;');
            $('#Modal_lkdk #prev_surat_akreditasi').attr('src','<?php echo base_url(); ?>'+data.detail[0].f_surat_akreditasi);
            $('#Modal_lkdk #lokasi_f_surat_akreditasi').val(data.detail[0].f_surat_akreditasi);
            $('#Modal_lkdk #error_surat_akreditasi').text(data.detail[0].komen_surat_akreditasi); 
            if ( (data.detail[0].komen_surat_akreditasi != "") && (data.detail[0].komen_surat_akreditasi != null)) {
              $('#Modal_lkdk #error_surat_akreditasi').addClass( "alert alert-danger" );  
            }

            $('#Modal_lkdk').modal({
              backdrop: 'static'
            }); // show bootstrap modal lkdk
            $('#Modal_lkdk #data_mutasi').show();
            $('#Modal_lkdk #data_diri').hide();
          break;

          default: //dkdk
            if (data.detail[0].pkj_wali == "LAINNYA") {
              $("#Modal_dkdk #div_pkj_wali_lain").show();
              $('[name="pkj_wali_lain"]').val(data.detail[0].pkj_wali_lain);
            } else {
              $("#Modal_dkdk #div_pkj_wali_lain").hide();
            }           
            
            if ( (data.detail[0].status_warga=="1")|| (data.detail[0].status_warga=="2")) {
              $("#Modal_dkdk #domisili_siswa ").val("dk");
            }else{
              $("#Modal_dkdk #domisili_siswa ").val("lk");
            }

            if (data.detail[0].id_jenjang_sekolah=='2'){
              $("#Modal_dkdk #jenjang_sd").show();
              $("#Modal_dkdk #jenjang_smp").hide();
              if (data.detail[0].kelas == "I") {
              $("#Modal_dkdk #kelas_1").attr('checked', 'checked');
              } else if (data.detail[0].kelas == "II") {
              $("#Modal_dkdk #kelas_2").attr('checked', 'checked');  
              } else if (data.detail[0].kelas == "III") {
              $("#Modal_dkdk #kelas_3").attr('checked', 'checked');  
              } else if (data.detail[0].kelas == "IV") {
              $("#Modal_dkdk #kelas_4").attr('checked', 'checked');  
              } else if (data.detail[0].kelas == "V") {
              $("#Modal_dkdk #kelas_5").attr('checked', 'checked');  
              } else {
              $("#Modal_dkdk #kelas_6").attr('checked', 'checked'); 
              }

            }else if(data.detail[0].id_jenjang_sekolah=='3'){
              $("#Modal_dkdk #jenjang_sd").hide();
              $("#Modal_dkdk #jenjang_smp").show();

              if (data.detail[0].kelas == "VII") {
                $("#Modal_dkdk #kelas_7").attr('checked', 'checked');
              } else if (data.detail[0].kelas == "VIII") {
                $("#Modal_dkdk #kelas_8").attr('checked', 'checked');  
              } else {
                $("#Modal_dkdk #kelas_9").attr('checked', 'checked'); 
              }
            }

            $('#Modal_dkdk #status_sekolah_asal').val(data.skl_asal[0].status_sekolah);
            get_sekolah_by_status('#Modal_dkdk #id_jenjang_sekolah','#Modal_dkdk #skl_asal','#Modal_dkdk #div_akr_skl_asal','#Modal_dkdk #status_sekolah_asal');
            for (var i = 0; i < data.daftar_skl_asal.length; i++) {
              $('#Modal_dkdk #skl_asal').append('<option value="' + data.daftar_skl_asal[i].id_sekolah +'|*'+ data.daftar_skl_asal[i].nama_sekolah + '">' + data.daftar_skl_asal[i].nama_sekolah + '</option>');
            }

            $('#Modal_dkdk #skl_asal').val(data.detail[0].skl_asal);
            $('#Modal_dkdk #akr_skl_asal').val(data.detail[0].akreditasi_skl_asal);  

            $('#Modal_dkdk #status_sekolah_tujuan').val(data.skl_tujuan[0].status_sekolah);

            for (var i = 0; i < data.daftar_skl_tujuan.length; i++) {
              $('#Modal_dkdk #skl_tujuan').append('<option value="' + data.daftar_skl_tujuan[i].id_sekolah +'|*'+ data.daftar_skl_tujuan[i].nama_sekolah + '">' + data.daftar_skl_tujuan[i].nama_sekolah + '</option>');
            }

            $('#Modal_dkdk #skl_tujuan').val(data.detail[0].skl_tujuan);
            $('#Modal_dkdk #akr_skl_tujuan').val(data.detail[0].akreditasi_skl_tujuan);  

            // Surat Pindah Sekolah
            $('#Modal_dkdk #div_prev_surat_pindah').attr('style','display:block;');
            $('#Modal_dkdk #prev_surat_pindah').attr('src','<?php echo base_url(); ?>'+data.detail[0].f_surat_pindah);
            $('#Modal_dkdk #lokasi_f_surat_pindah').val(data.detail[0].f_surat_pindah);
            $('#Modal_dkdk #error_surat_pindah').text(data.detail[0].komen_surat_pindah);
            if ( (data.detail[0].komen_surat_pindah != "") && (data.detail[0].komen_surat_pindah != null)) {
              $('#Modal_dkdk #error_surat_pindah').addClass( "alert alert-danger" );  
            }

            $('#Modal_dkdk #no_surat_pindah').val(data.detail[0].no_surat_pindah);
            $('#Modal_dkdk #tgl_surat_pindah').datepicker('update', data.detail[0].tgl_surat_pindah);
            
            // Surat Penerimaan
            $('#Modal_dkdk #div_prev_penerimaan_sekolah_baru').attr('style','display:block;');
            $('#Modal_dkdk #prev_penerimaan_sekolah_baru').attr('src','<?php echo base_url(); ?>'+data.detail[0].f_penerimaan_sekolah_baru);
            $('#Modal_dkdk #lokasi_f_penerimaan_sekolah_baru').val(data.detail[0].f_penerimaan_sekolah_baru);
            $('#Modal_dkdk #error_penerimaan_sekolah_baru').text(data.detail[0].komen_penerimaan_sekolah_baru);
            if ( (data.detail[0].komen_penerimaan_sekolah_baru != "") && (data.detail[0].komen_penerimaan_sekolah_baru != null)) {
              $('#Modal_dkdk #error_penerimaan_sekolah_baru').addClass( "alert alert-danger" );  
            }

            $('#Modal_dkdk #no_surat_penerimaan').val(data.detail[0].no_surat_penerimaan);
            $('#Modal_dkdk #tgl_surat_penerimaan').datepicker('update', data.detail[0].tgl_surat_penerimaan);
            
            $('#Modal_dkdk #div_prev_rapot_identitas_siswa').attr('style','display:block;');
            $('#Modal_dkdk #prev_rapot_identitas_siswa').attr('src','<?php echo base_url(); ?>'+data.detail[0].f_rapot_identitas_siswa);
            $('#Modal_dkdk #lokasi_f_rapot_identitas_siswa').val(data.detail[0].f_rapot_identitas_siswa);
            $('#Modal_dkdk #error_rapot_identitas_siswa').text(data.detail[0].komen_rapot_identitas_siswa);
            if ( (data.detail[0].komen_rapot_identitas_siswa != "") && (data.detail[0].komen_rapot_identitas_siswa != null)) {
              $('#Modal_dkdk #error_rapot_identitas_siswa').addClass( "alert alert-danger" );  
            }
            
            $('#Modal_dkdk #div_prev_rapot_nilai_akhir').attr('style','display:block;');
            $('#Modal_dkdk #prev_rapot_nilai_akhir').attr('src','<?php echo base_url(); ?>'+data.detail[0].f_rapot_nilai_akhir);
            $('#Modal_dkdk #lokasi_f_rapot_nilai_akhir').val(data.detail[0].f_rapot_nilai_akhir);
            $('#Modal_dkdk #error_rapot_nilai_akhir').text(data.detail[0].komen_rapot_nilai_akhir);
            if ( (data.detail[0].komen_rapot_nilai_akhir != "") && (data.detail[0].komen_rapot_nilai_akhir != null)) {
              $('#Modal_dkdk #error_rapot_nilai_akhir').addClass( "alert alert-danger" );  
            }
            
            $('#Modal_dkdk #div_prev_rapot_riwayat_pindah').attr('style','display:block;');
            $('#Modal_dkdk #prev_rapot_riwayat_pindah').attr('src','<?php echo base_url(); ?>'+data.detail[0].f_rapot_riwayat_pindah);
            $('#Modal_dkdk #lokasi_f_rapot_riwayat_pindah').val(data.detail[0].f_rapot_riwayat_pindah);
            $('#Modal_dkdk #error_rapot_riwayat_pindah').text(data.detail[0].komen_rapot_riwayat_pindah); 
            if ( (data.detail[0].komen_rapot_riwayat_pindah != "") && (data.detail[0].komen_rapot_riwayat_pindah != null)) {
              $('#Modal_dkdk #error_rapot_riwayat_pindah').addClass( "alert alert-danger" );  
            }

            // console.log(data.detail[0].f_surat_akreditasi);

            if ( (data.detail[0].f_surat_akreditasi != "") && (data.detail[0].f_surat_akreditasi != null) ) {  
              $('#Modal_dkdk #div_prev_surat_akreditasi').attr('style','display:block;');
              $('#Modal_dkdk #prev_surat_akreditasi').attr('src','<?php echo base_url(); ?>'+data.detail[0].f_surat_akreditasi);
              $('#Modal_dkdk #lokasi_f_surat_akreditasi').val(data.detail[0].f_surat_akreditasi);
              $('#Modal_dkdk #error_surat_akreditasi').text(data.detail[0].komen_surat_akreditasi); 
              if ( (data.detail[0].komen_surat_akreditasi != "") && (data.detail[0].komen_surat_akreditasi != null)) {
                $('#Modal_dkdk #error_surat_akreditasi').addClass( "alert alert-danger" );  
              }
            } else {
              $('#Modal_dkdk #div_group_f_surat_akreditasi').attr('style','display:none;');
            }        
            
            $('#Modal_dkdk').modal({
              backdrop: 'static'
            }); // show bootstrap modal dkdk
            $('#Modal_dkdk #data_mutasi').show();
            $('#Modal_dkdk #data_diri').hide();
          break;
          $('body').addClass('modal-open');
          $('body').attr('style','');
        } 
      },
      error: function (jqXHR, textStatus, errorThrown) {
        swal("Error!", "Aplikasi gagal mendapatkan data!", "error");
      }
  });
  
  $('.modal-title').text('Ubah'); // Set Title to Bootstrap modal title
}

function download_surat(id) {
  $.ajax({ url : "<?php echo base_url('frontend/ajax/ajax_download_surat_mutasi/'); ?>",
    type: "POST",
    data: {id,id},
    dataType: "JSON",
    success: function(data){
      if(data.status){
        window.open(data.file_output);
        swal("Success!", "Aplikasi berhasil mendapatkan surat persetujuan pindah sekolah peserta didik!", "success");
      }else{
        swal("Error!", "Aplikasi gagal mendapatkan surat persetujuan pindah sekolah peserta didik!", "error");
      }
    },
    error: function (jqXHR, textStatus, errorThrown){
      // alert('Error adding / update data');
      swal("Error!", "Aplikasi gagal menyimpan / mengubah data!", "error");
    }
  });
}

function reload_table(){
  table1.ajax.reload(null,false);
  table2.ajax.reload(null,false);
}
</script>