<style>
  .select2-selection--single {
      height: 100% !important;
  }
  .select2-selection__rendered{
      word-wrap: break-word !important;
      text-overflow: inherit !important;
      white-space: normal !important;
  }
  .bootstrap-select .btn {
    border-style: solid;
    border-left-width: 1px;
    border-left-color: #00DDDD;
    border-top: none;
    border-bottom: none;
    border-right: none;
    color: black;
    font-weight: 200;
    padding: 8px 8px;
    font-size: 14px;
    margin-bottom: 10px;
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
  }
</style>
<script>
var timer = 0;
function set_interval() {
  // the interval 'timer' is set as soon as the page loads
  timer = setInterval("auto_logout()", 3600000);
  // timer = setInterval("auto_logout()", 60000);
  // the figure '10000' above indicates how many milliseconds the timer be set to.
  // Eg: to set it to 5 mins, calculate 5min = 5x60 = 300 sec = 300,000 millisec.
  // So set it to 300000 // 1800000 30 menit  2700000 / 45 menit 3600000/ 1 jam
}



function reset_interval() {
  //resets the timer. The timer is reset on each of the below events:
  // 1. mousemove   2. mouseclick   3. key press 4. scroliing
  //first step: clear the existing timer

  if (timer != 0) {
    clearInterval(timer);
    timer = 0;
    // second step: implement the timer again
    timer = setInterval("auto_logout()", 3600000);
    // timer = setInterval("auto_logout()", 60000);
    // timer = setInterval("auto_logout()", 30000);
    // completed the reset of the timer
  }
}

function auto_logout() {
  // this function will redirect the user to the logout script
  // window.location = "<?php echo base_url('frontend/logout')?>";
  swal({
      title: "Session anda habis!",
      text: "Silahkan login kembali Kembali!",
      type: "error"
  }, function() {
    window.location = "<?php echo base_url('frontend/logout')?>"
  });
}

</script>
<!-- Sections -->

<link rel="stylesheet" href="<?php echo base_url('assets/dist/css/bootstrap-select.css'); ?>">
<script src="<?php echo base_url('assets/dist/js/bootstrap-select.js'); ?>"></script>
<section id="form" class="sections lightbg">
    <div class="container">

        <div class="heading-content text-center">
            <h3>Pengajuan Pindah Sekolah</h3>
        </div>

        <!-- Example row of columns -->
        <div class="row text-center">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="alert alert-info">
                <p>Untuk Pengajuan Pindah Sekolah <b>Non Formal/Homeschooling</b><br>Dapat Langsung Menghubungi <b>Dinas Pendidikan Kota Tangerang Di Gd. Pusat Pemerintahan Lantai 3</b></p>
              </div>
              <form id="jenis_mutasi">

                <!-- adun -->
                <div class="form-group">
                  <div class="col-md-12">
                    <label>Jenjang Sekolah : <span style="color: red;">*</span></label>
                    <select id="jenjang_sekolah" name="jenjang_sekolah" class="form-control" required style="width:100%">
                    </select>
                  </div>
                </div>
                <!-- adun close -->

                <div class="form-group">
                  <div class="col-md-12">
                    <label>Asal Sekolah : <span style="color: red;">*</span></label>
                    <select id="jenis_asal_sekolah" name="jenis_asal_sekolah" class="form-control" required style="width:100%">
                      <option value="">Pilih Sekolah</option>
                      <option value="dalam">Dalam Kota Tangerang</option>
                      <option value="luar">Luar Kota Tangerang</option>
                    </select>
                  </div>
                </div>
                
                <div class="form-group">
                  <div class="col-md-12">
                    <label>Tujuan Sekolah : <span style="color: red;">*</span></label>
                    <select id="jenis_tujuan_sekolah" name="jenis_tujuan_sekolah" class="form-control" required style="width:100%">
                      <option value="">Pilih Sekolah</option>
                      <option value="dalam">Dalam Kota Tangerang</option>
                      <option value="luar">Luar Kota Tangerang</option>
                    </select>
                  </div>
                </div>
              </form>
            </div>
            <hr>
            <button type="button" id="open_modul" onclick="open_modul();" class="btn btn-success">Tampilkan Form Pengajuan</button>


        </div>

        <!-- Modal Data Diri-->
        <div class="modal fade" id="Modal_lkdk">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Form Pindah Sekolah Luar Kota &rArr; Dalam Kota</h4>
                    </div>

                    <div class="modal-body">
                        <form method="post" id="form_lkdk" class="form-horizontal">
                            <input type="hidden" value="" name="id" id="id">
                            <input type="hidden" value="" name="id_siswa" id="id_siswa">
                            <input type="hidden" value="" name="id_warga" id="id_warga">
                            <input type="hidden" value="" name="id_validasi" id="id_validasi">
                            <input type="hidden" value="" name="id_jenjang_sekolah" id="id_jenjang_sekolah">
                            <input type="hidden" value="" name="id_status_domisili" id="id_status_domisili">
                            <input type="hidden" name="submit" id="submit" value="submit">
                            <input type="hidden" name="type" id="type" value="lkdk">

                            <div id="data_diri">
                              <div class="form-group">
                                  <div class="col-md-12">
                                    <label>Domisili Siswa :</label>
                                    <select name="domisili_siswa" id="domisili_siswa" class="form-control"  onchange="select_domisili(this);" style="width:100%">
                                      <option value=""> Pilih Domisili Siswa </option>
                                      <option value="dk"> Dalam Kota </option>
                                      <option value="lk"> Luar Kota </option>
                                    </select>
                                    <span class="help-block"></span>
                                  </div>
                              </div>

                              <div class="form-group">
                                <div class="col-md-6">
                                  <label>NIK Siswa : <span style="color: red;">*</span></label>
                                  <input type="text" name="nik" id="nik" class="form-control" maxlength="16" readonly onkeypress="return isNumberKey(event)" required style="width:100%">
                                  <span class="help-block"></span>   
                                  <div id="btn_ceknik" ><button class="btn btn-info" onclick="ceknik('lkdk');">Cek NIK</button></div>                       
                                  
                                  <div class="div_loading"></div>
                              </div>
                                    <div class="col-md-6">
                                        <label>NISN Siswa : <span style="color: red;">*</span></label>
                                        <input type="text" name="nisn" id="nisn" class="form-control" maxlength="10" readonly onkeypress="return isNumberKey(event)" required style="width:100%">
                                        <span class="help-block"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label>Nama Siswa : <span style="color: red;">*</span></label>
                                        <input type="text" name="nama_siswa" id="nama_siswa" class="form-control" readonly required style="width:100%">
                                        <span class="help-block"></span>
                                    </div>

                                    <!-- adun edit form jenjang smp -->
                                    <div class="col-md-6">
                                        <label>Kelas Saat Pindah : <span style="color: red;">*</span></label><br>

                                        <div id="jenjang_sd">
                                            <label class="form-check-label">
                                      <input type="radio" name="kelas" id="kelas_1" value="I" > Kelas 1
                                    </label>
                                            <label class="form-check-label">
                                      <input type="radio" name="kelas" id="kelas_2" value="II" > Kelas 2
                                    </label>
                                            <label class="form-check-label">
                                      <input type="radio" name="kelas" id="kelas_3" value="III"  > Kelas 3
                                    </label>
                                            <label class="form-check-label">
                                      <input type="radio" name="kelas" id="kelas_4" value="IV" > Kelas 4
                                    </label>
                                            <label class="form-check-label">
                                      <input type="radio" name="kelas" id="kelas_5" value="V" > Kelas 5
                                    </label>
                                            <label class="form-check-label">
                                      <input type="radio" name="kelas" id="kelas_6" value="VI" > Kelas 6
                                    </label>
                                        </div>

                                    <div id="jenjang_smp">
                                            <label class="form-check-label">
                                      <input type="radio" name="kelas" id="kelas_7" value="VII"  > Kelas 7
                                    </label>
                                            <label class="form-check-label">
                                      <input type="radio" name="kelas" id="kelas_8" value="VIII"> Kelas 8
                                    </label>
                                            <label class="form-check-label">
                                      <input type="radio" name="kelas" id="kelas_9" value="IX"  > Kelas 9
                                    </label>
                                        </div>

                                        <span class="help-block"></span>
                                    </div>
                                    <!-- close adun edit form jenjang smp -->
                                </div>
                           
                              <div class="form-group">
                                <div class="col-md-6">
                                  <label>Tempat Lahir :</label>
                                  <input name="tmp_lhr" id="tmp_lhr" class="form-control" readonly required style="width:100%" type="text">
                                  <span class="help-block"></span>
                                </div>

                                <div class="col-md-6">
                                  <label>Tanggal Lahir :</label>
                                  <input name="tgl_lhr" id="tgl_lhr" class="form-control datepicker" readonly required style="width:100%" type="text">
                                  <span class="help-block"></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6">
                                    <label>Nama Wali Siswa : <span style="color: red;">*</span></label>
                                    <input type="text" name="nama_wali" id="nama_wali" class="form-control" readonly required style="width:100%">
                                    <span class="help-block"></span>
                                </div>

                                <div class="col-md-6">
                                    <label>Pekerjaan Wali Siswa : <span style="color: red;">*</span></label>
                                    <select name="pkj_wali" id="pkj_wali" class="form-control " onchange="cek_pekerjaan('#Modal_lkdk #pkj_wali','#Modal_lkdk #div_pkj_wali_lain');" required style="width:100%;"></select>
                                    <span class="help-block"></span>
                                </div>
                            </div>

                            <div class="form-group" id="div_pkj_wali_lain" style="display: none;">
                                <div class="col-md-12">
                                    <label>Pekerjaan Diluar Daftar Diatas : <span style="color: red;">*</span></label>
                                    <input type="text" name="pkj_wali_lain" id="pkj_wali_lain" class="form-control" required style="width:100%;">
                                    <span class="help-block"></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <label>Alamat Tinggal : <span style="color: red;">*</span></label>
                                    <textarea name="alamat" id="alamat" class="form-control" readonly required style="width:100%; resize: none;"></textarea>
                                    <span class="help-block"></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6">
                                    <label>Provinsi Sekolah Asal : <span style="color: red;">*</span></label>
                                    <select name="prov_asal" id="prov_asal" class="form-control" onchange="get_kota('#Modal_lkdk #id_wly_skl_asal',this.value);" required style="width:100%"> </select>
                                    <span class="help-block"></span>
                                </div>

                                <div class="col-md-6">
                                    <label>Kota/Kabupaten Sekolah Asal : <span style="color: red;">*</span></label>
                                    <select name="id_wly_skl_asal" id="id_wly_skl_asal" class="form-control" readonly required style="width:100%">
                                    <option value=""> Pilih Kota/Kabupaten </option>
                                  </select>
                                    <span class="help-block"></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <label>Nama Sekolah Asal : <span style="color: red;">*</span></label>
                                    <input type="text" name="skl_asal" id="skl_asal"class="form-control selectpicker show-tick" data-live-search="true" required style="width:100%">
                                    <span class="help-block"></span>
                                </div>
                            </div>

                            <div class="alert alert-info">
                                <span>Jika status sekolah asal adalah "Negeri" maka akreditasi yang dipilih adalah "A"</span>
                            </div>

                            <div class="form-group div_akr_skl_asal" id="div_akr_skl_asal">
                                <div class="col-md-12">
                                    <label>Akreditasi Sekolah Asal : <span style="color: red;">*</span></label>
                                    <select name="akr_skl_asal" id="akr_skl_asal" class="form-control" required style="width:100%">
                                    <option value=""> Pilih Akreditasi Sekolah Asal </option>
                                    <option value="A"> A</option>
                                    <option value="B"> B</option>
                                    <option value="C"> C</option>
                                  </select>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                         </div>



                            <div id="data_mutasi">

                                <div class="alert alert-info">
                                    <span>File yang dikirim harus berekstensi ("JPG","JPEG","PNG","PDF") Dan File Tidak Boleh Lebih Dari 2MB</span>
                                </div>
                                <!-- Surat Pindah Dinas Pendidikan Asal -->
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Surat Keterangan Pindah Dari Dinas Pendidikan Wilayah Asal</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label>Surat Keterangan Pindah Dari Dinas Pendidikan Wilayah Asal: <span style="color: red;">*</span></label>
                                                <input type="file" accept=".png, .jpg, .jpeg, .pdf" id="f_surat_dinas_asal" name="f_surat_dinas_asal" onchange="start_upload('#form_lkdk #f_surat_dinas_asal','#form_lkdk #lokasi_f_surat_dinas_asal','#form_lkdk #div_prev_surat_dinas_asal','#form_lkdk #prev_surat_dinas_asal');">
                                                <input type="hidden" name="lokasi_f_surat_dinas_asal" id="lokasi_f_surat_dinas_asal">
                                                <div id="div_prev_surat_dinas_asal" class="div_prev_surat_dinas_asal embed-responsive embed-responsive-16by9" style="clear:both; display: none">
                                                    <iframe id="prev_surat_dinas_asal" class="prev_surat_dinas_asal embed-responsive-item" frameborder="0" scrolling="yes"></iframe>
                                                </div>
                                                <span class="help-block" id="error_surat_dinas_asal" style="color: red;"></span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label>Nomor Surat : <span style="color: red;">*</span></label>
                                                <input type="text" name="no_surat_dinas" id="no_surat_dinas" class="form-control" maxlength="30" required style="width:100%">
                                                <span class="help-block"></span>
                                            </div>

                                            <div class="col-md-6">
                                                <label>Tanggal Surat : <span style="color: red;">*</span></label>
                                                <input type="text" name="tgl_surat_dinas" id="tgl_surat_dinas" class="form-control datepicker" required style="width:100%">
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Surat Pindah Sekolah Asal -->
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Surat Keterangan Pindah Dari Sekolah Asal</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label>Surat Keterangan Pindah Dari Sekolah Asal : <span style="color: red;">*</span></label>
                                                <input type="file" accept=".jpg, .jpeg, .png, .pdf" id="f_surat_pindah" name="f_surat_pindah" onchange="start_upload('#form_lkdk #f_surat_pindah','#form_lkdk #lokasi_f_surat_pindah','#form_lkdk #div_prev_surat_pindah','#form_lkdk #prev_surat_pindah');">
                                                <input type="hidden" name="lokasi_f_surat_pindah" id="lokasi_f_surat_pindah">
                                                <div id="div_prev_surat_pindah" class="div_prev_surat_pindah embed-responsive embed-responsive-16by9" style="clear:both; display: none">
                                                    <iframe id="prev_surat_pindah" class="prev_surat_pindah embed-responsive-item" frameborder="0" scrolling="yes" width="400" height="400"></iframe>
                                                </div>
                                                <span class="help-block" id="error_surat_pindah" style="color: red;"></span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label>Nomor Surat : <span style="color: red;">*</span></label>
                                                <input type="text" name="no_surat_pindah" id="no_surat_pindah" class="form-control" maxlength="30" required style="width:100%">
                                                <span class="help-block"></span>
                                            </div>

                                            <div class="col-md-6">
                                                <label>Tanggal Surat : <span style="color: red;">*</span></label>
                                                <input type="text" name="tgl_surat_pindah" id="tgl_surat_pindah" class="form-control datepicker" required style="width:100%">
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label>Kota/Kabupaten Sekolah Tujuan : <span style="color: red;">*</span></label>
                                        <input type="hidden" name="id_wly_skl_tujuan" id="id_wly_skl_tujuan" class="form-control" value="36.71" readonly required style="width:100%">
                                        <input type="text" name="wly_skl_tujuan" id="wly_skl_tujuan" class="form-control" value="Kota Tangerang" readonly required style="width:100%">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <!-- Surat Penerimaan Sekolah Tujuan -->
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Surat Keterangan Penerimaan</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label>Surat Keterangan Penerimaan : <span style="color: red;">*</span></label>
                                                <input type="file" accept=".jpg, .jpeg, .png, .pdf" id="f_penerimaan_sekolah_baru" name="f_penerimaan_sekolah_baru" onchange="start_upload('#form_lkdk #f_penerimaan_sekolah_baru','#form_lkdk #lokasi_f_penerimaan_sekolah_baru','#form_lkdk #div_prev_penerimaan_sekolah_baru','#form_lkdk #prev_penerimaan_sekolah_baru');">
                                                <input type="hidden" name="lokasi_f_penerimaan_sekolah_baru" id="lokasi_f_penerimaan_sekolah_baru">
                                                <div id="div_prev_penerimaan_sekolah_baru" class="div_prev_penerimaan_sekolah_baru embed-responsive embed-responsive-16by9" style="clear:both; display: none">
                                                    <iframe id="prev_penerimaan_sekolah_baru" class="prev_penerimaan_sekolah_baru embed-responsive-item" frameborder="0" scrolling="yes" width="400" height="400"></iframe>
                                                </div>
                                                <span class="help-block" id="error_penerimaan_sekolah_baru" style="color: red;"></span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label>No. Surat Penerimaan Sekolah Tujuan : <span style="color: red;">*</span></label>
                                                <input type="text" name="no_surat_penerimaan" id="no_surat_penerimaan" class="form-control" maxlength="30" required style="width:100%">
                                                <span class="help-block"></span>
                                            </div>

                                            <div class="col-md-6">
                                                <label>Tanggal Surat Penerimaan Sekolah Tujuan : <span style="color: red;">*</span></label>
                                                <input type="text" name="tgl_surat_penerimaan" id="tgl_surat_penerimaan" class="form-control datepicker" required style="width:100%">
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label>Status Sekolah Tujuan : </label>
                                        <select name="status_sekolah_tujuan" id="status_sekolah_tujuan" class="form-control" onchange="get_sekolah_by_status('#form_lkdk #id_jenjang_sekolah','#form_lkdk #skl_tujuan','#form_lkdk #div_akr_skl_tujuan','#form_lkdk #status_sekolah_tujuan'); check_akreditasi_by_status_sekolah('#form_lkdk #akr_skl_asal','#form_lkdk #status_sekolah_tujuan','#form_lkdk #status_sekolah_asal');"
                                            required style="width:100%">
                                    <option value=""> Pilih Status Sekolah </option>
                                    <option value="N"> Negeri </option>
                                    <option value="S"> Swasta </option>
                                  </select>

                                        <!--   '#form_dkdk #id_jenjang_sekolah','#form_dkdk #skl_tujuan','#form_dkdk #div_akr_skl_tujuan','#form_dkdk #status_sekolah_tujuan' -->
                                        <span class="help-block"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label>Nama Sekolah Tujuan : <span style="color: red;">*</span></label>
                                        <select name="skl_tujuan" id="skl_tujuan" class="form-control selectpicker show-tick" data-live-search="true" required style="width:100%">
                                    <option value=""> Pilih Sekolah </option>
                                  </select>
                                        <span class="help-block"></span>
                                    </div>
                                </div>

                                <div class="form-group" id="div_akr_skl_tujuan" style="display: none;">
                                    <div class="col-md-12">
                                        <label>Akreditasi Sekolah Tujuan : <span style="color: red;">*</span></label>
                                        <select name="akr_skl_tujuan" id="akr_skl_tujuan" class="form-control" onchange="check_akreditasi('#form_lkdk #akr_skl_asal','#form_lkdk #akr_skl_tujuan','#form_lkdk #status_sekolah_tujuan');" required style="width:100%">
                                    <option value=""> Pilih Akreditasi Sekolah Asal </option>
                                    <option value="A"> A</option>
                                    <option value="B"> B</option>
                                    <option value="C"> C</option>
                                  </select>
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <!-- Rapot -->
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Rapot</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label>Identitas Siswa : <span style="color: red;">*</span></label>
                                                <input type="file" accept=".jpg, .jpeg, .png, .pdf" id="f_rapot_identitas_siswa" name="f_rapot_identitas_siswa" onchange="start_upload('#form_lkdk #f_rapot_identitas_siswa','#form_lkdk #lokasi_f_rapot_identitas_siswa','#form_lkdk #div_prev_rapot_identitas_siswa','#form_lkdk #prev_rapot_identitas_siswa');">
                                                <input type="hidden" name="lokasi_f_rapot_identitas_siswa" id="lokasi_f_rapot_identitas_siswa">
                                                <div id="div_prev_rapot_identitas_siswa" class="div_prev_rapot_identitas_siswa embed-responsive embed-responsive-16by9" style="clear:both; display: none">
                                                    <iframe id="prev_rapot_identitas_siswa" class="prev_rapot_identitas_siswa embed-responsive-item" frameborder="0" scrolling="yes" width="400" height="400"></iframe>
                                                </div>
                                                <span class="help-block" id="error_rapot_identitas_siswa" style="color: red;"></span>
                                            </div>
                                        </div>
                                        <hr>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label>Nilai Semester Terakhir : <span style="color: red;">*</span></label>
                                                <input type="file" accept=".jpg, .jpeg, .png, .pdf" id="f_rapot_nilai_akhir" name="f_rapot_nilai_akhir" onchange="start_upload('#form_lkdk #f_rapot_nilai_akhir','#form_lkdk #lokasi_f_rapot_nilai_akhir','#form_lkdk #div_prev_rapot_nilai_akhir','#form_lkdk #prev_rapot_nilai_akhir');">
                                                <input type="hidden" name="lokasi_f_rapot_nilai_akhir" id="lokasi_f_rapot_nilai_akhir">
                                                <div id="div_prev_rapot_nilai_akhir" class="div_prev_rapot_nilai_akhir embed-responsive embed-responsive-16by9" style="clear:both; display: none">
                                                    <iframe id="prev_rapot_nilai_akhir" class="prev_rapot_nilai_akhir embed-responsive-item" frameborder="0" scrolling="yes" width="400" height="400"></iframe>
                                                </div>
                                                <span class="help-block" id="error_rapot_nilai_akhir" style="color: red;"></span>
                                            </div>
                                        </div>
                                        <hr>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label>Riwayat Pindah Sekolah (Di Lembar Akhir Rapot) : <span style="color: red;">*</span></label>
                                                <input type="file" accept=".jpg, .jpeg, .png, .pdf" id="f_rapot_riwayat_pindah" name="f_rapot_riwayat_pindah" onchange="start_upload('#form_lkdk #f_rapot_riwayat_pindah','#form_lkdk #lokasi_f_rapot_riwayat_pindah','#form_lkdk #div_prev_rapot_riwayat_pindah','#form_lkdk #prev_rapot_riwayat_pindah');">
                                                <input type="hidden" name="lokasi_f_rapot_riwayat_pindah" id="lokasi_f_rapot_riwayat_pindah">
                                                <div id="div_prev_rapot_riwayat_pindah" class="div_prev_rapot_riwayat_pindah embed-responsive embed-responsive-16by9" style="clear:both; display: none">
                                                    <iframe id="prev_rapot_riwayat_pindah" class="prev_rapot_riwayat_pindah embed-responsive-item" frameborder="0" scrolling="yes" width="400" height="400"></iframe>
                                                </div>
                                                <span class="help-block" id="error_rapot_riwayat_pindah" style="color: red;"></span>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                </div>
                                <!-- Surat Akreditasi Sekolah Asal -->
                                <div class="form-group" id="div_group_f_surat_akreditasi">
                                    <div class="col-md-12">
                                        <label>Surat Akreditasi Sekolah Asal : <span style="color: red;">*</span></label>
                                        <input type="file" accept=".jpg, .jpeg, .png, .pdf" id="f_surat_akreditasi" name="f_surat_akreditasi" onchange="start_upload('#form_lkdk #f_surat_akreditasi','#form_lkdk #lokasi_f_surat_akreditasi','#form_lkdk #div_prev_surat_akreditasi','#form_lkdk #prev_surat_akreditasi');">
                                        <input type="hidden" name="lokasi_f_surat_akreditasi" id="lokasi_f_surat_akreditasi">
                                        <div id="div_prev_surat_akreditasi" class="div_prev_surat_akreditasi embed-responsive embed-responsive-16by9" style="clear:both; display: none">
                                            <iframe id="prev_surat_akreditasi" class="prev_surat_akreditasi embed-responsive-item" frameborder="0" scrolling="yes" width="400" height="400"></iframe>
                                        </div>
                                        <span class="help-block" id="error_surat_akreditasi" style="color: red;"></span>
                                    </div>
                                </div>

                            </div>

                        </form>
                    </div>

                    <div class="modal-footer">
                        <div class="pull-left">
                            <button type="button" id="btnPrev" onclick="prev('lkdk');" class="btn btn-warning btnPrev" role="tab" data-toggle="tab">Kembali</button>
                        </div>
                        <div class="pull-right">
                            <button type="button" id="btnNext" onclick="next('lkdk');" class="btn btn-primary btnNext" role="tab" data-toggle="tab">Selanjutnya</button>
                            <button type="button" id="btnSave" onclick="save('lkdk');" class="btn btn-success btnSave">Simpan</button>
                            <button type="button" id="btnCancel" class="btn btn-danger btnCancel" data-dismiss="modal">Batal</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="Modal_dkdk">
            <div class="modal-dialog">
              <div class="modal-content">

                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Form Pindah Sekolah Dalam Kota &rArr; Dalam Kota</h4>
                </div>

                <div class="modal-body">
                  <form method="post" id="form_dkdk" class="form-horizontal">
                    <input type="hidden" value="" name="id" id="id"> 
                    <input type="hidden" value="" name="id_siswa" id="id_siswa"> 
                    <input type="hidden" value="" name="id_warga" id="id_warga">
                    <input type="hidden" value="" name="id_validasi" id="id_validasi"> 
                    <input type="hidden" value="" name="id_jenjang_sekolah" id="id_jenjang_sekolah">
                    <input type="hidden" value="" name="id_status_domisili" id="id_status_domisili">
                    <input type="hidden" name="submit" id="submit" value="submit">
                    <input type="hidden" name="type" id="type" value="dkdk">

                    <div id="data_diri">
                      <div class="form-group">
                          <div class="col-md-12">
                            <label>Domisili Siswa :</label>
                            <select name="domisili_siswa" id="domisili_siswa" class="form-control"  onchange="select_domisili(this);" style="width:100%">
                              <option value=""> Pilih Domisili Siswa </option>
                              <option value="dk"> Dalam Kota </option>
                              <option value="lk"> Luar Kota </option>
                            </select>
                            <span class="help-block"></span>
                          </div>
                      </div>

                      <div class="form-group">
                        <div class="col-md-6">
                          <label>NIK Siswa : <span style="color: red;">*</span></label>
                          <input type="text" name="nik" id="nik" class="form-control" maxlength="16" readonly onkeypress="return isNumberKey(event)" required style="width:100%">
                          <span class="help-block"></span>   
                          <div id="btn_ceknik" ><button class="btn btn-info" onclick="ceknik('dkdk');">Cek NIK</button></div>                       
                          
                          <div class="div_loading"></div>
                      </div>

                        <div class="col-md-6">
                          <label>NISN Siswa : <span style="color: red;">*</span></label>
                          <input type="text" name="nisn" id="nisn" class="form-control" maxlength="10" onkeypress="return isNumberKey(event)" required style="width:100%">
                          <span class="help-block"></span>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-md-6">
                          <label>Nama Siswa : <span style="color: red;">*</span></label>
                          <input type="text" name="nama_siswa" id="nama_siswa" class="form-control" readonly required style="width:100%">
                          <span class="help-block"></span>
                        </div>

                        <!-- adun edit form jenjang smp -->
                      <div class="col-md-6">
                        <label>Kelas Saat Pindah : <span style="color: red;">*</span></label><br>
                        <div id="jenjang_sd">
                          <label class="form-check-label">
                            <input type="radio" name="kelas" id="kelas_1" value="I"> Kelas 1
                          </label>
                          <label class="form-check-label">
                            <input type="radio" name="kelas" id="kelas_2" value="II" > Kelas 2
                          </label>
                          <label class="form-check-label">
                            <input type="radio" name="kelas" id="kelas_3" value="III" > Kelas 3
                          </label>
                          <label class="form-check-label">
                            <input type="radio" name="kelas" id="kelas_4" value="IV"> Kelas 4
                          </label>
                          <label class="form-check-label">
                            <input type="radio" name="kelas" id="kelas_5" value="V" > Kelas 5
                          </label>
                          <label class="form-check-label">
                            <input type="radio" name="kelas" id="kelas_6" value="VI" > Kelas 6
                          </label>
                        </div>

                        <div id="jenjang_smp">
                          <label class="form-check-label">
                            <input type="radio" name="kelas" id="kelas_7" value="VII" > Kelas 7
                          </label>
                          <label class="form-check-label">
                            <input type="radio" name="kelas" id="kelas_8" value="VIII" > Kelas 8
                          </label>
                          <label class="form-check-label">
                            <input type="radio" name="kelas" id="kelas_9" value="IX" > Kelas 9
                          </label>
                        </div>
                        
                        <span class="help-block"></span>
                      </div>
                         <!-- close adun edit form jenjang smp -->

                      </div>
                         
                      <div class="form-group" style="display: block;">
                          <div class="col-md-6">
                            <label>Tempat Lahir :</label>
                            <input name="tmp_lhr" id="tmp_lhr" class="form-control" required="" style="width:100%" readonly="readonly" type="text">
                            <span class="help-block"></span>
                          </div>

                          <div class="col-md-6">
                            <label>Tanggal Lahir :</label>
                            <input name="tgl_lhr" id="tgl_lhr" class="form-control datepicker" required readonly="readonly" style="width:100%" type="text">
                            <span class="help-block"></span>
                          </div>
                      </div>

                      <div class="form-group">
                        <div class="col-md-6">
                          <label>Nama Wali Siswa : <span style="color: red;">*</span></label>
                          <input type="text" name="nama_wali" id="nama_wali" class="form-control" readonly required style="width:100%">
                          <span class="help-block"></span>
                        </div>

                        <div class="col-md-6">
                          <label>Pekerjaan Wali Siswa : <span style="color: red;">*</span></label>
                          <select name="pkj_wali" id="pkj_wali" class="form-control " onchange="cek_pekerjaan('#Modal_dkdk #pkj_wali','#Modal_dkdk #div_pkj_wali_lain');" required style="width:100%;"></select>
                          <span class="help-block"></span>
                        </div>
                      </div>

                      <div class="form-group" id="div_pkj_wali_lain" style="display: none;">
                        <div class="col-md-12">
                          <label>Pekerjaan Diluar Daftar Diatas : <span style="color: red;">*</span></label>
                          <input type="text" name="pkj_wali_lain" id="pkj_wali_lain" class="form-control" required style="width:100%;">
                          <span class="help-block"></span>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-md-12">
                          <label>Alamat Tinggal : <span style="color: red;">*</span></label>
                          <textarea name="alamat" id="alamat" class="form-control" readonly required style="width:100%;resize: none;"></textarea>
                          <span class="help-block"></span>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-md-12">
                          <label>Kota/Kabupaten Sekolah Asal : <span style="color: red;">*</span></label>
                          <input type="hidden" name="id_wly_skl_asal" id="id_wly_skl_asal" class="form-control" value="36.71" readonly required style="width:100%">
                          <input type="text" name="wly_skl_asal" id="wly_skl_asal" class="form-control" value="Kota Tangerang" readonly required style="width:100%">
                          <span class="help-block"></span>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-md-6">
                          <label>Status Sekolah Asal : <span style="color: red;">*</span></label>
                          <select name="status_sekolah_asal" id="status_sekolah_asal" class="form-control" onchange="get_sekolah_by_status('#form_dkdk #id_jenjang_sekolah','#form_dkdk #skl_asal','#form_dkdk #div_akr_skl_asal','#form_dkdk #status_sekolah_asal');" required style="width:100%">
                            <option value=""> Pilih Status Sekolah </option>
                            <option value="N"> Negeri </option>
                            <option value="S"> Swasta </option>
                          </select>
                          <span class="help-block"></span>
                        </div>

                        <div class="col-md-6">
                          <label>Nama Sekolah Asal : <span style="color: red;">*</span></label>
                          <select name="skl_asal" id="skl_asal" class="form-control show-tick" data-live-search="true" required style="width:100%">
                            <option value=""> Pilih Sekolah </option>
                          </select>
                          <span class="help-block"></span>
                        </div>
                      </div>

                      <div class="form-group" id="div_akr_skl_asal" style="display: none;">
                        <div class="col-md-12">
                          <label>Akreditasi Sekolah Asal : <span style="color: red;">*</span></label>
                          <select name="akr_skl_asal" id="akr_skl_asal" class="form-control" required style="width:100%">
                            <option value=""> Pilih Akreditasi Sekolah Asal </option>
                            <option value="A"> A</option>
                            <option value="B"> B</option>
                            <option value="C"> C</option>
                          </select>
                          <span class="help-block"></span>
                        </div>
                      </div>

                    </div>

                    <div id="data_mutasi">

                      <div class="alert alert-info">
                        <span>File yang dikirim harus berekstensi ("JPG","JPEG","PNG","PDF") Dan File Tidak Boleh Lebih Dari 2MB</span>
                      </div>

                      <div class="panel panel-primary">
                        <div class="panel-heading">
                          <h3 class="panel-title">Surat Keterangan Pindah Dari Sekolah Asal</h3>
                        </div>
                        <div class="panel-body">                          
                          <div class="form-group">
                            <div class="col-md-12">
                              <label>Surat Keterangan Pindah Dari Sekolah Asal : <span style="color: red;">*</span></label>
                              <input type="file" accept=".jpg, .jpeg, .png, .pdf" id="f_surat_pindah" name="f_surat_pindah" onchange="start_upload('#form_dkdk #f_surat_pindah','#form_dkdk #lokasi_f_surat_pindah','#form_dkdk #div_prev_surat_pindah','#form_dkdk #prev_surat_pindah');">
                              <input type="hidden" name="lokasi_f_surat_pindah" id="lokasi_f_surat_pindah">
                              <div id="div_prev_surat_pindah" class="div_prev_surat_pindah embed-responsive embed-responsive-16by9" style="clear:both; display: none">
                                <iframe id="prev_surat_pindah" class="prev_surat_pindah embed-responsive-item" frameborder="0" scrolling="yes" width="400" height="400"></iframe>
                              </div>
                              <span class="help-block" id="error_surat_pindah" style="color: red;"></span>
                            </div>
                          </div>

                          <div class="form-group">
                            <div class="col-md-6">
                              <label>Nomor Surat : <span style="color: red;">*</span></label>
                              <input type="text" name="no_surat_pindah" id="no_surat_pindah" class="form-control" maxlength="30" required style="width:100%">
                              <span class="help-block"></span>
                            </div>

                            <div class="col-md-6">
                              <label>Tanggal Surat : <span style="color: red;">*</span></label>
                              <input type="text" name="tgl_surat_pindah" id="tgl_surat_pindah" class="form-control datepicker" required style="width:100%">
                              <span class="help-block"></span>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-md-12">
                          <label>Kota/Kabupaten Sekolah Tujuan : <span style="color: red;">*</span></label>
                          <input type="hidden" name="id_wly_skl_tujuan" id="id_wly_skl_tujuan" class="form-control" value="36.71" readonly required style="width:100%">
                          <input type="text" name="wly_skl_tujuan" id="wly_skl_tujuan" class="form-control" value="Kota Tangerang" readonly required style="width:100%">
                          <span class="help-block"></span>
                        </div>
                      </div>

                      <div class="panel panel-primary">
                        <div class="panel-heading">
                          <h3 class="panel-title">Surat Keterangan Penerimaan</h3>
                        </div>
                        <div class="panel-body">
                          <div class="form-group">
                            <div class="col-md-12">
                              <label>Surat Keterangan Penerimaan : <span style="color: red;">*</span></label>
                              <input type="file" accept=".jpg, .jpeg, .png, .pdf" id="f_penerimaan_sekolah_baru" name="f_penerimaan_sekolah_baru" onchange="start_upload('#form_dkdk #f_penerimaan_sekolah_baru','#form_dkdk #lokasi_f_penerimaan_sekolah_baru','#form_dkdk #div_prev_penerimaan_sekolah_baru','#form_dkdk #prev_penerimaan_sekolah_baru');">
                              <input type="hidden" name="lokasi_f_penerimaan_sekolah_baru" id="lokasi_f_penerimaan_sekolah_baru">
                              <div id="div_prev_penerimaan_sekolah_baru" class="div_prev_penerimaan_sekolah_baru embed-responsive embed-responsive-16by9" style="clear:both; display: none">
                                <iframe id="prev_penerimaan_sekolah_baru" class="prev_penerimaan_sekolah_baru embed-responsive-item" frameborder="0" scrolling="yes" width="400" height="400"></iframe>
                              </div>
                              <span class="help-block" id="error_penerimaan_sekolah_baru" style="color: red;"></span>
                            </div>
                          </div>                      

                          <div class="form-group">
                            <div class="col-md-6">
                              <label>Nomor Surat : <span style="color: red;">*</span></label>
                              <input type="text" name="no_surat_penerimaan" id="no_surat_penerimaan" class="form-control" maxlength="30" required style="width:100%">
                              <span class="help-block"></span>
                            </div>

                            <div class="col-md-6">
                              <label>Tanggal Surat : <span style="color: red;">*</span></label>
                              <input type="text" name="tgl_surat_penerimaan" id="tgl_surat_penerimaan" class="form-control datepicker" required style="width:100%">
                              <span class="help-block"></span>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-md-12">
                          <label>Status Sekolah Tujuan : <span style="color: red;">*</span></label>
                          <select name="status_sekolah_tujuan" id="status_sekolah_tujuan" class="form-control" onchange="get_sekolah_by_status('#form_dkdk #id_jenjang_sekolah','#form_dkdk #skl_tujuan','#form_dkdk #div_akr_skl_tujuan','#form_dkdk #status_sekolah_tujuan'); check_akreditasi_by_status_sekolah('#form_dkdk #akr_skl_asal','#form_dkdk #status_sekolah_tujuan','#form_dkdk #status_sekolah_asal');" required style="width:100%">
                            <option value=""> Pilih Status Sekolah </option>
                            <option value="N"> Negeri </option>
                            <option value="S"> Swasta </option>
                          </select>
                          <span class="help-block"></span>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-md-12">
                          <label>Nama Sekolah Tujuan : <span style="color: red;">*</span></label>
                          <select name="skl_tujuan" id="skl_tujuan" class="form-control selectpicker show-tick" data-live-search="true" required style="width:100%">
                            <option value=""> Pilih Sekolah </option>
                          </select>
                          <span class="help-block"></span>
                        </div>
                      </div>

                      <div class="form-group" id="div_akr_skl_tujuan" style="display: none;">
                        <div class="col-md-12">
                          <label>Akreditasi Sekolah Tujuan : <span style="color: red;">*</span></label>
                          <select name="akr_skl_tujuan" id="akr_skl_tujuan" class="form-control" onchange="check_akreditasi('#form_dkdk #akr_skl_asal','#form_dkdk #akr_skl_tujuan','#form_dkdk #status_sekolah_tujuan');" required style="width:100%">
                            <option value=""> Pilih Akreditasi Sekolah Asal </option>
                            <option value="A"> A</option>
                            <option value="B"> B</option>
                            <option value="C"> C</option>
                          </select>
                          <span class="help-block"></span>
                        </div>
                      </div>

                      <div class="panel panel-primary">
                        <div class="panel-heading">
                          <h3 class="panel-title">Rapot</h3>
                        </div>
                        <div class="panel-body">
                          <div class="form-group">
                            <div class="col-md-12">
                              <label>Identitas Siswa : <span style="color: red;">*</span></label>
                              <input type="file" accept=".jpg, .jpeg, .png, .pdf" id="f_rapot_identitas_siswa" name="f_rapot_identitas_siswa" onchange="start_upload('#form_dkdk #f_rapot_identitas_siswa','#form_dkdk #lokasi_f_rapot_identitas_siswa','#form_dkdk #div_prev_rapot_identitas_siswa','#form_dkdk #prev_rapot_identitas_siswa');">
                              <input type="hidden" name="lokasi_f_rapot_identitas_siswa" id="lokasi_f_rapot_identitas_siswa">
                              <div id="div_prev_rapot_identitas_siswa" class="div_prev_rapot_identitas_siswa embed-responsive embed-responsive-16by9" style="clear:both; display: none">
                                <iframe id="prev_rapot_identitas_siswa" class="prev_rapot_identitas_siswa embed-responsive-item" frameborder="0" scrolling="yes" width="400" height="400"></iframe>
                              </div>
                              <span class="help-block" id="error_rapot_identitas_siswa" style="color: red;"></span>
                            </div>
                          </div> <hr>

                          <div class="form-group">
                            <div class="col-md-12">
                              <label>Nilai Semester Terakhir : <span style="color: red;">*</span></label>
                              <input type="file" accept=".jpg, .jpeg, .png, .pdf" id="f_rapot_nilai_akhir" name="f_rapot_nilai_akhir" onchange="start_upload('#form_dkdk #f_rapot_nilai_akhir','#form_dkdk #lokasi_f_rapot_nilai_akhir','#form_dkdk #div_prev_rapot_nilai_akhir','#form_dkdk #prev_rapot_nilai_akhir');">
                              <input type="hidden" name="lokasi_f_rapot_nilai_akhir" id="lokasi_f_rapot_nilai_akhir">
                              <div id="div_prev_rapot_nilai_akhir" class="div_prev_rapot_nilai_akhir embed-responsive embed-responsive-16by9" style="clear:both; display: none">
                                <iframe id="prev_rapot_nilai_akhir" class="prev_rapot_nilai_akhir embed-responsive-item" frameborder="0" scrolling="yes" width="400" height="400"></iframe>
                              </div>
                              <span class="help-block" id="error_rapot_nilai_akhir" style="color: red;"></span>
                            </div>
                          </div> <hr>

                          <div class="form-group">
                            <div class="col-md-12">
                              <label>Riwayat Pindah Sekolah (Di Lembar Akhir Rapot) : <span style="color: red;">*</span></label>
                              <input type="file" accept=".jpg, .jpeg, .png, .pdf" id="f_rapot_riwayat_pindah" name="f_rapot_riwayat_pindah" onchange="start_upload('#form_dkdk #f_rapot_riwayat_pindah','#form_dkdk #lokasi_f_rapot_riwayat_pindah','#form_dkdk #div_prev_rapot_riwayat_pindah','#form_dkdk #prev_rapot_riwayat_pindah');">
                              <input type="hidden" name="lokasi_f_rapot_riwayat_pindah" id="lokasi_f_rapot_riwayat_pindah">
                              <div id="div_prev_rapot_riwayat_pindah" class="div_prev_rapot_riwayat_pindah embed-responsive embed-responsive-16by9" style="clear:both; display: none">
                                <iframe id="prev_rapot_riwayat_pindah" class="prev_rapot_riwayat_pindah embed-responsive-item" frameborder="0" scrolling="yes" width="400" height="400"></iframe>
                              </div>
                              <span class="help-block" id="error_rapot_riwayat_pindah" style="color: red;"></span>
                            </div>
                          </div> <hr>
                        </div>
                      </div>

                      <div class="form-group" id="div_group_f_surat_akreditasi">
                        <div class="col-md-12">
                          <label>Surat Akreditasi Sekolah Asal : <span style="color: red;">*</span></label>
                          <input type="file" accept=".jpg, .jpeg, .png, .pdf" id="f_surat_akreditasi" name="f_surat_akreditasi" onchange="start_upload('#form_dkdk #f_surat_akreditasi','#form_dkdk #lokasi_f_surat_akreditasi','#form_dkdk #div_prev_surat_akreditasi','#form_dkdk #prev_surat_akreditasi');">
                          <input type="hidden" name="lokasi_f_surat_akreditasi" id="lokasi_f_surat_akreditasi">
                          <div id="div_prev_surat_akreditasi" class="div_prev_surat_akreditasi embed-responsive embed-responsive-16by9" style="clear:both; display: none">
                            <iframe id="prev_surat_akreditasi" class="prev_surat_akreditasi embed-responsive-item" frameborder="0" scrolling="yes" width="400" height="400"></iframe>
                          </div>
                          <span class="help-block" id="error_surat_akreditasi" style="color: red;"></span>
                        </div>
                      </div>

                    </div>

                  </form>
                </div>

                <div class="modal-footer">
                  <div class="pull-left">
                    <button type="button" id="btnPrev" onclick="prev('dkdk');" class="btn btn-warning btnPrev" role="tab" data-toggle="tab">Kembali</button>
                  </div>
                  <div class="pull-right">
                    <button type="button" id="btnNext" onclick="next('dkdk');" class="btn btn-primary btnNext" role="tab" data-toggle="tab">Selanjutnya</button>
                    <button type="button" id="btnSave" onclick="save('dkdk');" class="btn btn-success btnSave">Simpan</button>
                    <button type="button" id="btnCancel" class="btn btn-danger btnCancel" data-dismiss="modal">Batal</button>
                  </div>
                </div>

              </div>
            </div>
        </div>
        
        <div class="modal fade" id="Modal_dklk">
            <div class="modal-dialog">
              <div class="modal-content">

                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Form Pindah Sekolah Dalam Kota &rArr; Luar Kota</h4>
                </div>

                <div class="modal-body">
                  <form method="post" id="form_dklk" class="form-horizontal" enctype="multipart/form-data">
                    <input type="hidden" value="" name="id" id="id"> 
                    <input type="hidden" value="" name="id_siswa" id="id_siswa"> 
                    <input type="hidden" value="" name="id_warga" id="id_warga">  
                    <input type="hidden" value="" name="id_validasi" id="id_validasi">
                    <input type="hidden" value="" name="id_jenjang_sekolah" id="id_jenjang_sekolah">
                    <input type="hidden" value="" name="id_status_domisili" id="id_status_domisili">
                    <input type="hidden" name="submit" id="submit" value="submit">                      
                    <input type="hidden" name="type" id="type" value="dklk">

                    <div id="data_diri">
                    <div class="form-group">
                        <div class="col-md-12">
                          <label>Domisili Siswa :</label>
                          <select name="domisili_siswa" id="domisili_siswa" class="form-control"  onchange="select_domisili(this);" style="width:100%">
                            <option value=""> Pilih Domisili Siswa </option>
                            <option value="dk"> Dalam Kota </option>
                            <option value="lk"> Luar Kota </option>
                          </select>
                          <span class="help-block"></span>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-md-6">
                          <label>NIK Siswa : <span style="color: red;">*</span></label>
                          <input type="text" name="nik" id="nik" class="form-control" maxlength="16" readonly onkeypress="return isNumberKey(event)" required style="width:100%">
                          <span class="help-block"></span>   
                          <div id="btn_ceknik" ><button class="btn btn-info" onclick="ceknik('dklk');">Cek NIK</button></div>                       
                          
                          <div class="div_loading"></div>
                        </div>

                        <div class="col-md-6">
                          <label>NISN Siswa : <span style="color: red;">*</span></label>
                          <input type="text" name="nisn" id="nisn" class="form-control" maxlength="10" readonly onkeypress="return isNumberKey(event)" required style="width:100%">
                          <span class="help-block"></span>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-md-6">
                          <label>Nama Siswa : <span style="color: red;">*</span></label>
                          <input type="text" name="nama_siswa" id="nama_siswa" class="form-control" readonly required style="width:100%">
                          <span class="help-block"></span>
                        </div>
                        <!-- adun edit form jenjang smp -->
                        <div class="col-md-6">
                          <label>Kelas Saat Pindah : <span style="color: red;">*</span></label><br>
                          <div id="jenjang_sd">
                            <label class="form-check-label">
                              <input type="radio" name="kelas" id="kelas_1" value="I" > Kelas 1
                            </label>
                            <label class="form-check-label">
                              <input type="radio" name="kelas" id="kelas_2" value="II" > Kelas 2
                            </label>
                            <label class="form-check-label">
                              <input type="radio" name="kelas" id="kelas_3" value="III" > Kelas 3
                            </label>
                            <label class="form-check-label">
                              <input type="radio" name="kelas" id="kelas_4" value="IV" > Kelas 4
                            </label>
                            <label class="form-check-label">
                              <input type="radio" name="kelas" id="kelas_5" value="V" > Kelas 5
                            </label>
                            <label class="form-check-label">
                              <input type="radio" name="kelas" id="kelas_6" value="VI" > Kelas 6
                            </label>
                          </div>

                          <div id="jenjang_smp">
                            <label class="form-check-label">
                              <input type="radio" name="kelas" id="kelas_7" value="VII" > Kelas 7
                            </label>
                            <label class="form-check-label">
                              <input type="radio" name="kelas" id="kelas_8" value="VIII" > Kelas 8
                            </label>
                            <label class="form-check-label">
                              <input type="radio" name="kelas" id="kelas_9" value="IX" > Kelas 9
                            </label>
                          </div>
                          
                          <span class="help-block"></span>
                        </div>
                         <!-- close adun edit form jenjang smp -->
                      </div>

                      <div class="form-group" style="display: block;">
                          <div class="col-md-6">
                            <label>Tempat Lahir :</label>
                            <input name="tmp_lhr" id="tmp_lhr" class="form-control" required style="width:100%" readonly type="text">
                            <span class="help-block"></span>
                          </div>

                          <div class="col-md-6">
                            <label>Tanggal Lahir :</label>
                            <input name="tgl_lhr" id="tgl_lhr" class="form-control datepicker" required readonly style="width:100%" type="text">
                            <span class="help-block"></span>
                          </div>
                      </div>

                      <div class="form-group">
                        <div class="col-md-6">
                          <label>Nama Wali Siswa : <span style="color: red;">*</span></label>
                          <input type="text" name="nama_wali" id="nama_wali" class="form-control" readonly required style="width:100%">
                          <span class="help-block"></span>
                        </div>

                        <div class="col-md-6">
                          <label>Pekerjaan Wali Siswa : <span style="color: red;">*</span></label>
                          <select name="pkj_wali" id="pkj_wali" class="form-control " onchange="cek_pekerjaan('#Modal_dklk #pkj_wali','#Modal_dklk #div_pkj_wali_lain');" required style="width:100%;"></select>
                          <span class="help-block"></span>
                        </div>
                      </div>

                      <div class="form-group" id="div_pkj_wali_lain" style="display: none;">
                        <div class="col-md-12">
                          <label>Pekerjaan Diluar Daftar Diatas : <span style="color: red;">*</span></label>
                          <input type="text" name="pkj_wali_lain" id="pkj_wali_lain" class="form-control" required style="width:100%;">
                          <span class="help-block"></span>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-md-12">
                          <label>Alamat Tinggal : <span style="color: red;">*</span></label>
                          <textarea name="alamat" id="alamat" class="form-control" readonly required style="width:100%;resize: none;"></textarea>
                          <span class="help-block"></span>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-md-12">
                          <label>Kota/Kabupaten Sekolah Asal :</label>
                          <input type="hidden" name="id_wly_skl_asal" id="id_wly_skl_asal" class="form-control" value="36.71" readonly required style="width:100%">
                          <input type="text" name="wly_skl_asal" id="wly_skl_asal" class="form-control" value="Kota Tangerang" readonly required style="width:100%">
                          <span class="help-block"></span>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-md-6">
                          <label>Status Sekolah Asal :</label>
                          <select name="status_sekolah_asal" id="status_sekolah_asal" class="form-control" onchange="get_sekolah_by_status('#form_dklk #id_jenjang_sekolah','#form_dklk #skl_asal','#form_dklk #div_akr_skl_asal','#form_dklk #status_sekolah_asal');" required style="width:100%">
                            <option value=""> Pilih Status Sekolah </option>
                            <option value="N"> Negeri </option>
                            <option value="S"> Swasta </option>
                          </select>
                          <span class="help-block"></span>
                        </div>

                        <div class="col-md-6">
                          <label>Nama Sekolah Asal : <span style="color: red;">*</span></label>
                          <select name="skl_asal" id="skl_asal" class="form-control show-tick" data-live-search="true" required style="width:100%">
                            <option value=""> Pilih Sekolah </option>
                          </select>
                          <span class="help-block"></span>
                        </div>
                      </div>

                      <div class="form-group" id="div_akr_skl_asal" style="display: none;">
                        <div class="col-md-12">
                          <label>Akreditasi Sekolah Asal : <span style="color: red;">*</span></label>
                          <select name="akr_skl_asal" id="akr_skl_asal" class="form-control" required style="width:100%">
                            <option value=""> Pilih Akreditasi Sekolah Asal </option>
                            <option value="A"> A</option>
                            <option value="B"> B</option>
                            <option value="C"> C</option>
                          </select>
                          <span class="help-block"></span>
                        </div>
                      </div>

                    </div>

                    <div id="data_mutasi">

                      <div class="alert alert-info">
                        <span>File yang dikirim harus berekstensi ("JPG","JPEG","PNG","PDF") Dan File Tidak Boleh Lebih Dari 2MB</span>
                      </div>

                      <div class="panel panel-primary">
                        <div class="panel-heading">
                          <h3 class="panel-title">Surat Keterangan Pindah Dari Sekolah Asal</h3>
                        </div>
                        <div class="panel-body">
                          <div class="form-group">
                            <div class="col-md-12">
                              <label>Surat Keterangan Pindah Dari Sekolah Asal :</label>
                              <input type="file" accept=".jpg, .jpeg, .png, .pdf" id="f_surat_pindah" name="f_surat_pindah" onchange="start_upload('#form_dklk #f_surat_pindah','#form_dklk #lokasi_f_surat_pindah','#form_dklk #div_prev_surat_pindah','#form_dklk #prev_surat_pindah');">
                              <input type="hidden" name="lokasi_f_surat_pindah" id="lokasi_f_surat_pindah">
                              <div id="div_prev_surat_pindah" class="div_prev_surat_pindah embed-responsive embed-responsive-16by9" style="clear:both; display: none">
                                <iframe id="prev_surat_pindah" class="prev_surat_pindah embed-responsive-item" frameborder="0" scrolling="yes" width="400" height="400"></iframe>
                              </div>
                              <span class="help-block" id="error_surat_pindah" style="color: red;"></span>
                            </div>
                          </div> <hr>

                          <div class="form-group">
                            <div class="col-md-6">
                              <label>Nomor Surat : <span style="color: red;">*</span></label>
                              <input type="text" name="no_surat_pindah" id="no_surat_pindah" class="form-control" maxlength="30" required style="width:100%">
                              <span class="help-block"></span>
                            </div>

                            <div class="col-md-6">
                              <label>Tanggal Surat : <span style="color: red;">*</span></label>
                              <input type="text" name="tgl_surat_pindah" id="tgl_surat_pindah" class="form-control datepicker" required style="width:100%">
                              <span class="help-block"></span>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-md-12">
                          <label>Provinsi Sekolah Tujuan :</label>
                          <select name="prov_tujuan" id="prov_tujuan" class="form-control" onchange="get_kota('#Modal_dklk #id_wly_skl_tujuan',this.value);" required style="width:100%"></select>
                          <span class="help-block"></span>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-md-12">
                          <label>Kota/Kabupaten Sekolah Tujuan :</label>
                          <select name="id_wly_skl_tujuan" id="id_wly_skl_tujuan" class="form-control" required style="width:100%">
                            <option value=''> Pilih Kota/Kabupaten </option>
                          </select>
                          <span class="help-block"></span>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-md-12">
                          <label>Nama Sekolah Tujuan :</label>
                          <input type="text" name="skl_tujuan" id="skl_tujuan" class="form-control selectpicker show-tick" data-live-search="true" required style="width:100%">
                          <span class="help-block"></span>
                        </div>
                      </div>
                      
                      <div class="alert alert-info">
                        <span>Jika status sekolah tujuan adalah "Negeri" maka akreditasi yang dipilih adalah "A"</span>
                      </div>

                      <div class="form-group">
                        <div class="col-md-12">
                          <label>Akreditasi Sekolah Tujuan :</label>
                          <select name="akr_skl_tujuan" id="akr_skl_tujuan" class="form-control" required style="width:100%">
                            <option value=""> Pilih Akreditasi Sekolah Asal </option>
                            <option value="A"> A</option>
                            <option value="B"> B</option>
                            <option value="C"> C</option>
                          </select>
                          <span class="help-block"></span>
                        </div>
                      </div>

                      <div class="panel panel-primary">
                        <div class="panel-heading">
                          <h3 class="panel-title">Rapot</h3>
                        </div>
                        <div class="panel-body">
                          <div class="form-group">
                            <div class="col-md-12">
                              <label>Identitas Siswa : <span style="color: red;">*</span></label>
                              <input type="file" accept=".jpg, .jpeg, .png, .pdf" id="f_rapot_identitas_siswa" name="f_rapot_identitas_siswa" onchange="start_upload('#form_dklk #f_rapot_identitas_siswa','#form_dklk #lokasi_f_rapot_identitas_siswa','#form_dklk #div_prev_rapot_identitas_siswa','#form_dklk #prev_rapot_identitas_siswa');">
                              <input type="hidden" name="lokasi_f_rapot_identitas_siswa" id="lokasi_f_rapot_identitas_siswa">
                              <div id="div_prev_rapot_identitas_siswa" class="div_prev_rapot_identitas_siswa embed-responsive embed-responsive-16by9" style="clear:both; display: none">
                                <iframe id="prev_rapot_identitas_siswa" class="prev_rapot_identitas_siswa embed-responsive-item" frameborder="0" scrolling="yes" width="400" height="400"></iframe>
                              </div>
                              <span class="help-block" id="error_rapot_identitas_siswa" style="color: red;"></span>
                            </div>
                          </div> <hr>

                          <div class="form-group">
                            <div class="col-md-12">
                              <label>Nilai Semester Terakhir : <span style="color: red;">*</span></label>
                              <input type="file" accept=".jpg, .jpeg, .png, .pdf" id="f_rapot_nilai_akhir" name="f_rapot_nilai_akhir" onchange="start_upload('#form_dklk #f_rapot_nilai_akhir','#form_dklk #lokasi_f_rapot_nilai_akhir','#form_dklk #div_prev_rapot_nilai_akhir','#form_dklk #prev_rapot_nilai_akhir');">
                              <input type="hidden" name="lokasi_f_rapot_nilai_akhir" id="lokasi_f_rapot_nilai_akhir">
                              <div id="div_prev_rapot_nilai_akhir" class="div_prev_rapot_nilai_akhir embed-responsive embed-responsive-16by9" style="clear:both; display: none">
                                <iframe id="prev_rapot_nilai_akhir" class="prev_rapot_nilai_akhir embed-responsive-item" frameborder="0" scrolling="yes" width="400" height="400"></iframe>
                              </div>
                              <span class="help-block" id="error_rapot_nilai_akhir" style="color: red;"></span>
                            </div>
                          </div> <hr>

                          <div class="form-group">
                            <div class="col-md-12">
                              <label>Riwayat Pindah Sekolah (Di Lembar Akhir Rapot) : <span style="color: red;">*</span></label>
                              <input type="file" accept=".jpg, .jpeg, .png, .pdf" id="f_rapot_riwayat_pindah" name="f_rapot_riwayat_pindah" onchange="start_upload('#form_dklk #f_rapot_riwayat_pindah','#form_dklk #lokasi_f_rapot_riwayat_pindah','#form_dklk #div_prev_rapot_riwayat_pindah','#form_dklk #prev_rapot_riwayat_pindah');">
                              <input type="hidden" name="lokasi_f_rapot_riwayat_pindah" id="lokasi_f_rapot_riwayat_pindah">
                              <div id="div_prev_rapot_riwayat_pindah" class="div_prev_rapot_riwayat_pindah embed-responsive embed-responsive-16by9" style="clear:both; display: none">
                                <iframe id="prev_rapot_riwayat_pindah" class="prev_rapot_riwayat_pindah embed-responsive-item" frameborder="0" scrolling="yes" width="400" height="400"></iframe>
                              </div>
                              <span class="help-block" id="error_rapot_riwayat_pindah" style="color: red;"></span>
                            </div>
                          </div>
                        </div>
                      </div>


                    </div>

                  </form>
                </div>

                <div class="modal-footer">
                  <div class="pull-left">
                    <button type="button" id="btnPrev" onclick="prev('dklk');" class="btn btn-warning btnPrev" role="tab" data-toggle="tab">Kembali</button>
                  </div>
                  <div class="pull-right">
                    <button type="button" id="btnNext" onclick="next('dklk');" class="btn btn-primary btnNext" role="tab" data-toggle="tab">Selanjutnya</button>
                    <button type="button" id="btnSave" onclick="save('dklk');" class="btn btn-success btnSave">Simpan</button>
                    <button type="button" id="btnCancel" class="btn btn-danger btnCancel" data-dismiss="modal">Batal</button>
                  </div>
                </div>

              </div>
            </div>
        </div>

    </div> <!-- /container -->       
</section>
<script type="text/javascript">
$(document).ready(function () {
  // console.log($SESSION);

  //==========================================================================
    // setInterval(function(){
    //   var txt;
    //   var r = confirm("Waktu anda sudah habis, Apakah anda ingin lanjut ?");
    //   if (r == true) {
    //     location.reload();
    //   } else {
    //     window.location = "<?php echo base_url('frontend/logout'); ?>";
        
    //       txt = "You pressed Cancel!";
    //   }   
    // }, 1800000); // 1800000 (30 menit)  location.reload();

  get_jenjang_sekolah("#jenis_mutasi #jenjang_sekolah");
  $('#jenis_mutasi #jenis_asal_sekolah').change(function() {
    if ($('#jenis_mutasi #jenis_asal_sekolah').val() == "luar") {
      $('#jenis_mutasi #jenis_tujuan_sekolah').html('');
      // $('#jenis_mutasi #jenis_tujuan_sekolah').append('<option value="">Pilih Tujuan Sekolah</option>');

      $('#jenis_mutasi #jenis_tujuan_sekolah').append('<option value="dalam">Dalam Kota Tangerang</option>');
    } else {
      $('#jenis_mutasi #jenis_tujuan_sekolah').html('');
      $('#jenis_mutasi #jenis_tujuan_sekolah').append('<option value="">Pilih Sekolah</option>');
      $('#jenis_mutasi #jenis_tujuan_sekolah').append('<option value="dalam">Dalam Kota Tangerang</option>');
      $('#jenis_mutasi #jenis_tujuan_sekolah').append('<option value="luar">Luar Kota Tangerang</option>');
    }
  });

  $('[name="domisili_siswa"]').change(function() {
    $(this).parent().removeClass('has-error');
    $(this).next().empty();
  }); 

  $('[name="nik"]').change(function() {
    $(this).parent().removeClass('has-error');
    $(this).next().empty();
  });
  
  $('[name="nisn"]').change(function() {
    $(this).parent().removeClass('has-error');
    $(this).next().empty();
  });
  
  $('[name="nama_siswa"]').change(function() {
    $(this).parent().removeClass('has-error');
    $(this).next().empty();
  });

  $('[name="tmp_lhr"]').change(function() {
    $(this).parent().removeClass('has-error');
    $(this).next().empty();
  });

  $('[name="tgl_lhr"]').change(function() {
    $(this).parent().removeClass('has-error');
    $(this).next().empty();
  });

  $('[name="kelas"]').change(function() {
    $(this).parent().parent().removeClass('has-error');
    $(this).next().next().empty();
  });
  
  $('[name="nama_wali"]').change(function() {
    $(this).parent().removeClass('has-error');
    $(this).next().empty();
  });
  
  $('[name="pkj_wali"]').change(function() {
    $(this).parent().removeClass('has-error');
    $(this).next().empty();
  });
  
  $('[name="pkj_wali_lain"]').change(function() {
    $(this).parent().removeClass('has-error');
    $(this).next().empty();
  });
  
  $('[name="alamat"]').change(function() {
    $(this).parent().removeClass('has-error');
    $(this).next().empty();
  });
  
  $('[name="status_sekolah_asal"]').change(function() {
    $(this).parent().removeClass('has-error');
    $(this).next().empty();
    $('[name="status_sekolah_tujuan"]').val('');
    $('[name="skl_tujuan"]').html('<option value=""> Pilih Sekolah </option>');
  });
  
  $('[name="skl_asal"]').change(function() {
    $(this).parent().removeClass('has-error');
    $(this).next().empty();
  });
  
  $('[name="akr_skl_asal"]').change(function() {
    $(this).parent().removeClass('has-error');
    $(this).next().empty();

    $('[name="status_sekolah_tujuan"]').val('');
    $('[name="skl_tujuan"]').html('<option value=""> Pilih Sekolah </option>');
  });
  
  $('[name="id_wly_skl_asal"]').change(function() {
    $(this).parent().removeClass('has-error');
    $(this).next().empty();
  });

  // Error Upload dokumen  
  $('[name="no_surat_pindah"]').change(function() {
    $(this).parent().removeClass('has-error');
    $(this).next().empty();
  });
  
  $('[name="tgl_surat_pindah"]').change(function() {
    $(this).parent().removeClass('has-error');
    $(this).next().empty();
  });

  $('[name="no_surat_penerimaan"]').change(function() {
    $(this).parent().removeClass('has-error');
    $(this).next().empty();
  });
  
  $('[name="tgl_surat_penerimaan"]').change(function() {
    $(this).parent().removeClass('has-error');
    $(this).next().empty();
  });

  $('[name="no_surat_dinas"]').change(function() {
    $(this).parent().removeClass('has-error');
    $(this).next().empty();
  });
  
  $('[name="tgl_surat_dinas"]').change(function() {
    $(this).parent().removeClass('has-error');
    $(this).next().empty();
  });

  $('#prov_tujuan').change(function() {
    $(this).parent().removeClass('has-error');
    $(this).next().empty();

    // get_kota($('#prov_tujuan').val());
  });
  
  $('[name="id_wly_skl_tujuan"]').change(function() {
    $(this).parent().removeClass('has-error');
    $(this).next().empty();
  });
  
  $('[name="status_sekolah_tujuan"]').change(function() {
    $(this).parent().removeClass('has-error');
    $(this).next().empty();
  });
  
  $('[name="skl_tujuan"]').change(function() {
    $(this).parent().removeClass('has-error');
    $(this).next().empty();
  });
  
  $('[name="akr_skl_tujuan"]').change(function() {
    $(this).parent().removeClass('has-error');
    $(this).next().empty();
  });

  //File Upload
  $('[name="f_surat_pindah"]').change(function() {
    $(this).parent().removeClass('has-error');
    $(this).next().next().next().empty();
  });

  $('[name="f_rapot_identitas_siswa"]').change(function() {
    $(this).parent().removeClass('has-error');
    $(this).next().next().next().empty();
  });

  $('[name="f_rapot_nilai_akhir"]').change(function() {
    $(this).parent().removeClass('has-error');
    $(this).next().next().next().empty();
  });

  $('[name="f_rapot_riwayat_pindah"]').change(function() {
    $(this).parent().removeClass('has-error');
    $(this).next().next().next().empty();
  });

  $('[name="f_permohonan_pindah"]').change(function() {
    $(this).parent().removeClass('has-error');
    $(this).next().next().next().empty();
  });

  $('[name="f_surat_dinas_asal"]').change(function() {
    $(this).parent().removeClass('has-error');
    $(this).next().next().next().empty();
  });

  $('[name="f_penerimaan_sekolah_baru"]').change(function() {
    $(this).parent().removeClass('has-error');
    $(this).next().next().next().empty();
  });

  $('[name="f_surat_akreditasi"]').change(function() {
    $(this).parent().removeClass('has-error');
    $(this).next().next().next().empty();
  });

});

function ceknik(type) {
  switch(type){
    case 'dkdk':
      var nik = $('#form_dkdk #nik').val();
      $('#form_dkdk #nama_siswa').val("");
      $('#form_dkdk #tmp_lhr').html("");
      $('#form_dkdk #tgl_lhr').html("");
      $('#form_dkdk #alamat').html("");
      $('#form_dkdk #nama_wali').val("");
      $('#form_dkdk #pkj_wali').val("");
    break;

    case 'lkdk':
      var nik = $('#form_lkdk #nik').val();
      $('#form_lkdk #nama_siswa').val("");
      $('#form_lkdk #tmp_lhr').html("");
      $('#form_lkdk #tgl_lhr').html("");
      $('#form_lkdk #alamat').html("");
      $('#form_lkdk #nama_wali').val("");
      $('#form_lkdk #pkj_wali').val("");
    break;

    default:
      var nik = $('#form_dklk #nik').val();
      $('#form_dklk #nama_siswa').val("");
      $('#form_dklk #tmp_lhr').html("");
      $('#form_dklk #tgl_lhr').html("");
      $('#form_dklk #alamat').html("");
      $('#form_dklk #nama_wali').val("");
      $('#form_dklk #pkj_wali').val("");
    break;
  }
  
  $('.div_loading').append('<p id="p_loading">Sedang Mengambil Data...</p>');
  // reset_input('daftar');
  $.ajax({ url : '<?php echo base_url('frontend/api/ceknik'); ?>',
    type : 'POST',
    data:{nik: nik},
    dataType: "JSON",
    success: function(data){
      //  console.log(data);
      $('.div_loading').html('');
      var kota                = JSON.parse(data.kota);
      // console.log(kota);
      if (kota.constructor === Array) {
        switch(type){
          case 'dkdk':
            $('#form_dkdk #nama_siswa').val(kota[0].NAMA_LGKP);
            $('#form_dkdk #tmp_lhr').val(kota[0].TMPT_LHR);
            $('#form_dkdk #tgl_lhr').val(kota[0].TGL_LHR);
            $('#form_dkdk #alamat').val(kota[0].ALAMAT+' Kel.'+kota[0].NAMA_KEL+' Kec.'+kota[0].NAMA_KEC);
            $('#form_dkdk #nama_wali').val(kota[0].NAMA_LGKP_AYAH);
            if (kota[0].NAMA_LGKP_AYAH !== 0) {
              var ayah = JSON.parse(data.ayah);
              $('#form_dkdk #pkj_wali').val(ayah[0].JENIS_PKRJN.toUpperCase());
            }
           $('#form_dkdk #id_status_domisili').val("1");
          break;

          case 'lkdk':
            $('#form_lkdk #nama_siswa').val(kota[0].NAMA_LGKP);
            $('#form_lkdk #tmp_lhr').val(kota[0].TMPT_LHR);
            $('#form_lkdk #tgl_lhr').val(kota[0].TGL_LHR);
            $('#form_lkdk #alamat').val(kota[0].ALAMAT+' Kel.'+kota[0].NAMA_KEL+' Kec.'+kota[0].NAMA_KEC);
            $('#form_lkdk #nama_wali').val(kota[0].NAMA_LGKP_AYAH);
            if (kota[0].NAMA_LGKP_AYAH !== 0) {
              var ayah = JSON.parse(data.ayah);
              $('#form_lkdk #pkj_wali').val(ayah[0].JENIS_PKRJN.toUpperCase());
            }
           $('#form_lkdk #id_status_domisili').val("1");
          break;

          default: // dklk
            $('#form_dklk #nama_siswa').val(kota[0].NAMA_LGKP);
            $('#form_dklk #tmp_lhr').val(kota[0].TMPT_LHR);
            $('#form_dklk #tgl_lhr').val(kota[0].TGL_LHR);
            $('#form_dklk #alamat').val(kota[0].ALAMAT+' Kel.'+kota[0].NAMA_KEL+' Kec.'+kota[0].NAMA_KEC);
            $('#form_dklk #nama_wali').val(kota[0].NAMA_LGKP_AYAH);
            if (kota[0].NAMA_LGKP_AYAH !== 0) {
              var ayah = JSON.parse(data.ayah);
              $('#form_dklk #pkj_wali').val(ayah[0].JENIS_PKRJN.toUpperCase());
            }
           $('#form_dklk #id_status_domisili').val("1");
            
          break;
        }

      } else {
        swal("Perhatian!", "NIK Yang Dimasukan Tidak Ditemukan, Silahkan isi data diri", "warning");
        $('[name="nik"]').val('');
        if (type == "dkdk") {
          // $('#form_dkdk #nama_siswa').prop('readonly', false);
          // $('#form_dkdk #tmp_lhr').prop('readonly', false);
          // $('#form_dkdk #tgl_lhr').prop('readonly', false);
          // $('#form_dkdk #nama_wali').prop('readonly', false);
          // $('#form_dkdk #alamat').prop('readonly', false);
          $('#form_dkdk #nama_siswa').removeAttr("readonly");
          $('#form_dkdk #tmp_lhr').removeAttr("readonly");
          $('#form_dkdk #tgl_lhr').removeAttr("readonly");
          $('#form_dkdk #nama_wali').removeAttr("readonly");
          $('#form_dkdk #alamat').removeAttr("readonly");
          $('#form_dkdk #wly_skl_asal').removeAttr("readonly");
          $('#form_dkdk #id_status_domisili').val("2");
          

        } else if (type == "dklk") {
          $("#btn_ceknik").attr("style","display: none;");
          
          $('#form_dklk #nama_siswa').removeAttr("readonly");
          $('#form_dklk #tmp_lhr').removeAttr("readonly");
          $('#form_dklk #tgl_lhr').removeAttr("readonly");
          $('#form_dklk #nama_wali').removeAttr("readonly");
          $('#form_dklk #alamat').removeAttr("readonly");
          $('#form_dklk #wly_skl_asal').removeAttr("readonly");
          
          $('#form_dklk #id_status_domisili').val("2");
        } else if (type == "lkdk") {
          $("#btn_ceknik").attr("style","display: none;");
          
          $('#form_lkdk #nama_siswa').removeAttr("readonly");
          $('#form_lkdk #tmp_lhr').removeAttr("readonly");
          $('#form_lkdk #tgl_lhr').removeAttr("readonly");
          $('#form_lkdk #nama_wali').removeAttr("readonly");
          $('#form_lkdk #alamat').removeAttr("readonly");
          $('#form_lkdk #wly_skl_asal').removeAttr("readonly");
          
          $('#form_lkdk #id_status_domisili').val("2");
        }
      }
    },
    error: function (jqXHR, textStatus, errorThrown){
      $('.div_loading').html('');
      swal("Terjadi Kesalahan!", "Gagal Terhubung Dengan Server, Silahkan Coba Beberapa Saat Lagi!", "error");
    }
  });
}
// get_sekolah_by_status('#form_lkdk #id_jenjang_sekolah','#form_lkdk #skl_tujuan','#form_lkdk #div_akr_skl_tujuan','#form_lkdk #status_sekolah_tujuan');
function get_sekolah_by_status(jenjang_sekolah, target,target_div,source) { 
  console.log(jenjang_sekolah, target,target_div,source); 
  //var jenjang_sekolah = $('#jenis_mutasi #jenjang_sekolah').val();
  //alert(jenjang_sekolah);
  var status_sekolah = $(source).val();

  if (status_sekolah == "S") {
    $(target_div).attr('style','display: block;');
  } else{
    $(target_div).attr('style','display: none;');

    if ((source == "#form_lkdk #status_sekolah_asal #jenjang_sekolah") || (source == "#form_dkdk #status_sekolah_asal #jenjang_sekolah") || (source == "#form_dklk #status_sekolah_asal #jenjang_sekolah")) {
      $('[name="akr_skl_asal"]').val('');
    }
  }
  
  
 // if (jenjang_sekolah=="") {
 //      $("#Modal_dklk #id_jenjang_sekolah").val(jenjang_sekolah); 
 // }
  // source = tampungan ketiga
  switch(source) {

    case '#form_dkdk #status_sekolah_asal':
    case '#form_dkdk #status_sekolah_tujuan':
      var id_sekolah_asal = $('#form_dkdk #skl_asal ').val();
      var id_jenjang_sekolah = $("#form_dkdk #id_jenjang_sekolah").val(); 
      break;

    case '#form_lkdk #status_sekolah_asal':
    case '#form_lkdk #status_sekolah_tujuan':
      var id_sekolah_asal = "";
      var id_jenjang_sekolah = $("#form_lkdk #id_jenjang_sekolah").val(); 
      break;

    default:
      var id_sekolah_asal = $('#form_dklk #skl_asal ').val();
      var id_jenjang_sekolah = $("#form_dklk #id_jenjang_sekolah").val(); 
      break;
  }
  

  // console.log(status_sekolah);
  $.ajax({ url : '<?php echo base_url("frontend/ajax/ajax_get_sekolah_by_status/"); ?>',
    type : "POST",
    data : {status_sekolah : status_sekolah, id_sekolah_asal : id_sekolah_asal, jenjang_sekolah : id_jenjang_sekolah},
    dataType : "JSON",
    success : function(data) {
      $(target).html('');
      //tampungan kedua
      
      $(target).append(data.sekolah);
      // alert("testetst");
      $('.selectpicker').selectpicker('refresh');
      
      // $('#skl_asal').find('option:selected').prop('disabled', true);
    },
    error : function(jqXHR, textStatus, errorThrown) {      
      swal("Terjadi Kesalahan!", "Terjadi Kesalahan Dalam Mengambil Data!", "error");
    }
  });
}

function select_domisili(sel){
  if (sel.value=="dk") {
    $("#form_dklk #btn_ceknik").attr("style","display: block;");
    $('#form_dklk #nik').removeAttr("readonly");
    $('#form_dklk #nisn').removeAttr("readonly");
	  $('#form_dklk #nama_siswa').attr('readonly',true);
	  $('#form_dklk #tmp_lhr').attr('readonly',true);
	  $('#form_dklk #tgl_lhr').attr('readonly',true);
	  $('#form_dklk #nama_wali').attr('readonly',true);
	  $('#form_dklk #alamat').attr('readonly',true);
	  $('#form_dklk #wly_skl_asal').attr('readonly',true);

    $('#form_dklk #nama_siswa').val("");
    $('#form_dklk #tmp_lhr').val("");
    $('#form_dklk #tgl_lhr').val("");
    $('#form_dklk #alamat').val("");
    $('#form_dklk #nama_wali').val("");
    $('#form_dklk #pkj_wali').val("");

    // dkdk
    $("#form_dkdk #btn_ceknik").attr("style","display: block;");
    $('#form_dkdk #nik').removeAttr("readonly");
    $('#form_dkdk #nisn').removeAttr("readonly");
	  $('#form_dkdk #nama_siswa').attr('readonly',true);
	  $('#form_dkdk #tmp_lhr').attr('readonly',true);
	  $('#form_dkdk #tgl_lhr').attr('readonly',true);
	  $('#form_dkdk #nama_wali').attr('readonly',true);
	  $('#form_dkdk #alamat').attr('readonly',true);
	  $('#form_dkdk #wly_skl_asal').attr('readonly',true);

    $('#form_dkdk #nama_siswa').val("");
    $('#form_dkdk #tmp_lhr').val("");
    $('#form_dkdk #tgl_lhr').val("");
    $('#form_dkdk #alamat').val("");
    $('#form_dkdk #nama_wali').val("");
    $('#form_dkdk #pkj_wali').val("");

    // lkdk
    $("#form_lkdk #btn_ceknik").attr("style","display: block;");
    $('#form_lkdk #nik').removeAttr("readonly");
    $('#form_lkdk #nisn').removeAttr("readonly");
	  $('#form_lkdk #nama_siswa').attr('readonly',true);
	  $('#form_lkdk #tmp_lhr').attr('readonly',true);
	  $('#form_lkdk #tgl_lhr').attr('readonly',true);
	  $('#form_lkdk #nama_wali').attr('readonly',true);
	  $('#form_lkdk #alamat').attr('readonly',true);
	  $('#form_lkdk #wly_skl_asal').attr('readonly',true);

    $('#form_lkdk #nama_siswa').val("");
    $('#form_lkdk #tmp_lhr').val("");
    $('#form_lkdk #tgl_lhr').val("");
    $('#form_lkdk #alamat').val("");
    $('#form_lkdk #nama_wali').val("");
    $('#form_lkdk #pkj_wali').val("");

  }else{
    //dklk
    $("#form_dklk #btn_ceknik").attr("style","display: none;");
    $('#form_dklk #nik').removeAttr("readonly");
    $('#form_dklk #nisn').removeAttr("readonly");
	  $('#form_dklk #nama_siswa').removeAttr("readonly");
    $('#form_dklk #tmp_lhr').removeAttr("readonly");
    $('#form_dklk #tgl_lhr').removeAttr("readonly");
    $('#form_dklk #nama_wali').removeAttr("readonly");
    $('#form_dklk #alamat').removeAttr("readonly");
    $('#form_dklk #wly_skl_asal').removeAttr("readonly");

    $('#form_dklk #nama_siswa').val("");
    $('#form_dklk #tmp_lhr').val("");
    $('#form_dklk #tgl_lhr').val("");
    $('#form_dklk #alamat').val("");
    $('#form_dklk #nama_wali').val("");
    $('#form_dklk #pkj_wali').val("");
    $('#form_dklk #id_status_domisili').val("3");
    
    //dkdk
    $("#form_dkdk #btn_ceknik").attr("style","display: none;");
    $('#form_dkdk #nik').removeAttr("readonly");
    $('#form_dkdk #nisn').removeAttr("readonly");
	  $('#form_dkdk #nama_siswa').removeAttr("readonly");
    $('#form_dkdk #tmp_lhr').removeAttr("readonly");
    $('#form_dkdk #tgl_lhr').removeAttr("readonly");
    $('#form_dkdk #nama_wali').removeAttr("readonly");
    $('#form_dkdk #alamat').removeAttr("readonly");
    $('#form_dkdk #wly_skl_asal').removeAttr("readonly");

    $('#form_dkdk #nama_siswa').val("");
    $('#form_dkdk #tmp_lhr').val("");
    $('#form_dkdk #tgl_lhr').val("");
    $('#form_dkdk #alamat').val("");
    $('#form_dkdk #nama_wali').val("");
    $('#form_dkdk #pkj_wali').val("");
    $('#form_dkdk #id_status_domisili').val("3");

    //lkdk
    $("#form_lkdk #btn_ceknik").attr("style","display: none;");
    $('#form_lkdk #nik').removeAttr("readonly");
    $('#form_lkdk #nisn').removeAttr("readonly");
	  $('#form_lkdk #nama_siswa').removeAttr("readonly");
    $('#form_lkdk #tmp_lhr').removeAttr("readonly");
    $('#form_lkdk #tgl_lhr').removeAttr("readonly");
    $('#form_lkdk #nama_wali').removeAttr("readonly");
    $('#form_lkdk #alamat').removeAttr("readonly");
    $('#form_lkdk #wly_skl_asal').removeAttr("readonly");

    $('#form_lkdk #nama_siswa').val("");
    $('#form_lkdk #tmp_lhr').val("");
    $('#form_lkdk #tgl_lhr').val("");
    $('#form_lkdk #alamat').val("");
    $('#form_lkdk #nama_wali').val("");
    $('#form_lkdk #pkj_wali').val("");
    $('#form_lkdk #id_status_domisili').val("3");

  }
}

function check_akreditasi_by_status_sekolah(akr_asal,status_tujuan,status_asal) {  
  if ($(akr_asal).val() == "") { // if status sekolah = N
    var val = "A";
  } else {
    var val = $(akr_asal).val();
  }

  if ($(status_tujuan).val() == "N") {
    var akr_skl_asal = "A";
    if (akr_skl_asal < val) {
      swal("Perhatian!", "Akreditasi sekolah asal harus lebih tinggi dari sekolah tujuan!", "warning");
      $(status_tujuan).val('');
    }
  } else {
    
  }

  if ($(status_asal).val() == "S") {
    if ($(status_tujuan).val() == "S") {
      $("#Modal_dkdk #div_group_f_surat_akreditasi").hide();
    } else {
      $("#Modal_dkdk #div_group_f_surat_akreditasi").show();
    }
  }
  // console.log('dari check_akreditasi_by_status_sekolah',akr_skl_asal,val);
}

function check_akreditasi(source,target,status_sekolah_tujuan) {
  // console.log(source,target,status_sekolah_tujuan);
  if ($(status_sekolah_tujuan).val() == "N") {
    var akr_skl_asal = "A";
  } else {
    var akr_skl_asal = $(source).val();
  }
  
  var val = $(target).val();
  // besar ke kecil ditolak
  // kecil ke besar diterima
  console.log(akr_skl_asal,val);
  if (akr_skl_asal > val) {
    swal("Perhatian!", "Akreditasi sekolah asal harus lebih tinggi dari sekolah tujuan!", "warning");
    $(target).val('');
  } else {
    // swal("Terjadi Kesalahan!", "Masuk Else", "error");
  }
  // console.log('dari check_akreditasi',akr_skl_asal,val);
}

function cek_pekerjaan(source,target) {
  var pekerjaan = $(source).val();
  if (pekerjaan == "LAINNYA") {
    $(target).show()
  } else {
    $(target).hide();
  }
}

// adun edit
function get_jenjang_sekolah(target) {
  var url = "<?php echo base_url('frontend/ajax/ajax_jenjang'); ?>";
  $.ajax({ url : url,
    type : 'GET',
    dataType : "JSON",
    success: function(data) {
      $(target).html(data.jenjang_sekolah);
    },
    error: function(jqXHR, textStatus, errorThrown){      
      swal("Terjadi Kesalahan!", "Terjadi Kesalahan Dalam Mengambil Data!", "error");
    }
  });
}
// adun edit close

function logout() {
  window.location.href = "<?php echo base_url('frontend/logout'); ?>";
}

function get_prov(target) {
  $(target).html("");
  var url = "<?php echo base_url('frontend/ajax/ajax_get_provinsi'); ?>";
  $.ajax({ url : url,
    type : 'GET',
    dataType : "JSON",
    success: function(data) {
      $(target).html(data.prov);
    },
    error: function(jqXHR, textStatus, errorThrown){      
      swal("Terjadi Kesalahan!", "Terjadi Kesalahan Dalam Mengambil Data!", "error");
    }
  });
}

function get_kota(target,prov) {
  // var prov = $('#prov_daftar').val();
  $(target).html("");
  $.ajax({ url : '<?php echo base_url('frontend/ajax/ajax_get_kota/'); ?>',
    type : 'get',
    data:{prov: prov},
    dataType: "JSON",
    success: function(data){
      $(target).html(data.kota);
    },
    error: function (jqXHR, textStatus, errorThrown){
        swal("Terjadi Kesalahan!", "Terjadi Kesalahan Dalam Mengambil Data!", "error");
    }
  });
}

function get_pekerjaan(target) {
  $(target).html("");
  var url = "<?php echo base_url('frontend/ajax/ajax_pekerjaan'); ?>";
  $.ajax({ url : url,
    type : 'GET',
    dataType : "JSON",
    success: function(data) {
      $(target).html(data.pekerjaan);
    },
    error: function(jqXHR, textStatus, errorThrown){      
      swal("Terjadi Kesalahan!", "Terjadi Kesalahan Dalam Mengambil Data!", "error");
    }
  });
}

function save(type) {
  var data;
  var url;
  var modals;
  switch(type) {
    case 'dklk':
      modals = $('#Modal_dklk');
      $('#Modal_dklk #btnSave').text('sedang menyimpan...'); //change button text
      $('#Modal_dklk #btnSave').attr('disabled',true); //set button disable
      data = $('#form_dklk').serialize(); // reset form on modals

      if(save_method == 'add') {
        url = "<?php echo base_url('frontend/ajax_insert_mutasi')?>";
      } else if(save_method == 'edit'){
        url = "<?php echo base_url('frontend/ajax_update_mutasi')?>";
      }
      break;

    case 'lkdk':
      modals = $('#Modal_lkdk');
      $('#Modal_lkdk #btnSave').text('sedang menyimpan...'); //change button text
      $('#Modal_lkdk #btnSave').attr('disabled',true); //set button disable
      data = $('#form_lkdk').serialize(); // reset form on modals

      if(save_method == 'add') {
        url = "<?php echo base_url('frontend/ajax_insert_mutasi')?>";
      } else if(save_method == 'edit'){
        url = "<?php echo base_url('frontend/ajax_update_mutasi')?>";
      }     
      break;

    default: // dkdk
      modals = $('#Modal_dkdk');
      $('#Modal_dkdk #btnSave').text('sedang menyimpan...'); //change button text
      $('#Modal_dkdk #btnSave').attr('disabled',true); //set button disable
      data = $('#form_dkdk').serialize(); // reset form on modals

      if(save_method == 'add') {
        url = "<?php echo base_url('frontend/ajax_insert_mutasi')?>";
      } else if(save_method == 'edit'){
        url = "<?php echo base_url('frontend/ajax_update_mutasi')?>";
      }      
      break;
  }
  // console.log(data,url,modals);
  // alert('Masuk');
  // ajax adding data to database
  swal({
    title: "Anda Menyetujui!",
    text: "Dengan ini saya <?php echo $_SESSION['nama_warga']; ?> menyatakan dengan sebenarnya bahwa data yang saya isikan adalah benar. Jika dikemudian hari didapatkan ada data yang tidak benar maka saya akan bertanggungjawab dan bersedia menerima sanksi sesuai dengan ketentuan peraturan perundang-undangan yang berlaku.",
    type: "warning",
    showCancelButton: true,
    confirmButtonClass: "btn-success",
    confirmButtonText: "Ya, Saya Menyetujui",
    closeOnConfirm: true
  },
  function(isConfirm){
    if (isConfirm) {
      $.ajax({ url : url,
        type: "POST",
        data: data,
        dataType: "JSON",
        success: function(data){
          console.log(data);
          // debugger;
          if(data.status_login==true){

              if(data.status){

                swal("Berhasil!", "Berkas berhasil dikirim", "success");
                $('#form_dkdk')[0].reset(); // reset form on modals          
                $('#form_lkdk')[0].reset(); // reset form on modals
                $('#form_dklk')[0].reset(); // reset form on modals
                modals.modal('hide');
                reload_table();
                }else{

                $('[name="lokasi_f_rapot_identitas_siswa"]').parent().addClass(data.error_class['lokasi_f_rapot_identitas_siswa']);
                $('[name="lokasi_f_rapot_identitas_siswa"]').next().next().text(data.error_string['lokasi_f_rapot_identitas_siswa']);

                $('[name="lokasi_f_rapot_nilai_akhir"]').parent().addClass(data.error_class['lokasi_f_rapot_nilai_akhir']);
                $('[name="lokasi_f_rapot_nilai_akhir"]').next().next().text(data.error_string['lokasi_f_rapot_nilai_akhir']);

                $('[name="lokasi_f_rapot_riwayat_pindah"]').parent().addClass(data.error_class['lokasi_f_rapot_riwayat_pindah']);
                $('[name="lokasi_f_rapot_riwayat_pindah"]').next().next().text(data.error_string['lokasi_f_rapot_riwayat_pindah']);

                switch(type){
                  case 'lkdk':
                    $('[name="lokasi_f_surat_pindah"]').parent().addClass(data.error_class['lokasi_f_surat_pindah']);
                    $('[name="lokasi_f_surat_pindah"]').next().next().text(data.error_string['lokasi_f_surat_pindah']);

                    $('[name="no_surat_pindah"]').parent().addClass(data.error_class['no_surat_pindah']);
                    $('[name="no_surat_pindah"]').next().text(data.error_string['no_surat_pindah']);

                    $('[name="tgl_surat_pindah"]').parent().addClass(data.error_class['tgl_surat_pindah']);
                    $('[name="tgl_surat_pindah"]').next().text(data.error_string['tgl_surat_pindah']);

                    $('[name="lokasi_f_surat_dinas_asal"]').parent().addClass(data.error_class['lokasi_f_surat_dinas_asal']);
                    $('[name="lokasi_f_surat_dinas_asal"]').next().next().text(data.error_string['lokasi_f_surat_dinas_asal']);

                    $('[name="no_surat_dinas"]').parent().addClass(data.error_class['no_surat_dinas']);
                    $('[name="no_surat_dinas"]').next().text(data.error_string['no_surat_dinas']);

                    $('[name="tgl_surat_dinas"]').parent().addClass(data.error_class['tgl_surat_dinas']);
                    $('[name="tgl_surat_dinas"]').next().text(data.error_string['tgl_surat_dinas']);

                    $('[name="lokasi_f_penerimaan_sekolah_baru"]').parent().addClass(data.error_class['lokasi_f_penerimaan_sekolah_baru']);
                    $('[name="lokasi_f_penerimaan_sekolah_baru"]').next().next().text(data.error_string['lokasi_f_penerimaan_sekolah_baru']);

                    $('[name="skl_tujuan"]').parent().addClass(data.error_class['skl_tujuan']);
                    $('[name="skl_tujuan"]').next().text(data.error_string['skl_tujuan']);

                    $('[name="akr_skl_tujuan"]').parent().addClass(data.error_class['akr_skl_tujuan']);
                    $('[name="akr_skl_tujuan"]').next().text(data.error_string['akr_skl_tujuan']);

                    $('[name="no_surat_penerimaan"]').parent().addClass(data.error_class['no_surat_penerimaan']);
                    $('[name="no_surat_penerimaan"]').next().text(data.error_string['no_surat_penerimaan']);

                    $('[name="tgl_surat_penerimaan"]').parent().addClass(data.error_class['tgl_surat_penerimaan']);
                    $('[name="tgl_surat_penerimaan"]').next().text(data.error_string['tgl_surat_penerimaan']);

                    $('[name="lokasi_f_surat_akreditasi"]').parent().addClass(data.error_class['lokasi_f_surat_akreditasi']);
                    $('[name="lokasi_f_surat_akreditasi"]').next().next().text(data.error_string['lokasi_f_surat_akreditasi']);
                  break;

                  case 'dkdk':
                    $('[name="lokasi_f_surat_pindah"]').parent().addClass(data.error_class['lokasi_f_surat_pindah']);
                    $('[name="lokasi_f_surat_pindah"]').next().next().text(data.error_string['lokasi_f_surat_pindah']);

                    $('[name="no_surat_pindah"]').parent().addClass(data.error_class['no_surat_pindah']);
                    $('[name="no_surat_pindah"]').next().text(data.error_string['no_surat_pindah']);

                    $('[name="tgl_surat_pindah"]').parent().addClass(data.error_class['tgl_surat_pindah']);
                    $('[name="tgl_surat_pindah"]').next().text(data.error_string['tgl_surat_pindah']);

                    $('[name="lokasi_f_penerimaan_sekolah_baru"]').parent().addClass(data.error_class['lokasi_f_penerimaan_sekolah_baru']);
                    $('[name="lokasi_f_penerimaan_sekolah_baru"]').next().next().text(data.error_string['lokasi_f_penerimaan_sekolah_baru']);

                    $('[name="skl_tujuan"]').parent().addClass(data.error_class['skl_tujuan']);
                    $('[name="skl_tujuan"]').next().text(data.error_string['skl_tujuan']);

                    $('[name="akr_skl_tujuan"]').parent().addClass(data.error_class['akr_skl_tujuan']);
                    $('[name="akr_skl_tujuan"]').next().text(data.error_string['akr_skl_tujuan']);

                    $('[name="no_surat_penerimaan"]').parent().addClass(data.error_class['no_surat_penerimaan']);
                    $('[name="no_surat_penerimaan"]').next().text(data.error_string['no_surat_penerimaan']);

                    $('[name="tgl_surat_penerimaan"]').parent().addClass(data.error_class['tgl_surat_penerimaan']);
                    $('[name="tgl_surat_penerimaan"]').next().text(data.error_string['tgl_surat_penerimaan']);

                    $('[name="lokasi_f_surat_akreditasi"]').parent().addClass(data.error_class['lokasi_f_surat_akreditasi']);
                    $('[name="lokasi_f_surat_akreditasi"]').next().next().text(data.error_string['lokasi_f_surat_akreditasi']);
                  break;

                  default:
                    $('[name="lokasi_f_surat_pindah"]').parent().addClass(data.error_class['lokasi_f_surat_pindah']);
                    $('[name="lokasi_f_surat_pindah"]').next().next().text(data.error_string['lokasi_f_surat_pindah']);

                    $('[name="no_surat_pindah"]').parent().addClass(data.error_class['no_surat_pindah']);
                    $('[name="no_surat_pindah"]').next().text(data.error_string['no_surat_pindah']);

                    $('[name="tgl_surat_pindah"]').parent().addClass(data.error_class['tgl_surat_pindah']);
                    $('[name="tgl_surat_pindah"]').next().text(data.error_string['tgl_surat_pindah']);

                    // $('[name="lokasi_f_permohonan_pindah"]').parent().addClass(data.error_class['lokasi_f_permohonan_pindah']);
                    // $('[name="lokasi_f_permohonan_pindah"]').next().next().text(data.error_string['lokasi_f_permohonan_pindah']);
                  break;

                }
                swal("Terjadi Kesalahan!", "Ada Beberapa Data Yang Salah, Silahkan Koreksi Kembali!", "error");
                }
          }else{
            // swal("Session anda habis!", "Silahkan login kembali Kembali!", "error");
            swal({
                  title: "Maaf Session anda habis!",
                  text: "Silahkan login Kembali!",
                  type: "error"
              }, function() {
                window.location = "<?php echo base_url('frontend/logout')?>"
              });
          }
          

          // switch(type){
          //   case 'dklk':
          //     $('#Modal_dklk #btnSave').text('simpan'); //change button text
          //     $('#Modal_dklk #btnSave').attr('disabled',false); //set button disable
          //   break;

          //   case 'dkdk':
          //     $('#Modal_dkdk #btnSave').text('simpan'); //change button text
          //     $('#Modal_dkdk #btnSave').attr('disabled',false); //set button disable
          //   break;

          //   default:
          //     $('#Modal_lkdk #btnSave').text('simpan'); //change button text
          //     $('#Modal_lkdk #btnSave').attr('disabled',false); //set button disable
          //   break;
          // }
        },
        error: function (jqXHR, textStatus, errorThrown){
          // alert('Error adding / update data');
          swal("Terjadi Kesalahan!", "Aplikasi gagal menyimpan / mengubah data!", "error");

          // switch(type){
          //   case 'dklk':
          //     $('#Modal_dklk #btnSave').text('simpan'); //change button text
          //     $('#Modal_dklk #btnSave').attr('disabled',false); //set button disable
          //   break;

          //   case 'dkdk':
          //     $('#Modal_dkdk #btnSave').text('simpan'); //change button text
          //     $('#Modal_dkdk #btnSave').attr('disabled',false); //set button disable
          //   break;

          //   default:
          //     $('#Modal_lkdk #btnSave').text('simpan'); //change button text
          //     $('#Modal_lkdk #btnSave').attr('disabled',false); //set button disable
          //   break;
          // }
        }
      });
    }
    switch(type){
      case 'dklk':
        $('#Modal_dklk #btnSave').text('simpan'); //change button text
        $('#Modal_dklk #btnSave').attr('disabled',false); //set button disable
      break;

      case 'dkdk':
        $('#Modal_dkdk #btnSave').text('simpan'); //change button text
        $('#Modal_dkdk #btnSave').attr('disabled',false); //set button disable
      break;

      default:
        $('#Modal_lkdk #btnSave').text('simpan'); //change button text
        $('#Modal_lkdk #btnSave').attr('disabled',false); //set button disable
      break;
    }
    
  });
}

function open_modul() {
  //alert (id_warga);
  $("#form_dkdk #btn_ceknik").attr("style","display: none;");
  $("#form_dklk #btn_ceknik").attr("style","display: none;");
  $("#form_lkdk #btn_ceknik").attr("style","display: none;");

  
  var jenjang_sekolah = $('#jenis_mutasi #jenjang_sekolah').val();
  var jenis_asal_sekolah = $('#jenis_mutasi #jenis_asal_sekolah').val();
  var jenis_tujuan_sekolah = $('#jenis_mutasi #jenis_tujuan_sekolah').val();

  if ((jenis_asal_sekolah == "dalam") && (jenis_tujuan_sekolah == "dalam")) {
    add('dkdk',jenjang_sekolah);

  } else if((jenis_asal_sekolah == "luar") && (jenis_tujuan_sekolah == "dalam")) {
    add('lkdk',jenjang_sekolah);
  } else if((jenis_asal_sekolah == "dalam") && (jenis_tujuan_sekolah == "luar")) {
    add('dklk',jenjang_sekolah);
  } else {
    swal("Perhatian!", "Form Pengajuan yang Anda Minta Tidak Tersedia!", "warning");
  }
}

function add(type,jenjang_sekolah){
  save_method = 'add';
  
  switch(type) {

    case 'dklk': 
      $("#Modal_dklk #id_jenjang_sekolah").val(jenjang_sekolah); 
      // alert(jenjang_sekolah);
      switch(jenjang_sekolah){
        case '2':
        // case '7':
          $("#Modal_dklk #jenjang_sd").attr("style","display: block;");
          $("#Modal_dklk #kelas_1").attr('checked', 'checked'); 
          $("#Modal_dklk #jenjang_smp").attr("style","display: none;");
        break;
        default :
          $("#Modal_dklk #jenjang_smp").attr("style","display: block;");
          $("#Modal_dklk #jenjang_sd").attr("style","display: none;");
          $("#Modal_dklk #kelas_7").attr('checked', 'checked');
        break;
      } 
      $('#form_dklk')[0].reset(); // reset form on modals
      get_prov('#Modal_dklk #prov_tujuan');  
      get_pekerjaan('#Modal_dklk #pkj_wali'); 
      $('#Modal_dklk #btnSave').text('simpan'); //change button text
      $('#Modal_dklk #btnSave').attr('disabled',false); //set button enable
      break;


    case 'lkdk': 
      $("#Modal_lkdk #id_jenjang_sekolah").val(jenjang_sekolah); 
      // alert(jenjang_sekolah);

      switch(jenjang_sekolah){
        case '2':
        // case '7':
          $("#Modal_lkdk #jenjang_sd").attr("style","display: block;");
          $("#Modal_lkdk #jenjang_smp").attr("style","display: none;");
          $("#Modal_lkdk #kelas_1").attr('checked', 'checked'); 
        break;
        default :
          $("#Modal_lkdk #jenjang_smp").attr("style","display: block;");
          $("#Modal_lkdk #jenjang_sd").attr("style","display: none;");
          $("#Modal_lkdk #kelas_7").attr('checked', 'checked');
        break;
      }   
      $('#form_lkdk')[0].reset(); // reset form on modals 
      get_prov('#Modal_lkdk #prov_asal'); 
      get_pekerjaan('#Modal_lkdk #pkj_wali');
      // $("#Modal_lkdk #kelas_7").attr('checked', 'checked');  
      $('#Modal_lkdk #btnSave').text('simpan'); //change button text
      $('#Modal_lkdk #btnSave').attr('disabled',false); //set button enable
      break;


    default:
      $("#Modal_dkdk #id_jenjang_sekolah").val(jenjang_sekolah); 
      // alert(jenjang_sekolah);

      switch(jenjang_sekolah){
        case '2':
        // case '7':
          $("#Modal_dkdk #jenjang_sd").attr("style","display: block;");
          $("#Modal_dkdk #kelas_1").attr('checked', 'checked'); 
          $("#Modal_dkdk #jenjang_smp").attr("style","display: none;");
        break;
        default :
          $("#Modal_dkdk #jenjang_smp").attr("style","display: block;");
          $("#Modal_dkdk #jenjang_sd").attr("style","display: none;");
          $("#Modal_dkdk #kelas_7").attr('checked', 'checked');
        break;
      } 
      $('#form_dkdk')[0].reset(); // reset form on modal

      get_pekerjaan('#Modal_dkdk #pkj_wali');
      // $("#Modal_dkdk #kelas_7").attr('checked', 'checked'); 
      $('#Modal_dkdk #btnSave').text('simpan'); //change button text
      $('#Modal_dkdk #btnSave').attr('disabled',false); //set button enable
      break;
  }
    $('[name="alamat"]').empty();
    // Reset preview upload
    reset_prev('.div_prev_surat_pindah','.prev_surat_pindah');
    reset_prev('.div_prev_rapot_identitas_siswa','.prev_rapot_identitas_siswa');
    reset_prev('.div_prev_rapot_nilai_akhir','.prev_rapot_nilai_akhir');
    reset_prev('.div_prev_rapot_riwayat_pindah','.prev_rapot_riwayat_pindah'); 

    reset_prev('.div_prev_permohonan_pindah','.prev_permohonan_pindah');   
    reset_prev('.div_prev_surat_dinas_asal','.prev_surat_dinas_asal');   
    reset_prev('.div_prev_penerimaan_sekolah_baru','.prev_penerimaan_sekolah_baru');   
    reset_prev('.div_prev_surat_akreditasi','.prev_surat_akreditasi');
    
    $('.form-group').removeClass('has-error'); // clear error class
    $('.col-md-12').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    $('.btnNext').show();
    // dibawah hide
    // $('.btnPrev').show();
    // $('.btnSave').show();
    // $('.btnCancel').show();

    $('.btnPrev').hide();
    $('.btnSave').hide();
    $('.btnCancel').hide();
  switch(type) {
    case 'dklk':
      $('#Modal_dklk').modal({
          backdrop: 'static',
          keyboard: false
      }); // show bootstrap modal dklk
      // $('#Modal_dklk').modal('show'); // show bootstrap modal dklk
      $('#Modal_dklk #data_diri').show();
      $('#Modal_dklk #data_mutasi').hide();
      break;
    case 'lkdk':
      $('#Modal_lkdk').modal({
          backdrop: 'static',
          keyboard: false
      }); // show bootstrap modal lkdk 
      // $('#Modal_lkdk').modal('show'); // show bootstrap modal lkdk
      $('#Modal_lkdk #data_diri').show();
      $('#Modal_lkdk #data_mutasi').hide();
      break;
    default:
      $('#Modal_dkdk').modal({
          backdrop: 'static',
          keyboard: false
      }); // show bootstrap modal dkdk
      // $('#Modal_dkdk').modal('show'); // show bootstrap modal dkdk
      $('#Modal_dkdk #data_diri').show();
      $('#Modal_dkdk #data_mutasi').hide();
      break;
  } 
  
  $('.modal-title').text('Tambah Data Diri Siswa'); // Set Title to Bootstrap modal title
}

function next(type) { 
  // alert(type);
  // alert("test selanjutnya");
  // return false;
  $('.form-group').removeClass('has-error'); // clear error class
  $('.col-md-12').removeClass('has-error'); // clear error class
  $('.help-block').empty(); // clear error string
  $('.modal-title').text('Tambah Data Berkas Siswa'); // Set Title to Bootstrap modal title
  switch(type) {
    case 'dklk':
      var datadiri = $('#form_dklk').serialize();
      break;
    case 'lkdk':
      var datadiri = $('#form_lkdk').serialize();
      break;
    default:
      var datadiri = $('#form_dkdk').serialize();
      break;
  } 

  $.ajax({ url : "<?php echo base_url('frontend/valid_datadiri')?>",
      type: "POST",
      data: datadiri,
      dataType: "JSON",
      success: function(data){
        // if(data.status.warga_login=='TRUE'){
          if(data.status){
            //alert($_SESSION);
          switch(type) {
            case 'dklk':
              $("#Modal_dklk #btnPrev").show();
              $("#Modal_dklk #btnSave").show();
              $("#Modal_dklk #btnCancel").show();
              $("#Modal_dklk #btnNext").hide();

              $("#Modal_dklk #data_mutasi").show();
              $("#Modal_dklk #data_diri").hide();
            break;

            case 'lkdk':
              $("#Modal_lkdk #btnPrev").show();
              $("#Modal_lkdk #btnSave").show();
              $("#Modal_lkdk #btnCancel").show();
              $("#Modal_lkdk #btnNext").hide();

              $("#Modal_lkdk #data_mutasi").show();
              $("#Modal_lkdk #data_diri").hide();
            break;

            default:
              $("#Modal_dkdk #btnPrev").show();
              $("#Modal_dkdk #btnSave").show();
              $("#Modal_dkdk #btnCancel").show();
              $("#Modal_dkdk #btnNext").hide();

              $("#Modal_dkdk #data_mutasi").show();
              $("#Modal_dkdk #data_diri").hide();

              if ($('#Modal_dkdk #status_sekolah_asal').val() == "S") {
                $("#Modal_dkdk #div_group_f_surat_akreditasi").show();
              } else {
                $("#Modal_dkdk #div_group_f_surat_akreditasi").hide();
              }
            break;
          } 
        }else{
         // alert($_SESSION);
		
          // if($_SESSION['id_warga']==''){

          // }
          $('[name="domisili_siswa"]').parent().addClass(data.error_class['domisili_siswa']);
          $('[name="domisili_siswa"]').next().text(data.error_string['domisili_siswa']); 

          $('[name="nik"]').parent().addClass(data.error_class['nik']);
          $('[name="nik"]').next().text(data.error_string['nik']);
          
          $('[name="nisn"]').parent().addClass(data.error_class['nisn']);
          $('[name="nisn"]').next().text(data.error_string['nisn']);
          
          $('[name="nama_siswa"]').parent().addClass(data.error_class['nama_siswa']);
          $('[name="nama_siswa"]').next().text(data.error_string['nama_siswa']);
          
          $('[name="tmp_lhr"]').parent().addClass(data.error_class['tmp_lhr']);
          $('[name="tmp_lhr"]').next().text(data.error_string['tmp_lhr']);

          $('[name="tgl_lhr"]').parent().addClass(data.error_class['tgl_lhr']);
          $('[name="tgl_lhr"]').next().text(data.error_string['tgl_lhr']);

          $('[name="kelas"]').parent().parent().addClass(data.error_class['kelas']);
          $('[name="kelas"]').next().next().text(data.error_string['kelas']);
          
          $('[name="nama_wali"]').parent().addClass(data.error_class['nama_wali']);
          $('[name="nama_wali"]').next().text(data.error_string['nama_wali']);
          
          $('[name="pkj_wali"]').parent().addClass(data.error_class['pkj_wali']);
          $('[name="pkj_wali"]').next().text(data.error_string['pkj_wali']);
          
          $('[name="pkj_wali_lain"]').parent().addClass(data.error_class['pkj_wali_lain']);
          $('[name="pkj_wali_lain"]').next().text(data.error_string['pkj_wali_lain']);
          
          $('[name="alamat"]').parent().addClass(data.error_class['alamat']);
          $('[name="alamat"]').next().text(data.error_string['alamat']);
          
          $('[name="skl_asal"]').parent().addClass(data.error_class['skl_asal']);
          $('[name="skl_asal"]').next().text(data.error_string['skl_asal']);
          
          $('[name="akr_skl_asal"]').parent().addClass(data.error_class['akr_skl_asal']);
          $('[name="akr_skl_asal"]').next().text(data.error_string['akr_skl_asal']);
          
          $('[name="id_wly_skl_asal"]').parent().addClass(data.error_class['id_wly_skl_asal']);
          $('[name="id_wly_skl_asal"]').next().text(data.error_string['id_wly_skl_asal']);
          swal("Terjadi Kesalahan!", "Ada Beberapa Data Yang Salah, Silahkan Koreksi Kembali!", "error");
        }
      },
      error: function (jqXHR, textStatus, errorThrown){
        swal("Terjadi Kesalahan!", "Aplikasi gagal mendapatkan data!", "error");
      }
  });
}

function prev(type) {
  $('.modal-title').text('Tambah Data Diri Siswa'); // Set Title to Bootstrap modal title
  switch(type) {
    case 'dklk':
      $("#Modal_dklk #btnPrev").hide();
      $("#Modal_dklk #btnSave").hide();
      $("#Modal_dklk #btnCancel").hide();
      $("#Modal_dklk #btnNext").show();

      $("#Modal_dklk #data_mutasi").hide();
      $("#Modal_dklk #data_diri").show();
      break;

    case 'lkdk':
      $("#Modal_lkdk #btnPrev").hide();
      $("#Modal_lkdk #btnSave").hide();
      $("#Modal_lkdk #btnCancel").hide();
      $("#Modal_lkdk #btnNext").show();

      $("#Modal_lkdk #data_mutasi").hide();
      $("#Modal_lkdk #data_diri").show();
      break;

    default:
      $("#Modal_dkdk #btnPrev").hide();
      $("#Modal_dkdk #btnSave").hide();
      $("#Modal_dkdk #btnCancel").hide();
      $("#Modal_dkdk #btnNext").show();

      $("#Modal_dkdk #data_mutasi").hide();
      $("#Modal_dkdk #data_diri").show();
      break;

  }
}

function start_upload(form_file,temp,div_prev,prev) {

  var file = $(form_file).get(0).files[0];
  var ext = $(form_file).val().split('.').pop().toLowerCase();
  // console.log(ext);
  // console.log($.inArray(ext, ['png','jpg','jpeg','pdf']));
  if ($.inArray(ext, ['png','jpg','jpeg','pdf']) == -1) {      
    // alert('Format file yang dipilih ditolak.\nFile yang diterima berformat Gambar ("JPG","JPEG","PNG","PDF").');
    swal("Perhatian!", 'Format file yang dipilih ditolak.\nFile yang diterima berformat Gambar ("JPG","JPEG","PNG","PDF")!', "warning");
    $(form_file).val('');
    // document.getElementById(form_file).value = "";
    // document.getElementById(div_prev).setAttribute('style','display:none');
  } else {
    //upload file
    // console.log(file);
    do_upload(file,temp,div_prev,form_file,prev);
  }
}

function PreviewImage(source,target) { 
  // file = $(source).get(0).files[0];
  // full_url = '<?php echo base_url(); ?>'+file;
  // file_url = URL.createObjectURL(file);  
  $(target).attr('src',source);
}

function do_upload(file,temp,div_prev,form_file,prev) {
  var url = '<?php echo base_url("frontend/upload"); ?>';
  $('#Modal_dklk #btnSave').text('sedang mengunggah berkas...'); //change button text
  $('#Modal_dklk #btnSave').attr('disabled',true); //set button disable

  $('#Modal_dkdk #btnSave').text('sedang mengunggah berkas...'); //change button text
  $('#Modal_dkdk #btnSave').attr('disabled',true); //set button disable

  $('#Modal_lkdk #btnSave').text('sedang mengunggah berkas...'); //change button text
  $('#Modal_lkdk #btnSave').attr('disabled',true); //set button disable
  
  $(prev).attr('src',"<?php echo base_url('assets/images/loading.gif'); ?>");
  // var file_data = $('#sortpicture').prop('files')[0];   
  var form_data = new FormData();
  form_data.append('file', file);            
  $.ajax({ url: url, // point to server-side PHP script 
    dataType: 'text',  // what to expect back from the PHP script, if anything
    cache: false,
    contentType: false,
    processData: false,
    data: form_data,                         
    type: 'post',
    dataType: "JSON",
    success: function(data){
      console.log(data);
     if (data.status) {
        del_prev_file(temp);
        $(temp).val(data.url_file);
        $(div_prev).attr('style','display:block');
        // document.getElementById(div_prev).setAttribute('style','display:block'); 
        if (data.url_file!="") {
          PreviewImage(data.url_file,prev);
        }  else {
            //swal("Terjadi Kesalahan!", "", "error");
        }
          $('#Modal_dklk #btnSave').text('Simpan'); //change button text
          $('#Modal_dklk #btnSave').attr('disabled',false); //set button disable

          $('#Modal_dkdk #btnSave').text('Simpan'); //change button text
          $('#Modal_dkdk #btnSave').attr('disabled',false); //set button disable

          $('#Modal_lkdk #btnSave').text('Simpan'); //change button text
          $('#Modal_lkdk #btnSave').attr('disabled',false); //set button disable
        
      } else {        
        swal("File Tidak Boleh Lebih Dari 2 MB!", "", "error");
        $(form_file).val("");
        reset_prev(div_prev,prev);
      }
    },
    error: function (jqXHR, textStatus, errorThrown){
      // $('#div_loading').html('');
      swal("Terjadi Kesalahan!", "", "error");
      $('#Modal_dklk #btnSave').text('Simpan'); //change button text
      $('#Modal_dklk #btnSave').attr('disabled',false); //set button disable

      $('#Modal_dkdk #btnSave').text('Simpan'); //change button text
      $('#Modal_dkdk #btnSave').attr('disabled',false); //set button disable

      $('#Modal_lkdk #btnSave').text('Simpan'); //change button text
      $('#Modal_lkdk #btnSave').attr('disabled',false); //set button disable
    }
  });
}

function del_prev_file(temp) {
  var file_tmp = $(temp).val();
    $.ajax({ url : "<?php echo base_url('frontend/del_prev_file')?>",
      type: "POST",
      data: {file_tmp:file_tmp},
      dataType: "JSON",
      success: function(data){
        if (data.status) {
        }
      },
      error: function (jqXHR, textStatus, errorThrown){
        //swal("Terjadi Kesalahan!", "", "error");
      }
    });
}

function reset_prev(div_prev,prev) {
  $(prev).attr('src',"");
  $(div_prev).attr('style','display:none');
}
</script>

<?php include 'list_mutasi.php'; ?>
<?php include 'cek_pdf.php'; ?>